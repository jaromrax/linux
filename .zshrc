# If you come from bash you might have to change your $PATH.  a
# export PATH=$HOME/bin:$HOME/.local/bin:/usr/local/bin:$PATH
# Path to your oh-my-zsh installation.
 export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
# THOSE I LIKE MOST:
ZSH_THEME="robbyrussell"  #  malo info
ZSH_THEME="gianu"      #  edi ale kratka path
ZSH_THEME="jreese"    # ok , divny barvy
ZSH_THEME="tjkirch"   # 2tadky
ZSH_THEME="philips"   # hodiny ale kratka path
ZSH_THEME="pmcgee" # twoline, jako jsem mel a hodiny
ZSH_THEME="tonotdo" #   hodiny, v podstate perf. ale divy barv
ZSH_THEME="tjkirch_mod2"   # 1tadka s hodinama MY BEST TRY

#ZSH_THEME="random"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"   # UNSENSITIVE IS THE BEST

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"  # NO AUTOUPDATES

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"
HIST_STAMPS="yyyy-mm-dd"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder


# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.


#  git-OBVIOUS   ggl ggp gca ga glol
# catimg... catimg image.png
#  debian...apt commands-i dont use
#  python ...  completions for interpreter (no idea)
#  web-search .... google bing ... commands
#  battery ???
#  screen ... automatic setting titles and status, OK
#
##   cp ... cpv==rsync
#          ytdl --- my youtube-dl
#          emacs !!! one window # CANNOT UNDERSTAND
#
plugins=(git docker catimg web-search  cp python )



echo -n ____ ohmyzsh start ______
source $ZSH/oh-my-zsh.sh
echo ____ .zshrc start ___


# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal liases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. liases can be placed here, though oh-my-zsh
# users are encouraged to define liases within the ZSH_CUSTOM folder.
# For a full list of active liases, run `lias`.
#
# Example liases
# lias zshconfig="mate ~/.zshrc"
# lias ohmyzsh="mate ~/.oh-my-zsh"

###########################################################
###########################################################
# VERY GOOD
###########################################################
# ALIASES
###########################################################
###########################################################

scrlog(){
    echo ... screenshot and LOG
  gnome-screenshot -f ~/a.png && elog -h 10.10.104.21 -p 9000 -l demo -a author=`hostname` -a type=Other -a Subject=ScreenShot "Screenshot" -f ~/a.png

}


alias krusader='krusader --left . --right .'
alias dat='date +"%Y%m%d_%H%M%S"'
alias ai=cmd_ai
alias cris=cronvice


pirat(){

firefox  --private-window  https://webshare.cz &
firefox  -new-tab --private-window  https://fastshare.cloud &
firefox  -new-tab --private-window  https://csfd.cz &
firefox  -new-tab --private-window   https://kukaj.io &
firefox  -new-tab --private-window   https://prehraj.to &
firefox  -new-tab --private-window https://www.imdb.com &
firefox  -new-tab --private-window  https://bestsimilar.com &

}

orgcpim(){
    echo i... copy missing images to curr dir. Give the org filename
    echo ... I DONT KNOW ANYMORE
    LECT=`pwd | xargs basename`
    if [ "$LECT" != "03_Lectures" ]; then
	eco X... not in folder 03_Lectures. quiting
	exit 0
    fi
    if [ "$1" = "" ]; then
	echo No filename /for local PNG/ was given.
	return
    fi

    SAVEIFS=$IFS   # Save current IFS (Internal Field Separator)
    IFS=$'\n'      # Change IFS to newline char
    LIS=$((grep -e "[png|jpg|PNG|JPG]" "$1" | grep file | grep "[" | grep -v \# | sed -e "s/\].*//g ;s/\[//g ; s/.*://" ))
    LIS=($LIS) # split the `names` string into an array by the same name
    IFS=$SAVEIFS   # Restore original IFS
    #echo $LIS
    echo $LIS | while read -r line ; do
	#echo -n "i... ${line} "
	# your code goes here
	if [ ! -f "${line}" ]; then
	    #echo  ... not ok
	    fdfind ${line} .. | tail -1 | xargs -I III  echo cp III ./
	    #echo .... "do you want to copy this? y/n"
	    #read  "an?Do you want to copy?" # cannot use it-intereferes with other read
	    #echo $an
	    #echo $an
	    #if  [[ "$an" =~ ^[Yy]$ ]]; then #[ "$an" = "y" ]; then
		echo ..copying
		fdfind ${line} .. | tail -1 | xargs -I III  cp III ./
	    #fi
	fi
    done
}



# ===========================================================
#          OPERATE THE LAST FILE IN SCREENSHOTS
#-------------------------------------------------------------

mvdo(){
    DIR=~/Downloads;

    DOLAFILE=$(ls -tr1 "$DIR" | tail -1 )
    DOLAFULL=$(echo $DOLAFILE | awk -v dir="$DIR" '{print dir "/" $0}');

    #DOLA=$(ls -tr1 "$DIR" | tail -1 | awk -v dir="$DIR" '{print dir "/" $0}');
    if [ -e "$DOLAFILE" ]; then
	echo i... existing file ${DOLAFILE}
	TIMESTAMP=$(date +"%Y%m%d%H%M%S")
	EXT="${DOLAFILE##*.}"
	BASENAME="${DOLAFILE%.*}"
	DEST_FILE="$DIR/$BASENAME.$EXT"

	#echo mv "${DOLAFULL}" "${DEST_FILE}"
	echo mv "${DOLAFULL}" "${DIR}/${BASENAME}-${TIMESTAMP}.$EXT"
    else
	echo i... no problem copying
	echo mv "${DOLAFULL}" ./
	mv "${DOLAFULL}" ./
    fi
    }

lsgq(){
    echo i... viewing last  PNG screenshot to  ${1}.png
    # if [ "$1" = "" ]; then
    #	echo No filename /for local PNG/ was given.
    #	return
    # fi
    ls -tr1 ~/Pictures/Screenshots/Screenshot* | tail -1 | xargs  -I III  geeqie III
}

lscp(){
    echo i... copying last PNG screenshot to  ${1}.png
    if [ "$1" = "" ]; then
	echo No filename /for local PNG/ was given.
	return
    fi
    ls -tr1 ~/Pictures/Screenshots/Screenshot* | tail -1 | xargs  -I III  cp III  ${1}.png
}

lsmv(){
    echo i... MOVING last PNG screenshot to  ${1}.png
    if [ "$1" = "" ]; then
	echo No filename /for local PNG/ was given.
	return
    fi
    ls -tr1 ~/Pictures/Screenshots/Screenshot* | tail -1 | xargs  -I III  mv III  ${1}.png
}


# echo youtube-dl is old, yewtube may be better
# what about yt-dlp ??
ytd(){
    LOW=0
    if [ "$1" = "l" ]; then
	ADD=$2
	LOW=1
    else
	ADD=$1
    fi
    echo PREPARING DOWNLOAD ... $ADD
    #res=`  yt-dlp -F $1 2>/dev/null | grep -v only | grep fps`

    res=`yt-dlp -F "$ADD" 2>/dev/null | grep -v only | grep -v images`
    echo $res
    echo "=================================================="

    #res=`yt-dlp -F "$1" 2>/dev/null | grep -v only | grep -v images | grep -e " mp4 \| 3gp "`

    res=`yt-dlp -F "$ADD" 2>/dev/null | grep -v only | grep -v images | grep -e " mp4 "`

    echo $res
    echo "=================================================="

    #return
#    if [ "$?" != "0" ]; then
#	echo i... autoupdating yt
#	pip3 install youtube-dl --upgrade
#	res=`  youtube-dl -F $1 2>/dev/null | grep -v only | grep fps`
#    fi
    echo i... good video audio:
    echo $res
    echo i... extracting types
    type=`echo $res | cut -f 1 -d " " `
    lin=`echo $res | wc -l`
    if [ "$lin" = "1" ]; then
	echo i... ONE SOLUTION ONLY:
	yt-dlp -f $type $ADD
    else
	echo i... more solutions:  # $res
	frst=`echo $res | head -1  | cut -f 1 -d " " `
	best=`echo $res | tail -1  | cut -f 1 -d " " `
	if [  "$LOW" = "1" ]; then
	    yt-dlp -f  $frst $ADD
	else
	    echo " 1/2 ?"
	    read an
	    echo RESULT: /${an}/
	    if [ "$an" = "1" ]; then
		yt-dlp -f  $frst $ADD
	    fi
	    if [ "$an" = "2" ]; then
		yt-dlp -f  $best $ADD
	    fi
	fi
    fi
#echo youtube-dl is old yewtube may be better or yt-dlp
}





#https://www.tensorflow.org/lite/guide/python
#
#
#
#echo "deb https://packages.cloud.google.com/apt coral-edgetpu-stable main" | sudo tee /etc/apt/sources.list.d/coral-edgetpu.list
#curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
#sudo apt-get update
#sudo apt-get install python3-tflite-runtime
#
#pip3 install tflite-runtime
#OR pip3 install tensorflow
#
# sudo  apt install tor


# install msmtp; in /etc/msmptrc:
# defaults
#auth           on
#tls            on
#tls_trust_file /etc/ssl/certs/ca-certificates.crt
#logfile        ~/.msmtp.log
#account        gmail
#host           smtp.gmail.com
#port           587
#from           ja@gmail.com
#user           ja@gmail.com
#password       16char-from-security-apps-generate-pass
#account default : gmail
#
#echo -e "Subject: Testing email from $HOSTNAME\n\nThis is a test email." | msmtp ja@gmail.com
# ... with mutt
#mutt -a z.jpg -s "test" -- ja@gmail.com < /dev/null
#

gmaila(){
 if [ "$1" = "" ]; then
   echo No attachement given
   return
 fi
 if [ "$2" = "" ]; then
   echo No email address given
   return
 fi
 mutt -a "$1" -s "Attached file: ${1}" -- ${2} < /dev/null
}


gmail(){
 echo ... send only subject
 if [ "$1" = "" ]; then
   echo No subject given
   return
 fi
 if [ "$2" = "" ]; then
   echo No email address given
   return
 fi
 mutt  -s "${1}" -- ${2} < /dev/null
}


alias sign='gpg --detach-sign --armor'
alias -s asc="gpg"
alias speed='speedtest-cli --bytes --no-upload'
alias rsync='rsync --progress'

alias toi='toilet -f mono9'

rsynchelp(){
    echo "_______________ rsync help _______________"
    echo "FOR BTRFS with snapshots USE:"
    echo "rsync -aAXv          --exclude="
    echo "rsync -aAXv --delete --exclude="
}


alias watch='watch --color'
#alias watchc=watch --color
# pbzip2 is too old now....
#alias pbzip2='pbzip2 -v -9'

alias wdr="dstat -d --disk-util --disk-tps"

nmapo(){
    # OS detection, mac addr
    sudo nmap -Pn -O -sV --fuzzy "$1"
    }

dfcc(){
  dfc -text,btrfs,vfat,fuse -lf -c always -n | grep -v /boot | grep -v "ox/common"  | grep -v '/var/lib' | sed -e 's/].*%/]/' | sed -e 's/\s*$//g' | sed -e 's/MOUNTED//g'
}

wdfc(){
    watch "dfc -text,btrfs,vfat,fuse -lf -c always  -n 2>/dev/null | grep -v /boot | grep -v "ox/common" | grep -v '/var/lib' | sed -e 's/].*%/]/' | sed -e 's/\s*$//g' | sed -e 's/MOUNTED//g'"
}

wdu(){
    watch --differences "du -h"
}

wtail(){
    watch --differences "ls -trhl | tail -12"
    }


# ===========================================================
#          GITLAB GIT   GIL
#-------------------------------------------------------------
function gila() {
for dir in */; do                                                                        [9:49:27]
  (cd "$dir" &&  GLAB_NO_WARNINGS=1 glab issue list | grep -e '^#')
done
}
ggll(){
    git pull
    git pull --tags
}

gic(){
    echo "GLAB(GIT) ISSUE CREATE"
    glab issue create
}

gil(){
    if [ "$1" != "" ]; then
	cd $1
	glab issue list | tail -n +2 |  grep -v -e '^$'

	cd ..
    else
	echo "GLAB(GIT) ISSUE LIST"
	glab issue list
    fi
}

# gils(){
#     echo "GLAB issue list for all folders..."
#     for di in `ls -1d */ `; do
# 	cd $di
# 	echo -ne `pwd` "\r"
# 	if [ -e ".git/config" ]; then
# 	    grep "gitlab.com" .git/config > /dev/null
# 	    if [ "$?" = "0" ]; then
# 		PRI=`glab issue list | tail -n +2  |  grep -v -e '^$'`
# 		if [ "$PRI" != "" ]; then
# 		    echo -ne "                                             \r"
# 		    echo -e "$PRI \r"
# 		fi
# 	    fi
# 	fi
# 	cd ..
#     done
#}

giv(){
    echo "GLAB(GIT) ISSUE VIEW"
    glab issue view $1
}
gie(){
    echo "GLAB(GIT) ISSUE edit"
    glab issue update $1 -d -
}

gh(){
    echo ________________ git help __________________________
    echo "GET TAGS FORCE   git fetch origin --tags --force   "
    echo "STEP BACK LOCAL  git reset --soft HEAD^"
    echo "STEP BACK LOCAL  git reset --hard  # clear all after commit"
    echo "DECLINE last commit  git reset HEAD^"
    echo "STEP BACK PUSHED git revert HEAD   (not sure)"
    echo "STEP BACK PUSHED git revert HEAD^"
    echo " look back       git reflog"
    echo " look back       gitk  (apt i)"
    echo "                 git log --all; glol --all"
    echo "JUMP to END      git checkout -"
    echo "ABORT MERGE      git merge --abort"
    echo "     AND STASH   git stash"
    echo "AMEND COMMIT     git commit --amend -m \"a new message\" "
    echo "FORCE PULL       git reset --hard origin/main"
    echo " "
    echo "git config credential.helper store"
    echo "            ... a powerfull passsaver"
    echo ___________________________
    echo "glab:   gil(or all gils) gic giv gie ; glab issue close 1"
    echo "     glab auth login   (gitlab,web) "
    }


# ===========================================================
#          Android
#-------------------------------------------------------------

adbtui(){
  echo  ' rename -n  "s/[\(\+\)\:\?\,\!]/_/g" *'
  cd ~/go/bin; ./adbtuifm --local ~/Videos --remote /storage/3066-3130/Download/
  echo  ' rename     "s/[\(\+\)\:\?\,\!]/_/g" *'
  cd -
}



# commit(){
#     git diff | ai "Create oneliner git commit command with -a -m parameters " ;
#     echo ... pushing immediatelly
#     git push
#     }



# ===========================================================
#           ALIASES
#-------------------------------------------------------------

alias last='ls -1 /var/log/wtmp* | xargs -n 1 last -a -f'



# .config/mimeapps.list   put text/plain=emacs.desktop and thats it
#text/plain=emacs.desktop
#text/x-python3=emacs.desktop
#text/markdown=emacs.desktop
#application/x-shellscript=emacs.desktop
# ------this makes a strange output -------------- alias x=`xdg-open`
# xdg-open a.org will launch emacs

#alias less='less -FSRX'
export LESS='-iFRX'


# ------------------- end of aliases






# to use pyROOT  import ROOT


# ===========================================================
#   ROOT PATHS CERN
#-------------------------------------------------------------

export PYTHONPATH=$HOME/root/lib/
export ROOTSYS=$HOME/root
export PATH=$ROOTSYS/bin:~/bin:$PATH
export LD_LIBRARY_PATH=$ROOTSYS/lib:$ROOTSYS/lib/root:$LD_LIBRARY_PATH




# geant4 ----------- to test---------
if [ -e $HOME/geant/bin/geant4.sh ]; then
    echo "i... Geant4 found ... running shell script"
    source $HOME/geant/bin/geant4.sh
fi
#if [ -e $GEANT4_INSTALL_DATADIR/bin/geant4.sh ]; then
#$GEANT4_INSTALL_DATADIR/bin/geant4.sh
#fi
###### export PATH=$PATH:$GEANT4_INSTALL_DATADIR/bin
#
#  cern root needs that maybe
#
if [ -e $HOME/root/bin/thisroot.sh ]; then
    echo "i... ROOT found ...   running shell script"
    source $HOME/root/bin/thisroot.sh
fi





####### earlier i had special completions:
#autoload bashcompinit
#bashcompinit
#source /etc/bash_completion.d/myservice

setopt no_share_history    ## VERY GOOD FOR ZSH NOT TO SHARE


# stickytape and python3 stuff
export PATH=$PATH:$HOME/.local/bin/

# here the RUST goes (cargo install ,  apt install rust-all )
export PATH=$PATH:$HOME/.cargo/bin/


# USE zsh.org
table=$(cat <<EOF
|-------+----------+----------------------+--------------+-------------+------|
| disk  | ncdu     | dfc/dfcc/wdfc        | ag fdfinf    | fslint      | fdup |
|       | iotop    | wdr(apt i dstat)     | dpigs        | bleechbit   | duf  |
| net   | bwm-ng   | wondershaper         | nethogs      | nload       | cbm  |
|       | bmon     | iftop                | jnettop      | mmv (go)    |      |
|       | dnstop   | speedtest-cli        | iptstate     | sntop(ping) | mtr  |
| logs  | lnav     | grc less log..       |              |             |      |
| users | whowatch | sysdig -c sy_users   |              |             |      |
|-------+----------+----------------------+--------------+-------------+------|
| tops  | htop     | neoss  (npm)         | bpytop       | glances     | atop |
|       | nmon(d)  | curl  cheat.sh/ls    | curl wttr.in | powertop    |      |
|       | arpwatch | apt i inotify-tools  | visidata vd  |             |      |
 debug like  'A="./vlog.py" ; while inotifywait -e close_write \$A; do \$A; done'
EOF
)
echo $table
# echo "|music|mpv     |cmus                  |mps-youtube |==mpsyt    |     "
# echo "|disk |ncdu    |dfc/dfcc/wdfc         |pydf        |fslint     |fdup "
# echo "|     |iotop   |wdr(apt i dstat)      |dpigs fdfind|bleechbit  |duf  "
# echo "|net  |bwm-ng  |wondershaper          |nethogs     |nload      |cbm  "
# echo "|     |bmon    |iftop                 |jnettop     |mmv (go)   |     "
# echo "|     |dnstop  |speedtest-cli         |curl wttr.in|sntop(ping)|mtr  "
# echo "|     |        |qr-filetransfer (go)  |iptstate    |           |     "
# echo "|logs |lnav    |grc less log..        |            |           |     "
# echo "|users|whowatch|sysdig -c sy_users    |            |           |     "
# echo "|tops |htop    |powertop              |bpytop      |glances    |atop "
# echo "|     |nmon(d) |curl  cheat.sh/command|curl wttr.in|neoss      |     "
# echo "|     ||apt i inotify-tools   |visidata vd |           |     "
# echo .
# echo ... debug code like:
# echo 'A="./vlog.py" ; while inotifywait -e close_write $A; do $A; done'


# zsh plugins make problems:L    2025  ubuntu24
# i remove problem with plugins == git and emacs
#
export EDITOR=nano
#


# ===========================================================
# #  LOCAL / GLOBAL HISTORY
#-------------------------------------------------------------

bindkey "${key[Up]}" up-line-or-local-history
bindkey "${key[Down]}" down-line-or-local-history

up-line-or-local-history() {
    zle set-local-history 1
    zle up-line-or-history
    zle set-local-history 0
}
zle -N up-line-or-local-history
down-line-or-local-history() {
    zle set-local-history 1
    zle down-line-or-history
    zle set-local-history 0
}
zle -N down-line-or-local-history

bindkey "^[[1;5A" up-line-or-history    # [CTRL] + Cursor up
bindkey "^[[1;5B" down-line-or-history  # [CTRL] + Cursor down

#
###export HISTTIMEFORMAT="%d/%m/%y %T "
#export HISTTIMEFORMAT='%F %T  '  # stackx

#
#
#https://transfer.sh/
#

# transfer() { if [ $# -eq 0 ]; then echo -e "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"; return 1; fi
# tmpfile=$( mktemp -t transferXXX ); if tty -s; then basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g'); curl --progress-bar -H "Max-Downloads: 1" -H "Max-Days: 5" --upload-file "$1" "https://transfer.sh/$basefile" >> $tmpfile; else curl --progress-bar -H "Max-Downloads: 1" -H "Max-Days: 5" --upload-file "-" "https://transfer.sh/$1" >> $tmpfile ; fi; cat $tmpfile; rm -f $tmpfile; }
# #



BOGOMIPS=$( cat /proc/cpuinfo | grep bogomips | uniq )
NPROCESSORS=$( cat /proc/cpuinfo | grep bogomips | wc -l )
echo -en  $BOGOMIPS
echo -en "  " $NPROCESSORS
echo " threads"

#$( cat /proc/cpuinfo | grep bogomips | uniq )
if [ "$BOGOMIPS" = "" ]; then
    NPROCESSORS=$( cat /proc/cpuinfo | grep processors | wc -l )
    ARM=$( cat /proc/cpuinfo | grep ARM  )
    echo -en  $NPROCESSORS
    echo  "  " $ARM
fi



# ===========================================================
#  ??????????????????
#-------------------------------------------------------------

#-------------RPI0 ****** CHECK IF NEEDED !!!!!!!!!!!!!!!!!!
export LD_LIBRARY_PATH=/opt/opencv-4.1.0/lib:$LD_LIBRARY_PATH


#  TUI
# gping  fzf  chkservice
# aerc:mail
# adbtuifm .. adb     gpg-tui    neoss    nmtui   recoverpy
#
# neoss    apt install nodejs npm;  npm install -g neoss
#


# ===========================================================
#      PYTHON /********************************
#-------------------------------------------------------------
# PYTHON ENVIRONMENT
#
# # echo -ne loading  virtualenvwrapper.sh " "
# if there was a problem
#   ModuleNotFoundError: No module named 'virtualenv.seed.via_app_data'
#
#      sudo apt remove --purge python3-virtualenv
#
which virtualenvwrapper.sh >/dev/null
if [ "$?" = "0" ]; then
    VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
    export WORKON_HOME=~/.envs
    mkdir -p $WORKON_HOME
    source  `which virtualenvwrapper.sh`
    # mkvirtualenv openai
    # workon openai
    # deactivate
# else
#     echo X
#     echo X
#     echo X... virtualenvwrapper not found....
#     echo sudo -H pip3 install virtualenvwrapper
#     echo X
#     echo X
fi
 # update-alternatives --list python
 # update-alternatives --install /usr/bin/python python /usr/bin/python2.7 2
 # update-alternatives --install /usr/bin/python python /usr/bin/python3.6 1
 # update-alternatives --list python
 # update-alternatives --config python

#================== PYENV - different python versions
## # pyenv configs
## # install pyenv
## # git clone https://github.com/pyenv/pyenv.git ~/.pyenv
#
# pyenv global
# pyenv local
# pyenv versions
# pyenv install -l
# pyenv install 3.9.7
#
# git clone https://github.com/yyuu/pyenv-virtualenv.git   $HOME/.pyenv/plugins/pyenv-vir
# pyenv virtualenv  3.9.7  mypython9
# pyenv activate mypython9
#
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"

if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi

# ===========================================================
#FIX      24011 - PIP3 suddenly started to request KDE keyring!!
#-------------------------------------------------------------

export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring



# `refresh cmd` executes clears the terminal and prints
# the output of `cmd` in it.
function refresh {
  tput clear || exit 2; # Clear screen. Almost same as echo -en '\033[2J';
  #bash -ic "$@"; # was bash -ic
}


# # Like watch, but with color
# function cwatch {
#    while true; do
#      CMD="$@";
#      # Cache output to prevent flicker. Assigning to variable
#      # also removes trailing newline.
#      output=`refresh "$CMD"`;
#      # Exit if ^C was pressed while command was executing or there was an error.
#      exitcode=$?; [ $exitcode -ne 0 ] && exit $exitcode
#      printf '%s' "$output";  # Almost the same as echo $output
#      sleep 1;
#    done;
# }

# SAMBA DISK
#
#[samba]
#path = /home/nopassw
##guest only = yes
#browsable = yes
#writable = yes
#read only = no
#force create mode = 0666
#force directory mode = 0777
#
#[sambapass]
#path = /home/passw
#read only = no
#writeable = yes
#valid users = user1
#create mask = 0664
#directory mask = 0775
#force user = user1
#
# .... apt install samba smbclient
#..... smbpasswd -a user1
# .... systemctl restart smbd
# .... smbclient  //localhost/samba


#===========================   LS ORG COLORED
export LS_COLORS="rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:*.org=4;33"


#
#==--------------------------- OPENAI_PIPE
#
#gem install openai_pipe
if [ -e ${HOME}/.openai.token ]; then
    export OPENAI_ACCESS_TOKEN=`cat ${HOME}/.openai.token`
    #alias ai="openai_pipe"
fi




# ===========================================================
#   Create mp4 from mp3.....  shouldnt be here, but...
#-------------------------------------------------------------

function spectrogram {
   input=$1
   ffmpeg -i $input -filter_complex showspectrum=mode=combined:scale=sqrt:color=plasma:slide=1:fscale=log:s=600x360:win_func=gauss:start=200 -y -acodec copy ${input}.mp4
   echo "i... to play with subtitles: mpv --sub-file=${input}.srt ${input}.mp4"
   echo "i... to encode    subtitles:ffmpeg -i sample_2.mp4 -vf \"subtitles=${input}.srt\"  ${input}.mp4"
}

#------------------------------------- FZF.... not fully understood
# this ctrl-y is accesible from keybarrd-emacs and terminal...
#  ctrl-alt-y   copies content of the file
# use also like  emacs $(fzf)


export FZF_DEFAULT_OPTS="
--exact
--layout=reverse
--info=inline
--height=80%
--multi
--preview-window=:hidden
--preview '([[ -f {} ]] && (bat --style=numbers --color=always {} || cat {})) || ([[ -d {} ]] && (tree -C {} | less)) || echo {} 2> /dev/null | head -200'
--color='hl:148,hl+:154,pointer:032,marker:010,bg+:237,gutter:008'
--prompt='∼ ' --pointer='▶' --marker='✓'
--bind '?:toggle-preview'
--bind 'ctrl-a:select-all'
--bind 'ctrl-e:execute(echo {+} | xargs -o emacs)'
--bind 'ctrl-v:execute(code {+})'
--bind 'ctrl-y:execute(readlink -f {} | xclip -selection clipboard)'
--bind 'ctrl-alt-y:execute-silent(xclip -selection clipboard {})'
"

# ---------------------------------------------------  PiZERO
# RPIzero is sad with darkgreen and without this
export TERM=xterm-256color


# -------------------------------------------- nvm ( npm start; npm, jsnode etc... ) for zigbee2mqtt

# ===========================================================
#  FUTURE STUFF WITH  ZIGBEE
#-------------------------------------------------------------

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion



# ===========================================================
#          KITTY
#-------------------------------------------------------------
# kitty icat ******************** KITTY ****************
# uase c-s-click
alias lh="ls --hyperlink=auto"
alias icat="kitty +kitten icat"
alias idiff="kitty +kitten diff $1 $2"
# function kt-cron() {
#   #export PROJECT_DIR=$1
#   kitty --session ~/.config/kitty/cron.conf
# }
function kt-tel() {
  #export PROJECT_DIR=$1
  kitty --session ~/.config/kitty/telegf.conf
}


# ===========================================================
#       screen completion
#------------------------------------------------------------

_screen_sessions() {
  local sessions
  sessions=("${(f)$(screen -ls | awk '/\t/ {print $1}')}")
  _describe -t sessions 'screen session' sessions
}

# Explicitly bind the completion for `screen -x`
zstyle ':completion:*:*:screen:*' tag-order 'options:session'
zstyle ':completion:*:*:screen:*:session' menu yes
compdef _screen_sessions screen

# ===========================================================
#
#------------------------------------------------------------
