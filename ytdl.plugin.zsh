_ytv() {
    full=`  youtube-dl -F $1 | grep -v only`
    mp4=`echo $full | grep "mp4 " `
    echo mp4: $mp4
    mp640=`echo ${mp4} | grep "640x" | cut -d " " -f 1 `
    if [ "$mp640" != "" ]; then
	youtube-dl -f ${mp640} $1
    else
	echo mp4 and 640 : $mp640
	x640=`echo $full | grep "640x" | cut -d " " -f 1 `
	youtube-dl -f ${x640} $1
	
    fi
    
}

alias ytv=_ytv
