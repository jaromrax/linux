;; ======================================================
;; hide menu bar, scrollbar and toolbar
;; ------------------------------------------------------
;;;;;;(package-initialize)
(menu-bar-mode 0)
(tool-bar-mode 0)
(scroll-bar-mode 0)

;; ======================================================
;; highlight matching paranthesis
;; ------------------------------------------------------
(show-paren-mode 1)

;; ======================================================
;; do not show default startup screen
;; ------------------------------------------------------
(setq inhibit-startup-message t)

;; ======================================================
;; switch on ido mode for fast navigation  ??
;; ------------------------------------------------------
(ido-mode 1)

;; ======================================================
;; use y or n instead of yes or no
;; ------------------------------------------------------
(fset 'yes-or-no-p 'y-or-n-p)

;; ======================================================
;; do not create annoying backup files
;; ------------------------------------------------------
(setq make-backup-files nil)

;; ======================================================
;; turn off auto save mode
;; ------------------------------------------------------
(auto-save-mode 0)

;; ======================================================
;; show line number
;; ------------------------------------------------------
;; (global-linum-mode 1)
(global-display-line-numbers-mode 1)


;; ======================================================
;; show column number
;; ------------------------------------------------------
(column-number-mode 1)

;; ======================================================
;; delete selection NO
;; ------------------------------------------------------
;;(delete-selection-mode 1)

;; ======================================================
;; show a vertical line at column  16.
;;    Change: M-x set-variable RET display-fill-column... RET 4 RET
;; ------------------------------------------------------
;;(setq-default display-fill-column-indicator-column fill-column)
;;(setq-default display-fill-column-indicator-column fill-column)
;;(setq-default display-fill-column-indicator-column 8)
;;(add-hook 'prog-mode-hook 'display-fill-column-indicator-mode)
(add-hook 'python-mode-hook
          (lambda ()
            (setq display-fill-column-indicator-column 4)
            (display-fill-column-indicator-mode 1)))




;; ======================================================
;; remove trailing white spaces before save
;; ------------------------------------------------------
(add-hook 'before-save-hook 'delete-trailing-whitespace)


;; ======================================================
;; Enable copy and pasting from clipboard ??
;; ------------------------------------------------------
(setq x-select-enable-clipboard t)


;; ;;; ============================================================
;; ;;  COLOR with m-x customize-face   auto-dim...face --- apply_and_save
;; ;;; ------------------------------------------------------
(require 'auto-dim-other-buffers)
(add-hook 'after-init-hook (lambda ()
  (when (fboundp 'auto-dim-other-buffers-mode)
    (auto-dim-other-buffers-mode t))))



;;; ======================================================
;;       ONE ZOOM FOR ALL buffers
;;; ------------------------------------------------------
(defadvice text-scale-increase (around all-buffers (arg) activate)
  (dolist (buffer (buffer-list))
    (with-current-buffer buffer
      ad-do-it)))

;;; ======================================================
;;       font size - unit is 1/10
;;; ------------------------------------------------------
(set-face-attribute 'default nil :height 100)



;; ;;; ======================================================
;; ;;  GEANT .mac   coloring...1st attempt..
;; ;;; ------------------------------------------------------

(setq geant-highlights
      '(
	("^#.*" . 'font-lock-comment-face)
	("^\s.*" . 'font-lock-warning-face)
	;; /  after warning....
        ("gun\\|energy\\|\\(particle\\)\s\\|direction\\|\\/ion" . 'font-lock-keyword-face)
	("/" . 'font-lock-constant-face)
	("control\\|cout\\|prefixString\\|execute\\|vis\\|viewer\\|set\\|trajectories\\|storeTrajectory\\|autoRefresh\\|drawVolume\\|zoom\\|flush\\|modeling\\|open\\|verbose\\|scene\\|add\\|axes\\|tracking\\|endOfEventAction\\|enable\\|disable\\|drawByParticleID-0\\|drawByCharge-0\\|create\\|drawByParticleID\\|drawByCharge\\|viewpointThetaPhi" . 'font-lock-function-name-face)
	("run\\|beamOn\\|initialize" . font-lock-type-face)
	("year\\|mm\\|keV\\|MeV" . font-lock-builtin-face)
	;; numbers after comments else they are broken
	("\s\\([\-]*[0-9\.]*\\)\\|e\\+[0-9]*" . font-lock-constant-face)
	)
      )

(define-derived-mode geant-mode fundamental-mode "geant"
  "major mode for editing mymath language code."
  (setq font-lock-defaults '(geant-highlights)))
(add-to-list 'auto-mode-alist '("\.mac$" . geant-mode))


;; ======================================================
;; Minor mode for a nice writing environment
;; ------------------------------------------------------
;;
;; olivetti-mode
(add-hook 'text-mode-hook 'olivetti-mode)
;; UNBIND ORGMODE TABLE
(define-key olivetti-mode-map (kbd "C-c }") nil)
(define-key olivetti-mode-map (kbd "C-c {") nil)
(setq-default olivetti-body-width 99)
