# FOR REPORT writing +  Zotero help
# ________________________________________ you need:
# ### sudo apt install latexmk
# sudo apt install texlive-luatex
# ;; .emacs contains:
# (setq org-latex-pdf-process (list
#    "latexmk -pdflatex='lualatex -shell-escape -interaction nonstopmode' -pdf -f  %f"))
# --------------------------------------------------------------------- HELP END ----
#+OPTIONS: toc:nil        (no default TOC at all)
#+OPTIONS: creator:nil
#+OPTIONS: author:nil
#  +OPTIONS: date:nil
#+OPTIONS: tex:t
#+OPTIONS: num:nil
#+OPTIONS: tex:dvipng

# # ---- dont interpret ^_ as in math... # # +OPTIONS: ^:nil  # # +OPTIONS: _:nil

#+LATEX_CLASS_OPTIONS: [a4paper,12pt]
#+LATEX_HEADER: \usepackage{parskip}
#+LATEX_HEADER: \usepackage[margin=2cm]{geometry}
#+LATEX_HEADER:  \usepackage[utf8]{luainputenc}
# +LATEX_HEADER: \usepackage{nopageno}

#  ???? I need to test this vs. \bibliography{}
#+BIBLIOGRAPHY: biblio
# see unuseful  https://www.bibtex.com/bibliography-styles-author-date/
# \bibliographystyle{plain} # no sorting....not a nice order
# \bibliographystyle{unsrt} # sorts but long names
# \bibliographystyle{ieeetr} # MY BEST FOR THE MOMENT
#     \bibliography{bibfile_filename_noext} ...
#+LATEX_HEADER: \bibliographystyle{vancouver}
# ???? I need to test this again - where is the bibfile defined for this?
# use  org-reftex-citation to enter a \cite


https://github.com/Wookai/paper-tips-and-tricks

* Nice \null^{nat}Mo

/Žluťoučký kůň úpěl ďábelské ódy/


#    +LATEX: \setlength\itemsep{-2.9em}
#+LATEX: \let\tempone\itemize
#+LATEX: \let\temptwo\enditemize
#+LATEX: \renewenvironment{itemize}{\tempone\addtolength{\itemsep}{-0.5\baselineskip}}{\temptwo}

 - The item 1  \cite{avrigeanuLowenergyDeuteroninducedReactions2013}
 - The item 2
 - Item 3

#+ATTR_LATEX: :align r|l|l|l|l
|               | channel | half-live | expected \gamma lines | with intensity |
|---------------+---------+-----------+----------------------+----------------|
| /             | <       | <         | <                    |                |
| \null^{91m}Mo | (d,p2n) | 1.1 m     | 652 keV              |            48% |
| \null^{92}Tc  | (d,2n)  | 4.2 m     | 773, 1509 keV        |           100% |

\vskip 5mm

#+BEGIN_center
#+ATTR_LATEX: :width 0.45\textwidth :center nil
[[file:91mMo.png]]
#+ATTR_LATEX: :width 0.45\textwidth :center nil
[[file:92Tc.png]]
#+END_center

\vskip 5mm

** Zotero help

- Use betterbibtex export in zotero
  - downolad xpi  https://github.com/retorquere/zotero-better-bibtex/releases/tag/v7.0.0
  - Zotero-tools-plugins-fromfile
  - terrible strings....
    - Customize  citation key   EDIT-Settings-BetterBibTex: auth.lower+ year+Pages
    - HINT refresh keys - if neede, else old keys remain there
- ZOTERO leaves  LATEX format characters in BIBTEX exported bib !
  - to avoid that: use #LaTeX as TAG !!
  - HINT Multiple TAGS seem to have a problem for #LaTeX, single #LaTeX TAG works fine
- Since Syncing, there are fragile SQLITE at   ~/01_Dokumenty/04_ourpublic/ZOTERO/
  - zotero.sqlite
  - zotero.sqlite-journal
  - better-bibtex.sqlite
  - my ~cp_bak_zotero~ script should make a backup each time to resolve the situation

** Bibliography part


# ** References
\footnotesize
# \small
# \bibliography{biblio}
#
# output = "biblio.bib"
#       if os.path.exists(output):
#           print(f"X... sorry, {output} exists, but it may be ok...")
#       else:
#           with open(output,"w") as f:
#               f.write(report_bib)
#
# @article{avrigeanuLowenergyDeuteroninducedReactions2013,
#   title = {Low-energy deuteron-induced reactions on ${}^{93}$Nb},
#   author = {Avrigeanu, M. and Avrigeanu, V. and Bém, P. and Fischer, U. and Honusek, M. and Koning, A. J. and Mrázek, J. and Šimečková, E. and Štefánik, M. and Závorka, L.},
#   year = {2013},
#   month = jul,
#   volume = {88},
#   pages = {014612},
#   publisher = {American Physical Society},
#   doi = {10.1103/PhysRevC.88.014612},
#   abstract = {The activation cross sections of (d,p), (d,2n), (d,2np+nd+t), (d,2nα), and (d,pα) reactions on 93Nb were measured in the energy range from 1 to 20 MeV using the stacked-foil technique. Then, within a simultaneous analysis of elastic scattering and reaction data, the available elastic-scattering data analysis was carried out in order to obtain the optical potential for reaction cross-section calculations. Particular attention was paid to the description of the breakup mechanism and direct reaction stripping and pick-up, followed by pre-equilibrium and compound-nucleus calculations. The measured cross sections as well as all available deuteron activation data of 93Nb were compared with results of local model calculations carried out using the codes fresco and stapre-h and both default and particular predictions of the code talys-1.4 and tendl-2012-evaluated data.},
#   file = {/home/ojr/01_Dokumenty/04_ourpublic/ZOTERO/storage/C7J6LQJC/Avrigeanu et al. - 2013 - Low-energy deuteron-induced reactions on $ ^ 93 $.pdf},
#   journal = {Phys. Rev. C},
#   number = {1}
# }
