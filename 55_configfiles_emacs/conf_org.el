;;; ======================================================
;; Install some packages
;;; ------------------------------------------------------
(setq package-selected-packages '(orgtbl-join dockerfile-mode docker markdown-mode))


;;; ======================================================
;; ORGTBL  table
;;; ------------------------------------------------------
(require 'orgtbl-join)

;;; ======================================================
;; html export
;;; ------------------------------------------------------
;;; C-c C-e h o
(require 'htmlize)

;;; ======================================================
;;  org-tempo  -  should allow <s ...  sh -does zsh anyway
;; NO ORG-TEMPO .... DO C-c C-,  instead
;;; ------------------------------------------------------
;;; <s TAB
;; (require 'org-tempo)

;; (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
;; (add-to-list 'org-structure-template-alist '("sh" . "src sh"))
;; (add-to-list 'org-structure-template-alist '("py" . "src python :results replace output   :session test :exports both"))
;; (add-to-list 'org-structure-template-alist '("ex" . "example"))
;; (add-to-list 'org-structure-template-alist '("ve" . "verbatim"))



;;; ======================================================
;;   Basics - recommended.
;;; ------------------------------------------------------

(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
;; --- this remembers this row and org-insert-link in org will insert it
(global-set-key "\C-cl" 'org-store-link)
;;  agenda menu
(global-set-key "\C-ca" 'org-agenda)
;; (global-font-lock-mode 1)  Not needed in recent emacsen


;;; ======================================================
;;  suggestion from so
;; https://stackoverflow.com/questions/11384516/how-to-make-all-org-files-under-a-folder-added-in-agenda-list-automatically
;;; ------------------------------------------------------


;;; ======================================================
;;   Set ORG directory and define C-c '
;;; ------------------------------------------------------

(setq org-agenda-files '("~/01_Dokumenty/00_orgmode_data"))
;; Cycle AGENDA files ... see org-switchb
(global-set-key (kbd "C-'") 'org-cycle-agenda-files)
;; -----------
;; (load-library "find-lisp")
;; (setq org-agenda-files
;;    (find-lisp-find-files "~/01_Dokumenty/00_orgmode_data" "\.org$"))
;; -----------


;;; ======================================================
;;; ======================================================
;;; ======================================================
;;  Older things - repeating earlier  used stuff
;;; ------------------------------------------------------


;; ORGTBL : org-mode can work with orgtbl-  create and send
(add-hook 'message-mode-hook 'turn-on-orgtbl)

 ;;; ==================================================
 ;;; AGENDA
 ;;; -------------------------------------------------
 ;;;  SHORTCUT IN LINUX ______ Ctrl-Alt emacs  ~/01_Dokumenty/00_orgmode_data/00README.org
;;;
 ;;; =========== ANY FILE THERE WILL BE IN THE RING ============
;;;
;; Already above : (setq org-agenda-files '("~/01_Dokumenty/00_orgmode_data"))
;;;;;   '("~/01_Dokumenty/00_orgmode_data/agenda.org" "~/01_Dokumenty/00_orgmode_data/home.org" "~/01_Dokumenty/00_orgmode_data/00README.org"))
;; Already bellow : (setq org-default-notes-file "~/01_Dokumenty/00_orgmode_data/notes.org")
;; (setq org-format-latex-options
;;   '(:foreground default :background default :scale 1.0 :html-foreground "Black" :html-background "Transparent" :html-scale 1.0 :matchers
;;		 ("begin" "$1" "$" "$$" "\\(" "\\[")))

;;; ======================================================
;;; ======================================================
;;; ======================================================
;;; ======================================================
;;  definitions in notes  journal
;;; ------------------------------------------------------

(setq org-default-notes-file "~/01_Dokumenty/00_orgmode_data/notes.org")
;; org-add-note xxx NOT TO THIS FILE xxx


;;; ======================================================
;;   CAPTURE JOURNAL;NOTES; whatever.....   C-c c  [nj...]
;;   after note,journal is captured ...  press C-c to go back
;;; ------------------------------------------------------

;;    %a is filename - seems to be; creates title,stamp and ok
;;;;;;;(require 'org-capture)
(setq org-capture-templates
      '(("n" "Note - KNIHA: Entry" entry
         (file "~/01_Dokumenty/00_orgmode_data/notes.org" )
         "* %?\n%U \n")

      ( "j" "Journal entry with paste buffer" plain
          (file+olp+datetree "~/01_Dokumenty/00_orgmode_data/journal.org")
          "  - *%<%H:%M:%S>*   -  %^{PROMPT}\n      - from file: %a\n      - from chapter: %i \n      %?\n\n"   :unarrowed t  )
;;;;;;; end of list
      )
)



;; BIND the key C-c c
(global-set-key (kbd "C-c c") 'org-capture)
;;; ======================================================
;; test F6
;;; ------------------------------------------------------
;;(global-set-key (kbd "<f6>") 'org-capture)



;; ;;; ======================================================
;; ;; Some things for REFIL ---- not using really
;; ;;; ------------------------------------------------------

;; ;; something for refile.....
;; (setq org-outline-path-complete-in-steps nil)         ; Refile in a single go
;; ;; easier to correct choose the target for refiling
;; (setq org-refile-use-outline-path t)                  ; Show full paths for refilin

;; (setq org-log-into-drawer t)

;;; ======================================================
;; Navigate links with ENTER /and click too/
;;; ------------------------------------------------------


 (setq org-preview-latex-default-process 'dvipng)
 (setq org-return-follows-link t)
;;
;;

;;
;;; ======================================================
;;; ======================================================
;;; ======================================================
;; probably very important   LANGUAGES x  LANGUAGES x LANGUAGES x
;;; ------------------------------------------------------

;;(require 'ob-ipython)
;; ;; ;; enable python for in-buffer evaluation
(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)
   (shell . t)
   (latex . t)
   (dot . t)
   (ipython . t)
   )
 )



;; all python code be safe
(defun my-org-confirm-babel-evaluate (lang body)
  (not (string= lang "python"))
;;  (not (string= lang "ipython")) ;; i dont know AND operator
;;  (not (string= lang "shell"))
  )


;;; -- this seem to avoid y/n message
(setq org-confirm-babel-evaluate 'my-org-confirm-babel-evaluate)
;;;;
(setq python-shell-interpreter "python3")
;;;;(setq python-shell-interpreter "/home/ojr/.local/bin/ipython")

;;;---this is needed to avoid 'not found'..... "python3"
(setq org-babel-python-command "python3")
;;;;(setq org-babel-python-command "/home/ojr/.local/bin/ipython")
;;;(setq org-babel-python-command "ipython3 --no-banner --classic --no-confirm-exit")
;;
;;

;; this removes the message in org codeblock.... not solving anything else
(setq python-shell-completion-native-disabled-interpreters '("python3"))
;;
;;
;;
;;


;; ======================================================
;;  Screenshot from emacs  USE --------- F6 -------------
;;; ------------------------------------------------------
(defun my-org-screenshot ()
  "Take a screenshot into a time stamped unique-named file in the
same directory as the org-buffer and insert a link to this file."
  (interactive)
  (setq filename
        (concat
         (make-temp-name
          (concat (buffer-file-name)
                  "_"
                  (format-time-string "%Y%m%d_%H%M%S_")) ) ".jpg"))
  (call-process "import" nil nil nil filename)
  (insert (concat "  [[file:" filename "]]"))
  ;;  (org-display-inline-images)

)



;;; --- this might be helpful with image width. not yet....
(setq org-image-actual-width nil)
(add-hook 'org-babel-after-execute-hook 'org-display-inline-images 'append)
;;;---------------





;;; ======================================================
;; TODO KEYWORDS..... I have some latex export colors too...
;;; ------------------------------------------------------
;;;-------------latex converter use #+pragma tex:t
;;(setq org-latex-to-mathml-convert-command
;;      "latexmlmath \"%i\" --presentationmathml=%o")
;; ... use with shift-Left(right).....
(setq org-todo-keywords
  '(
(sequence "WAITING" "TODO" "DOING" "|" "DELEGATED" "POSTPONED" "DONE" "CANCELED")
;;;(sequence "SENT" "APPROVED" "|" "PAID")
))

(setq org-todo-keyword-faces
  '(("TODO" . (:foreground "#ff39a3" :weight bold))
("STARTED" . "#E35DBF")
("WAITING" . (:foreground "#00dd00" :background "#ddffdd" :weight bold))
("CANCELED" . (:foreground "white" :background "#4d4d4d" :weight bold))
("DELEGATED" . "pink")
("POSTPONED" . "#008080")))
;;;;; HOOK FOR AGENDA OPEN
(defadvice org-agenda (around split-vertically activate)
  (let ((split-width-threshold 40))  ; or whatever width makes sense for you
    ad-do-it))

;;

;;; ======================================================
;;    C-c in orgmode python - keep indents
;;; ------------------------------------------------------
;;;;; python in C-c ' breaks idents
(setq org-src-preserve-indentation t)
;;to nil and org-edit-src-content-indentation to 0.





;;; ======================================================
;;;   minted colors -
;;; ------------------------------------------------------
;;       Successfully installed pygments-2.10.0 - knows asc
;;(add-to-list 'org-latex-packages-alist '("" "minted"))
;;(setq org-latex-listings 'minted)
;;(setq org-src-fontify-natively t)
;;;JMOJMO
(setq org-latex-listings 'minted)
;;;
;;; JM...problematic minted options (when no other definitions) removed
(setq org-latex-minted-options
      '(("frame" "lines") ("linenos=true")))


;;; ======================================================
;;  running pdflatex
;;; ------------------------------------------------------
;;
;; this was the best answer for citations.... WORKS FANTASTIC but Cs - solved by utf8 line with lua
(setq org-latex-pdf-process (list
   "latexmk -pdflatex='lualatex -shell-escape -interaction nonstopmode' -pdf -f  %f"))




;; use scrbook here
(with-eval-after-load 'ox-latex
   (add-to-list 'org-latex-classes
                '("scrbook"
                  "\\documentclass{scrbook}"
                  ("\\chapter{%s}" . "\\chapter*{%s}")
                  ("\\section{%s}" . "\\section*{%s}")
                  ("\\subsection{%s}" . "\\subsection*{%s}")
                  ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))



;; ======================================================
;;   Convert md to org.....
;; ------------------------------------------------------
  (defun markdown-convert-buffer-to-org ()
    "Convert the current buffer's content from markdown to orgmode format and save it with the current buffer's file name but with .org extension."
    (interactive)
    (shell-command-on-region (point-min) (point-max)
                             (format "pandoc -f markdown -t org -o %s"
                                     (concat (file-name-sans-extension (buffer-file-name)) ".org"))))


;; ;;; ======================================================
;; ;;   collapsing BLOCKS  vim-collapse  --- ???????????????/
;; ;;; ------------------------------------------------------

;; ;;; orgmode - collapse codeblocks with C-c t
;; ;;; ============  and auto collapse CODEblocks on startup  NOT NICE - REMOVED
;; ;;(defvar org-blocks-hidden nil)

;; (defun org-toggle-blocks ()
;;   (interactive)
;;   (if org-blocks-hidden
;;       (org-show-block-all)
;;     (org-hide-block-all))
;;   (setq-local org-blocks-hidden (not org-blocks-hidden)))

;; (add-hook 'org-mode-hook 'org-toggle-blocks)
;; (define-key org-mode-map (kbd "C-c t") 'org-toggle-blocks)

;;; ======================================================
;;    good with orgmode --- visual line mode : long lines
;;; ------------------------------------------------------
(add-hook 'org-mode-hook 'visual-line-mode)


;; ;;; ======================================================
;; ;;      revert pdf when opened by emacs.....on recompilation...
;; ;;; ------------------------------------------------------
(setq revert-without-query '(".+\.pdf"))





;; ;;; ======================================================
;; ;;      On export org to LATEX to PDF,  replace AGENDA labels with Colors
;; ;;; ------------------------------------------------------
(with-eval-after-load 'ox
  (defun my-org-latex-filter (text backend info)
    (when (org-export-derived-backend-p backend 'latex)
      (let ((replacements '(("\\\\sffamily TODO" . "\\\\textcolor[HTML]{FF39A3}{TODO}")
                            ("\\\\sffamily STARTED" . "\\\\textcolor[HTML]{E35DBF}{STARTED}")
                            ("\\\\sffamily DONE" . "\\\\textcolor[HTML]{00AA00}{DONE}")
                            ("\\\\sffamily DOING" . "\\\\textcolor{red}{DOING}")
                            ("\\\\sffamily WAITING" . "\\\\textcolor[HTML]{77FF77}{WAITING}")
                            ("\\\\sffamily CANCELED" . "\\\\textcolor[HTML]{888888}{CANCELED}")
                            ("\\\\sffamily DELEGATED" . "\\\\textcolor[HTML]{FFC0CB}{DELEGATED}")
                            ("\\\\sffamily POSTPONED" . "\\\\textcolor[HTML]{008080}{POSTPONED}"))))
        (dolist (pair replacements text)
          (setq text (replace-regexp-in-string (car pair) (cdr pair) text))))))

(add-to-list 'org-export-filter-final-output-functions 'my-org-latex-filter))


;;
;; HTML ---------------------------------  proffesional design -----------------
;;https://jessekelly881-imagine.surge.sh/
;;https://jessekelly881-rethink.surge.sh/
;;https://github.com/jessekelly881
;;
;; ;;; ======================================================
;; ;;      override default orgmode shortcut with magit util_functions
;; ;;; ------------------------------------------------------
;;(with-eval-after-load 'org
;;     (define-key org-mode-map (kbd "C-c C-m") 'magit-status)) ;
