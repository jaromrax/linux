

;;; ======================================================
;;; *
;;; ------------------------------------------------------
(defun templ-multiimage ()
  "Create multi-image Figure with a caption (a hack)"
  (interactive)
  (insert-file-contents "~/.emacs.d/template_multiimage.org"))



;;; ======================================================
;;; *
;;; ------------------------------------------------------
(defun templ-codeblock ()
  "Create NAMED python codeblock"
  (interactive)
  (insert-file-contents "~/.emacs.d/template_codeblock.org"))



;;; ======================================================
;;; *
;;; ------------------------------------------------------
(defun templ-tab2fig ()
  "From NAMED table create FIGURE in python."
  (interactive)
  (insert-file-contents "~/.emacs.d/template_tab2fig.org"))



;;; ======================================================
;;; *
;;; ------------------------------------------------------
(defun templ-header ()
  "Just a header with margins and utf8  ~/.emacs.d/template_header.org."
  (interactive)
  (insert-file-contents "~/.emacs.d/template_header.org"))



;;; ======================================================
;;; *
;;; ------------------------------------------------------
(defun templ-table ()
  "Tables, xchange rates  ~/.emacs.d/template_table.org."
  (interactive)
  (insert-file-contents "~/.emacs.d/template_table.org"))


;;; ======================================================
;;; *
;;; ------------------------------------------------------
(defun templ-literate ()
  "Literal programming  ~/.emacs.d/template_literal.org."
  (interactive)
  (insert-file-contents "~/.emacs.d/template_literal.org"))


;;; ======================================================
;;; *
;;; ------------------------------------------------------
(defun templ-booklet ()
  "A5 booklet  ~/.emacs.d/template_booklet.org."
  (interactive)
  (insert-file-contents "~/.emacs.d/template_booklet.org"))

;;; ======================================================
;;; *
;;; ------------------------------------------------------

(defun templ-beamer ()
  "Presentation  ~/.emacs.d/template_beamer.org."
  (interactive)
  (insert-file-contents "~/.emacs.d/template_beamer.org"))


;;; ======================================================
;;; *
;;; ------------------------------------------------------

(defun templ-calculator ()
  "Tables to dataframe  ~/.emacs.d/template_calculator.org."
  (interactive)
  (insert-file-contents "~/.emacs.d/template_calculator.org"))



;;; ======================================================
;;; *
;;; ------------------------------------------------------
(defun templ-code ()
  "Minted code  ~/.emacs.d/template_code.org."
  (interactive)
  (insert-file-contents "~/.emacs.d/template_code.org"))


;;; ======================================================
;;; *
;;; ------------------------------------------------------

(defun templ-report ()
  "Report with bibtex  ~/.emacs.d/template_report.org."
  (interactive)
  (insert-file-contents "~/.emacs.d/template_report.org"))



;;; ======================================================
;;; *  utilities to use advanced templates
;;; ------------------------------------------------------

(defun read-config (file)
  "Read key-value pairs from FILE into an alist.for XFLD templates"
  (with-temp-buffer
    (insert-file-contents file)
    (let (result)
      (while (re-search-forward "^\\([^=]+\\)=\\(.*\\)$" nil t)
        (push (cons (match-string 1) (match-string 2)) result))
       (message "Config read: %S" result)  ; Debug message
       (message "AHOJKY ... read-config ends...")  ; Debug message
     result)))


(defun replace-in-string (what with in)
  "replace string for XFLD templates"
  (replace-regexp-in-string (regexp-quote what) with in nil 'literal))


;; (defun read-config (file)
;;   "Read configuration from FILE and return an association list."
;;   (with-temp-buffer
;;     (insert-file-contents file)
;;     (let (config)
;;       (while (re-search-forward "^\\([^=]+\\)=\\(.+\\)$" nil t)
;;         (push (cons (match-string 1) (match-string 2)) config))
;;       config)))

;;; ======================================================
;;; *
;;; ------------------------------------------------------
(defun templ-cesta ()
  "Travel report ~/.emacs.d/template_cesta.org with replacements."
  (interactive)
  (message "start   template  cesta :" )  ; Debug message
  (let* ((template-file "~/.emacs.d/template_cesta.org")
         (config-file "~/.org_personaldata.ini")
         (config (read-config config-file))
         (template (with-temp-buffer
                     (insert-file-contents template-file)
                     (buffer-string)))
         (date (format-time-string "%d.%m.%Y"))
         (signature (or (cdr (assoc "signame" config)) "xxx"))
         (institute (or (cdr (assoc "institute" config)) "xxx"))
         (replacements `(("XFLD_DATE" . ,date)
                         ("XFLD_SIGNATURE" . ,signature)
                         ("XFLD_INSTITUTE" . ,institute)))
	 );;let
    (message "Config: %S" config)  ; Debug message
    (message "Initial Template: %s" template)  ; Debug message
    (dolist (replacement replacements)
      (setq template (replace-regexp-in-string (car replacement)
                                               (cdr replacement)
                                               template t)
	    ) ;;setq
      ) ;; dolist
    (message "Final Template: %s" template)  ; Debug message
    (insert template)
    )
  )

;;


;;; ======================================================
;;; *
;;; ------------------------------------------------------

(defun templ-note ()
  "Official note  ~/.emacs.d/template_note.org with replacements."
 (interactive)
  (message "start   template  note ***************** :" )  ; Debug message
  (let* ((template-file "~/.emacs.d/template_note.org")
         (config-file "~/.org_personaldata.ini")
         (config (read-config config-file))
         (template (with-temp-buffer
                     (insert-file-contents template-file)
                     (buffer-string)))
         ;;(date  (format-time-string "%d.%m.%Y") )
         (date1 (format-time-string "%d.%m.%Y") )
         (date2 (format-time-string "%02Y") )
         (signature (or (cdr (assoc "signame" config)) "xxx"))
         (dept (or (cdr (assoc "dept" config)) "xxx"))
         (instt (or (cdr (assoc "instt" config)) "xxx"))
         (recepient (or (cdr (assoc "recepient" config)) "xxx"))
         (mob (or (cdr (assoc "mob" config)) "xxx"))
         (tel (or (cdr (assoc "tel" config)) "xxx"))
         (email (or (cdr (assoc "email" config)) "xxx"))
         (department (or (cdr (assoc "department" config)) "xxx"))
         (street (or (cdr (assoc "street" config)) "xxx"))
         (city (or (cdr (assoc "city" config)) "xxx"))
         (country (or (cdr (assoc "country" config)) "xxx"))
         (institute (or (cdr (assoc "institute" config)) "xxx"))
         (replacements `(
			 ("XFLD_DATE" . ,date1)
                         ("XFLD_YEAR2" . ,date2)
                         ("XFLD_SIGNATURE" . ,signature)
                         ("XFLD_DEPT" . ,dept)
                         ("XFLD_INSTT" . ,instt)
                         ("XFLD_RECEPIENT" . ,recepient)
                         ("XFLD_MOB" . ,mob)
                         ("XFLD_TEL" . ,tel)
                         ("XFLD_EMAIL" . ,email)
                         ("XFLD_DEPARTMENT" . ,department)
                         ("XFLD_STREET" . ,street)
                         ("XFLD_CITY" . ,city)
                         ("XFLD_COUNTRY" . ,country)
                         ("XFLD_INSTITUTE" . ,institute)))
	 );; closest let
   ;; );;  let
    (message "1 All is defined here. DOLIST is coming" )
    (message "2 All is defined here. DOLIST is coming" )
    ;;(message "Config: %S" config)  ; Debug message
    ;;  (message "Initial Template: %s" template)  ; Debug message
    (message "RRR: %s" replacements)  ; Debug message
    ;;    (message "date: %s" date1)  ; Debug message
    (dolist (replacement replacements)
      ;;(message "replacing  %s with  %s"  (car replacement) (cdr replacement) )  ; Debug message
        (setq template (replace-regexp-in-string (car replacement)
                                                 (cdr replacement)
                                                 template t
						 )
	      );;setq
	);;dolist
    ;;;);;last let
    (message "Now I will insert the templace with replacements ***************")  ; Debug message
    (insert template)
    ) ;;let
  )
