#+OPTIONS: toc:nil        (no default TOC at all)
#+LATEX_HEADER: \usepackage[margin=1.4in]{geometry}
#    I need czech ---------------
#+LATEX_HEADER: \usepackage[utf8]{luainputenc}
#      THIS was for cs czech
#+LaTeX_HEADER: \linespread{1.0}
#+latex_class_options: [a4paper,12pt]
# ================== no page numbering
#+LATEX_HEADER: \usepackage{nopageno}
#+LATEX_HEADER: \usepackage{fancyhdr}
#+LATEX_HEADER: \usepackage{xcolor}
# =================== no indent filewide https://emacs.stackexchange.com/questions/16889/how-to-control-newline-and-indent-when-export-to-latex-from-org-mode-file
#+LATEX: \setlength\parindent{0pt}

#+LATEX_HEADER: \definecolor{ublue}{RGB}{0,147,221}

#+LATEX_HEADER: \pagestyle{fancy}
#+LATEX_HEADER: \fancyhead{}
#+LATEX_HEADER: \fancyfoot{}
#+LATEX_HEADER: \fancyhead[C]{\footnotesize Sdělení z odd.: XFLD_DEPT, XFLD_INSTT, XFLD_SIGNATURE - XFLD_RECEPIENT}
# # +LATEX_HEADER: \fancyhead[CO,CE]{HiPo - target  beam test}
# # +LATEX_HEADER: \fancyhead[LE,RO]{\slshape \rightmark}
# # +LATEX_HEADER: \fancyhead[LO,RE]{\slshape \leftmark}
# # +LATEX_HEADER: \fancyfoot[RO, LE] {\thepage}
#+LATEX_HEADER: \fancyfoot[R]{\color{ublue}\footnotesize Mobil: XFLD_MOB}
#+LATEX_HEADER: \fancyfoot[L]{\color{ublue}\footnotesize Telefon: XFLD_TEL}
#+LATEX_HEADER: \fancyfoot[C]{\color{ublue}\footnotesize email: XFLD_EMAIL}
# # +LATEX_HEADER: \fancyfoot[RE,RO]{\it --- draft version ---}
# # +LATEX_HEADER: \headrulewidth 0pt
# # +LATEX_HEADER: \footrulewidth 0 pt

#+LATEX_HEADER: \setlength\headheight{26pt}
#+LATEX_HEADER: \lhead{\includegraphics[width=1cm]{logo.png}}



#+LATEX_HEADER:\newcommand{\mysignrule}[1]{%
# +LATEX_HEADER:\vspace{3mm}
# +LATEX_HEADER:\rule{\linewidth}{0.5pt}\newline
#+LATEX_HEADER:#1\par
#+LATEX_HEADER:}


# find . -not -path "./.stversions/*"  -iname "*org" -exec grep -H  "\.j\.:" {} \;

# ============================================================== HELP===
# FOR NOTICE LETTER - with an official header
# _________________________________________
#  you need a logo ...
# --------------------------------------------------------------------- HELP

# ============================================================== START===
# \footnotesize Sdělení z odd.: DEP, INS ACAD, City - Name Surnme, Tit.
# +LATEX: \hskip 1cm do odd.: ředitel xxx -
\normalsize

# # ----------- this creates some vskip..... it may have been used for ^{}, but \null is better
\hphantom{}

#+LATEX: \vskip 3mm
# +LATEX: \hrule
#+LATEX: \vskip 3mm

#+LATEX: \hfill
Date: XFLD_DATE

#+LATEX: \hfill
č.j.: 04xxx/XFLD_YEAR2



* To whom it may concern:
  :PROPERTIES:
  :UNNUMBERED: t
  :END:
#+LATEX: \hskip 1cm


Hereby we declare, that


#+begin_center

Centered text

#+end_center

 protection against COVID-19 imposed by the government of the Czech Republic.

#+LATEX: \vskip 3mm

These rules - in the mentioned period of the stay -  required to present a valid negative COVID-19 test at least
once per 7 days.


#+LATEX: \vskip 3cm


#+LATEX:\begin{flushright}
#+LATEX:\begin{minipage}{0.45\linewidth}
#+LATEX:\mysignrule{XFLD_SIGNATURE}
\vskip 2mm
#+LATEX:\mysignrule{XFLD_DEPARTMENT}
#+LATEX:\mysignrule{XFLD_INSTITUTE }
#+LATEX:\mysignrule{XFLD_STREET}
#+LATEX:\mysignrule{XFLD_CITY}
#+LATEX:\mysignrule{XFLD_COUNTRY}
#+LATEX:\end{minipage}
#+LATEX:\end{flushright}


# +LATEX: \vskip 3mm
# +LATEX: \hrule

# +LATEX: \hfill

#   +LATEX: \footnote{Telefon: }
