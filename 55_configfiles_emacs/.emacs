;;
(require 'package)
;;
;;  WHEN NEW PGP SIGNATURE:
;;      set THIS check to nil (uncomment)
;;(setq package-check-signature nil)
;; DO m-x  package-refresh-contents,  list-packages,  package-install gnu-elpa-keyring-update
;;      AND RESTART

;; when TO INSTALL :  M-x package-install-refresh-contents ... package-install

;; This contains OLD versions 27.1 of vertico marginalia
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
;; This contains most of the extra things I use ....
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; IDK...
(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/") t)

;; UBUNTU 24.04 generates lot of warnings -----------------------
(setq warning-minimum-level :error)
(setq warning-minimum-log-level :error)

(setq byte-compile-warnings nil)
   (with-eval-after-load 'bytecomp
     (add-to-list 'byte-compile-not-obsolete-vars 'dash-functional))
(setq avy-all-windows t)


;; ---------------end of hack -----------------------


(package-initialize)

;; ===============================================================
;; I want to have all used pkgs listed here!
;;----------------------------------------------------------------
;; define packages to use (the last are those hard to install)
(setq my-packages
      '(auto-dim-other-buffers  ;; Basic:Dim inactive buffers
        cmake-mode              ;; CMake files support
        gnu-elpa-keyring-update ;; Update GPG keys for ELPA
        olivetti                ;; Distraction-free writing
        wc-mode                 ;; UTIL:Word count mode
        vimish-fold             ;; UTIL:Code folding - C-f C-d fold-defold
        multiple-cursors        ;; UTIL:Multiple cursors editing
        phi-search              ;; UTIL:Search AND Interactive REPLACE
        ag                      ;; UTIL:Silver searcher integration
;;	elpy                    ;; UTILS: flycheck python OK - dont use elpy
        blacken                 ;; UTILS:Python code formatter - i dkn too
;;        reformatter             ;; UTILS:Define code reformatters - i dkn
        flycheck-pycheckers     ;; UTILS:Python syntax checking flycheck-mode
        diff-hl                 ;; for magit
        magit                   ;; git: 'g', 's', 'c c' 'c-c' 'P u'
        lua-mode                ;; MODES:Lua programming support
        arduino-mode            ;; MODES:Arduino development
	markdown-mode           ;; MODES: markdown
;;        emojify                 ;; MODES: :( slows down, troubles
;;        stripes                 ;; MODES: Nice,but breaks selecting color...
;;        highlight-indentation   ;;MODES:delayed very much messed C++ editing!!!
        undo-tree               ;; MODES:Visualize undo history C-x u; C-/ OR C-S-/
        minimap                 ;; MODES: Show code minimap on side
        bm                      ;; MODES:Bookmark management ;mapped to F2,C-F2
        electric-operator       ;; MODES:Automatic spacing around operators
        orgtbl-join             ;; Org:Join tables in Org mode
        htmlize                 ;; Org:Convert buffer to HTML
	translate-mode          ;; testing translate mode
        browse-kill-ring        ;; visual kill ring/clipboard
;;
        transpose-frame         ;; from horizontal to vertical split
        ob-ipython              ;; Org:IPython support in Org Babel
        vertico                 ;; UTILS:Vertical completion UI - VERSION!
        marginalia              ;; UTILS:Annotate completion candidates - VERSION!
	))

(unless package-archive-contents
  (package-refresh-contents))

(dolist (pkg my-packages)
  (let* ((pkg-name (if (listp pkg) (car pkg) pkg))
         (pkg-comment (if (listp pkg) (cadr pkg) ""))
         (status (condition-case err
                     (if (package-installed-p pkg-name)
                          (progn
                           (require pkg-name nil 'noerror)
                           "... already installed")
                       (if (assoc pkg-name package-archive-contents)
                           (progn
                             (package-install pkg-name)
			     (require pkg-name nil 'noerror)
                             "... installed now")
                         "... NOT FOUND"))
                   (error (format "... *ERROR*: %s" err)))))
    (message (format "Processing package: %-60s - %s %s" pkg-name status pkg-comment))))


;;; ============================================================================
;;;        custom variables - emacs edits this here!!
;;; ----------------------------------------------------------------------------
;;;
;; custom-set-faces I CONF_COLORS;   custom-set-variables ****  THE TWO UNIQUE THINGS
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(inhibit-startup-screen t)
 ;; '(package-selected-packages '(elpy orgtbl-join dockerfile-mode docker markdown-mode))
 )

;;; ============================================================================
;;;        colors faces backgrounds
;;; ----------------------------------------------------------------------------
;;;
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auto-dim-other-buffers-face ((t (:background "#999999"))))
 '(font-lock-comment-face ((t (:foreground "#444444" :background "LightGray"))))
 '(font-lock-doc-face ((t (:inherit nil :foreground "dark green" :slant italic))))
 '(org-block ((t (:background "white smoke" :foreground "black" :extend t))))
 '(org-block-begin-line ((t (:underline "#A7A6AA" :foreground "#008ED1" :background "#EAFAFF"))))
 '(org-block-end-line ((t (:overline "#A7A6AA" :foreground "#008ED1" :background "#EAAFF"))))
 '(org-code ((t (:inherit shadow :background "ghost white" :foreground "saddle brown"))))
 '(org-level-1 ((t (:inherit outline-1 :weight bold :height 1.3))))
 '(org-level-2 ((t (:inherit outline-2 :foreground "dark green" :weight bold :height 1.2))))
 '(org-level-3 ((t (:inherit outline-3 :foreground "dark orange" :weight bold :height 1.1))))
 '(org-verbatim ((t (:inherit shadow :background "cornsilk")))))

;;; Override for programming languages blocks
;;; list-colors-display to see
(setq org-src-block-faces '(("emacs-lisp" (:background "#EEE2FF" :extend t))
;;;			    ("python" (:background "DarkSeaGreen1" :extend t))
			    ("python" (:background "honeydew1" :extend t))
			    ("sh" (:background "AntiqueWhite1" :extend t))
			    ("bash" (:background "AntiqueWhite1" :extend t))
                            ("shell" (:background "Gold" :extend t))))
 ;;;--------- color for orgmode blocksreg.......

 ;;; ----------  selection color and face ------ second selection is alt-mouse1
 ;;; (set-face-attribute 'region nil :background "#666" :foreground "#ffffff")
(set-face-attribute 'region nil :background "#ffd700" :foreground "#0000aa")


(load "~/.emacs.d/conf_basic_behavior")
(load "~/.emacs.d/conf_util_functions")
(load "~/.emacs.d/conf_use_modes")
(load "~/.emacs.d/conf_macros")
;; you need to install pip3 install jupyter to avoid ob-python json122
(load "~/.emacs.d/conf_org")
(load "~/.emacs.d/conf_templates")

;;(require 'org-journal) ;; something not useful i think
;;(setq org-journal-dir "~/01_Dokumenty/00_orgmode_data/journal")


;; (setq exec-path (append exec-path '("/usr/bin/")))
;; ;;(load "auctex.el" nil t t)
;; (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
;; (org-babel-do-load-languages
;;  'org-babel-load-languages
;;  '((latex . t)))
;; ;;
;; ;; apt install dvipng
;; ;;
;;=======================================================================
;;  guerrila trick for TIKZ
;;-------------------------------------------------------------------------
(setq org-preview-latex-process-alist
      '((imagemagick :programs ("pdflatex" "convert")
                :description "dvi > png"
                :message "you need to install the programs: latex and dvipng."
                :use-xcolor t
                :image-input-type "dvi"
                :image-output-type "png"
                :image-size-adjust (1.0 . 1.0)
                :latex-compiler ("latex -interaction nonstopmode  %o %f")
                :image-converter ("convert -density %D -trim-antialias %f -quality 100 %O")
		)
	(dvisvgm :programs ("latex" "dvisvgm")
                 :description "dvi > svg"
		 :message "you need to install the programs: pdflatex and dvisvgm."
                 :use-xcolor t
                 :image-input-type "dvi"
                 :image-output-type "svg"
                 :image-size-adjust (1.0 . 1.0)
                 :latex-compiler ("latex -interaction nonstopmode -output-directory %o %f")
                 :image-converter ("dvisvgm  %f -n -b min -c %S -o %O")
	 )
	)
)
;;  %f is full input  %O is full output?
(setq org-preview-latex-default-process 'dvisvgm)

(setq org-latex-create-formula-image-program 'dvisvgm) ;; or 'imagemagick or 'svg
(define-key org-mode-map (kbd "C-c C-x C-l") 'org-latex-preview)
(setq org-format-latex-options (plist-put org-format-latex-options :scale 2.0))

(setq org-latex-packages-alist '(
				 ("" "tikz" t)
				 ("" "circuitikz" t)
				 )
)
;;(setq org-preview-latex-default-process 'imagemagick)
