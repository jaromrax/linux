;;; ======================================================
;; code bloks macros...
;;  ..I lost some <s <e from orgmode earlier, here is aa replace
;;
;;
;;
;;; ------------------------------------------------------

;;; ======================================================
;; NEW PYTHON PROGRAM
;;; ------------------------------------------------------
;;
;;  JMO: newpy - new python
;;
(fset 'newpy
   [C-home home ?# ?! ?/ ?u ?s ?r ?/ ?b ?i ?n ?/ ?e ?n ?v ?  ?p ?y ?t ?h ?o ?n ?3 return return ?f ?r ?o ?m ?  ?f ?i ?r ?e ?  ?i ?m ?p ?o ?r ?t ?  ?F ?i ?r ?e return return ?d ?e ?f ?  ?m ?a ?i ?n ?\( ?\) ?: return ?p ?r ?i ?n ?t ?\( ?\) return return home ?i ?f ?  ?_ ?_ ?n ?a ?m ?e ?_ ?_ ?= ?= ?\" ?_ ?_ ?m ?a ?i ?n ?_ ?_ ?\" ?: return ?F ?i ?r ?e ?\( ?m ?a ?i ?n ?\) return return return])


;;; ======================================================
;;   org block src
;;; ------------------------------------------------------
;;
;;  JMO: newpysrc block
;;
(fset 'newpysrc
   (kmacro-lambda-form [?\C-w ?# ?+ ?b ?e ?g ?i ?n ?_ ?s ?r ?c ?  ?p ?y ?t ?h ?o ?n ?  ?: ?r ?e ?s ?u ?l ?t ?s ?  ?r ?e ?p ?l ?a ?c ?e ?  ?o ?u ?t ?p ?u ?t ?  ?: ?s ?e ?s ?s ?i ?o ?n ?  ?t ?e ?s ?t ?  ?: ?e ?x ?p ?o ?r ?t ?s ?  ?r ?e ?s ?u ?l ?t ?s return ?\C-y ?# ?+ ?e ?n ?d ?_ ?s ?r ?c return return] 0 "%d"))

;;; ======================================================
;; org - df to table template
;;; ------------------------------------------------------
;;
;;  JMO: newpytab prepare to result df as orgtable
;;
(fset 'newpytab
   (kmacro-lambda-form [?# ?+ ?b ?e ?g ?i ?n ?_ ?s ?r ?c ?  ?p ?y ?t ?h ?o ?n ?  ?: ?r ?e ?s ?u ?l ?t ?s ?  ?v ?a ?l ?u ?e ?  ?r ?a ?w ?  ?: ?r ?e ?t ?u ?r ?n ?  ?t ?a ?b ?u ?l ?a ?t ?e ?\( ?d ?f ?, ?h ?e ?a ?d ?e ?r ?s ?= ?d ?f ?. ?c ?o ?l ?u ?m ?n ?s ?, ?t ?a ?b ?l ?e ?f ?m ?t ?= ?\' ?o ?r ?g ?t ?b ?l ?\' ?\) return return ?# ?+ ?e ?n ?d ?_ ?s ?r ?c return up up ?i ?m ?p ?o ?r ?t ?  ?p ?a ?n ?d ?a ?s ?  ?a ?s ?  ?p ?d return ?i ?m backspace backspace ?f ?r ?o ?m ?  ?t ?a ?b ?u ?l ?a ?t ?e ?  ?i ?m ?p ?o ?r ?t ?  ?t ?a ?b ?u ?l ?a ?t ?e return] 0 "%d"))


;;; ======================================================
;;  org - beamer  mycolumn create
;;; ------------------------------------------------------
;;
(fset 'mycolumn
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([kp-multiply kp-multiply 32 67 111 108 117 109 110 48 53 return 32 32 32 32 58 80 82 79 80 69 82 84 73 69 83 58 return 32 32 32 32 58 66 69 65 77 69 82 95 101 110 118 58 32 99 111 108 117 109 110 return 32 32 32 32 58 66 69 65 77 69 82 95 99 111 108 58 32 48 46 53 return 32 32 32 32 58 69 78 68 58 return] 0 "%d")) arg)))

;;; ======================================================
;; org - image    myimage
;;; ------------------------------------------------------
;;
(fset 'myimage
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([35 kp-add 65 84 84 82 95 76 65 84 69 88 58 32 58 111 112 116 105 111 110 115 32 97 110 103 108 101 61 48 32 58 99 101 110 116 101 114 32 116 114 117 32 58 119 105 100 116 104 32 48 46 57 57 92 116 101 120 116 119 105 100 116 104 return 32 32 91 91 102 105 108 101 58] 0 "%d")) arg)))

;;; ======================================================
;;  org - example block   myexample
;;; ------------------------------------------------------
;;
;;   myexample
;;
(fset 'myexample
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([kp-multiply kp-multiply kp-multiply 32 69 120 97 109 112 108 101 32 98 108 111 99 107 return 32 32 32 32 58 80 82 79 80 69 82 84 73 69 83 58 return 32 32 32 32 58 66 69 65 77 69 82 95 101 110 118 58 32 101 120 97 109 112 108 101 98 108 111 99 107 return 32 32 32 32 58 69 78 68 58 return] 0 "%d")) arg)))

;;; ======================================================
;; org - beamer presentation -  myuncover
;;; ------------------------------------------------------
;;
(fset 'myuncover
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([35 kp-add 66 69 65 77 69 82 58 32 92 117 110 99 111 118 101 114 60 49 45 62 123 return return 35 kp-add 66 69 65 77 69 82 58 32 125 return] 0 "%d")) arg)))

;;; ======================================================
;; org - new block - newpyquote  ; put selection into a  block
;;; ------------------------------------------------------
;;
;;  JMO: newpyquote - selection and put into quote block
;;
(fset 'newpyquote
   (kmacro-lambda-form [?\C-w ?# ?+ ?b ?e ?g ?i ?n ?_ ?q ?u ?o ?t ?e return ?\C-y ?# ?+ ?e ?n ?d ?_ ?q ?u ?o ?t ?e return] 0 "%d"))


;;; ======================================================
;;   org - new block - newpyexam ;put selection into a  block
;;; ------------------------------------------------------
;;
(fset 'newpyexam
   (kmacro-lambda-form [?\C-w ?# ?+ ?b ?e ?g ?i ?n ?_ ?e ?x ?a ?m ?p ?l ?e return ?\C-y ?# ?+ ?e ?n ?d ?_ ?e ?x ?a ?m ?p ?l ?e return] 0 "%d"))




;;================================================
;; ----------------   create slide  -----------------
;; JMO: EXT: sil/si   GENERATE SLIDE
;;   slide image left  ............
(defun beamer-sil ()
  "create slide with left image using create_org.py"
  (interactive)
  (let ((selected-text (buffer-substring (region-beginning) (region-end))))
    (goto-char (region-end))
    (insert "\n")
    (insert "\n")
    (shell-command (concat "/usr/local/bin/create_org.py sil \"" selected-text "\"") t)
    (goto-char (region-end))
    (insert "\n")
    (insert "\n")
    ))

;;   slide image   ............
;; JMO: EXT: sil/si   GENERATE SLIDE
(defun beamer-si ()
  "create slide  using create_org.py"
  (interactive)
  (let ((selected-text (buffer-substring (region-beginning) (region-end))))
    (goto-char (region-end))
    (insert "\n")
    (insert "\n")
    (shell-command (concat "/usr/local/bin/create_org.py si \"" selected-text "\"") t)
    (goto-char (region-end))
    (insert "\n")
    (insert "\n")
    ))
;;



;;================================================
;; ------------------ GPT  test ------------------------
;;................................................
;; JMO: EXT: gpt (app.py)
;; install from git...   cmd_ai
;; (defun qgpt-base (with-r)
;;   "call cmd_ai on a selected text. With -r switch; variant:Echo"
;;   (interactive)
;;   (let ((selected-text (buffer-substring (region-beginning) (region-end)))
;; 	(action (read-string "What to do with the selected text? (default: 'just do that') "))
;; 	;;(action (read-string "What to do with the selected text? "))
;; 	)
;;     (goto-char (region-end))
;;     (insert "\n")
;;     (insert "> ")

;; (let ((cmd (executable-find "cmd_ai")))
;;   (if cmd
;;       (let ((full-cmd (concat cmd " \""
;; 			      (concat "With the TEXT do exactly what COMMAND says. The COMMAND: "
;; 				      action " . The TEXT: " selected-text "  \" "
;; 				      (if with-r "-r " "") "-n echo -a assistant ")
;; 			      )
;; 		      )
;; 	    )
;;         (message "Executing command: %s" full-cmd)
;;         (shell-command full-cmd)
;;         (insert-file-contents "/tmp/cmd_ai_last_response_echo.txt")
;; 	(goto-char (point-max))
;; 	(insert "\n")

;; 	)
;;     (message "cmd_ai not found in PATH")))
;;     )
;;     ;;(goto-char (region-end)) ;; doesnt jump after the inserted text
;;     ;;(insert "EndOfResponse\n")
;;     )


;;; ======================================================
;;;
;;; ------------------------------------------------------
;;;
;; (defun gpt-base (with-r)
;;   "Base function to call cmd_ai on selected text with optional -r switch and Echo."
;;   (let* ((selected-text (buffer-substring (region-beginning) (region-end)))
;;          (action (read-string "What to do with the selected text? (default: 'just do that') "))
;;          (final-action (if (string-empty-p action) "Follow what TEXT says." action))
;;          (cmd (executable-find "cmd_ai")))
;;     (if cmd
;;         (let ((full-cmd (concat cmd " \""
;;                                 (concat "With the TEXT do exactly what COMMAND says. The COMMAND: "
;;                                         final-action " . The TEXT: " selected-text "  \" "
;;                                         (if with-r "-r " "") "-n echo -a assistant "))))
;;           (message "Executing command: %s" full-cmd)
;;           (goto-char (region-end))
;;           (insert "\n")
;;           (insert "\n")
;;           (shell-command full-cmd)
;;           (insert-file-contents "/tmp/cmd_ai_last_response_echo.txt")
;;           (goto-char (point-max))
;; 	  (insert "\n")
;; 	  )
;;       (message "cmd_ai not found in PATH"))))

(defun gpt-base (with-r)
  "Base function to call cmd_ai on selected text with optional -r switch."
  (let* ((selected-text (buffer-substring (region-beginning) (region-end)))
         (action (read-string "What to do with the selected text? (leave empty for default behavior) "))
         (prompt (if (string-empty-p action)
                     selected-text ;; Use just the selected text if action is empty
                   (concat "With the TEXT do exactly what COMMAND says. The COMMAND: "
                           action " . The TEXT: " selected-text))) ;; Full prompt otherwise
         (cmd (executable-find "cmd_ai")))
    (if cmd
        (let ((full-cmd (concat cmd " \"" prompt "\" "
                                (if with-r "-r " "") "-n echo -a assistant ")))
          (message "Executing command: %s" full-cmd)
          (goto-char (region-end))
          (insert "\n\n ")
          (shell-command full-cmd)
          (insert-file-contents "/tmp/cmd_ai_last_response_echo.txt")
          (goto-char (point-max))
	  (insert "\n\n ")
	  ) ;; Jump to the end of the inserted text
      (message "cmd_ai not found in PATH"))))


;;; ======================================================
;;;
;;; ------------------------------------------------------
;;;
(defun gpt-new ()
  "Call cmd_ai on selected text with -r switch."
  (interactive)
  (gpt-base t)) ;; Call base function with -r

;;; ======================================================
;;;
;;; ------------------------------------------------------
;;;
(defun gpt-cont ()
  "Call cmd_ai on selected text without -r switch."
  (interactive)
  (gpt-base nil)) ;; Call base function without -r





;;; ======================================================
;;;
;;; ------------------------------------------------------
;;
(defun gpt-read-input ()
  "void finc - Speak to gpt in minibuffer"
  (interactive)
  (let ((user-input (read-from-minibuffer "Enter input: ")))
    (message "You entered: %s" user-input)))

;;; ======================================================
;;;   translate
;;; ------------------------------------------------------
;; ================== TRANSLATE == problems, but:sudo apt-get install translate-shel
;; dont use google-translate .... tkk problem; dont use go-translat ... list problem...;; dontuse  tmalsburg txl
;; google-translate-mode.el --- Google Translate minor mode
;; dont use translate-shell
;; This file is distributed as part of translate-shell
;; URL: https://github.com/soimort/translate-shell
;; Last-Updated: 2015-04-08 Wed

;; ----- apt install translate-shell ; usage trans -b -s en -t cs
;; DEFINITIONS:

;; ;;; ======================================================
;; ;;;
;; ;;; ------------------------------------------------------
;; ;;
;; (defcustom trans-command "trans"
;;   "trans command name."
;;   :type 'string
;;   :group 'google-translate-mode)

;; ;;; ======================================================
;; ;;;
;; ;;; ------------------------------------------------------
;; ;;
;; (defcustom trans-verbose-p nil
;;   "Is in verbose mode. (default: nil)"
;;   :type 'boolean
;;   :group 'google-translate-mode)


;;; ======================================================
;;;
;;; ------------------------------------------------------
;;
(defun trans (verbose source target text)
  "Translate - call trans"
  (shell-command-to-string
   (concat trans-command
           (if verbose "" " -b")
           (if source (concat " -s " source) "")
           (if target (concat " -t " target) "")
           " " "\"" text "\"" )))

;;; ======================================================
;;; en2cs word selection
;;; ------------------------------------------------------
;;
(defun en2cs ()
  "Insert translation of the current word right after."
  (interactive)
  (insert " ")
  (insert
   (trans trans-verbose-p "en" "cs" (buffer-substring (mark) (point)) )))
;; JMO: EXT: en2cs view


;;; ======================================================
;;;  cs2en word selection
;;; ------------------------------------------------------
;;;
(defun en2cs-view ()
  "Popup translation of the current word right after."
  (interactive)
  (let ((translation
         (trans t "en" "cs" (current-word))))
    (x-popup-menu t (list "Translation"
                          (list "PANE"
                                (list translation nil))))))

;;
;;
;;; ======================================================
;;; JMO: EXT: cs2en I added selection - instead of currentbuffer
;;; ------------------------------------------------------
;;
(defun cs2en ()
  "Insert translation of the current word right after."
  (interactive)
  (insert " ")
  (insert
   (trans trans-verbose-p "cs" "en" (buffer-substring (mark) (point)) )))


;;; ======================================================
;;; JMO: EXT: cs2en view
;;; ------------------------------------------------------
;;
(defun cs2en-view ()
  "Popup translation of the current word right after."
  (interactive)
  (let ((translation
         (trans t "cs" "en" (current-word))))
    (x-popup-menu t (list "Translation"
                          (list "PANE"
                                (list translation nil))))))
;; ------------------------------------------------------------- END OF TRANSLATE-SHELL
