;;;  ALL tricks + qltool + my upper + mybeamer
;;;
;;;   ALL GLOBAL-SET-KEY  HERE*****************************
;;; _______________________________________________________
;;;



;;; ======================================================
;;  m-x  bm-toggle  ... better than  c-x r m  c-x r l
;;; ------------------------------------------------------
(require 'bm)
(global-set-key (kbd "<C-f2>") 'bm-toggle)
(global-set-key (kbd "<f2>")   'bm-next)
(global-set-key (kbd "<S-f2>") 'bm-previous)


;;; ======================================================
;;;   browse kill-ring
;;; ------------------------------------------------------
(global-set-key (kbd "C-c k") 'browse-kill-ring)

;;; ======================================================
;;;   windmove bindings NICE
;;; ------------------------------------------------------
(windmove-default-keybindings)
(global-set-key (kbd "M-<left>") 'windmove-left)
(global-set-key (kbd "M-<right>") 'windmove-right)
(global-set-key (kbd "M-<up>") 'windmove-up)
(global-set-key (kbd "M-<down>") 'windmove-down)


;;; ======================================================
;;;   show  word line counts
;;; ------------------------------------------------------
(require 'wc-mode)



;;; ======================================================
;;; test
;;; ------------------------------------------------------
;; select a region and convert it to uppercase or lowercase
;; keys: c-x c-u or c-x c-l
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)



;;; ======================================================
;;  FOLD CODE  NOT using it?  C- Fold Defold
;;; ------------------------------------------------------
(require 'vimish-fold)
(global-set-key (kbd "C-c f") 'vimish-fold)
(global-set-key (kbd "C-c d") 'vimish-fold-delete)
;;; ======================================================
;;
;;; ------------------------------------------------------
;;(global-set-key (kbd "C-c t") 'vimish-fold-toggle)
;; global keyboard shortcuts ... NOT ANYMORE...C-/ is UNDOTREE
;;(global-set-key (kbd "C-/") 'comment-or-uncomment-region)


;;; ======================================================
;;;  never used
;;; ------------------------------------------------------
(defun insert-file-name ()
  "Insert the full path file name into the current buffer."
  (interactive)
  (insert (buffer-file-name (window-buffer (minibuffer-selected-window)))))

;;; ======================================================
;;;   multiple cursors ..... NICE, some more power to it?
;;; ------------------------------------------------------
(require 'multiple-cursors)
(global-set-key (kbd "C-c m c") 'mc/edit-lines)
;; mark-all-like-this


;;; ======================================================
;;;     phi-search  M-x phi-replace   ! ineractive REPLACE
;;; ------------------------------------------------------
(require 'phi-search)



;;; ======================================================
;;;      magit NICE
;;;    g:refresh; s:stage; c c :commit C-c finish
;;;    P u  : push
;;; ------------------------------------------------------
(require 'magit)
(add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)
(add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)
;; (setenv "GIT_ASKPASS" "git-credential-cache") ;; this doesnt work
;;(setenv "GIT_CREDENTIAL_HELPER" "store")
;;(setq auth-sources '("~/.authinfo.gpg" "~/.authinfo" "~/.git-credentials"))

;; (add-hook 'magit-mode-hook
;;              (lambda ()
;;                ((magit-pre-refresh . diff-hl-magit-pre-refresh)
;; 		(magit-post-refresh . diff-hl-magit-post-refresh))
;;                ))
;; the default C-c m I USE FOR MC !! DEFINE C-c C-m NOW
(global-set-key (kbd "C-c C-m")  'magit-status)
;; HOWEVER default C-c C-m IN orgmode IS for  org-insert-heading


;;; ======================================================
;;;  Needed: override default orgmode shortcut with magit
;;; ------------------------------------------------------
(with-eval-after-load 'org
     (define-key org-mode-map (kbd "C-c C-m") 'magit-status)) ;
;; Highlight uncommitted changes using VC
;; this may be surprising!!! the brackets ... NICE
(require 'diff-hl)
(global-diff-hl-mode 1)


;;; ======================================================
;;     ag grepsearch within directory NICE
;;; ------------------------------------------------------
;;; apt install silversearch-ag ... GREP ... m-x ag nice
(require 'ag)



;;; ======================================================
;;   python - blacken - pip install    PYTHON CHECK
;;; ------------------------------------------------------
;;;
;;;  elpy mode and blacken (needs pip install blacken)
;;;  errors on the fly...
;;;
;;(require 'elpy) ;; broken install now 2411
;;; ======================================================
;;    this rewrites the python code
;;; ------------------------------------------------------
(require 'blacken)
;;(elpy-enable)


;;; ======================================================
;;  oh, something with python, blackenm flycheck... not sure
;; no idea....
;;; ------------------------------------------------------
;;(require 'reformatter)


;;; ======================================================
;; pyflaskes not nice, pycheckers may be ... IDK flycheck-mode
;; may use   pylint`, `flake8`, and `mypy` simult
;;; ------------------------------------------------------
;;  m-x flycheck-mode
;;  too many statements, branches ... when there is more than 50
;;  open *Messages* to see results
;;(require 'flycheck-pyflakes)
(require 'flycheck-pycheckers)



;;; ======================================================
;;;     vertico + marginalia - longer m-x helps  NICE
;;; ------------------------------------------------------
(require 'vertico)
(vertico-mode)
;; Option 2: Replace `vertico-insert' to enable TAB prefix expansion.
(keymap-set vertico-map "TAB" #'minibuffer-complete)


;;; ======================================================
;;;       d marginalia - longer m-x helps,  NICE
;;; ------------------------------------------------------
;;; Enable rich annotations using the Marginalia package
(require 'marginalia)
(marginalia-mode)



;; ;;; ======================================================
;; ;;       save-hist ... i dont need
;; ;;; ------------------------------------------------------
;; Persist history over Emacs restarts. Vertico sorts by history position.
;; (require 'savehist
;;   :init
;;   (savehist-mode))



;; ;;; ======================================================
;; ;;       orderless - i dont like i think
;; ;;; ------------------------------------------------------
;; ;; Optionally use the `orderless' completion style.
;; (require 'orderless
;;   :init
;;   ;; Configure a custom style dispatcher (see the Consult wiki)
;; ;;  (setq orderless-style-dispatchers '(+orderless-consult-dispatch orderless-affix-dispatch)
;; ;;         orderless-component-separator #'orderless-escapable-split-on-space)
;;   (setq completion-styles '(orderless basic)
;; 	completion-category-defaults nil
;; 	completion-category-overrides '((file (styles partial-completion)))))



;; Optionally use the `orderless' completion style.


;; (require 'orderless
;;   :init
;;   ;; Configure a custom style dispatcher (see the Consult wiki)
;;   ;; (setq orderless-style-dispatchers '(+orderless-consult-dispatch orderless-affix-dispatch)
;;   ;;       orderless-component-separator #'orderless-escapable-split-on-space)
;;   (setq completion-styles '(orderless basic)
;;         completion-category-defaults nil
;;         completion-category-overrides '((file (styles partial-completion)))))

;;


;; ;;; ======================================================
;; ;;   colors in ocde output  :results verbatim :wrap example -ORG code OUTPUT
;; ;;; ------------------------------------------------------
;; ;;; needs xterm-color ;; i needed to see ANSI colors?
;; (require 'xterm-color)
;; (defun org-babel-color-results()
;;   "Color org babel results.
;; Use the :wrap header argument on your code block for best results.
;; Without it, org may color overtop with whatever default coloring it
;; uses for literal examples.
;; Depends on the xterm-color package."
;;   (interactive)
;;   (when-let ((result-start (org-babel-where-is-src-block-result nil nil)))
;;     (save-excursion
;;       (goto-char result-start)
;;       (when (looking-at org-babel-result-regexp)
;;         (let ((element (org-element-at-point)))
;;           (pcase (org-element-type element)
;;             (`fixed-width
;;              (let ((post-affiliated (org-element-property :post-affiliated element))
;;                    (end (org-element-property :end element))
;;                    (contents (org-element-property :value element))
;;                    (post-blank (org-element-property :post-blank element)))
;;                (goto-char post-affiliated)
;;                (delete-region post-affiliated end)
;;                (insert (xterm-color-filter contents))
;;                (org-babel-examplify-region post-affiliated (point))
;;                (insert (make-string post-blank ?\n ))
;;                ))
;;             ((or `center-block `quote-block `verse-block `special-block)
;;              (let ((contents-begin (org-element-property :contents-begin element))
;;                    (contents-end (org-element-property :contents-end element)))
;;                (goto-char contents-begin)
;;                (let ((contents (buffer-substring contents-begin contents-end)))
;;                  (delete-region contents-begin contents-end)
;;                  (insert (xterm-color-filter contents)))))))))))
;; (add-hook 'org-babel-after-execute-hook 'org-babel-color-results)

;; ;;; ======================================================
;; ;;   MAGICAL display of ANSI escape sequences BUT DISAPPEARS on SAVE???
;; ;;; ------------------------------------------------------

;;(add-to-list 'auto-mode-alist '("\\.log\\'" . ansi-colors-display))

;;; ======================================================
;;;   I dont remember, maybe broken/dangerous
;;; ------------------------------------------------------
;;(defun ansi-colors-display ()
;;  (interactive)
;;  (ansi-color-apply-on-region (point-min) (point-max)))
;;
;;  TO export...????????????????   DO NOT SAVE!!!!!
;;

;;;------------------------------------
;;;  This allows to run emacs-lisp code from org ** MOVED TO CONF_ORG.EL
;;;------------------------------------
;; (org-babel-do-load-languages
;;  'org-babel-load-languages
;;  '((emacs-lisp . t))
;;  '((shell . t))
;;  )





;;-----------------------------------------------------------------
;;      My Trick **    C-x  C-q    USES QL700 printer   ***
;;-----------------------------------------------------------------
;;; ======================================================
;;;   help function - pairs from TABLE
;;; ------------------------------------------------------
(defun org-table-create-pairs ()
  "Prepared TO CREATE PAIS - Get the column names and values at the current cursor position in an Org table."
  (interactive)
  (let* ((table (org-table-to-lisp))
         (pos (org-table-current-column))
         (line (max 1 (org-table-current-line)))
         (line2 (1- (1- line)))
         (header (car table))
         (data (remove 'hline (cdr table)))
         (row (nth line2  data ))
         (pairs (mapcar* (lambda (h r) (cons h (or r "N/A"))) header row))
         (result (mapconcat (lambda (pair)
             (format "%s:%s" (car pair) (cdr pair)))
             pairs
              ";"))
  )
 (message "%s" result)
result))

;;; ======================================================
;;;   qrtool show
;;; ------------------------------------------------------
(defun create-qr-code-show (&optional mincp)
  " Prepeared : CALLS a function to "
  (interactive)
  (setq mincp (or mincp ""))
  (setq aaa (org-table-create-pairs))
  (setq command (format "qltool \"%s\" -q %s" aaa mincp))
  (message "COMMAND=%s" command)
  (shell-command command)
  ;;(start-process-shell-command "create-qr" nil command)
)

;;; ======================================================
;;;   qrtool print
;;; ------------------------------------------------------
(defun  create-qr-code-print ()
"Print qr code from org table row"
   (interactive)
   (create-qr-code-show "-c p")
)


;;; ======================================================
;;;   qrtool print from table keypresses
;;; ------------------------------------------------------
(global-set-key (kbd "C-c q") 'create-qr-code-show)
(global-set-key (kbd "C-c w") 'create-qr-code-print)



;;-----------------------------------------------------------------
;;      BEAMER TRICKS   ***
;;-----------------------------------------------------------------

 ;; (fset 'beamer-only
 ;;         (lambda (&optional arg) "Insert #+BEAMER: \\only<1-2> { ... }" (interactive "p")
 ;;           (insert "#+BEAMER: \\only<1-2> {\n#+BEAMER: }\n")))

;;; ======================================================
;;;   make beamer section
;;; ------------------------------------------------------
(defun beamer-only (start end)
  "Wrap the selected region with #+BEAMER: \\only<1> { ... }"
  (interactive "r")
  (save-excursion
    (goto-char end)
    (insert "\n#+BEAMER: }\n")
    (goto-char start)
    (insert "#+BEAMER: \\only<1> {\n")))


  (fset 'beamer-column
         (lambda (&optional arg) "Insert Beamer column properties" (interactive "p")
           (insert "** Text                                                            :B_column:\n    :PROPERTIES:\n    :BEAMER_env: column\n    :BEAMER_col: 1.0\n    :END:\n")))


;;; ======================================================
;;;   upper index latex beamer-wrap-region-upperindex
;;; ------------------------------------------------------
(defun upper-index-latex()
  "Wrap the selected region with \\null^{REGION}."
  (interactive)
  (when (use-region-p)
    (let ((start (region-beginning))
          (end (region-end)))
      (goto-char end)
      (insert "}")
      (goto-char start)
      (insert "\\null^{"))))

;;; ======================================================
;;;   make index for html1
;;; ------------------------------------------------------
(defun upper-index-html1()
  "Wrap the selected region with  \\(^{3}\text{He}\\)"
  (interactive)
  (when (use-region-p)
    (let ((start (region-beginning))
          (end (region-end)))
      (goto-char end)
      (insert "}\\text{")
      (goto-char start)
      (insert "\\(^{"))))

;;; ======================================================
;;;   make index for html2
;;; ------------------------------------------------------
(defun upper-index-html2()
  "Wrap the selected region with  \\(^{3}\text{He}\\)"
  (interactive)
;;  (when (use-region-p)
;;    (let ((start (region-beginning))
;;          (end (region-end)))
;;      (goto-char end)
      (insert "}\\)")
      ;;      (goto-char start)
      ;;      (insert "\\(^{")
      )
;;    )
;;  )
