
;;; ======================================================
;;  lua edits  and markdown mode
;;; ------------------------------------------------------
;;; -------------- Using LUA edit...
(require 'lua-mode)
(require 'markdown-mode)

(setq markdown-command "pandoc")

;;; ======================================================
;;;;;arduino --- oh yes, but remember how to use
;;; ------------------------------------------------------
(require 'arduino-mode)


;; ;;; ======================================================
;; ;;;;;emojify --- emojify :bulb:   but also :) :/ :# in orgmode
;; ;;; ------------------------------------------------------
;;  maybe responsible for slowdown
;; (require 'emojify)
;; (add-hook 'org-mode-hook 'emojify-mode)



;;; ======================================================
;;      stripes BUT IT breaks  selecting color
;;; ------------------------------------------------------
;;(require 'stripes)
;;;;; (add-hook 'org-mode-hook 'stripes-mode)


;;; ======================================================
;;  Highlight tabs - breaks emojify
;;; ------------------------------------------------------
;; shows grey bars in code-tabs???
;; --------- delays very much  when remote C++ editing
;;(require 'highlight-indent-guides)
;;(require 'highlight-indentation)
;;(add-hook 'prog-mode-hook 'highlight-indent-guides-mode)


;;; ======================================================
;;  Highlight python
;;; ------------------------------------------------------
;; (defvar font-lock-operator-face 'font-lock-operator-face)

;; (defface font-lock-operator-face
;;   '((((type tty) (class color)) nil)
;;     (((class color) (background light))
;;      (:foreground "dark red"))
;;     (t nil))
;;   "Used for operators."
;;   :group 'font-lock-faces)

;; (defvar font-lock-end-statement-face 'font-lock-end-statement-face)

;; (defface font-lock-end-statement-face
;;   '((((type tty) (class color)) nil)
;;     (((class color) (background light))
;;      (:foreground "DarkSlateBlue"))
;;     (t nil))
;;   "Used for end statement symbols."
;;   :group 'font-lock-faces)

;; (defvar font-lock-operator-keywords
;;   '(("\\([][|!.+=&/%*,<>(){}:^~-]+\\)" 1 font-lock-operator-face)
;;     (";" 0 font-lock-end-statement-face)))
;; (add-hook 'python-mode-hook
;;                   '(lambda ()
;;                      (font-lock-add-keywords nil font-lock-operator-keywords t))
;;                   t t)
;;; customize-face   font -lock-comment-face orange

;;;





;; ;;; ======================================================
;; ;;    undo-tree    Use C-x u to visualize ;
;;        C-/ undo  C-? redo;  SAME KEYS(+shift) GREAT
;; ;;; ------------------------------------------------------
(require 'undo-tree)
(global-undo-tree-mode)


;;; ======================================================
;;  m-x  minimap for python - Side map of the full code
;;; ------------------------------------------------------
(require 'minimap)





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; https://github.com/emacs-tw/awesome-emacs?tab=readme-ov-file ;;
;; crux - some utils. like  Wed 24 Jul 2024 08:47:41 CEST       ;;
;; 							        ;;
;; expand-region = abbrev.				        ;;
;; 							        ;;
;; quickrun - needs 'python' definition, too many languages x   ;;
;; 							        ;;
;; deft - create file quick x				        ;;
;; 							        ;;
;; avy - fast goto					        ;;
;; 							        ;;
;; sublimity - ??					        ;;
;; 							        ;;
;; company - complete any .... to see			        ;;
;; 							        ;;
;; lorem-ipsum - generate text +			        ;;
;;
;;
;; rainbow-delimiter-mode ... very slight colors {{{}}}
;;
;;
;; electric-pair-mode ... maybe deletes extra bracket by itself?
;;    autoinserts matching.}
;;
;; origami ... fold functions .... too slow on remote
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;; ======================================================
;;  ADDS spaces around operators
;;(electric-operator-add-rules-for-mode 'python-mode
;;  (cons "->" " -> ")
;;  (cons "=>" " => "))
;;; ------------------------------------------------------
(require 'electric-operator)
(add-hook 'python-mode-hook #'electric-operator-mode)
(add-hook 'c++-mode-hook #'electric-operator-mode)
(add-hook 'c-mode-hook #'electric-operator-mode)


;;; ======================================================
;;
;;; ------------------------------------------------------
