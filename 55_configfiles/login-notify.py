#!/usr/bin/env python3
#
#session optional pam_exec.so seteuid /usr/local/bin/login-notify.py
#~/usr/local/bin/login-notify.py~

#login-notify.sh
#https://askubuntu.com/questions/179889/how-do-i-set-up-an-email-alert-when-a-ssh-login-is-successful
#
# chmod +x login-notify.sh
# EDIT: /etc/pam.d/sshd  AND APPEND
# session optional pam_exec.so seteuid /path/to/login-notify.sh
#session optional pam_exec.so seteuid /etc/postfix/login-notify.py
#
#$PAM_USER from $PAM_RHOST on $host"
#subject="LOGIN: $PAM_USER from $PAM_RHOST on $host"
#subject="$PAM_USER@$host from $PAM_RHOST"

with open("/tmp/login-notify.log","a") as f:
    f.write("NEWLOG-----------------------------------\n")

import os
import socket
import requests


from notifator import telegram
from fire import Fire
import datetime as dt
import subprocess as sp

with open("/tmp/login-notify.log","a") as f:
    f.write("     all imported--------------------------\n")


def run_arping(IP):
    uid = os.getuid()
    CMD = f"arping -c 1 -w 1 -r {IP}"
    with open("/tmp/login-notify.log","a") as f:
        f.write(f"   {CMD} with {uid} \n")
    try:
        res = sp.check_output(CMD.split())
        res = res.decode("utf8").strip() # [x.strip() for x in res]
    except:
        res = "badIP??"
    with open("/tmp/login-notify.log","a") as f:
        f.write(f"   mac: {res} \n")
    return res

def sendmess():
    with open("/tmp/login-notify.log","a") as f:
        f.write("     start\n")

    host=socket.gethostname()
    rhost="unknown"
    user="unknown"
    #print("D... host=",host)

    with open("/tmp/login-notify.log","a") as f:
        f.write("     host\n")

    if 'PAM_RHOST' in os.environ:
        rhost=os.environ['PAM_RHOST']

    with open("/tmp/login-notify.log","a") as f:
        f.write(f"     rhost  {rhost}\n")

    if 'PAM_USER' in os.environ:
        user=os.environ['PAM_USER']

    with open("/tmp/login-notify.log","a") as f:
        f.write(f"     ruser {user}\n")

    what = ":/"
    if 'PAM_TYPE' in os.environ:
        what = os.environ['PAM_TYPE']
        # what = "logout"
        what = "\n"+f" - {what}"

    with open("/tmp/login-notify.log","a") as f:
        f.write(f"     what {what}\n")

    tty = ""
    if 'PAM_TTY'  in os.environ:
        tty = os.environ['PAM_TTY']
        tty = "\n"+f" - {tty}."

    # xtty = ":-"
    # if 'PAM_XDISPLAY'  in os.environ:
    #     xtty = os.environ['PAM_XDISPLAY']
    #     xtty = "\n"+f" - {xtty}."
    #
    with open("/tmp/login-notify.log","a") as f:
        f.write(f"     tty {tty} \n")

    mac = run_arping(rhost)
    mac = "\n"+f" - {mac}"

    time = dt.datetime.now().strftime("%a %X")

    messg = f"{user}@{host} < {rhost} {what} {mac} {tty} {time}"
    telegram.bot_send("Logins", messg)

    with open("/tmp/login-notify.log","a") as f:
        f.write("#   last record...\n")
        f.write(f"------------------{dt.datetime.now()}--")
        f.write("\n")
        f.write("\nexact messg:"+messg)
        #f.write(f"{user}@{host} < {rhost} / w={what} / t={tty}.")
        f.write("\n")
        f.write("__________________________________________________\n")


##################################################
##################################################
##################################################
##################################################
##################################################
if __name__=="__main__":
    Fire( sendmess )
