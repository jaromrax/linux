To start, authentication is needed...

like

~glab auth login~


I predefined in zshrc:

#+begin_src sh
gil
giv 2
gie 2
#+end_src


or
~glab issue view 2 -w~
