#!/usr/bin/env python3

from fire import Fire
from console import fg,fx
import subprocess as sp

ntp = """
systemctl status systemd-timesyncd

apt install ntp
sntp
systemctl status ntp

apt install ntpsec-ntpdate
ntpdig

# if limited by firewall....
apt install htpdate

crontab -e
#  5 * * * * /usr/bin/htpdate -a www.wikipedia.org

22000 ... syncthing
"""

def binput(q):
    print()
    print("_"*len(q))
    print(q)
    print("_"*len(q))
    return input("> ")


def run(cmd, name):
    print(f"{fg.yellow}     {cmd}{fg.default}")
    sp.run( cmd.split(), check=True )
    if len(name)>1:
        print(f"{fg.green}!... {name} ENABLED{fg.default}")



def main():
    print("i... UFW setup")

    print("i... UFW  RESET ALL")
    run("sudo ufw reset", "")
    print("i... BASIC SETUP")
    CMD = "sudo ufw default deny incoming"
    run(CMD, "")
    CMD = "sudo ufw default allow outgoing"
    run(CMD, "")


    print("i... ALLOW SSH")
    CMD = "sudo ufw allow ssh"
    run(CMD, "SSH")


    if binput("?... ALLOW http and https   y/n?")=='y':
        run("sudo ufw allow http", "HTTP")
        run("sudo ufw allow https", "HTTPS")



    if binput("?... ALLOW TAILSCALE0  y/n?")=='y':
        run("sudo ufw allow in on tailscale0", "TAILSCALE")


    if binput("?... ALLOW SyncThing  y/n?")=='y':
        #run("sudo ufw allow syncthing")
        run("sudo ufw allow 22000","")
        run("sudo ufw allow 21027","SYNCTHING")


    if binput("?... ALLOW WEBCAM 8000  y/n?")=='y':
        run("sudo ufw allow 8000", "WEBCAM")




    if binput("?... ALLOW grafana on 3000  y/n?")=='y':
        run("sudo ufw allow 3000", "GRAFANA")

    if binput("?... ALLOW InfluxDB on 8086  y/n?")=='y':
        run("sudo ufw allow 8086", "INFLUXDB")



    if binput("?... ALLOW SEREAD PORTS 8099,8100  y/n?")=='y':
        run("sudo ufw allow 8099", "SEREAD TCP")
        run("sudo ufw allow 8100", "SEREAD UDP")


    if binput("?... ALLOW NTP  y/n?")=='y':
        run("sudo ufw allow ntp", "NTP")


        print("PROBLEMATIC PART WITH ntp")
        print(ntp)





    print("#"*40)
    if binput("?... ENABLE UFW NOW   y/n?")=='y':
        run("sudo ufw enable", "")
        run("sudo ufw status verbose","")
        print(f"{fg.green}!... UFW ++++++++++++++++++ ENABLED{fg.default}")
    else:
        print(f"!... {fg.red} UFW ----------------- NOT ENABLED NOW{fg.default}")
        print(f"!!!!!!!!!!!1 NO FIREWALL PRESENT   !!!!!!!!!!!")




    print("myservice zmq  local 5678")
    print("? tor 9050 ")
    print("? cupsd 631 ")
    #print("influxdb local 8088")


if __name__=="__main__":
    print("https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-ubuntu-20-04")
    Fire(main)
