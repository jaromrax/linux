# Every version has its problems

- grub locks at 30 seconds timeout after reboot (November20)
  - `GRUB_RECODRFAIL_TIMEOUT` to positive values helps to continue

```
GRUB_DEFAULT=0
GRUB_TIMEOUT_STYLE=menu
#GRUB_TIMEOUT_STYLE=hidden
#GRUB_TIMEOUT_STYLE="countdown"

GRUB_TIMEOUT=3
GRUB_RECORDFAIL_TIMEOUT=5
GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`
#GRUB_CMDLINE_LINUX_DEFAULT="quiet splash nouveau.modeset=0 mem_sleep_default=deep"
GRUB_CMDLINE_LINUX_DEFAULT="nouveau.modeset=0 mem_sleep_default=deep"
#GRUB_CMDLINE_LINUX_DEFAULT=""
GRUB_CMDLINE_LINUX=""
```
