
## https://unix.stackexchange.com/questions/208047/how-do-i-transfer-installed-packages-and-settings-from-one-distro-to-another

# STORE
```
sudo apt-get install apt-clone

export ISS=`cat /etc/issue.net | sed -e 's/ //g'`
export TAG=`date +"${ISS}_%Y%m%d_%H%M%S${HOST}"`
apt-clone clone $TAG
```
# eventually --with-dpkg-repack
#Copy foo.apt-clone.tar.gz to the new machine and run



# RESTORE

```
sudo apt update
sudo apt -y upgrade
sudo apt-get install apt-clone
sudo apt-clone restore foo.apt-clone.tar.gz
```


