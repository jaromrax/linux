#!/usr/bin/python3

import os
import re
import sys
import subprocess

def get_exit( CMD ):
    print("i... running ",CMD)
    try:
        #process = subprocess.Popen(CMD, stdout=subprocess.PIPE)
        print('i...   initiating check_output/check_call  no .split  shell True')
        code = subprocess.check_call( CMD , shell=True )
        #s=process.communicate()[0]    # execute it, the output goes to the stdout
        exit_code = code    # when finished, get the exit code
    except:
        print("X... problem detected in  ... ",CMD)
        exit_code=1
    return exit_code



def apt_cache_pkgnames():
    print("i... running apt-caceh pkgnames")
    CMD="apt-cache pkgnames"
    CMD="apt list --installed"
    pkgs=[]
    ok=False
    try:
        process = subprocess.Popen(CMD.split(), stdout=subprocess.PIPE)
        s=process.communicate()[0]    # execute it, the output goes to the stdo$
        exit_code = process.returncode    # when finished, get the exit code
        #print("i... result:",s)
        pkgs=pkgs+s.rstrip().split()
        pkgs=[  i.decode("utf8") for i in pkgs ]
        pkgs=[  i.split("/")[0] for i in pkgs ]
        ok=True
    except:
        print("!... problem ",CMD)
        exit_code=1
    if not ok:quit()
    return pkgs



import getpass
username = getpass.getuser()
if username!="root":
    print("X...    root user necessary ==", username )
    quit()
#if os.getlogin()!="root":
#    print("X...    root user necessaery ==", os.getlogin() )
#    quit()
############################################
#  FILE WITH OUTPUT  i may append outputs to this
#
tmpp=subprocess.Popen('mktemp',shell=True,stdout=subprocess.PIPE)
aatmp=tmpp.communicate()[0].decode('utf-8').rstrip()
print( aatmp )






pkgs_installed=apt_cache_pkgnames()
print( "i... INSTALLED:",  len(pkgs_installed ) , "packages" ,"\n")
#quit()


###############   look at aptitude
print("i--- test aptitude")
r=get_exit( 'aptitude --version' )
print("i--- test aptitude 2")
if r!=0:
    print("now: sudo apt-get -y install aptitude")
    r=get_exit( "sudo apt-get -y install aptitude" )


print("i... aptitude update")
subprocess.check_call( "sudo aptitude update".split() )

print("i... aptitude upgrade")
subprocess.check_call( "sudo aptitude -y upgrade".split() )


################   xterm is necessary to run this script
r=get_exit( 'which xterm')  # -v not defined
if r!=0:
    print("sudo aptitude -y install xterm")
    r=get_exit( "sudo aptitude -y install xterm" )
# xterm first time here....
#subprocess.check_call( "xterm  -geometry 90x30  -e ' tail -f "+aatmp+" ' &" , shell=True )




#f=open('debian_jessie.txt','r')
f=open( sys.argv[1],'r')
apli=f.read().split('\n')
f.close()
if len(apli)<2:
    print("File with packages too short. Quitting")
    quit()

############  packages in the list:
runa=[]
for ap in apli:
    #print( apli.index(ap), re.findall('^#',ap) , len(re.findall('^#',ap) ) ,ap)
    if len(re.findall('^\s*#' ,ap))==0 and len(ap.strip())>0:
        ap=re.sub('#.+$','',ap)
        runa.append(ap.strip())
print("i... TO INSTALL: ",runa)
print("i... total {} packages  from   {} lines in file: ".format(len(runa), len(apli), sys.argv[1] ) )
#quit()

for p in runa:
    ret=-1 # already installed == my code

    print("{:22s}... ".format(p), end='')
    sys.stdout.flush()

    if not p in pkgs_installed:
        ####for p in runa[:3]:
        #tmpp=subprocess.Popen('mktemp',shell=True,stdout=subprocess.PIPE)
        #tmp=tmpp.communicate()[0].decode('utf-8').rstrip()
        ##print(tmp)
        #cmd='/usr/bin/xterm -geometry 90x30 -e \'sudo aptitude install -y '+p+';echo $? >'+tmp+'\''
        cmd="/usr/bin/xterm -geometry 120x10+2+2 -e 'sudo aptitude install -y "+p+" '"
        res=subprocess.check_call( cmd , shell=True)
        print("RES==", res, end="  ")
        ret=int(res)
        #cmd='aptitude install -y '+p
    #    ##print(cmd, end='')
        #child=subprocess.Popen(cmd, shell=True )
        #exc=child.communicate()[0]
        #f=open(tmp,'r')
        #ret=int( f.read().rstrip() )


#    print("{:22s}... ".format(p), end='')
#    sys.stdout.flush()
    if ret==255:
        print("{} (try as root)".format(ret) )
    elif ret==0:
        print("{} [OK]".format(ret) )
    elif ret==-1:
        print("{} [Prev.OK]".format(ret) )
    else:
        print(ret)
    #print(  child.returncode )
###r=os.popen('xterm -e "aptitude install -y '+p+'"')

