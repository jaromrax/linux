# Voila renders jupyter

## install nodejs


see https://github.com/nodejs/help/wiki/Installation

Get `wget https://nodejs.org/dist/v10.16.3/node-v10.16.3-linux-x64.tar.xz`

check with `sha256sum` and get  `d2271fd8cf997fa7447d638dfa92749ff18ca4b0d796bf89f2a82bf7800d5506  node-v10.16.3-linux-x64.tar.xz`






```
VERSION=v10.16.3
DISTRO=linux-x64
sudo mkdir -p /usr/local/lib/nodejs
sudo tar -xJvf node-$VERSION-$DISTRO.tar.xz -C /usr/local/lib/nodejs 
 ```
 append to `~/.profile`
``` 
# Nodejs
VERSION=v10.16.3
DISTRO=linux-x64
export PATH=/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin:$PATH
```

refresh - reload profile and make links `. ~/.profile`

```
sudo ln -s /usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin/node /usr/bin/node
sudo ln -s /usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin/npm /usr/bin/npm
sudo ln -s /usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin/npx /usr/bin/npx
```

## install jupyterlab, voila   and render with voila

see https://github.com/jupyterlab/jupyterlab

`workon test` and `pip install jupyterlab`

Go to the good environment and `pip install voila`

and  `jupyter labextension install @jupyter-voila/jupyterlab-preview`

Use `jupyter-lab` for the preview extension (NOT enable extension manager first, than at left bar BUT view/voila)
and to  render use `voila ntb.ipynb`

## using input ipywidgets

```
#  jupyter nbextension enable --py widgetsnbextension   #  for jupyter?
jupyter labextension install @jupyter-widgets/jupyterlab-manager
jupyter-lab  # let us see

```
