#!/usr/bin/python3
import os
from subprocess import Popen, PIPE
import datetime
import argparse
#####from progressbar import ProgressBar###NOT IMPLICIT

INIFILE="ubuntu1604.txt"

parser = argparse.ArgumentParser()
parser.add_argument('seznam', action="store")
args=parser.parse_args()
print( 'Using list of programs ==',args.seznam )



# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total: 
        print()

def candy( prg ,maxlen , n , nmax ):
    now=datetime.datetime.now().strftime("%H:%M:%S").rstrip()
#    print("=================================={:3d}/{:3d}".format(n,nmax) )
    print("===== {:{:d}s} =====     [{:s}] .".format(prg,int(maxlen),now) )
#    print("==================================")
    return datetime.datetime.now()

def candy2( prg ,maxlen, starttime):
    now=datetime.datetime.now().strftime("%H:%M:%S")

#    print("==================================")
    print("----- {:{:d}s} OK---     [{:s}]  {} sec.\n".format("    OK",int(maxlen),now, datetime.datetime.now()-starttime) )
#    print("==================================")
#    return datetime.datetime.now()

def candyerror( prg,maxlen, starttime ):
    now=datetime.datetime.now().strftime("%H:%M:%S")
#    print("==================================")
    print("!!!!! {:{:d}s}   ERR     [{:s}]  {} sec.".format("    ERROR",int(maxlen),now, datetime.datetime.now()-starttime) )
#    print("==================================")
#    return datetime.datetime.now()



with open( INIFILE ) as f:
    prgl=f.readlines()
prgl2=[]
maxlen=1
for e in prgl:
    e=e.strip()
    x=e.find("#")
    if x>0: e=e[:x]
    if x!=0:
        prgl2.append(e)
        if len(e)>maxlen: maxlen=len(e)
TOTAL=len(prgl2)
print(prgl2, TOTAL,'elements  maximum len', maxlen)


#process=Popen('echo ahoj vole'.split(), stdout=PIPE, stderr=PIPE)
#stdout,stderr=process.communicate()
#print(stdout.decode('utf') )

###################################
# MAIN LOOP

errors=[]
tota=0
printProgressBar(tota, TOTAL, prefix = 'Progress:', suffix = 'Complete', length = 50)
for e in prgl2:
    tota=tota+1
    starttime=candy( e ,maxlen , tota, TOTAL )
#    CMD='aptitude -y install '+e
# wine means crash
    CMD='xterm -e apt-get -y install '+e
    process=Popen( CMD.split(), stdout=PIPE, stderr=PIPE)
    stdout,stderr=process.communicate()
    res=process.returncode
    if res!=0:
        candyerror( e,maxlen,starttime)
        errors.append(e)
        print(stdout.decode('utf') )
    else:
        #print(stderr.decode('utf') )
        candy2( e,maxlen, starttime )
    printProgressBar(tota, TOTAL, prefix = 'Progress:', suffix = 'Complete', length = 50)
print('\n\nLIST OF ERRORS:\n', errors,'\n')
