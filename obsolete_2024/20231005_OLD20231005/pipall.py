#!/usr/bin/env python3

import sys
import subprocess as sp
import os
import pathlib
import time

#================= import packages hard way=====================
import_list0=['fire', 'alive_progress']

#------flashcam needs this. At least.
# imutils need numpy
import_list=['gunicorn', 'flask','flask_httpauth','numpy','opencv-python','imutils']

# myservice
import_list=['stickytape','logzero','blessings','terminaltables','colorclass','gevent']

# linux
#   influx  sshc
import_list=['influxdb']


#====================================================== NOW I WANT TO JOIN ALL here


import_list=['gunicorn', 'flask','flask_httpauth','numpy','opencv-python','imutils', 'pantilthat','imagezmq',
             'stickytape','logzero','blessings','terminaltables','colorclass','gevent','zmq',
             'fire','flask','influxdb','pyserial_asyncio','threading','pantilthat',
             'qrcode','ansicolors','brother_ql',
             'notifator', "sympy", "scipy", "pandas", "matplotlib", "h5py", "twine", "bumpversion",
             "virtualenvwrapper",
             "bpytop","httpie",
             "speedtest",
             #"youtube-dl",
             "ipython"
 ]


#==========================================install check_call======
def install(package):
    # sp.check_call([sys.executable, "-m", "pip", "install", package])
    sp.check_call([ "pip3", "install", package])

#==========================================install import======
def install_and_import(packagelist):
    import importlib
    importnames={}
    for i in packagelist:
        importnames[i]=i
    # BUT some packages have different names
    importnames['opencv-python']="cv2"
    importnames['pyserial_asyncio']="serial_asyncio"
    importnames['ansicolors']="colors"
    # importnames['ipython']="ipython3"

    for i in packagelist:
        try:
            importlib.import_module( importnames[i] )
        except ImportError:
            install(i)
        finally:
            print("I... importing",i) # check if fire is imported
            globals()[i] = importlib.import_module( importnames[i] )
            print("I... imported:",i)


#
#====================================== imports after install
#
print("================ import fire and alive_progress=============")
install_and_import( import_list0 )  # fire and alive
print("=== fire and alive_progress SHOULD BE IMPORTED =============")

# doensnt import!!!!!
from alive_progress import alive_bar,config_handler
from fire import Fire


#======================================================NORMAL CODE
#
#======================================================NORMAL CODE




#====================================================== RUN CODE
def launch(CMD, fake=False):
    print("D... launchng", CMD)
    if not fake:
        res=sp.check_output( CMD.split() ).decode("utf8").rstrip()
        #print("D...",res)
        return res




#======================================================main
def main():


    print("================ import the rest ===========================")
    print( import_list )
    ll=len(import_list)
    config_handler.set_global( length=24) # 25 was OK
#    config_handler.set_global( length=30) # 25 was OK
    with alive_bar( ll ) as bar:   # declare your expected total
        for i in range( ll ):
#            if i%10==0:
            print("D... i",i, import_list[i] )
            bar.text( import_list[i] )
            install_and_import( [import_list[i]] )
            time.sleep(0.2)





#========================================================MAIN======
#========================================================MAIN======
#========================================================MAIN======
if __name__=="__main__":
    Fire( main )
