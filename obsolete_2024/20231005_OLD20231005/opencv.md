Just a temporary record..
-----------------------------


**TEST ON UBUNTU** - I have done `git clone https://github.com/opencv/opencv`

```
aptitude install libjpeg8-dev libtiff4-dev libjasper-dev libpng12-dev libavcodec-dev libavformat-dev libswscale-dev libv4l-dev


aptitude install libgtk2.0-dev

aptitude  install libatlas-base-dev gfortran`
``

Then, I try cmake in `opencv.build`:

NOT THIS....
```
 cmake -D CMAKE_BUILD_TYPE=RELEASE \          
    -D CMAKE_INSTALL_PREFIX=/usr/local \
    -D INSTALL_C_EXAMPLES=ON \
    -D INSTALL_PYTHON_EXAMPLES=ON \
    -D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib/modules \
    -D BUILD_EXAMPLES=ON  ../opencv
```


BUT THIS...  **TEST ON UBUNTU**
```
 cmake -D CMAKE_BUILD_TYPE=RELEASE \   
    -D CMAKE_INSTALL_PREFIX=/usr/local \
   -D PYTHON_DEFAULT_EXECUTABLE=/usr/bin/python3 \
   -D PYTHON_INCLUDE_DIRS=/usr/include/python3.5 \ 
   -D PYTHON_LIBRARY=/usr/lib/python3.5   ../opencv
```

and simply
```
make -j4
sudo make install
sudo ldconfig
````

but in **TEST ON UBUNTU** i tried
```
cmake --build .
sudo cmake --build . --target install
```




```
pip3 install numpy
aptitude install python-opencv
```




On Raspberry
=============
http://www.pyimagesearch.com/2016/04/18/install-guide-raspberry-pi-3-raspbian-jessie-opencv-3/

Prerequisites;
```
 aptitude install build-essential git cmake pkg-config
 aptitude install libjpeg-dev libtiff5-dev libjasper-dev libpng12-dev
 aptitude install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
 aptitude  install libxvidcore-dev libx264-dev
 aptitude  install libgtk2.0-dev
 aptitude install libatlas-base-dev gfortran
 aptitude install python3-dev
```

Download:

```
wget -O opencv32.zip https://github.com/Itseez/opencv/archive/3.2.0.zip
unzip opencv32.zip
sudo pip3 install numpy
wget -O opencv_contrib.zip https://github.com/Itseez/opencv_contrib/archive/3.2.0.zip
 unzip opencv_contrib.zip

mkdir opencv.build
cd opencv.build
```

compilation:

```
 cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local  -D INSTALL_PYTHON_EXAMPLES=ON   -D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib-3.2.0/modules -D BUILD_EXAMPLES=ON -D PYTHON_DEFAULT_EXECUTABLE=/usr/bin/python3  -D PYTHON_INCLUDE_DIRS=/usr/include/python3.4 -D PYTHON_LIBRARY=/usr/lib/python3.4     ../opencv-3.2.0 
```
 
 Without modules, it fits to 1GB:
 
 ```
  cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local    -D BUILD_EXAMPLES=ON -D PYTHON_DEFAULT_EXECUTABLE=/usr/bin/python3  -D PYTHON_INCLUDE_DIRS=/usr/include/python3.4 -D PYTHON_LIBRARY=/usr/lib/python3.4     ../opencv-3.2.0

 make -j4
 sudo make install
 sudo ldconfig

```



Cleaning space
-------------------
```
 sudo apt-get purge libreoffice libreoffice-base libreoffice-base-core libreoffice-base-drivers libreoffice-calc libreoffice-common libreoffice-core
sudo apt-get purge minecraft-pi
sudo aptitude purge wolfram-engine
sudo apt-get purge sonic-pi

sudo apt-get clean
sudo apt-get autoremove
```



SCRIPT FROM GITHUB
====================

```
######################################
# INSTALL OPENCV ON UBUNTU OR DEBIAN #
######################################

# |         THIS SCRIPT IS TESTED CORRECTLY ON         |
# |----------------------------------------------------|
# | OS             | OpenCV       | Test | Last test   |
# |----------------|--------------|------|-------------|
# | Ubuntu 16.04.2 | OpenCV 3.2.0 | OK   | 20 May 2017 |
# | Debian 8.8     | OpenCV 3.2.0 | OK   | 20 May 2017 |
# | Debian 9.0     | OpenCV 3.2.0 | OK   | 25 Jun 2017 |

# 1. KEEP UBUNTU OR DEBIAN UP TO DATE

aptitude -y update
aptitude  -y upgrade
aptitude  -y dist-upgrade
aptitude  -y autoremove


# 2. INSTALL THE DEPENDENCIES

# Build tools:
aptitude install -y build-essential cmake

# GUI (if you want to use GTK instead of Qt, replace 'qt5-default' with 'libgtkglext1-dev' and remove '-DWITH_QT=ON' option in CMake):

aptitude install -y qt5-default libvtk6-dev

# Media I/O:
aptitude install -y zlib1g-dev libjpeg-dev libwebp-dev libpng-dev libtiff5-dev  libopenexr-dev libgdal-dev

aptitude install -y libjasper-dev  # not found on stretch

# Video I/O:
aptitude  install -y libdc1394-22-dev libavcodec-dev libavformat-dev libswscale-dev libtheora-dev libvorbis-dev libxvidcore-dev libx264-dev yasm libopencore-amrnb-dev libopencore-amrwb-dev libv4l-dev libxine2-dev

# Parallelism and linear algebra libraries:
aptitude  install -y libtbb-dev libeigen3-dev

# Python:
aptitude  install -y python-dev python-tk python-numpy python3-dev python3-tk python3-numpy

# Java:
aptitude  install -y ant default-jdk

# Documentation:
aptitude  install -y doxygen


# 3. INSTALL THE LIBRARY (YOU CAN CHANGE '3.2.0' FOR THE LAST STABLE VERSION)

aptitude  install -y unzip wget

VERSION=3.4.0.tar.gz
wget https://github.com/opencv/opencv/archive/$VERSION
tar -xvzf $VERSION
mv opencv-3.4.0 OpenCV

#wget https://github.com/opencv/opencv/archive/3.2.0.zip
#unzip 3.2.0.zip
#rm 3.2.0.zip
#mv opencv-3.2.0 OpenCV


cd OpenCV
mkdir build
cd build
cmake -DWITH_QT=ON -DWITH_OPENGL=ON -DFORCE_VTK=ON -DWITH_TBB=ON -DWITH_GDAL=ON -DWITH_XINE=ON -DBUILD_EXAMPLES=ON -DENABLE_PRECOMPILED_HEADERS=OFF ..
make -j4
 make install
 ldconfig


# 4. EXECUTE SOME OPENCV EXAMPLES AND COMPILE A DEMONSTRATION

# To complete this step, please visit 'http://milq.github.io/install-opencv-ubuntu-debian'.
```