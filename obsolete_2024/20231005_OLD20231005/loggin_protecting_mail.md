Local Mail server - quite a problem on ubuntu!
--------------------

*p34, edie - long response of mutt,  edie: hostname=fjfi, no psad, no rk, no arpw*

*gigajm - good psad,*

install mutt as a local mail client `aptitude install  mutt` 

 * exim4 (raspberry) - `aptitude install mail mutt` 
 
 * postfix (ubuntu 1604) - `aptitude install mailutils` I did also  `sudo dpkg-reconfigure postfix` 
 
 
And after `echo ahoj | mail $USER` creates the mailbox. `mutt` can see this mail on both systems.


To get system  mails as user:
---------------------

Probably you have a postfix `systemctl restart postfix` AFTER:
`nano /etc/aliases` and putting `root: user@localhost` and `postmaster: user@localhost`  inside. After run command `newaliases`


Probably you have a postfix `systemctl restart postfix` AFTER


If necessary to send emails via mutt,
----------------------
change `.muttrc` or  `/etc/Muttrc`:
```
set from = "anyaddress@anydomain.com"
set hostname = hostname.com
set envelope_from = yes
set timeout=20
```

```
mutt -F /home/user/.muttrc  -s "SUBJECT"  any@address.com -a /home/user/attachedfile.txt < /home/user/bodyofmessage.txt
```





Programs that should send logs:
-----------------------------

* psad - port scan detection (needs iptables)
 No iptables LOG, no mails. Put in `rc.local`
 ```
 iptables -A INPUT -j LOG
 iptables -A FORWARD -j LOG
 ```
   - *it overwhelms the logs*:
   - i tried in `psad.conf`:
             - `SYSLOG_DAEMON  rsyslog;`
             - `ENABLE_SYSLOG_FILE  N;`


* rkhunter - *maybe needs to set `REPORT_EMAIL=root@localhost` OR: `MAIL-ON-WARNING=root` (probably in cron)
     I have put `MAIL-ON-WARNING=root` into `rkhunter.conf`

* **denyhosts** - seamless (as daemon in ubu)

	- i tried `DENY_THRESHOLD_INVALID = 2`
	- `DENY_THRESHOLD_VALID = 4`

* arpwatch - seamless# arpw as service (as daemon in ubu)
   - put names into  /var/lib/arpwatch/arp.dat
     *worked on fedo nicely*

* mdadm - if installed and used

* # tmpfs for SSD disks

*  fstrim in cron (weekly, it should already be)  and and smartmon as serviece

*  smartmontools - smartctld - seamless, running as daemon (ubu) `system smartd status`

