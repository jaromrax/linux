linux install
============

Ubuntu
============

Install
------------

1/ Create ubuntu installation usb

2/ use probably uefi mode, no parallel windows boot after though

3/ root partition with 28GB is ok, salt is there, mov 20GB used, 32GB even better, motion is there, 18GB used



Postinstall 1
--------------------

Prepend to the use `.bashrc` this:

```
if [ `whoami` = "root" ]; then
    export HOME=/root
#    /root/.bashrc
#    exit
fi
```



```
apt-get update
apt-get upgrade
apt-get install aptitude
aptitude install git
```

git clone this repo to your disk

**mysql:**

`mysql -u root`

```
CREATE USER 'myname'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON * . * TO 'myname'@'localhost';
```

`mysql -u myname -p`

```
create database test;
```



Postinstall 2
--------------------

Install `aptitude install python3-pip`


Postinstall 3
--------------------



Oh my zsh
-------------
 Install ```apt install zsh``` and continued from http://ohmyz.sh/

 sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"


` cp tjkirch_mod2.zsh-theme ~/.oh-my-zsh/themes/`

It is necessary to insert `ZSH_THEME="tjkirch_mod2"`
```
sed  -i "s/^\(ZSH_THEME *= *\).*/\1\"tjkirch_mod2\"/" ~/.zshrc
```

3/ copy `cp .zsh to ~/`

4/ `emacs:` copy `cp .emacs  ~/.emacs` and install markdown: `M-x package-install RET markdown-mode RET`

5/ theme like random muse tjkirch_mod  jnrowe  tonotdo

6/ plugins=(git cloudapp web-search catimg debian)

JUPYTER  -  in python3
----------

1/  `pip3 install jupyter`  however on debian-jessie I had a problem

4/ jupyterlab  ... https://github.com/jupyterlab/jupyterlab


this should work on port 8888,  Try also ``` jupyter lab```

5/

Try also this ?  https://github.com/ipython-contrib/jupyter_contrib_nbextensions

if this works.... https://github.com/quantopian/qgrid

Publish on  http://nbviewer.jupyter.org/

habits https://www.dataquest.io/blog/jupyter-notebook-tips-tricks-shortcuts/


6/ `pip3 uninstall terminado` - if you dont want terminal in jupyter

## Hardening

### /etc/ssh/sshd_config
```
PermitRootLogin no
PasswordAuthentication no
Port 22
Banner /etc/warning.txt
systemctl restart sshd
```

https://www.computerworld.com/article/3144985/linux/linux-hardening-a-15-step-checklist-for-a-secure-linux-server.html

### UFW

- `ufw default deny incoming`
- `ufw default allow outgoing`
- `ufw allow ssh`
- `ufw allow from 192.168.1.162 to any port 22`

https://www.techrepublic.com/article/getting-started-with-uncomplicated-firewall/


See https://www.cyberciti.biz/tips/linux-security.html for 40 tips.
