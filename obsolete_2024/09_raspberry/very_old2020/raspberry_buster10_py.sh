#!/bin/bash
#
echo THIS ENABLES TO RUN MYSERVICE2
#
sudo -H pip3 install stickytape
sudo -H pip3 install pyzmq
# myservice
sudo -H pip3 install logzero
sudo -H pip3 install blessings
sudo -H pip3 install colorclass
sudo -H pip3 install terminaltables
sudo -H pip3 install gevent
sudo -H pip3 install greenlet
# pix2
sudo -H pip3 install sensehat
# more
sudo -H pip3 install flask
sudo -H pip3 install gunicorn
sudo -H pip3 install click
sudo -H pip3 install itsdangerous
sudo -H pip3 install Jinja2
sudo -H pip3 install MarkupSafe
sudo -H pip3 install picamera
sudo -H pip3 install Werkzeug
sudo -H pip3 install flask_httpauth
sudo -H pip3 install zconfig
#-------
sudo -H pip3 install ipython
sudo -H pip3 install numpy
sudo -H pip3 install pandas
sudo -H pip3 install parso
sudo -H pip3 install pygments
#----------
sudo -H pip3 install notifator

