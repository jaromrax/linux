#### BASIC ####
       mc
       htop
       traceroute
       nmap
       screen
       git
       synergy
       ntp
#       ntpdate # problem in 1604 with ntp
#NO       ntpdoc
       parted
	   gparted
       emacs
#### VIDEO #####
       mpv
# not in ubutnu    chkconfig replace:
###       sysvrcconf
#       mencoder
#       mplayer
###       mplayer2
###       ffmpeg # not in trusty
       enca
       lame
       handbrake
       handbrake-cli
       openshot
       blender
# no avidemux    avidemux
#no luvcview       luvcview
       guvcview

#NO       v4lutils
# #   to play av formats
#NO       ubunturestrictedextras
#NO       libavcodecextra

########## SSH LIKE ######
       openssh-client
      openssh-server
       sshfs
       autossh
       curl
       ## curl and wget already in
###### PICTURES ######	
       gimp
       geeqie
	   jpegoptim
       imagemagick
       inkscape
# #  ffmpegmt # not in trusty
       libimlib2
#NO       libimlib2dev
       roxterm
       terminator
#NO       unisongtk
       kdiff3
	   unetbootin
	   chkrootkit
       links
       x11vnc
       x2x
       tightvncserver
       wine
	   virtualbox
	   unity-scope-virtualbox
	   virtualbox-qt
	   virtualbox-dkms
       fuseiso
       k3b
       netcat
#NO       netcat6
       lsof
       pbzip2
       unrar
       powertop
#       # everpad # not in trusty ....
       flashplugininstaller
       texlive
       texlive-full   # +2GB
#NO       compizconfigsettingsmanager
#NO       unitytweaktool
# #   openJRE...most probably needed
#NO       icedtea7plugin
#NO       openjdk7jre
#aptubuntutrustyremove:
#  pkg.removed:
#     pkgs:
#       unitylensshopping
       mysql-client
       mysql-workbench
       sqlite3
	   
### funny:
      toilet
#NO       lmsensors
#NO       indicatormultiload
       hddtemp
       psensor
       xbindkeys
       pv
       rlwrap
################
   mysql-server 
   # run   mysql_secure_installation
   ######## FOR GOOGLECHROME
   libappindicator1
######## root prerequisites ##########
 dpkg-cross
 libxext-dev
 libxpm-dev 
 libxft-dev
 cmake
 gfortran
 libc6-dev-i386
 libncurses-dev   # for VME tes
 libzmq5-dev   #  zmq in gregory
##### security
  rkhunter
  mutt
  ### send first email  echo ahoj | mail ouser
  ###  /etc/aliases  change from root to ouser --i must check
  smartmontools
  ####  fstrim in cron  and and smartmon as serviece
  ### tmpfs for SSD disks
  arpwatch
  ## arpw as service
  denyhosts
##### LATELY
 zsh 
 python-pip
 python-tk
 python-imaging-tk
 python-pi.imagetk
 python3-pip
 python3-tk
 python3-imaging-tk
 python3-pi.imagetk
 xvfb
 viking
 gpsbabel
 mdadm
 gtk-recordmydesktop
 xbacklight
 libusb-1.0-0-dev  ## phidgets need to compile with
 ######
 x2x
 xdotool
 ###
 nfs-common    # NFS client in ubuntu
#########
markdown  # for emacs
auctex    # for emacs
