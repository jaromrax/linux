git
mc
htop
terminator
#-terminator ON: 20200616
iotop
iftop
traceroute
nmap 
screen
# NOT synergy
ntp
#--------------
parted 
gparted
emacs
#----- media  youtube-dl goes with mpv; ffmpeg IS
mpv
audacity
enca
lame
handbrake
handbrake-cli
openshot
guvcview
#------------ ssh
openssh-client
openssh-server
sshfs
autossh
clusterssh
curl
#-------------- PICS
gimp
geeqie
jpegoptim
imagemagick
inkscape
libimlib2
#-------------- unison? 
kdiff3
#-------------- kits
chkrootkit
links
x11vnc
x2x
tightvncserver
wine64
virtualbox
fuseiso
#----------- useless k3b
netcat
lsof
powertop
nmon
dfc
ncdu
encfs  # asks for INTERACTION !
whois
rename
#----------- zips
pbzip2
pigz
pixz
unrar
lbzip2
zstd
p7zip-full
#---------------
pandoc
texlive
texlive-xetex
auctex # FOR EMACS
#----------------- ?influx
mysql-client
mysql-server
#mysql-workbench not in 2004
sqlite3
mongodb
mongodb-server
mongodb-clients
#----------------- funny
toilet
hddtemp
psensor
xbindkeys
pv
rlwrap
#------------------for googlechrome
libappindicator1
#----------------- root
dpkg-cross
libxext-dev
libxpm-dev
libxft-dev
cmake
gfortran
libc6-dev-i386
libncurses-dev
libzmq5-dev
#-----------------
smartmontools
arpwatch
#denyhosts  NOT in 2004
zsh
python3-pip
python3-tk
xvfb
viking
gpsbabel
#gtk-recordmydesktop NOT in 2004
xbacklight
libusb-1.0-0-dev  #phidgets to compile?
xdotool
nfs-common
gnome-shell-extensions
#----------------------- pcloud -------
libboost-system-dev
libboost-program-options-dev
libpthread-stubs0-dev
libudev-dev
#----------------------- telldus
libconfuse-common
libconfuse-dev
build-essential
libftdi1
libftdi-dev
#------------------------ opencv
libjpeg-dev
libpng-dev
libtiff-dev
libavcodec-dev
libavformat-dev
libswscale-dev
libv4l-dev
libxvidcore-dev
libx264-dev
libatlas-base-dev
#------------ qemu kvm
qemu-kvm
libvirt-clients
libvirt-daemon-system
bridge-utils
virt-manager
#---------------
elpa-markdown-mode
rkhunter
# mutt needs to respond question
mutt # respond question !
#NOT fbi caca-utils feh
apt-clone
cstocs
#monad !NOT IN 2004 !
#inotifywait NOT IN 2004
inotify-tools


















