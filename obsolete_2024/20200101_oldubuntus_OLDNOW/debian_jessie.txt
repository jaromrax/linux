#### BASIC ####
       mc
       htop
       traceroute
       nmap
       screen
       git
       synergy
       ntp
       ntpdate
#NO       ntpdoc
       parted
	   gparted
       emacs
#### VIDEO #####
       mpv
# not in ubutnu    chkconfig replace:
###       sysvrcconf
       mencoder  # ubuntu has it and needs
#       mplayer
###       mplayer2
###       ffmpeg # not in trusty
       enca
       lame
       handbrake
       handbrakecli
       openshot
       blender
# no avidemux    avidemux
#no luvcview       luvcview
       guvcview
       motion
#NO       v4lutils
# #   to play av formats
#NO       ubunturestrictedextras
#NO       libavcodecextra

########## SSH LIKE ######
       opensshclient
#no       opensshserver
       sshfs
       autossh
   
###### PICTURES ######	
       gimp
       geeqie
	   jpegoptim
       imagemagick
       inkscape
# #  ffmpegmt # not in trusty
       libimlib2
#NO       libimlib2dev
       roxterm
#NO       unisongtk
       kdiff3
	   unetbootin
	   chkrootkit
       links
       x11vnc
       tightvncserver
       wine
	   virtualbox
	   unity-scope-virtualbox
	   virtualbox-qt
	   virtualbox-dkms
       fuseiso
       k3b
       netcat
#NO       netcat6
       lsof
       pbzip2
       unrar
       powertop
#       # everpad # not in trusty ....
       flashplugininstaller
       texlive
#NO       compizconfigsettingsmanager
#NO       unitytweaktool
# #   openJRE...most probably needed
#NO       icedtea7plugin
#NO       openjdk7jre
#aptubuntutrustyremove:
#  pkg.removed:
#     pkgs:
#       unitylensshopping
       mysql-client
       mysql-workbench
       sqlite3
	   
### funny:
      toilet
#NO       lmsensors
#NO       indicatormultiload
       hddtemp
       psensor
       xbindkeys
       pv
################
   mysql-server 
   # run   mysql_secure_installation
   ######## FOR GOOGLECHROME
   libappindicator1
######## root prerequisites ##########
 dpkg-cross
 libxext-dev
 libxpm-dev 
 libxft-dev
 cmake
 gfortran
 libc6-dev-i386
#
 zsh
 curl   
 terminator

