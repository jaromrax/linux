#### BASIC ####
# encfs and rkhunter? postfix need INPUT
       mc
       htop
       iotop
       iftop
       traceroute
       nmap
       screen
       git
       synergy
       ntp
#       ntpdate # problem in 1604 with ntp
#NO       ntpdoc
       parted
	   gparted
       emacs
#### VIDEO #####
       mpv
       audacity
# not in ubutnu    chkconfig replace:
###       sysvrcconf
#       mencoder
#       mplayer
###       mplayer2
###       ffmpeg # not in trusty
       enca
       lame
       handbrake
       handbrake-cli
       openshot
       blender
# no avidemux    avidemux
#no luvcview       luvcview
       guvcview

#NO       v4lutils
# #   to play av formats
#NO       ubunturestrictedextras
#NO       libavcodecextra

########## SSH LIKE ######
      # encfs   # move to end
       openssh-client
      openssh-server
       sshfs
       autossh
       curl
       ## curl and wget already in
###### PICTURES ######
       gimp
       geeqie
	   jpegoptim
       imagemagick
       inkscape
# #  ffmpegmt # not in trusty
       libimlib2
#NO       libimlib2dev
#       roxterm # NO in BIONIC
       terminator
#NO       unisongtk
       kdiff3
#	   unetbootin #NO in BIONIC
	   chkrootkit
       links
       x11vnc
       x2x
       tightvncserver
       wine64
	   virtualbox
	   unity-scope-virtualbox
	   virtualbox-qt
	   virtualbox-dkms
       fuseiso
       k3b
       netcat
#NO       netcat6
       lsof
       pbzip2
       pigz
       pxz
       unrar
       powertop
#       # everpad # not in trusty ....
#       flashplugininstaller#### not in 1804
       texlive
       texlive-xetex
       auctex # FOR EMACS
       # beamer with pgfcore works etc...
#NO       compizconfigsettingsmanager
#NO       unitytweaktool
# #   openJRE...most probably needed
#NO       icedtea7plugin
#NO       openjdk7jre
#aptubuntutrustyremove:
#  pkg.removed:
#     pkgs:
#       unitylensshopping
       mysql-client
       mysql-workbench
       sqlite3

### funny:
      toilet
#NO       lmsensors
#NO       indicatormultiload
       hddtemp
       psensor
       xbindkeys
       pv
       rlwrap
################
   mysql-server
   # run   mysql_secure_installation
   ######## FOR GOOGLECHROME
   libappindicator1
######## root prerequisites ##########
 dpkg-cross
 libxext-dev
 libxpm-dev
 libxft-dev
 cmake
 gfortran
 libc6-dev-i386
 libncurses-dev   # for VME tes
 libzmq5-dev   #  zmq in gregory
##### security
  # rkhunter # moved to end
  ### send first email  echo ahoj | mail ouser
  ###  /etc/aliases  change from root to ouser --i must check
  smartmontools
  ####  fstrim in cron  and and smartmon as serviece
  ### tmpfs for SSD disks
  arpwatch
  ## arpw as service
  denyhosts
##### LATELY
 zsh
 python-pip
 python-tk
 python-imaging-tk
# python-pi.imagetk ### not i 1804
 python3-pip
 python3-tk
# python3-imaging-tk ### not in 1804
# python3-pi.imagetk ### not in 1804
 xvfb
 viking
 gpsbabel
 mdadm
 gtk-recordmydesktop
 xbacklight
 libusb-1.0-0-dev  ## phidgets need to compile with
 ######
 x2x
 xdotool
 ###
 nfs-common    # NFS client in ubuntu ; mount.nfs
 #  now - gnome in 1804
 gnome-shell-extensions
# pcloud
libboost-system-dev
libboost-program-options-dev
libpthread-stubs0-dev
 libudev-dev
# telldus
 libconfuse-common
 libconfuse-dev
 build-essential
 libftdi1
 libftdi-dev
 # mongodb
 mongodb
 mongodb-server
 mongodb-clients
#################################
#   ctrl-shif taken  by tweaks / keyb / additional layout/ switching to another
# https://askubuntu.com/questions/1029588/18-04-ctrlshift-to-change-language
#
#   terminator as default sudo update-alternatives --config x-terminal-emulator
#
#https://mytechnicalthoughts.wordpress.com/2018/05/05/how-to-change-default-terminal-emulator-in-ubuntu-18-04/
#
# opencv:
 libjpeg-dev
 libpng-dev
 libtiff-dev
 libavcodec-dev
 libavformat-dev
 libswscale-dev
 libv4l-dev
 libxvidcore-dev
 libx264-dev
 libatlas-base-dev
 gfortran
#=================
#  qemu kvm
 qemu-kvm
 libvirt-clients
 libvirt-daemon-system
 bridge-utils virt-manager
#
pandoc
#============== utilities for
rename
whois
#==================== ZIP SUITE
#
p7zip-full
zstd
#pigz #is here, pxz,pbzip2 also
lbzip2
#========= emacs markdown
elpa-markdown-mode
#=======terminal tools
dfc
ncdu
#============ AT END...NEED INTERACTION
encfs
rkhunter
# mutt needs to respond question
  mutt
#---------- last things
#### framebuffers ... tiv is the best however from github
#fbi
#caca-utils
#feh
#-- disk nice monitoring
nmon
#---------- seems nicer way to install all than classics
apt-clone
# encoding
cstocs
#------------- seemed great, finally, I used python matlab
#plantUML
#---------------- indispensable to vnc one app
monad
#sysbench
#------------ check the file state, launch compilation
inotifywait
inotify-tools
