#!/bin/bash

function say(){
    echo "####===> " $1
    sleep 1
}

echo "================== trying to install OPENCV ========="
echo "================== trying to install OPENCV ========="
echo "================== trying to install OPENCV ========="


echo "================== virtual environment first ========="
echo "================== virtual environment first ========="
echo "================== virtual environment first ========="

VEN="cv3"
#this doesnt work......
#source $HOME/.zshrc

echo loading  virtualenvwrapper.sh
VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
source  `which virtualenvwrapper.sh`




say "VIRTUAL ENVIRONMENT - create or workon"

workon $VEN
if [ "$?" != "0" ]; then
    mkvirtualenv $VEN
    workon $VEN
fi
say " WAS cv3 CREATED ? ? ? ? - sleep 6... "
sleep 6

#https://www.pyimagesearch.com/2018/05/28/ubuntu-18-04-how-to-install-opencv/

say "UPDATE"

sudo apt-get update
sudo apt-get upgrade


say "INSTALL PRE REQUISITES"

sudo apt-get install build-essential cmake unzip pkg-config

sudo apt-get install libjpeg-dev libpng-dev libtiff-dev

sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
 sudo apt-get install libxvidcore-dev libx264-dev


  sudo apt-get install libgtk-3-dev
sudo apt-get install libatlas-base-dev gfortran
sudo apt-get install python3-dev

echo "========================== GOING TO BIULD =============="
echo "========================== GOING TO BIULD =============="
echo "========================== GOING TO BIULD =============="

say "CREATE DIRS 3rd_parties"

mkdir  $HOME/02_GIT
mkdir  $HOME/02_GIT/3rd_parties
cd     $HOME/02_GIT/3rd_parties



echo "=================== GET AND UNZIP ======================"
echo "=================== GET AND UNZIP ======================"
echo "=================== GET AND UNZIP ======================"

say "DOWNLOAD 344"

wget -c -O opencv-3.4.4.zip https://github.com/opencv/opencv/archive/3.4.4.zip
wget -c -O opencv_contrib-3.4.4.zip https://github.com/opencv/opencv_contrib/archive/3.4.4.zip

say "UNZIP"

unzip -n opencv-3.4.4.zip
unzip -n opencv_contrib-3.4.4.zip

say "NUMPY"

pip install numpy


#### cd ~/opencv
cd  $HOME/02_GIT/3rd_parties/opencv-3.4.4
#$ mv opencv_contrib-3.4.4 opencv_contrib
mkdir build
rm -rf build/*
cd build

say "cmake "

cmake -D CMAKE_BUILD_TYPE=RELEASE \
	-D CMAKE_INSTALL_PREFIX=/usr/local \
	-D INSTALL_PYTHON_EXAMPLES=ON \
	-D INSTALL_C_EXAMPLES=OFF \
	-D OPENCV_ENABLE_NONFREE=ON \
	-D OPENCV_EXTRA_MODULES_PATH=$HOME/02_GIT/3rd_parties/opencv_contrib-3.4.4/modules \
	-D PYTHON_EXECUTABLE=~/.virtualenvs/${VEN}/bin/python \
	-D BUILD_EXAMPLES=ON ..  #\
#	-D ENABLE_PRECOMPILED_HEADERS=OFF
# the last line solves potential stdlib.h problem

######################################
#
#  swap file on raspberry
##
############################################


echo "sudo nano /etc/dphys-swapfile"
echo "sudo nano /etc/dphys-swapfile"
echo "sudo nano /etc/dphys-swapfile"
echo "CONF_SWAPSIZE=2048"
echo "CONF_SWAPSIZE=2048"
echo "CONF_SWAPSIZE=2048"
echo "sudo /etc/init.d/dphys-swapfile stop"
echo "sudo /etc/init.d/dphys-swapfile start"

sleep 2

say "MAKE"


if [ "$?" ==  "0" ]; then
    make -j8
fi

echo "=============== DO YOURSELF ==================="
echo "=============== DO YOURSELF ==================="
echo "=============== DO YOURSELF ==================="
echo cd  $HOME/02_GIT/3rd_parties/opencv-3.4.4
echo workon cv3
echo sudo make install
echo sudo ldconfig
echo ======= VARIANTS =====================
echo cd /usr/local/python/cv2/python-3.6
echo cd /usr/local/python/cv2/python-3.7
echo cd /usr/local/python/cv2/python-3.8
echo sudo mv cv2.cpython-36m-x86_64-linux-gnu.so   cv2.so
echo sudo mv cv2.cpython-37m-arm-linux-gnueabihf.so  cv2.so
echo sudo  mv cv2.cpython-38-x86_64-linux-gnu.so cv2.so

echo " ------------------- LINK YOUR VENV "
echo cd ~/.virtualenvs/${VEN}/lib/python3.6/site-packages/
echo cd /home/pi/.virtualenvs/cv3/lib/python3.7/site-packages
echo cd  ~/.virtualenvs/cv3/lib/python3.8/site-packages/
echo =======================
echo ln -s /usr/local/python/cv2/python-3.6/cv2.so  cv2.so
echo ln -s /usr/local/python/cv2/python-3.7/cv2.so cv2.so
echo ln -s /usr/local/python/cv2/python-3.8/cv2.so cv2.so
#
# to run pycamfw:
# pip3 install -r requirements_opencv.txt
#pip3 install flask
#pip3  click itsdangerous Jinja2 flask_httpauth gunicorn gevent
#CAMERA=opencv ./app.py
#
