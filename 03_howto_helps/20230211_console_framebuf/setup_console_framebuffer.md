Using framebuffer on PC
================


Repair  systemd
-----------
```bash
cat /sys/devices/virtual/tty/console/active
root@edie:/etc# systemctl edit getty@tty4
[Service]
ExecStart=
ExecStart=-/sbin/agetty -a USER_I_WANT --noclear %I $TERM
root@edie:/etc# systemctl restart getty@tty4
```



Consoles 1-6
-------------

http://askubuntu.com/questions/173220/how-do-i-change-the-font-or-the-font-size-in-the-tty-console

```sudo dpkg-reconfigure console-setup```

select Latin, Slavic and TerminusBold

```console-setup```  or ```reboot```


bash
----------
```
if [ "$TTY" = "/dev/tty4" ]; then
 echo TERM 4
 newsbeuter
 exit
fi
```


Pictures
----------
```fbi ```

video
-----------
```mpv -vo drm ```




```
mpv -vo drm   video.avi

mpv -vo drm --speed 0.2  *.jpg  *.jpg
```


audio from youtube
-------------------
```pip3 install mps-youtube```


podcasts
------------

	- newsbeuter
	- podbeuter





ANDROID  with TERMUX
===============
- install termux
- install hacker keyboard

- use  like `pkg install mc`
- or like `apt install python`
- `pip install mps-youtube`
- `pip install youtue-dl`
- `pip install bashplotlib`



Python libraries
===================

- tables on terminal - `pip install terminaltables`
- ansi printing on screen - `pip install Blessings`
