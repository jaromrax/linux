#+TBLNAME:tops
|-------+----------+------------------------+--------------+-------------+------|
| music | mpv      | cmus                   | mps-youtube  | ==mpsyt     |      |
| disk  | ncdu     | dfc/dfcc/wdfc          | pydf         | fslint      | fdup |
|       | iotop    | wdr(apt i dstat)       | dpigs fdfind | bleechbit   | duf  |
| net   | bwm-ng   | wondershaper           | nethogs      | nload       | cbm  |
|       | bmon     | iftop                  | jnettop      | mmv (go)    |      |
|       | dnstop   | speedtest-cli          | curl wttr.in | sntop(ping) | mtr  |
|       |          | qr-filetransfer (go)   | iptstate     |             |      |
| logs  | lnav     | grc less log..         |              |             |      |
| users | whowatch | sysdig -c sy_users     |              |             |      |
|-------+----------+------------------------+--------------+-------------+------|
| tops  | htop     | powertop               | bpytop       | glances     | atop |
|       | nmon(d)  | curl  cheat.sh/command | curl wttr.in | neoss       |      |
|       | arpwatch | apt i inotify-tools    | visidata vd  |             |      |


#+NAME:echo
#+BEGIN_SRC python :var x=tops :results output
mx=list(range(len(x[0])))
for i in x:
    c = -1
    for j in i:
        c+=1
        if len(j)>mx[c]:
            mx[c] = len(j)
# print(mx)
for i in x:
    print('echo "', end="")
    c = -1
    for j in i:
        c+=1
        print("|{j:{mx}s}".format(mx=mx[c], j=j), end = "")
    print('"')
#+END_SRC

#+RESULTS: echo
#+begin_example
echo "|music|mpv     |cmus                  |mps-youtube |==mpsyt    |     "
echo "|disk |ncdu    |dfc/dfcc/wdfc         |pydf        |fslint     |fdup "
echo "|     |iotop   |wdr(apt i dstat)      |dpigs fdfind|bleechbit  |duf  "
echo "|net  |bwm-ng  |wondershaper          |nethogs     |nload      |cbm  "
echo "|     |bmon    |iftop                 |jnettop     |mmv (go)   |     "
echo "|     |dnstop  |speedtest-cli         |curl wttr.in|sntop(ping)|mtr  "
echo "|     |        |qr-filetransfer (go)  |iptstate    |           |     "
echo "|logs |lnav    |grc less log..        |            |           |     "
echo "|users|whowatch|sysdig -c sy_users    |            |           |     "
echo "|tops |htop    |powertop              |bpytop      |glances    |atop "
echo "|     |nmon(d) |curl  cheat.sh/command|curl wttr.in|neoss      |     "
echo "|     |arpwatch|apt i inotify-tools   |visidata vd |           |     "
#+end_example

#+BEGIN_SRC sh :var x=echo :results output
echo $x
#+END_SRC

#+RESULTS:
: echo "|music|mpv |cmus |mps-youtube |==mpsyt | " echo "|disk |ncdu |dfc |pydf |fslint |fdup " echo "| |iotop | | | | " echo "|net |bwm-ng |wondershaper |nethogs |nload |cbm " echo "| |bmon |iftop |jnettop | | " echo "| | |speedtest-cli |curl wttr.in|sntop(ping)|mtr " echo "| | |dnstop |iptstate | | " echo "|logs |lnav |grc less log.. | | | " echo "|users|whowatch|sysdig -c spy_users | | | " echo "|tops |htop |powertop |bpytop |GLANCES |(atop)" echo "| |nmon(d) |curl cheat.sh/command"|curl wttr.in| | " echo "| |arpwatch| | | | "
