* Straight installer/package manager

 https://github.crookster.org/switching-to-straight.el-from-emacs-26-builtin-package.el/

** Installing:

  - as simple as: ~straight-use-package~ ~ob-ipython~
  - and for permanent, copy the recommended line
      - ~(straight-use-package 'ob-ipython)~ to =.emacs=
