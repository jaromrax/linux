* EMACS - all basic for emacs in one place

** Fast orgmode stuff

 - stars make collapsable headers
   - ~shift TAB~ will unfold all
 - for inline code - use "~" or "=" pairs
 - want to customize any color?
    - Like inline code? ~M-x customize-group RET org-faces RET~
    - want to  create =verbatime BLOCKS= ?
 - ~<sTAB~ creates a code block (sh sql lisp ...syntaxes)

#+BEGIN_SRC
Just add: " < " + one of the letters below
s   #+BEGIN_SRC ... #+END_SRC
e   #+BEGIN_EXAMPLE ... #+END_EXAMPLE
q   #+BEGIN_QUOTE ... #+END_QUOTE
v   #+BEGIN_VERSE ... #+END_VERSE
c   #+BEGIN_CENTER ... #+END_CENTER
l   #+BEGIN_LaTeX ... #+END_LaTeX
L   #+LaTeX:
h   #+BEGIN_HTML ... #+END_HTML
H   #+HTML:
a   #+BEGIN_ASCII ... #+END_ASCII
A   #+ASCII:
i   #+INDEX: line
I   #+INCLUDE: line
#+END_SRC



*** Tables in org


 This was longer to find a functional way. At the end:

 -Positives:
     - you get tables creation and modification
     - create with ~| a~, head with ~|-~ and tabs
	     - or ~C-c |~ and ~C-c -~
     - moving columns and rows `alt+arow`
	 - adding with ~shift-alt-arrow~
	 - deleting with ~shift-alt-left~
	 - sorting with ~C-c ^~
     - long cells view can be restricted ......
     - when you ask ~ps-print-~ you have a nice output
	 - formulas: ~C-c =~ or enter ~:=vsum($1..$3)~,
	     - ~C-c~ on TMBE line recalc
		 - ~C-u C-c~ make all column as formula

 - Negatives:
      - in principle ONE WAY only "table translation" from org to latex
	  - but ~orgtbl-create-~ helps to tranfer TO latex
      - you loose some emacs shortcut func. (shift-down)
	  - I change into org-mode in this file .... why


*** Installation of orgtbl

 Put
 #+BEGIN_SRC lisp
;; ORGTBL : org-mode can work with orgtbl-  create and send
(add-hook 'message-mode-hook 'turn-on-orgtbl)
 #+END_SRC

into ~.emacs~. You need to run  ~package-list-packages~  and install some ~orbtbl-~ packages.
Then launch ~emacs something.tex~ to get latex syntax immediately (sometimes).

 - ~M-x org-mode~ - it may even not be  NEEDED?
 - ~M-x orgtbl-~ TAB will offer only ~-mode~. Try ~orgtbl-s~ TAB, it reveals other commands.
 - ~orgtbl-insert-radio-table~ makes latex preparation of the table.
      - ~orgtbl-create-or-convert-from-region~ makes table from scratch or from tabular inside (needs many edits).
 - point to the table, say ~orgtbl-send-table~ and everything is done.
 - tweaking: ~#+ORGTBL: SEND mytab orgtbl-to-latex :splice t :skip 1~
     - ~t~ only the values, not ~tabular~
	 - ~1~ skips first (title) row
 - ~normal-mode~ goes to normal, but sending still works...

*** REMARK TO orgtbl:

 Quite a problem with math:

 see https://tex.stackexchange.com/questions/186605/with-orgtbl-how-to-ensure-that-braces-and-dollars-are-not-escaped

 This ~generic~ worked for me:
#+BEGIN_SRC
#+ORGTBL: SEND compil orgtbl-to-generic :splice t :skip 0 :sep  " & " :lend " \\\\"  :fmt (1 "$%s$") :raw t :no-escape t
| ^{9}\mathrm{Be}(p,\gamma)       |    |   6.586 | 1 | 1 |
#+END_SRC



*** table

| 1 | 2 | 3 | 4 | SUM |
|---+---+---+---+-----|
| 5 | 4 | 3 | 2 |  14 |
| 7 | 7 | 7 | 8 |  29 |
| 9 | 8 | 7 | 6 |  30 |
- #+TBLFM: $5=vsum($1..$4)





* Pythoning in emacs


There is quite a problem with tabs and spaces in emacs. Once it is mixed,
try ~M-x whitespace-mode~ to see what is wrong.

A good setting in ~.emacs~ is
#+BEGIN_SRC lisp
;############ 4 spaces python
;(setq-default indent-tabs-mode nil)
;(setq-default tab-width 4)
;; Python Hook
;(add-hook 'python-mode-hook
;          (function (lambda ()
;                      (setq indent-tabs-mode nil
;                            tab-width 2))))
(custom-set-variables
 '(python-guess-indent nil)
 '(python-indent 4))
#+END_SRC

a commented part of it did not really work ...





* pdf for emacs - not sure how useful....

Replacement for Docview for PDF files - *created on-demand and stored in memory*.

https://github.com/politza/pdf-tools

#+BEGIN_SRC sh
 aptitude install libpng-dev libz-dev
 aptitude install libpoppler-glib-dev
 aptitude install libpoppler-private-dev
#+END_SRC


~M-x package-install RET pdf-tools RET~


see also:

https://github.com/emacs-tw/awesome-emacs



* latex for emacs


https://www.emacswiki.org/emacs/AUCTeX

https://www.emacswiki.org/emacs/AUCTeX

Basicaly =aptitude install auctex=  ~M-x tex-mode~
or one can put

#+BEGIN_SRC lisp
;   TEX MODE
;
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq TeX-save-query nil)
;(setq TeX-PDF-mode t)
#+END_SRC

into =~/.emacs=

Control:  ~C-c C-c~ ... this will either compile or compile or display
~C-c C-p C-s~ will tex a section, picture.


SPELL CHECK IN ANY TEX:

#+BEGIN_SRC lisp
;  spellcheck in tex with emacs
;
(setq ispell-program-name "aspell") ; could be ispell as well,
(setq ispell-dictionary "english") ; this can be set to any language your spell-checking program supports

(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-buffer)
#+END_SRC




* jupyter for emacs - unseen

untested and I didnt manage to run it:
#+BEGIN_SRC lisp

(package-initialize)
(require 'ein)
(require 'ein-loaddefs)
(require 'ein-notebook)
(require 'ein-subpackages)
#+END_SRC







* Markdown mode

  - see /emacs.md/,  the document can look like this

#+BEGIN_SRC
---
geometry: margin=1.5in
mainfont: Arial
fontsize: 10pt
---

# This document:
 - look at with emacs `markdown-mode`
 - pandoc
    - with `markdown-preview` while `markdown-command` is set to pandoc - maybe in `customize-mode`
#+END_SRC


*** Markdown tricks - maybe also older stuff

and after emacs restart
~M-x package-install RET markdown-mode RET~
as written in [http://jblevins.org/projects/markdown-mode/]()

To load automatically on start - put into /~/.emacs/:

#+BEGIN_SRC lisp
;add the path where all the user modules will be located
;
;
(autoload 'markdown-mode "markdown-mode.el"
	"Major mode for editing Markdown files" t)
	(setq auto-mode-alist
		(cons '("\\.md" . markdown-mode) auto-mode-alist)
	)
#+END_SRC



*** markdown install catches:

 -  Install ~aptitude install markdown~ to be able to  ~C-c C-c p~ and render to html

 - BUT **Better** (why?) - install - from Melpa - flymd: use firefox in ~~/.emacs~:

#+BEGIN_SRC lisp
;; -------- use firefox - not chrome - for markdown flymd
 (defun my-flymd-browser-function (url)
   (let ((browse-url-browser-function 'browse-url-firefox))
     (browse-url url)))
 (setq flymd-browser-open-function 'my-flymd-browser-function)
#+END_SRC

 - Try ~sudo xdg-mime default emacs24.desktop text/markdown~ to be able
open ~xdg-open whatever.md~ whatever in emacs.





* vimish-fold

 When install ~vimish-fold~, and ~.emacs~ has this config:


 it is possible to collapse parts. ~vimish-fold-mode~ may keep things persistent.


* multiple-cursors feature


it is possible to write the same on several lines in the same time

~M-x package-install RET multiple-cursors RET~

*https://github.com/magnars/multiple-cursors.el*

https://www.youtube.com/watch?v=jNa3axo40qM

#+BEGIN_SRC lisp
(require 'multiple-cursors)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
;(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
(global-set-key (kbd "C-*") 'mc/mark-all-like-this)
#+END_SRC



* I cant believe - but problematic

/ob-ipython/ can be installed, I saw
plots here directly, but it remains difficult
to  setup ipython

*DO NOT RUN*, not installed

#+BEGIN_SRC  ipython :results drawer :session pysession
%matplotlib inline
import numpy as np
import matplotlib.pyplot as plt

fig=plt.figure()
plt.hist(np.random.randn(10000), bins=100)

#+END_SRC

#+RESULTS
:RESULTS:
:END:
