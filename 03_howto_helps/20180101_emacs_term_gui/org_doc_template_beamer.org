#+startup: beamer
#+LATEX_CLASS: beamer
#+LATEX_CLASS_OPTIONS: [smaller,a4paper,presentation]
#+BEAMER_FRAME_LEVEL: 2
#+LATEX_HEADER: \usepackage{times} \usetheme{Madrid}
#+TITLE: Open in orgmode, use "C-c C-e l P" to create beamer pdf
#+AUTHOR: Jaromir
#+DATE: 2020
#+OPTIONS: TeX:t LaTeX:t toc:nil skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0
#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)


# Maketitle is the key. It is automatic, whoever  +TITLE must be defined...
\newcommand\x{0.5}
\setlength{\unitlength}{12cm}

* Emacs.org mode
:PROPERTIES:
  :EXPORT_LaTeX_CLASS: beamer
  :EXPORT_LaTeX_CLASS_OPTIONS: [presentation,smaller]
  :EXPORT_BEAMER_THEME: default
  :EXPORT_FILE_NAME: presentation.pdf
  :END:


#+ATTR_LATEX: :caption {float wrap ... usualy} :options angle=10 :center nil :float wrap  :width 0.2\textwidth :placement {r}{0.2\textwidth}
[[file:emacs_logo.png]]

The major trick was not to run "C-c C-e l p" /which creates a simple latex pdf/,

*but*

the same with uppercase "*P*" to run beamer thing.

 slide consists of some text with a number of bullet points:

- the first, *very* @important@, /point/ !
- the previous point shows the use of the special markup which
  translates to the Beamer specific /alert/ command.

\vskip 1cm
HELO SLIDE TWO


* Main body


HELLO HERE

** Some block                                                  :B_alertblock:
:PROPERTIES:
:BEAMER_env: alertblock
:BEAMER_envargs: <1>
:END:
The above list could be numbered or any other type of list.

The image uses /wrap/ or /nil/ or /:center nil/, other things do not work much
\hfill \(\qed\)

** Another block                                                    :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_envargs: <2>
    :END:
 I just press C-c C-b and select a letter to get the block. All.


** Possible blocks....                                              :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :END:
alertblock, block, /theorem/, example, frame is NEW FRAME, /definition/,
 /proof/, Column??, fullframe (FULL), /NoteNH/, /verse/,
/strucureenv/,



* Twocols

** A block :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_col: 0.6
    :END:
/Structureenv/ WORKS here. But it is blue. /quote/  works too but is /italic/.
    - this slide consists of two columns
    - the first (left) column has no heading and consists of text
    - the second (right) column has an image and is enclosed in an
      @example@ block
    -  /env/ *column* works great, /beamercolorbox/ too

** A screenshot                                                    :B_column:
   :PROPERTIES:
   :BEAMER_env: block
   :BEAMER_col: 0.4
   :END:

Ahoj ahoj, column can be here too
#+ATTR_LATEX: :width 0.5\textwidth
    file:emacs_logo.png


*  Last Slide at all               :B_frame:
  :PROPERTIES:
  :BEAMER_env: frame
  :END:
