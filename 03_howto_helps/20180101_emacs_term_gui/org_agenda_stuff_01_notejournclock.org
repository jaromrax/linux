* ORGMODE knows several things

 - timers
 - clocks
 - *other file notes* - *CAPTURE-*:
   - journals - time sorted entries (in separate file)
   - notes - subsections under *Notes* section (in separate file)
 - *infile notes* -  *DRAWERS*:
   - =cc cz= notes - go into LOGBOOK (if it is setup like this)
   - logbooks - :LOGBOOK: drawer in a section can contain

See WEB sites:
  - https://orgmode.org/worg/org-tools/
  - https://www.youtube.com/watch?v=0TS3pTNGFIA&list=PLVtKhBrRV_ZkPnBtt_TD1Cs9PJlU0IIdE&index=1
  - https://github.com/legalnonsense/elgantt/
  - https://org-web.org/sample

  - http://doc.norang.ca/org-mode.html#HandlingNotes


* Calendar
  SCHEDULED: <2021-01-30 Sat> DEADLINE: <2021-03-18 Thu>
   -  =C-c C-s= schedule STAMP
   -  =C-c C-d= deadline STAMP
 I cannot open agenda =c-A A=.
 Magenta STAMP appear (and can be changed with =cc cs=) always right under the section

  =cc .= <2021-01-28 Thu> makes agenda STAMP
  =cc != [2021-01-28 Thu] makes STAMP without agenda entry
  =shift arrows= move by day


* Drawers - Local-file Notes in LOGBOOK
  :LOGBOOK:
   CLOCK: [2021-01-26 Tue 18:09]--[2021-01-26 Tue 18:09] =>  0:00
   :END:
  /every subsection can have its own logbook, I try to create 1st line logbook/

   :LOGBOOK
   - Note taken on [2021-01-26 Tue 16:11] \\
     orgnote...cc cz...
   - Note taken on [2021-01-26 Tue 16:09] \\
     note inserted with c-c c-z i ..... ... no date?
   :END
   - [ ] c-c c-z i take a note.... should go to
   - [ ] BUT Drawer *logbook* doesnt contain anything by himself...
   - [ ] BUT I can do =options-custamizeemacs-specificoption=
     - =org-log-into-drawer=
   - [ ]  and it works - =cc cz= notes go to logbook
   - [ ]  recorded with timestamp



* Capture-setting - F6:
** Notes
  /This is for keeping a separate logbook (without setup: with =.note= name) and maybe with the ref to (sub)section/

  -  =m-x global-set-key= =F6= =org-capture=
  - OR ~(global-set-key (kbd "<f6>") 'org-capture)~ - works.
  - then F6 and shift-c  define the capture (task) ?
  - see =.notes= file, the notes do *NOT* appear here.... ?

   [[file:~/.notes]]

  *Later* <2021-01-28 Thu> I created a new =.emacs=, that defines few styles and target files....



* Timer

  - =c-c c-x ;=  *SET* the countdown in bottom-line
    - =c-c c-x .= prints the value
    - =,= pause, continue
    - set to
  - =c-c c-x ,= to pause, continue
  - =c-c c-x 0= to *START* timer to now
  - =c-c c-x _= *STOPS* the timer


  - =alt-enter= NEW item, section... formats nicely the list.....



  - 0:01:24 :: timer list - =c-c c-x .= and two colons and a word, =alt-enter=

  - 0:01:50 :: ok, this works


* Clocking the working time


   - Note taken on [2021-01-26 Tue 18:06] \\
     some note dada
   :LOGBOOK:
   CLOCK: [2021-01-26 Tue 18:09]--[2021-01-26 Tue 18:09] =>  0:00
   CLOCK: [2021-01-26 Tue 18:05]--[2021-01-26 Tue 18:09] =>  0:04
   CLOCK: [2021-01-26 Tue 16:05]--[2021-01-26 Tue 18:05] =>  2:00
   CLOCK: [2021-01-26 Tue 11:06]--[2021-01-26 Tue 16:03] =>  4:57
   :END:

 /First Set ='org-clock-into-drawer "CLOCKING")= to =~/.emacs= ./

    - [ ] didnt work (alt-enter checkbox), still to logbook


   - =c-c c-x c-i= : IN
   - =c-c c-x c-o= : IN
   - since I have defined LOGBOOK DRAWER, this goes there
   - =c-c c-x c-q= cancel clocking
   - =c-c c-x d= show time


* Links
<<<link>>>
  - classical
      - file
      - file with description
  - heading link - link to the subsection
  - link to #+NAME - create theis pragma .... (not tested)
  - *radio* link on the fly, search the file, use <<<>>> and *activate!* with  =c-c c-c=

    [[file:tst.org][this same file]] - file with description
    [[A new heading with the link]] - this may be the name of the section



* Checklists

 /Summary: shift-arrow creates todo labels, schedule and deadline can be set dates from
calendar, NOTES (outside)  and LOGBOOK notes can be simply created  - e.g. LOGBOOK DRAWER   ./

  - use =shift-alt-enter to add a checklist=
  - use =cc cc= to check/uncheck
  - use [/] or [%] to get overview
  - use =shift-arrow= to change status
  - Schedule and deadline - see calendar later =cc cs= =cc cd=


** DONE Checklist [20%]
   SCHEDULED: <2021-02-25 Thu>
   - Note taken on [2021-01-26 Tue 18:11] \\
     subsec checklist
   :LOGBOOK:
   - Note taken on [2021-01-26 Tue 16:17] \\
     subsection logbook
   :END:
  - [ ] shoft-alt-enter after =-= mark
  - [ ]
  - [X] aqqw dqw
  - [ ]  =shift-alt-enter=
  - [ ]




** TODO Calendar - problem with Agenda....
   DEADLINE: <2021-01-29 Fri> SCHEDULED: <2021-01-28 Thu>
   -  =C-c C-s= schedule
   -  =C-c C-d= deadline
 I cannot open agenda =c-A A=





** DONE Logbook drawer takes notes
   :LOGBOOK:
   - Note taken on [2021-01-27 Wed 11:18] \\
     hoahoa
   CLOCK: [2021-01-26 Tue 18:09]--[2021-01-26 Tue 18:09] =>  0:00
   :END:
  /every subsection can have its own logbook, I try to create 1st line logbook/

   - [ ] c-c c-z i take a note.... should go to *LOGBOOK*
   - [ ] BUT Drawer *logbook* doesnt contain anything by himself...
   - [ ] BUT I can do =options-custamizeemacs-specificoption=
     - =org-log-into-drawer=
   - [ ]  and it works - =cc cz= notes go to logbook
   - [ ]  recorded with timestamp


* APPENDIX 1 - SETUP FOR journals


 - see examples :  https://pages.sachachua.com/.emacs.d/Sacha.html
 - see manuals: https://orgmode.org/manual/Template-elements.html



I Like to have this
  - *Notes* - /new subsections/ - with quick notes at beginning
  - *Journal* - /item date sorted bold/ - with item quick podcast remarks

#+BEGIN_SRC lisp

(setq org-capture-templates
      `( ("j" "Journal entry with paste buffer" plain
         (file+olp+datetree "~/00README_notes.org")
;;         "   %T - %a\n     %i\n      %?\n\n"
         "  - *%<%H:%M:%S>*   - %a\n     %i\n      %?\n\n"
         :unnarrowed t)


	("p" "Podcast log - timestamped" item
         (file+olp+datetree "~/00README_notes.org")
         "*%<%H:%M:%S>* %^{Note}"
;;         "%T %^{Note}"
         :immediate-finish t)

        ("N" "Note - own subsection IN section Notes" entry
         (file+headline ,"~/00README_notes.org" "Notes")
         "* %?\n%U - %a")

        ("i" "Interrupting task" entry
         (file ,"~/00README_notes.org")
         "* STARTED %^{Task}"
         :clock-in :clock-resume)


        ("n" "Quick note - mess with Notes" item
         (file+headline  "~/00README_notes.org" "Notes"))
      ))
#+END_SRC
