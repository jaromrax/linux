#!/usr/bin/env python3
import vaex as vx
import numpy as np
import time

import sys
from pympler import asizeof

N=int( sys.argv[1] )

print("COncat ",N," x  1 milion entries x 4 columns  tables")

frame_data = np.random.randint(0, 100, size=(2**26))  # size of 1GB in vx

df = vx.from_arrays(x = frame_data, y=frame_data**2 )

df['x'] = df.x.astype('float64')
df['y'] = df.y.astype('float64')

df['z'] = df['x']+df['y'] + np.random.randint(0,1000)

df['r2'] = (df['x']+df['y'])**2

df['r02'] = (df['x']+df['x'])**0.2

print(df )
print("SIZE:", asizeof.asizeof(df)/1024/1024 )
print("SIZE:", df.byte_size()/1024/1024 )

# doesnot work
#df.loc['Column_Total']= df.sum(numeric_only=True, axis=0)
#df.loc[:,'Row_Total'] = df.sum(numeric_only=True, axis=1)

print("concat")
big_df = df
#vx.concat([df for _ in range(N)]) # x1GB frames

#print("sort")
#big_df = big_df.sort('x')


print("pythoagor")
big_df['pythagoras'] = (big_df['x']**2+big_df['y']**2)**0.5

print(big_df)
print("SIZE:", asizeof.asizeof(big_df)/1024/1024 )
print("SIZE:", big_df.byte_size()/1024/1024 )

print("apply sum")
res = big_df.sum(big_df.x, delay=False)
#big_df.sum( 'x' )
print(res)
print(big_df.types)

#print(big_df.apply(lambda col: col.sum()))

#print(big_df.apply(lambda x: x.sum()))

input("press ENTER")
#for i in range(10,0,-1):
#    print(i)
#    time.sleep(1)
