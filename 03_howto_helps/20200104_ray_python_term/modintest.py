#!/usr/bin/env python3

#mkdir /home/tmp
#chmod a+rwx /home/tmp
#chmod +t /home/tmp/
#export TMPDIR=/home/tmp
# --- doesnt help, modin is not in /tmp but
#

import time
import sys

import ray
#ray.init(temp_directory="/home/tmp")

import modin.pandas as pd
import modin.pandas as pd
import numpy as np

# 2GB - 13G  (6)
#  2GB 36sec
#

N=int( sys.argv[1] )


#frame_data = np.random.randint(0, 5, size=(2**10, 2**3)) # 1GB takes 5GB

print("i... creating {} GB that wlil take {} GB in maximum".format(N,5*N))

frame_data = np.random.randint(0, 100, size=(N*2**20, 2**8)) # 1GB takes 5GB


df = pd.DataFrame(frame_data).add_prefix("x")

print("i... df created, new column now")

df['x1'] = df['x1'].astype(np.float64)


# works with modin  SORTVALUE + RESETINDEX
print("i... sorting")
df = df.sort_values('x0')
print("i... reset index")
df = df.reset_index(drop=True)  # THIS IS CORRECT !!!!



print("i... shift operation ")
# SHIFT works with modin
df['xp'] = 0 - df['x1'].shift()
df['xs'] = df['x1'] - df['x1'].shift()


print("i... sum operation")

# SUMS work with modin
df.loc['Column_Total']= df.sum(numeric_only=True, axis=0)
df.loc[:,'Row_Total'] = df.sum(numeric_only=True, axis=1)

print(df)

#print("COncat ",N," x  2GB  tables")
#big_df = pd.concat([df for _ in range(N)]) # x1GB frames
#print(big_df.apply(lambda col: col.sum()))



input("press ENTER")
#for i in range(10,0,-1):
#    print(i)
#    time.sleep(1)
