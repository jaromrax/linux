#!/usr/bin/env python3

import sys


import modin.pandas as pd
#import pandas as pd
import numpy as np


def somefunc(data):
    return (data ** 1.305+ data/7.11)**0.5



if __name__=="__main__":

    N=int( sys.argv[1] )

    frame_data = np.random.randint(0, 100, size=(N*2**10, 2**2))

    #frame_data = np.random.randint(0, 100, size=(N*2**20, 2**8)) # 1GB takes 5GB

    df = pd.DataFrame(frame_data).add_prefix("x")
    print( type(df) )

#    df['x1'] = df['x1'].astype(np.float64)

    df['xo'] = df['x1'].apply(somefunc)
    print(df)


    # # works with modin  SORTVALUE + RESETINDEX
    # print("sorting")
    # df = df.sort_values('x0')
    # print("reset index")
    # df = df.reset_index(drop=True)  # THIS IS CORRECT !!!!

    # # SHIFT works with modin
    # df['xp'] = 0 - df['x1'].shift()
    # df['xs'] = df['x1'] - df['x1'].shift()

    # # SUMS work with modin
    # df.loc['Column_Total']= df.sum(numeric_only=True, axis=0)
    # df.loc[:,'Row_Total'] = df.sum(numeric_only=True, axis=1)

    # print(df)



    #input("press ENTER")
