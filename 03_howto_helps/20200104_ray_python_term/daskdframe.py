#!/usr/bin/env python3

import sys



import dask.dataframe as dd
import numpy as np
import pandas as pd

def somefunc(data):
    return (data ** 1.305+ data/7.11)**0.5



if __name__=="__main__":

    N=int( sys.argv[1] )

    df = pd.DataFrame({'x': range(2**N), 'y': range(2**N)})
    #arr = np.random.randint(0, 1000, size=2**N)

    # create dask frame and series
    ddf = ddf = dd.from_pandas(df, npartitions=5)
    #darr = dd.from_array(arr)
    # give it a name to use as a column head
    #darr.name = 'z'



    #ddf2 = ddf.merge(darr.to_frame())


    ddf['q'] = ((ddf.x**2 + ddf.x**2)**0.5).compute()
    ddf['z'] = ((ddf.x + ddf.x**2)**0.5).compute()

    ddf = ddf.sort("q")

    ddf.compute()
    print(ddf)
    input("enter")
