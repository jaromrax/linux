Website
===========

Installation
--------------
```
 sudo  aptitude install apache2
 sudo  systemctl enable apache2
 sudo  service apache2 status
sudo  emacs /var/www/html/index.html
```

User dir
-----------

As a user, make `~/public_html`

```
 a2enmod userdir
 nano /etc/apache2/mods-available/userdir.conf #just to verify
```

Redirect all root requests to user
----------

Put `RedirectMatch ^/$ /~ojr/` into `/etc/apache2/apache2.conf`.

Make `service apache2 restart`



# Deploy flask app - Make proxy to port 25000

```
emacs /etc/apache2/sites-available/default
<Location />
        ProxyPass http://localhost:25000
        ProxyPassReverse  http://localhost:25000
    </Location>


 sudo apt-get install libapache2-mod-wsgi python-dev
 sudo a2enmod wsgi
 #proxy enable
 sudo a2enmod proxy
 sudo a2enmod proxy_http
 sudo  systemctl restart apache2
```
