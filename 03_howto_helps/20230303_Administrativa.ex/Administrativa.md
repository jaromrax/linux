# Administrativni programy



Socialka potrebuje XFA formule v PDF

http://askubuntu.com/questions/455135/how-do-i-install-adobe-acrobat-reader-from-the-repository

```
sudo apt-get install libgtk2.0-0:i386 libnss3-1d:i386 libnspr4-0d:i386 lib32nss-mdns libxml2:i386 libxslt1.1:i386 libstdc++6:i386
wget -c http://ardownload.adobe.com/pub/adobe/reader/unix/9.x/9.5.5/enu/AdbeRdr9.5.5-1_i386linux_enu.deb
sudo dpkg -i AdbeRdr9.5.5-1_i386linux_enu.deb
acroread
```

Je dost zasadni problem prinutit chrome otevirat pdf v acroreadu: create: `/usr/share/applications/AdobeReader.desktop` and there should be possible to find a possibility to *open with* in **Nautilus** (or something) and *default* in properties right-click item. Maybe copied to `~/.local/share/applications/`.


```
[Desktop Entry]
Name=AcrobatReader
Comment=View multi-page documents
Keywords=pdf;
TryExec=acroread
Exec=acroread %U
StartupNotify=true
Terminal=false
Type=Application
Icon=acroread
X-GNOME-DocPath=
Categories=GNOME;GTK;Office;Viewer;Graphics;2DGraphics;VectorGraphics;
MimeType=application/pdf;application/x-bzpdf;application/x-gzpdf;application/x-xzpdf;application/x-ext-pdf;
X-Ubuntu-Gettext-Domain=acroread
```



### FoxitReader

 pekne komentovani a oznacovani, ale na pdflatex mi to zkolabovalo


# Administrativni postupy


### pdf na socialku 2020

 - skenovat cernobile a text do Dropboxu
 - prejmenovat listy
 - join pdf pages:
 ```
\documentclass{article}
\usepackage{pdfpages}
\begin{document}
\includepdf[pages=-]{atrofka1.pdf}
\includepdf[pages=-]{atrofka2.pdf}
\end{document}
 ```
 a pdflatex



### zmensit pdf aby se vesly do priloh  (ebook/ screen ):

```
 ls soukrpojisteni.pdf studium.pdf zavisla_cinnost.pdf -1 |  xargs -n 1 -I III gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile=III_small.pdf III
 ```

zmensit jeste vic pdf aby se vesly do priloh:
```
 ls soukrpojisteni.pdf studium.pdf zavisla_cinnost.pdf -1 |  xargs -n 1 -I III gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile=III_small.pdf III
 ```

 nebo

```
ls *.pdf -1 | grep -v small |  xargs -n 1 -I III gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile=III_small.pdf III
```

 **Poslat:**
` echo "Message Body Here" | mutt -s "Subject Here" -a backup.zip user@example.com`






### Markdown convert to pdf

```
pandoc README.md -t latex -o README.pdf
pandoc posudek_mrazek.md -V geometry:margin=1in -o posudek_mrazek.pdf
rm lat.pdf; pandoc -S Texy.md  --latex-engine=pdflatex -o lat.pdf; evince lat.pdf
```

With emacs and immediate viewing in a browser:
```
Alt-x flymd-flyit
```
