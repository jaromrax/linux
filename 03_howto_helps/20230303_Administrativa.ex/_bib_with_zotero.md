# Download the code

https://www.zotero.org/download/

Or use even 24h old DEB

https://github.com/retorquere/zotero-deb

```
wget -qO- https://github.com/retorquere/zotero-deb/releases/download/apt-get/install.sh | sudo bash
sudo apt update
sudo apt install zotero
```

# Download chrome connector

same  https://www.zotero.org/download/


# Plugins:

https://www.zotero.org/support/plugins

http://www.zotfile.com/

https://github.com/whiskyechobravo/kerko

https://demo.kerko.whiskyechobravo.com/

https://github.com/retorquere/zotero-better-bibtex

https://gitlab.com/egh/zotxt-emacs
