# Spellchecking

### In Emacs

`alt-x ispell` will work. Even with possibilities shown in upper window. Possible responses [a]ccept [r]eplace by editing [i]nsert to private dictionary.

`alt-x ispell-pdict-save` saved the private dictionary. And then asked after every [i] for save.

`alt-x flyspell-mode` underlines the misspelled words.

Location of private dict:
```
$HOME/.aspell.en.prepl
$HOME/.aspell.en.pws
```
