# Using remote screens

*purpose of this help is to summarize variants
for remote screen usage with emphasis on
continuous (Latex) compilation*


### History
 In early days, the following things appear to work
- `x2vnc` - linked VNC and X-display - two seamless displays on two different PCs.
             There was some issue to connect 3-monitors with one large screen.
- `synergy` - one keyboard and mouse for two different PCs
- `x11vnc` - remote access to the X-screen of a PC


### Standalone remote display

*One effective possibility - useful for keyboard-less devices - may be  extra vnc server with xmonad interface*

 There are multiple vnc server/client possibilities. See
 [ask-ubuntu](https://askubuntu.com/questions/438002/best-vnc-remote-desktop-application).
 They recommend `tigervnc`, `realvnc`, `remmina`, `tightvnc` is the last. The `aptitude search vnc`
 shows:
 - `directvnc` - framebuffer, for fb-devices
 - `gvncviewer` - gtk
 - `linuxvnc` - access to `tty`
 - `novnc` - html5 client
 - `ssvnc` - with tunel helper
 - `tigervnc` -
 - `tightvncserver` - default ubuntu 1804
 - `xtightvncviewer` - default ubuntu 1804
 - `vnc-server` - placeholder
 - `x11vnc` - see above
 - `x2vnc` - see above
 - `vnc4server` - quite old - the choice
 - `xvnc4viewer` - quite old - the choice

 In fact, `realvnc` is not free, but they say, that (old from 2015) `vnc4server` is in fact the `realvnc`.

#### Why vnc4server ?

 I tried several combinations, vncserver with geometry 900x1600, and the only viewer
 that worked correctly with **full-screen** F8 (didn't lock-up) was `xvnc4viewer`.
 It was not possible to use `tightvncserver` because tight encoding was mandatory,
 and xvnc4 knows only *hextile* and *raw*.

#### Set-up xvnc4

```
 update-alternatives  --list vncviewer
#
 update-alternatives  --list vncserver
#/usr/bin/tightvncserver
#/usr/bin/vnc4server
#============ GO=============
update-alternatives  --config vncserver
# select the number
```

### ON SERVER

Install `apt install xmonad`, prepare the startup file `~/.vnc/xstartup`.
```
#!/bin/sh

xrdb $HOME/.Xresources
xsetroot -solid grey
#x-terminal-emulator -geometry 80x24+10+10 -ls -title "$VNCDESKTOP Desktop" &
#x-window-manager &
evince $HOME/01_Dokumenty/04_ourpublic/20200116_Frontiers/anc_perspective2.pdf &
xmonad &
# Fix to make GNOME work
export XKL_XMODMAP_DISABLE=1
/etc/X11/Xsession
```

This startup will keep full-screen `evince` that continuously changes on PDF changed.


Now run the server with `vncserver -geometry 900x1600 ` and kill eventually with `vncserver -kill :2`
and `vncserver -kill :1`.

OPTIONALLY `while inotifywait -e close_write anc_perspective.tex; do  ./runall; done `


### ON CLIENT

`vncviewer host:5901` and F8. Or `vncpasswd ~/.vncpasswd` and `vncviewer -passwd ~/.vncpasswd host:5901`
