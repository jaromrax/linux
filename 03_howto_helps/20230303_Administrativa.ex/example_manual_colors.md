---
title: "MANUAL CAEN digitizer with gregory"
date: May 26, 2020
geometry: margin=1cm
output: pdf_document
---

[//]: # pandoc  README.md   -t latex  --highlight-style=tango  -o README.pdf


[//]: # \let\oldtextbf\textbf
[//]: # \renewcommand\textbf[1]{{\color{blue}\oldtextbf{#1}}}

\let\oldtextit\textit
\renewcommand\textit[1]{{\color{green}\oldtextit{#1}}}

\let\oldtexttt\texttt
\renewcommand\texttt[1]{{\color{red}\oldtexttt{#1}}}


## Fast start

Napiš do terminálovýho okna:
```bash
goo
```

Tím se spustí - odevšad dostupný - komunikační program **gregory** a uzivatel uvidi:

```bash
Info in <THttpEngine::Create>: Starting HTTP server on port 9009
i... binary path: /home/ojr/02_GIT/GITLAB/vme/02_gregory
i... data   path: /home/ojr/DATA/
i... logging setting to /home/ojr/DATA/gregory_commands.log

.l1    load V1724REAL
0001>
```



##  Prikazy:

 - 1. Musi se pripojit k digitizeru:

    - `.l1` - tecka L 1 - pripoji se a nahraje paramettry. Musi tam nekde byt videt ohlaseni ROM FPGA

    ```bash
	Connected to CAEN Digitizer Model DT5780M, recognized as board 0
	ROC FPGA Release is 04.08 - Build FA29
	AMC FPGA Release is 128.32 - Build F925 (fw)
    ```

      - `.q` - tecka Q - znamena uplny *QUIT*, vsechno se stopne, i data se stopnou a program uplne skonci.
	      Pokud se da omylem `q` bez tecky, musi se bud znova dat `.l1` anebo `.q`.


 - 2. **START a STOP jednotlivych RUNU**:

    - `te` nebo `test` - zacne nabor, ALE NEZAPISUJE SE NA DISK
    - `sta` nebo `start`  nebo `start 1p4fe` - zacne nabor, UKLADAJI SE DATA DO run00xx...
    - `sto` nebo `stop`  - stop
    - `sa`  nebo `save` nebo `save 1p3fe` - ulozi single spektra
	- `cl` nebo `clear` -  vymaze spektra z pameti, START nebo STOP je nevymaze, jen `cl`
    - `ini` nebo `init` - znova nahraje parametry  `dpp_pha.ini` do digitizeru


 - 3. **MONITORING BEHU**:
     - `sts` nebo `status` - zobrazi informace o filu a cislu runu
	 - `.h` - napoveda pro dalsi prikazy
	 - `http://147.231.102.82:9009` - web server se spektry a A/D sondami online
	 - `/home/ojr/DATA/` - zde se ukladaji data


## High Voltage

Napiš do terminálovýho okna:
```bash
gov
```
a pak jen klavesy, nakonec `q`.
