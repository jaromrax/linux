

# INFLUX

## install and run

```
apt install influxdb influxdb-client
sudo systemctl enable --now influxdb
```

## Create user and database
see "'" in user
```
CREATE USER --- WITH PASSWORD '-----' WITH ALL PRIVILEGES
SHOW USERS
CREATE DATABASE "test"
CREATE DATABASE "data"
```

There should be something like `grant all on test to aaa`

## show things

```
show measurements
name: measurements
name
----
192.168.0.249
rpi03a1_ard04


show retention policies
name    duration shardGroupDuration replicaN default
----    -------- ------------------ -------- -------
autogen 0s       168h0m0s           1        true
```

## select

```
use test
select * from "192.168.0.249"
name: 192.168.0.249
time                value
----                -----
1602928794975660530 249
1602928795054931094 249
```

```
 select * from "rpi03a1_ard04"
name: rpi03a1_ard04
time                R2     T2    avg command dt       loop measurement valuea0 valuea1 valuea2
----                --     --    --- ------- --       ---- ----------- ------- ------- -------
1602928685010673679 108.14 22.23 100 quasi;  1.039119 387  ard04       390.79  478.58  86.59
1602928715140498251 108.42 22.16 100 quasi;  0.981669 417  ard04       390.78  476.17  86.39
1602928745058709617 108.1  22.24 100 quasi;  0.97628  447  ard04       390.81  477.57  86.62
1602928775057871257 108.24 22.2  100 quasi;  1.040682 477  ard04       390.65  477.98  86.52
1602928804971858393 108.61 22.1  100 quasi;  0.970183 507  ard04       390.67  477.63  86.25
1602928835055149766 109.29 21.92 100 quasi;  0.979311 537  ard04       390.71  477.62  85.76
1602928864875683512 108.4  22.16 100 quasi;  0.99358  567  ard04       390.82  476.64  86.4
1602928894982826962 108.86 22.04 100 quasi;  0.981802 597  ard04       390.79  478.1   86.07
```
