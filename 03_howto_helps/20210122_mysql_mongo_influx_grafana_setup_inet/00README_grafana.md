# Grafana - use on every desktop... influx is important

 see https://grafana.com/docs/grafana/latest/installation/debian/

 ```
sudo apt-get install -y apt-transport-https
sudo apt-get install -y software-properties-common wget
wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -

echo "deb https://packages.grafana.com/oss/deb stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list

sudo apt-get update
sudo apt-get install grafana


sudo systemctl daemon-reload
sudo systemctl start grafana-server
sudo systemctl status grafana-server
```


# Influx source

  - extremely simple way
	- name `I-19`
	- URL `http://192.168.0.19:8086`
	- leave server
	- no Auth
	- Dbase test, user, pass, no http method chosen


# Data input
   - use field() mean() math() ; time($__interval) fill(null); time series
   - A0 calibration math *0.21699-61.111
   -
