# ALL HELPS: 
- rootkits
- audit
- 

## Several rules to handle rootkits




### install (in not installed)

```
aptitude install chkrootkit
aptitude install rkhunter
aptitude install lynis
```

### rkhunter - NUMBER 1 - special treatment at upgrades

[stack](https://unix.stackexchange.com/questions/373718/rkhunter-gives-me-a-warning-for-usr-bin-lwp-request-what-should-i-do-debi)

It needs to know the local package manager: `/etc/rkhunter.conf.local` contains `PKGMGR=DPKG` on Ubuntu.

[stack ](https://unix.stackexchange.com/questions/302386/update-only-upgraded-packages-with-rkhunter-propupd)

Keep `APT_AUTOGEN="false"` in `/etc/default/rkhunter`

```
rkhunter --update && rkhunter -c -sk --rwo
# IF ALL OK
aptitude update && aptitude upgrade  && rkhunter --propupd
```


### chkrootkit - just solve false positives

not existing tcpd, executables in /tmp ...

```
sudo chkrootkit -q
```

### lynis

It will give lot of advices. Some (install debsecan) are useless for ubuntu.

```
lynis audit system
```



