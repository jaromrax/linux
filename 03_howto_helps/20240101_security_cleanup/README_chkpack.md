# Special security packages (on 84):

- **apt-listchanges**
- debsums
```
 Description: tool for verification of installed package files against MD5 checksums
 debsums can verify the integrity of installed package files against MD5 checksums
 installed by the package, or generated from a .deb archive.
 ```

- fail2ban -  better than hostsdeny, but no sharing of lists...
- lynis
- **needrestart**
