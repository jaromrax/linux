#!/bin/sh

# login-noti

#https://askubuntu.com/questions/179889/how-do-i-set-up-an-email-alert-when-a-ssh-login-is-successful
#
# chmod +x login-notify.sh
# EDIT: /etc/pam.d/sshd  AND APPEND
# session optional pam_exec.so seteuid /path/to/login-notify.sh



# Change these two lines:
sender="zdenek-84@email.cz"
sender="FEDO"
recepient="root"  # or the webmail address


if [ "$PAM_TYPE" != "close_session" ]; then
    host="`hostname`"
    subject="SSH Login "$sender": $PAM_USER from $PAM_RHOST on $host"
    # Message to send, e.g. the current environment variables.
    message="`env`"
    echo "$message" | mail -r "$sender" -s "$subject" "$recepient"
fi
