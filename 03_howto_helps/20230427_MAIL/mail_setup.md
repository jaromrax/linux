# Fast Mass Setup (13pc's)
 - create /var/mail/root with rw----
 - own to root:mail
 - /etc/aliases : put nobody_or_whoever: root
 - run `newaliases`
 - install postfix, install (agian) sendmail (if postfix didnt help)
 - test with `mail -s "test009" root < dev/null`

# THE CLASSICAL MAIL SETUP

  * the mail setup  has several problematic moments.
  * a typical possibility is to set it localy to get notifications from
	- smartctl
	- denyhosts
	- arpwatch
	- mdadm ... if any
	- psad ... if any and know howto
	- sudo fails
	- extrathings (your backups of borg, your login-notify)

   * the advanced possibility is to have one local mail server **LMS** and clients around sending all notifications to this server
   * the most advenced would be to have local mail server with SMTP enabled **LMSSE** to the internet (to use e.g. `transfer` directly) or send mail notification to the outer network



TYPICAL START with postfix:
```
dpkg-reconfigure postfix
service postfix reload
```

# LMSSE 20190418 - MAXIMUM SETUP - WORKS /not on gigaj/

*i can send mails via google*

   - maybe https://www.linuxbabe.com/ubuntu/automatic-security-update-unattended-upgrades-ubuntu-18-04

   - THIS SEEMED TO WORK (set google account to lower security, check /var/log/mail.log):
https://www.howtoforge.com/tutorial/configure-postfix-to-use-gmail-as-a-mail-relay/

```
nano /etc/postfix/sasl_passwd
[smtp.gmail.com]:587    name@gmail.com:pass
chmod 600 /etc/postfix/sasl_passwd
nano /etc/postfix/main.cf
###################3 gmail  #relayhost =
relayhost = [smtp.gmail.com]:587
smtp_use_tls = yes
smtp_sasl_auth_enable = yes
smtp_sasl_security_options =
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt


nano /etc/postfix/master.cf  # uncomment...i tried y
tlsmgr unix - - n 1000? 1 tlsmg


postmap /etc/postfix/sasl_passwd ;service postfix reload; systemctl restart postfix.service;

# ENABLE LESS SECURE APP IN GAMIL
echo a | mail -r me@seznam.cz -s "test"  name@gmail.com
```

### LMSSE + forwarding the incomming mail


```
# in /etc/postfix/virtual # remove gigajm from
@gigajm         ak@gmail.com
@localdomain    ak@gmail.com
@zdenek         ak@gmail.com
@zdenekdomain   ak@gmail.com
# in  mydestination REMOVE gigajm,
# for one client machine i have there also [xxx.xxx.xxx.xxx] of server
#  forwarding ==========  IN /etc/postfix/main.cf ADD
#
virtual_alias_domains = localdomain,zdenek,zdenekdomain,gigajm
virtual_alias_maps = hash:/etc/postfix/virtual
########    COMPILE AND  RESTART
 sudo postmap /etc/postfix/virtual ; sudo postmap /etc/postfix/sasl_passwd ; sudo service postfix reload; sudo systemctl restart postfix.service;
```



# 201903 - local net mail  **LMS**:

***postfix:*** - configure two machines (ufw hole for 25 ....)

*  check `/etx/postfix/master.cf` for port (==smtpd) and existence the line `smtp .. smtpd`

*  And in `/etc/postfix/main.cf` put
```
# COMMENT: makes problems with validation...
#myorigin = /etc/mailname

smtpd_banner = $myhostname ESMTP $mail_name (Ubuntu)
biff = no

# appending .domain is the MUA's job.
append_dot_mydomain = no

# Uncomment the next line to generate "delayed mail" warnings
#delay_warning_time = 4h

readme_directory = no

# See http://www.postfix.org/COMPATIBILITY_README.html -- default to 2 on
# fresh installs.
# COMMENT: may help to keep @2
compatibility_level = 2

# TLS parameters
smtpd_tls_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem
smtpd_tls_key_file=/etc/ssl/private/ssl-cert-snakeoil.key
smtpd_use_tls=yes
smtpd_tls_session_cache_database = btree:${data_directory}/smtpd_scache
smtp_tls_session_cache_database = btree:${data_directory}/smtp_scache

# See /usr/share/doc/postfix/TLS_README.gz in the postfix-doc package for
# information on enabling SSL in the smtp client.

# COMMENT: must be like this
smtpd_relay_restrictions = permit_mynetworks permit_sasl_authenticated defer_unauth_destination
# COMMENT: YOUR HOSTNEMA
myhostname = zdenek
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
# COMMENT:
mydestination = $myhostname, zdenek, localhost.localdomain, , localhost
relayhost =
relaydomain =
# COMMENT : THIS IS THE KEY
mynetworks = 127.0.0.0/8 xxx.xxx.xxx.0/24
#mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128
mailbox_size_limit = 0
recipient_delimiter = +
# COMMENT: ALL
inet_interfaces = all
inet_protocols = all
# COMMENT: ADDED  NOT NEEDED
lmtp_host_lookup = native
smtp_host_lookup=native
#disable_dns_lookups = yes
ignore_mx_lookup_error = yes
```

*  In `/etc/hosts` introduce your gigaj, both machines

*  In `/etc/aliases` of the client, set the forward to server gigaj
```
postmaster:    or@gigaj
root:   or@gigaj
or:   or@gigaj
```

*  run
```
newaliases
systemctl restart postfix
```
* send mail from client to gigaj `echo ahoj|mail root`  ***DONE***







# Local/Internet Mail server - Ubuntu


*p34, edie - long response of mutt,  edie: hostname=fjfi, no psad, no rk, no arpw*

*gigajm - good psad,*


### Basic installations - to cleanup

 * install mutt as a local mail client `aptitude install  mutt`

 * exim4 (raspberry) - `aptitude install mail mutt`

 * postfix (ubuntu 1604) - `aptitude install mailutils` I did also  `sudo dpkg-reconfigure postfix`


And after `echo ahoj | mail $USER` creates the mailbox. `mutt` can see this mail on both systems.


## To get system  mails as user:


Probably you have a postfix `systemctl restart postfix` AFTER:
`nano /etc/aliases` and putting `root: user@localhost` and `postmaster: user@localhost`  inside. After run command `newaliases`


Probably you have a postfix `systemctl restart postfix` AFTER


## If necessary to send emails via mutt,

change `.muttrc` or  `/etc/Muttrc`:
```
set from = "anyaddress@anydomain.com"
set hostname = hostname.com
set envelope_from = yes
set timeout=20
```

```
mutt -F /home/user/.muttrc  -s "SUBJECT"  any@address.com -a /home/user/attachedfile.txt < /home/user/bodyofmessage.txt
```





# 20190227 - mutt and postfix - MX server

  - mess with `/etc/mailname` : once I have put something there and I needed to change back to `localhost`

  - `dpkg-reconfigure postfix`
      - **internet site**
      - **system mail name**: `localhost` else it will deliver `root` somewhere else
	  - **root and postmaster**: `root` or another user, modifiy with `/etc/aliases`
	  - **other destinations**: localhost is ok, + I tried publicIP
	  - **force**:  `no?`
	  - **local networks**: I try also my other `public IP/32` not to ignore it
  - `service postfix reload`

 This setup gets
 - rejected from gmail due to no authentication
 - rejected from email.cz due to a bad email address


  Sending as `mail target@seznam.cz -r root@mypublicip` works with seznam. The report doesnt arrive back.




# 201902 - postfix outgoing internet mail that can be accepted by seznam.cz, not by gmail


1- setup postfix as internet mail
2- as domain, do gmail.com (the outgoing emails will look like user@gmail.com)
3- vi /etc/postfix/main.cf ; append ilne : `smtp_generic_maps = hash:/etc/postfix/generic`
4- vi /etc/postfix/generic; put line user@gmail.cop  yourrealname@gmail.com as in https://www.cyberciti.biz/tips/howto-postfix-masquerade-change-email-mail-address.html
5- and `postmap /etc/postfix/generic` and `/etc/init.d/postfix restart`
6-



Programs that should send logs:
-----------------------------

* psad - port scan detection (needs iptables)
 No iptables LOG, no mails. Put in `rc.local`
 ```
 iptables -A INPUT -j LOG
 iptables -A FORWARD -j LOG
 ```
   - *it overwhelms the logs*:
   - i tried in `psad.conf`:
             - `SYSLOG_DAEMON  rsyslog;`
             - `ENABLE_SYSLOG_FILE  N;`


* rkhunter - *maybe needs to set `REPORT_EMAIL=root@localhost` OR: `MAIL-ON-WARNING=root` (probably in cron)
     I have put `MAIL-ON-WARNING=root` into `rkhunter.conf`

* **denyhosts** - seamless (as daemon in ubu)

	- i tried `DENY_THRESHOLD_INVALID = 2`
	- `DENY_THRESHOLD_VALID = 4`

* arpwatch - seamless# arpw as service (as daemon in ubu)
   - put names into  /var/lib/arpwatch/arp.dat
     *worked on fedo nicely*

* mdadm - if installed and used

* # tmpfs for SSD disks

*  fstrim in cron (weekly, it should already be)  and and smartmon as serviece

*  smartmontools - smartctld - seamless, running as daemon (ubu) `system smartd status`
