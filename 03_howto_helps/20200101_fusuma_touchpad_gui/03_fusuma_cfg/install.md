```
sudo gpasswd -a $USER input  
cd ~/.config
take  touchpad_fusuma
emacs config.yml
sudo apt-get install libinput-tools
sudo apt-get install xdotool  
sudo apt install ruby  
sudo fusuma
```

The config:
```
swipe:
  3: 
    left: 
      command: 'xdotool key super+Left'
    right: 
      command: 'xdotool key super+Right'
    up: 
      command: 'xdotool key ctrl+alt+Up'
    down: 
      command: 'xdotool key ctrl+alt+Down'
  4:
    left: 
      command: 'xdotool key alt+Left'
    right: 
      command: 'xdotool key super+Right'
    up: 
      command: 'xdotool key ctrl+alt+Down'
    down: 
      command: 'xdotool key ctrl+alt+Up'
pinch:
  in:
    command: 'xdotool key ctrl+plus'
  out:
     command: 'xdotool key ctrl+minus'

threshold:
  swipe: 0.2
  pinch: 0.4

interval:
  swipe: 0.5
  pinch: 0.1
```


You ma need to switch off 3figer paste??
