Dont forget to record here each new trick

* Remove spaces in itemize

  #+begin_example
#+LATEX_HEADER: \usepackage{enumitem}
#    with setlist{nolistsep} - removes space in itemize

#     +LATEX_HEADER: \raggedbottom

#+LATEX: \setlist{nolistsep}

  #+end_example
