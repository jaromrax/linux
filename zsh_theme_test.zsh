#!/bin/zsh
#
#  this is mockup for tjkirch_mod2 theme
#
#

function getn(){
    #echo i... iam in function getn

    NAME=$1
    MAX=$2
    SILENT=$3
    stopme=0
    NAME=$NAME+`echo $NAME | base64`

   # linewidth=100, no Aradix, decimal
    stop=`echo -n $NAME | od -w100 -A none -d | sed "s/\s//g" `
    added=`expr $(echo "$stop" | sed 's/[0-9]/ + &/g' | sed 's/^ +//g')`
    if [ "$?" != "0" ]; then
	echo X... $NAME stopme=1  expr1
	stopme=1
    fi

    stop=`expr $MAX + $added`
    if [ "$?" != "0" ]; then
	echo X... $NAME stopme=1  expr2
	stopme=1
    fi
    stop=`echo $stop % $MAX | bc`

    if [ "$SILENT" = "0" ]; then
    echo ________ debug: ________________
    echo -n  $NAME | od -A none -chd
    echo ________________________________
    echo  NAME= $NAME ... first level /od/ = $stop " "

    echo second level ... add all digits:
    echo "$stop" | sed 's/[0-9]/ + &/g' | sed 's/^ +//g'
    echo result = $added
    #echo -n "..." modulo  $stop " "
    #echo  "D... modulo="  $stop

    fi

    echo $stop
    if [ "$stopme" = "1" ]; then
	exit 1
    fi

    }

#=====================================================



# =============== all possible combinations===============
#echo D... define AR
AR=(black red blue magenta yellow white green cyan)

declare -A fgc=( ["black"]='\033[00;30m'  ["blue"]='\033[00;34m'  ["magenta"]='\033[0;31m'   ["yellow"]='\033[0;35m'   ["white"]='\033[0;37m'   ["green"]='\033[0;32m'   ["cyan"]='\033[0;36m' )

precol='\033['
fgcc='31'
midcol=';'
bgcc='44'
aftcol="m"


declare -A bgc=( ["black"]='30'  ["red"]='31'  ["blue"]='34'  ["magenta"]='35'   ["yellow"]='33'   ["white"]='37'   ["green"]='32'   ["cyan"]='36'  )
declare -A fgc=( ["black"]='40'  ["red"]='41'  ["blue"]='44'  ["magenta"]='45'   ["yellow"]='43'   ["white"]='47'   ["green"]='42'   ["cyan"]='46'  )
reset_color="\033[00m"

#echo -e $precol$bgcc$midcol$fgcc$aftcol AHOJ_AHOJ_AHOJ $reset_color
#echo -e $precol$bgc[blue]$midcol$fgc[yellow]$aftcol blue on yellow $reset_color

#echo D... define EXCLUDE
#EXCLUDE=(8 9 11 12 14  22 23 24 43 36 42 8 13 14 15 21 29 30 33 34 35 36 41 42)
# exclude all at once, else problem
EXCLUDE=()
EXCLUDE=(10 17 23 24 34 35 47 49 56 57 58  59 60 61 )


#echo D... RUN COMBINATIONS and fill aracolo
con=0
ARACOLO=()
for i in $AR[@]; do
 for j in $AR[@]; do
  if [ $i != $j ]; then
      # print colors
      con=$(( $con + 1 ))
      #echo fg=$i bg=$j
      if    [[ ! " ${EXCLUDE[@]} " =~ " ${con} " ]]; then
          ARACOLO=($ARACOLO "$precol$bgc[$i]$midcol$fgc[$j]$aftcol" )
      fi
  fi
 done
done



combin=${#ARACOLO[@]}
#echo D... NUMBER OF GOOD COLOR COMBINATIONS= $combin

num=0
# print color combintations 2
for i in ${ARACOLO[@]}; do
 num=`expr $num + 1`
  echo -n  "${i}${num}   "
done
echo $reset_color

# =========================================================

echo -n "i... color rnd USER:" $reset_color
getn $USER $combin 1
stop1=$stop
echo $ARACOLO[$stop1] USER $reset_color

echo -n "i... color rnd HOST:"$reset_color
getn $HOST $combin 1
stop2=$stop
echo $ARACOLO[$stop2] HOST $reset_color


p1=$ARACOLO[$stop1]
p2=$ARACOLO[${stop2}]
colo="{$p1%}%n%{$reset_color%}@%{$p2%}%m%{$reset_color%}"


###########################
#  this is probably some trick for telling PTS so that one can redirect to it
###########################
PTS=` ps | grep $$ | awk '{print $2}' |  awk -F "/" '{print $2}'`
# i used ps -q pid  but raspberry crashes
ps  -p $PPID -f | awk '{print $9}' >/dev/null
if [ "$?" = "0" ]; then
    #=========== this is for scripting to terminal ======================
    #echo terminator $PTS
    echo "parent=\`ps -ef | grep pts/$PTS | grep zsh |  awk '{print \$3}'\`"  > $HOME/.scriptterm
    echo "if [ \"\$parent\" != \"\" ]; then " >> $HOME/.scriptterm
    echo "cat > /dev/pts/$PTS" >> $HOME/.scriptterm
    echo "fi" >>$HOME/.scriptterm
fi


function prompt_char {
    if [ $UID -eq 0 ]; then echo "%{$fgc[red]%}#%{$reset_color%}"; else echo $; fi
}


PROMPT='%(?,,%{$fgc[red]%}FAIL: $?%{$reset_color%}
)%$colo: %{$fg_bold[blue]%}%~%{$reset_color%}$(git_prompt_info) %_$(prompt_char) '

RPROMPT='%{$fgc[green]%}[%*]%{$reset_color%}'
