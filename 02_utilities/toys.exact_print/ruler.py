#!/usr/bin/env python3

from matplotlib.pyplot import figure
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.image as mpimg

from fire import Fire



def main(filename="rocket_0.png", w=13.5,h=20.5):
    # insert image inside the graph
    image = mpimg.imread(filename)


    figure_width, figure_height = 15., 15. # cm

    # IMMAGE REAL SIZE
    figure_width, figure_height = 13.5, 20.5 # cm

    figure_width, figure_height = w,h # cm

    # Experimental compensation 7cm was 6.8cm =>
    es=7.0/6.8 # experimental scaling  desiredsize/realsize

    # two texts are bellow............

    #-------------------------------------------------------
    # print on A4
    a4w, a4h = 21.0, 29.7 #cm

    left_margin = 3.   # cm
    top_margin = 3.    # cm

    right_margin = 5.  # cm
    bottom_margin = 5. # cm

    efigure_width, efigure_height = es*figure_width, es*figure_height

    right_margin = a4w - left_margin - efigure_width  # cm
    bottom_margin = a4h - top_margin - efigure_height # cm

    box_width = left_margin + efigure_width + right_margin   # cm
    box_height = top_margin + efigure_height + bottom_margin # cm

    cm2inch = 1/2.54 # inch per cm



    # specifying the width and the height of the box in inches
    fig = figure(figsize=(box_width*cm2inch,box_height*cm2inch))
    ax = fig.add_subplot(111)

    ax.imshow(image, extent=[0,figure_width,0,figure_height])
    #plt.plot(x, y)
    #plt.plot(x[0], y[0], 'og')
    #plt.plot(x[-1], y[-1], 'ob')
    #plt.show()

    #ax.plot([1,2,3])

    ax.set_ylim(0,figure_height)
    ax.set_xlim(0,figure_width)

    #ax.plot([3, 4],[1, 1],label='One cm?')
    #ax.plot([6,6],[1,2], label='One cm?')
    #ax.legend()
    #ax.grid()

    # NOT ax.gca().xaxis.set_major_locator(plt.MultipleLocator(1))
    # NOT plt.gca().xaxis.set_minor_locator(plt.MultipleLocator(1))
    # ommiting ticks   ax.set_xticks(ax.get_xticks()[::1])

    tick_spacing = 1
    ax.xaxis.set_major_locator(ticker.MultipleLocator(tick_spacing))
    ax.yaxis.set_major_locator(ticker.MultipleLocator(tick_spacing))

    plt.text( figure_width/2 -3, figure_height-1.5,
              "##                    ##",
              fontsize=18)

    plt.text( 2.9, 1.2,
              "X",
              fontsize=24)


    fig.subplots_adjust(left   = left_margin / box_width,
                        bottom = bottom_margin / box_height,
                        right  = 1. - right_margin / box_width,
                        top    = 1. - top_margin   / box_height,
                        )
    fig.savefig('15x15cm.png', dpi=300)

    # dpi = 128 is what works in my display for matching the designed dimensions.
    return






if __name__=="__main__":
    print("i... PRINT WITH lpr to keep dimensions")
    Fire(main)
