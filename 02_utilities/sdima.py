#!/usr/bin/env python3

import subprocess as sp
from fire import Fire
import datetime as dt
import os

import getpass # ?? username

import shutil
import psutil

import sys
from subprocess import PIPE, run




def detect_removables():
    """
    use lsblk to detect removable drives
    lsblk -a -o PATH,FSSIZE,RM
    """
    res = sp.check_output( ["lsblk","-a","-o","PATH,FSSIZE,RM"] ).decode("utf8")
    #res = sp.check_output( ["lsblk","-a","-i","PATH,RM"] )

    res = res.split("\n")
    #take good
    res = [ x for x in res if len(x)>2 ]

    # take removables
    res = [ x for x in res if x[-1]=="1" ]
    print(res)

    # - we have removeble devices now
    # - just the devices + size
    res = [ x.split()[:-1] for x in res ]
    print(res)

    #  partitions   only ....
    parts = [ x for x in res if len(x[0])>=9 ]
    #print(parts)

    # remove partitions allow sda sdb sdc....
    disks = [ x[0] for x in res if len(x[0])==8 ]
    print(disks)



    disksize={}
    print("i...      |_{:12s}__|__{:5s}_____|".format("_"*12, "_"*5 ) )
    for d in disks:

        #hdd = shutil.disk_usage( d )
        #total, used, free = hdd.total, hdd.used, hdd.free
        #print( hdd  )

        total = sp.check_output( ['lsblk',d] ).decode("utf8").split("\n")
        total = [ x for x in total if len(x)>2 ] # remove empty line
        total = total[-1]
        total = total.split()[3] # 29.3G
        total = float(total[:-1])


        print("i... {} Total: {} GiB".format(d, total) )
        # print("Used:  %d GiB" % (used // (2**30)))
        # print("Free:  %d GiB" % (free // (2**30)))
        disksize[d]=total  # // (2**28)
        #disksize[d]=32

        print("i... disk | {:12s}  |  {:5} GB  |".format(d, disksize[d]) )

    print("i...      | {:12s}  |  {:5s}     |".format(" ", " " ) )
    print("i...      |_{:12s}__|__{:5s}_____|".format("_"*12, "_"*5 ) )
    print("D... returned disksize",disksize)
    return disksize




def s2i(rpname="SD_PiZERO"):
    """
    SD to IMAGE
    """

    sd = detect_removables()
    selsd=""
    key = ""
    if len(sd)==1:
        key = list(sd.keys())[0]
        print("i... taking only removable dev", key )
        selsd = key
    else:
        print("X... select device", sd)
        quit()

    if (sd[key]<3) or (sd[key]>66):
        print("?... size< 3 or > 66")
        print("X... I DONT BELIEVE IT IS SD CARD !!!!!")
        print("X... SIZE IS STRANGE !!!!!")
        #quit()
    stamp = dt.datetime.now().strftime("%Y%m%d_%H%M%S")
    imname = rpname+"_"+stamp+".img"
    print("i... ", imname)
    size = int(sd[key]*1024*1024*1024)
    if os.path.isfile(imname):
        print("X... file already exists")
        quit()
#    CMD="sudo dd bs=4M if="+selsd+" | pv -s "+str(size)+" -r -p -e -t |    dd bs=4M  of="+imname+"  conv=fsync"

    CMD="sudo dd bs=4M if="+selsd+" | pv -s "+str(size)+" -r -p -e -t |  zstd -T0 --stdout -10 > " +imname+".zst "

    CMD2 = "umount /media/"+getpass.getuser()+"/*"

    print()
    print(CMD)
    #print(CMD + "  && sleep 10 &&  " + CMD2 )



def i2s(imname):
    """
     IMAGE to SD-CARD. Give me image name
    """
    sd = detect_removables()
    selsd=""
    key = ""
    if len(sd)==1:
        key = list(sd.keys())[0]
        print("i... taking only removable dev", key)
        selsd = key
    else:

        print("X... select device problem ???, nothing or too many", sd)
        quit()

    if (sd[key]<3) or (sd[key]>66):
        print("?... size<3 or >66")
        print("X... I DONT BELIEVE IT IS SD CARD !!!!!")
        print("X... SIZE IS STRANGE !!!!!")
        #quit()
    #stamp = dt.datetime.now().strftime("%Y%m%d_%H%M%S")
    #imname = rpname+"_"+stamp+".img"
    print("i... ", imname)
    size = int(sd[key]*1024*1024*1024)
    print(f"D... SD size :{size/1024/1024:.1f} MB (we use IMG size for dd)")

    if os.path.isfile(imname):
        print(f"i... {imname}     .....   file  exists")
    else:
        print(f"X... {imname}     ......  file not exist!")
        sys.exit(1)


    IS_ZST = False
    if (imname.find(".zst")>len(imname)-5):
        IS_ZST = True
        print("i... getting size from  zstd --test  .... wait a little")
        size = 32
        CMD = ["zstd", "-t", imname ]
        print(f"D... ",CMD)

        # PIPE discouraged
        # p = Popen(['program', 'arg1'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        # output, err = p.communicate(b"input data that is passed to subprocess' stdin")
        # rc = p.returncode

        # py 3.5+
        # CMD = ['echo', 'hello']
        result = run(CMD, stdout=PIPE, stderr=PIPE, universal_newlines=True)
        # print(f"i... result: {result.returncode} stdout={result.stdout} stderr={result.stderr}")

        sizeline = result.stderr.strip().split("\n")[-1]
        print(f"D... sizeline {sizeline}")
        size = sizeline.strip().split()[-2]
        print(f"D... size {size}")
        print(f"i... SIZE from zstd --test == {size}")

    else:
        size = os.path.getsize(imname)
    print("i... IMG size", size)


    if IS_ZST:
        CMD = "zstd -d --stdout  "+imname+" | pv -s "+str(size)+" -r -p -t -e |  sudo dd bs=4M  of="+selsd+" conv=fsync"
    else:
        CMD = "dd bs=4M if="+imname+" | pv -s "+str(size)+" -r -p -t -e |  sudo dd bs=4M  of="+selsd+" conv=fsync"



    CMD2 = "umount /media/"+getpass.getuser()+"/*"

    #    CMD="sudo dd bs=4M if="+selsd+" | pv -s "+str(size)+" -r -p |    dd bs=4M  of="+imname+"  conv=fsync"

    print()
    print(CMD + " && sync && echo sleeping && sleep 10 && echo UMOUNTING NOW ... && " + CMD2 )


if __name__=="__main__":
    Fire({"s2i":s2i,
          "i2s":i2s})
    #print("i... for xz... TRY!?","xzcat ~/Downloads/ev3dev-yyyy-mm-dd.img.xz | sudo dd bs=4M of=/dev/sda; sync")
