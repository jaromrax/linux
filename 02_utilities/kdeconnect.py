#!/usr/bin/env python3

# kdeconnect-cli -d a6fcd6022eba5db7 --share /home/milous/Downloads/temp/179.\ schůzka\ Více\ jest\ věřiti\ mistru\ Janovi\ pravdomluvnému...mp3

from fire import Fire

import subprocess as sp

import os
import pathlib
from shutil import copyfile

import unicodedata
import time
from alive_progress import alive_bar,config_handler





#=================================================
def strip_accents(text):
    try:
        text = unicode(text, 'utf-8')
    except NameError: # unicode is a default on python 3
        pass
    text = unicodedata.normalize('NFD', text)\
           .encode('ascii', 'ignore')\
           .decode("utf-8")
    return str(text)



#=================================================
def launch(CMD, fake=False):
    print("D... launchng", CMD)
    if not fake:
        res=sp.check_output( CMD.split() ).decode("utf8").rstrip()
        #print("D...",res)
        return res



#=================================================
def get_phone(device=""):
    CMD="kdeconnect-cli -l"
    res=launch(CMD)
    res=res.split("\n")
    res=[ i[1:].strip() for i in res if i[0]=='-']
    print("D... # of paired:", len(res))
    res=[ i for i in res if i.find("reachable")>=0 ]

    accdevice=[]
    if len(res)==1:
        print("D... only one reachable => ")
        accdevice=res[0]
    elif len(res)==0:
        print("___  NO DEVICE REACHABLE: ")
        quit()
    else:
        print("i... default device     :",device)
        print("i... these are reachable:",res)
        if device=="":
            quit()
            return
        for i in res:
            print(" ... ",i , "#",device,"=>",device[0].lower(), end="")
            if (len(device)>0) and (device[0].lower()==i[0].lower()):
                print(" <=== [ok]")
                accdevice=i
            print()
        #print("### DECIDE WHICH DEVICE: ... program ends here")
        #quit()
    print(accdevice)
    if len(accdevice)==0:
        print("D... no device selected: try ./kdeconnect.py /path/file a")
        quit()
    name,code=accdevice.split(":")
    code=code.strip().split()[0]
    return code



#=================================================
#=================================================
#=================================================
def send(fname, device=""):
    """
    remove spaces, utf8, selfcopy
    and send with kdeconnect0cli
    just sleep to have enoughtime to use 4MByte/s
    remove selfcopied file
    nload enp3s0   IS FANTASTIC TO SEE THE TRANSFER
    """
    copied=False # did i do copy?
    code=get_phone(device)
    print(fname, type(fname) )

    #----------------------------------change utf8
    if (fname.find(" ")>=0) or (fname!=strip_accents(fname)):
        dst=fname.replace(" ","_")
        dst=strip_accents(dst)
        if fname!=dst:
            copyfile(fname, dst)
            copied=True
        fname2=dst
    else:
        fname2=fname
        copied=False

    #----------------------------compare local path x global
    bn=os.path.basename(fname2)
    #print("D...   fname=",fname2)
    #print("D...    base=",bn)
    if bn==fname2:
        path=pathlib.Path().absolute()
        #print("D..x   path==",path, str(path), type(path) )
        #print(str(path))
        fname2=str(path)+"/"+fname2
    CMD="kdeconnect-cli -d "+code+" --share "+fname2

    res=launch(CMD)

    # ONLY SLEEP AND HOPE----------------------------------
    size=os.path.getsize(fname2)/1024/1024
    MBS=3
    wai=int(size/MBS)
    if copied:
        print("!... waiting before removing file: "+fname2+" ... length:",size,"MB")
    else:
        print("i... waiting - estimating transfer at "+str(MBS)+" MB/s")

    print(" ... i wait:", wai,"s, as I think "+str(MBS)+"MB/s is safe")
    with alive_bar( wai  ) as bar:
        for i in range(wai):
            bar()
            time.sleep(1)
    if copied:
        print("!... REMOVING NOW:", fname2)
        time.sleep(1)
        os.remove( fname2 )



#=================================================
#=================================================
#=================================================
if __name__=="__main__":

    Fire(send)
