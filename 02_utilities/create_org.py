#!/usr/bin/env python3
"""
ORGMODE  HELPER
book    Create a book on QL700
code    code printing using minted; see `pygmentize -L styles`
rep     create report with citations
ppt     create presentation


https://tex.stackexchange.com/questions/487710/a-whole-list-of-math-fonts-for-greek-letters-in-latex

https://www.tug.org/FontCatalogue/allfonts.html



"""
# see https://github.com/GeneKao/orgmode-latex-templates
# see also flyspell
# https://emacs.stackexchange.com/questions/2171/what-options-are-there-for-writing-better-non-programming-text-in-emacs/41834#41834



from fire import Fire

import os
import datetime as dt
import json

FNAME_CONFIG = "~/.create_org.json"

######################################################################################


help_table = r"""
FOR  one table / spreadsheet  calculation
_________________________________________

see others
"""

help_cest = r"""
FOR CESTOVNI ZPRAVU
_________________________________________
sudo apt install texlive-luatex

"""


help_note = r"""
FOR NOTICE LETTER - with an official header
_________________________________________
 you need a logo ...
"""

######################################################################################
############

help_ppt=r"""
FOR PRESENTATION
same as report (?)
________________________________________
sudo apt install latexmk
sudo apt install texlive-luatex
;; .emacs contains:
(setq org-latex-pdf-process (list
   "latexmk -pdflatex='lualatex -shell-escape -interaction nonstopmode' -pdf -f  %f"))

TEST WITH:
 lualatex -shell-escape -interaction nonstopmode  -recorder  "a.tex"
#
IF luainputenc fails ??? I cannot getit: gigjm WORKS zen NOT
SEE
 /usr/share/doc/texlive-doc/lualatex/luainputenc
 /usr/share/doc/texlive-doc/lualatex/luainputenc/luainputenc.pdf
 /usr/share/texlive/texmf-dist/tex/lualatex/luainputenc
 /usr/share/texlive/texmf-dist/tex/lualatex/luainputenc/luainputenc.lua
 /usr/share/texlive/texmf-dist/tex/lualatex/luainputenc/luainputenc.sty

______________________________________
IN EMACS
--------------------------------------
c-c c-e l(atex) P(beamer)

HELP: https://github.com/fniessen/refcard-org-beamer/blob/master/README.org
"""
######################################################################################
############


help_report=r"""
FOR REPORT:
________________________________________
### sudo apt install latexmk
sudo apt install texlive-luatex
;; .emacs contains:
(setq org-latex-pdf-process (list
   "latexmk -pdflatex='lualatex -shell-escape -interaction nonstopmode' -pdf -f  %f"))
"""

######################################################################################
############


help_code=r"""
FOR CODE:
________________________________________
pip3 install pygments
(setq org-latex-listings 'minted)
(setq org-latex-minted-options
      '(("frame" "lines") ("linenos=true")))

"""

######################################################################################
############

help_book=r"""
FOR BOOK:
________________________________________
apt install texlive-fonts-extra
mkdir -p ${HOME}/texmf/dist ${HOME}/texmf/tex/latex
cd ${HOME}/texmf/dist
wget https://mirrors.ctan.org/macros/latex/contrib/koma-script.zip
wget https://mirrors.ctan.org/fonts/libertine.zip
cd ../tex/latex
unzip ../../dist/koma-script.zip ; unzip ../../dist/libertine.zip

(require 'ox-latex)~ before  ~addtolist~
;; use scrbook here
(with-eval-after-load 'ox-latex
   (add-to-list 'org-latex-classes
                '("scrbook"
                  "\\documentclass{scrbook}"
                  ("\\chapter{%s}" . "\\chapter*{%s}")
                  ("\\section{%s}" . "\\section*{%s}")
                  ("\\subsection{%s}" . "\\subsection*{%s}")
                  ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))

"""

######################################################################################
############




help_calc=r"""
FOR CALC:
________________________________________
 - M-x set-variable org-latex-caption-above
 - OR (customize-group "org-export-latex")

 - https://orgmode.org/manual/Results-of-Evaluation.html
 - https://emacs.stackexchange.com/questions/63939/org-mode-how-to-place-a-table-left-aligned-on-the-paper
 - https://emacs.stackexchange.com/questions/19105/how-to-reference-named-table-or-code-block-in-org-mode



"""



######################################################################################
###
######################################################################################
###
######################################################################################
###


#========================================================================================TABLE


tableorg=r"""
#+OPTIONS: toc:nil        (no default TOC at all)
# # -------------- dont interpret ^_ as in math...
#+OPTIONS: ^:nil
#+OPTIONS: _:nil


# -----------------------------------------  colored blocks
# -----------pip install Pygments; mod .emacs
#+LaTeX_HEADER: \usepackage{minted}
#+LaTeX_HEADER: \usepackage{bookmark}
# --------- margins
#+LATEX_HEADER: \makeatletter \@ifpackageloaded{geometry}{\geometry{margin=2cm}}{\usepackage[margin=2cm]{geometry}} \makeatother
#+LaTeX_HEADER: \usemintedstyle{xcode}
#       monokai, paraiso-dark,, zenburn ... ... NOT shell /cd |
#       fruity .... NOT  + blackbg
#       colorful ... ok
#       vs ... too simple
#       inkpot ... yellowish-brown
#       vim ... pipe too light
#       gruvbox-dark,sas, stata, abap,algol, lovalace, igor, native, rainbow_dash, tango, manni, borland, autumn:), murphy, material, trac    ... italic
#       rrt ... too light green
#       perldoc, pastie, xcode, arduino ... ok (pastie,arduino (gray comments)
#  +LATEX_HEADER: % !TeX TXS-program:compile = txs:///pdflatex/[--shell-escape]

#    ## +ATTR_LATEX: :options linenos,frame=single,breaklines=true,bgcolor=gray!10!white

# PDF:

* Trip CZ-FR
# ---------------------------------------------------------------------------------------

Trip comments

#+CONSTANTS: pi=3.14159265358979323846

#+NAME: ttt
#+ATTR_LATEX:  :placement [H]
#+CAPTION:  Expenses
| toll to EUR | toll back EUR | oil fr EUR | material fr EUR | oil cz CZK |          |
|-------------+---------------+------------+-----------------+------------+----------|
|         7.9 |           3.9 |     117.77 |          103.96 |     1070.4 |          |
|        26.3 |           6.1 |     103.37 |           23.98 |     783.53 |          |
|        14.4 |           6.5 |     119.58 |           36.96 |       2000 |          |
|         9.9 |           2.9 |            |                 |            |          |
|         9.3 |           2.4 |            |                 |            |          |
|         5.8 |           9.2 |            |                 |            |          |
|         2.3 |          16.1 |            |                 |            |          |
|         3.9 |           7.9 |            |                 |            |          |
|-------------+---------------+------------+-----------------+------------+----------|
|        79.8 |           55. |     340.72 |           164.9 |    3853.93 |          |
|-------------+---------------+------------+-----------------+------------+----------|
|       1596. |         1100. |     6814.4 |           3298. |    3853.93 | 16662.33 |
#+TBLFM: @10$1..@10$5=vsum(@2..@9)::@11$1..@11$4=@10*$kurz::@11$6=vsum($1..$5)::@11$5=@10$5
#+CONSTANTS: kurz=20

| Pro kurz EURo | 20 | CZK/EUR |
#+TBLFM: $2=$kurz

* Notes:
 - use
   - C-c } ... cell addresses
   - C-c { ... debugging
   - vsum vmean ... vector
   - := when full column needed only
   - double click on CONSTANTS
   - ranges @10$1..@10$4 = vsum(@2..@9) ... sum range of rows
   - $ ... are columns
   - @> @>> ... last row, before last row
# % ---------------------------------------------------------------------------------------


"""




#========================================================================================CALC

calcorg=r"""
#+OPTIONS: toc:nil        (no default TOC at all)
# # -------------- dont interpret ^_ as in math...
#+OPTIONS: ^:nil
#+OPTIONS: _:nil


# -----------------------------------------  colored blocks
# -----------pip install Pygments; mod .emacs
#+LaTeX_HEADER: \usepackage{minted}
#+LaTeX_HEADER: \usepackage{bookmark}
# --------- margins
#+LATEX_HEADER: \makeatletter \@ifpackageloaded{geometry}{\geometry{margin=2cm}}{\usepackage[margin=2cm]{geometry}} \makeatother
#+LaTeX_HEADER: \usemintedstyle{xcode}
#       monokai, paraiso-dark,, zenburn ... ... NOT shell /cd |
#       fruity .... NOT  + blackbg
#       colorful ... ok
#       vs ... too simple
#       inkpot ... yellowish-brown
#       vim ... pipe too light
#       gruvbox-dark,sas, stata, abap,algol, lovalace, igor, native, rainbow_dash, tango, manni, borland, autumn:), murphy, material, trac    ... italic
#       rrt ... too light green
#       perldoc, pastie, xcode, arduino ... ok (pastie,arduino (gray comments)
#  +LATEX_HEADER: % !TeX TXS-program:compile = txs:///pdflatex/[--shell-escape]

#    ## +ATTR_LATEX: :options linenos,frame=single,breaklines=true,bgcolor=gray!10!white

# PDF:

# ---------------------------------------------------------------------------------------
* Expected sources

#+NAME: LM0
#+ATTR_LATEX:  :placement [H]
#+CAPTION: Ukol LM pocatecni stav
| LMcesty | LMsluzby | LMmaterial |
|---------+----------+------------|
|     100 |      100 |        100 |
|         |          |            |


#+NAME: CANAM
#+ATTR_LATEX: :placement [H]
#+CAPTION: Canam pocatecni stav
| CANAM |
|-------|
| 100  |



#+NAME: REZIE
#+ATTR_LATEX: :placement [H]
#+CAPTION: Rezie pocatecni stav
| Rezie |
|-------|
| 100 |

# ---------------------------------------------------------------------------------------

*  Spent sources


#+NAME: spent
#+ATTR_LATEX: :placement [H]
#+CAPTION: Ruzne
| Subj              | who |   cost | ukol       | date |
|-------------------+-----+--------+------------+------|
| /                 | <   |      < | <          |    < |
| Ridici jednotka   | VG  | 10     | LMmaterial |    8 |
| HPGe oprava       | JM  |   10   | LMsluzby   |    1 |
| cesta             | MGB |   10   | LMcesty    |    9 |
|-------------------+-----+--------+------------+------|
| Telefony          | R   |     10 | REZIE      |    0 |
|-------------------+-----+--------+------------+------|
| Beam              | MS  |    10 | CANAM      |    8 |


# ---------------------------------------------------------------------------------------


* Demanded sources

#+NAME: demand
#+ATTR_LATEX: :placement [H]
#+CAPTION: Aktualni pozadavky
 | Subj                   | who   | cost | ukol       |
 |------------------------+-------+------+------------|
 | /                      | <     |    < | <          |
 | Tiskarna               | RB    |   10 | LMmaterial |
 |------------------------+-------+------+------------|
 |          Poplatek      | J   S |   10 | LMcesty    |
 |------------------------+-------+------+------------|
 | Beam-Proton            | ChOB  |   10 | CANAM      |
 |------------------------+-------+------+------------|
 | switch                 | ST    | 10   | REZIE      |



# ---------------------------------------------------------------------------------------


* Recalculated: Original/Current/Final

 /recalculated... everytime, update tables first by C-c/


\hrule
\vskip 5mm

# just prin              see....thttps://orgmode.org/manual/Results-of-Evaluation.html
# +begin_src python :var tbl=LM0  :colnames no  :results output
# before 2020
# #+begin_src python :var tbl=LM0  :colnames no  :exports both :results value table :return t2
# 2020+ with tabulate package
# #+begin_src python :var LM0=LM0 REZIE=REZIE CANAM=CANAM :colnames no :results value raw :return tabulate(dflm, headers=dflm.columns, tablefmt='orgtbl')
#+begin_src python :var LM0=LM0 REZIE=REZIE CANAM=CANAM spent=spent demand=demand :colnames no :results output drawer replace :exports results
import pandas as pd
from tabulate import tabulate
import numpy as np

def load_ot(tbl):   # create dataframe for s0,d0....
    dflm = pd.DataFrame(tbl, columns=tbl[0]).iloc[1:, :]
    dflm.replace('', np.nan, inplace=True)
    dflm.dropna( inplace= True)
    return dflm

def output(dflm):  # print orgtable - :output drawer to get it exported later
    # round it
    dflm = dflm.applymap(lambda x: round(x, 3) if isinstance(x, (float, int)) else x)

    # add vert lines
    df1 = pd.DataFrame([[np.nan] * len(dflm.columns)], columns=dflm.columns)
    dflm = dflm.append(df1, ignore_index = True)
    dflm.iloc[-1, 0] = "/"
    for i in range(1,len(dflm.iloc[-1,0:])):
        dflm.iloc[-1,i] = "<"
    # print-----------------
    print()
    print( tabulate(dflm, headers=dflm.columns, tablefmt='orgtbl', showindex=False) )
    print() # DONE

def substract( tblname ):  #  substract fields from the last row of dflm
    # TAKE s0 and KEEP just  numeric
    dd = dfs[ tblname ][ pd.to_numeric(dfs[tblname]['cost'], errors='coerce').notnull()]
    for i,row in dd.iterrows():
        UKO = row['ukol']
        COS = row['cost']
        #print(i, COS, UKO )
        dflm[UKO].iloc[-1]-= COS

dfs = {}  #  all spendings  s-potrebovane/d-emanded
for i in ['spent','demand']:
    dfs[i] = load_ot( eval(i) )

dflm = load_ot(LM0) # MAIN
#dfca = load_ot(CANAM)  # and CANAM
#dfre = load_ot(REZIE) # and REZIE

# join
dflm['CANAM'] = load_ot(CANAM) #dfca
dflm['REZIE'] = load_ot(REZIE) #dfre
dflm = pd.concat([dflm]*2, ignore_index=True) # DUPLICATE ORIGINAL

for i in ['spent']: substract( i )
dflm = pd.concat([dflm,dflm.tail(1)]*1, ignore_index=True) # DUPLICATE ORIGINAL LASTLINE
#print(dd)

for i in ['demand']: substract( i )

#print(dflm)
#print(dfca)
output( dflm)
#output( dfca)
#output( dfre)


#+end_src


% ---------------------------------------------------------------------------------------

"""


#========================================================================================CESTOVNI

cestorg=r"""
#+OPTIONS: toc:nil        (no default TOC at all)
#+LATEX_HEADER: \usepackage[margin=23mm]{geometry}  \usepackage[utf8]{luainputenc}
#+LaTeX_HEADER: \linespread{1.0}
#+latex_class_options: [a4paper,10pt]
# ================== no page numbering
#+LATEX_HEADER: \usepackage{nopageno}
#+LATEX_HEADER: \usepackage{fancyhdr}
#+LATEX_HEADER: \usepackage{xcolor}
# =================== no indent filewide https://emacs.stackexchange.com/questions/16889/how-to-control-newline-and-indent-when-export-to-latex-from-org-mode-file
#+LATEX: \setlength\parindent{0pt}

#+LATEX_HEADER: \definecolor{ublue}{RGB}{0,147,221}

# +LATEX_HEADER: \pagestyle{fancy}
# +LATEX_HEADER: \fancyhead{}
# +LATEX_HEADER: \fancyfoot{}


#+LATEX_HEADER:\newcommand{\mysignrule}[1]{%
# +LATEX_HEADER:\vspace{3mm}
# +LATEX_HEADER:\rule{\linewidth}{0.5pt}\newline
#+LATEX_HEADER:#1\par
#+LATEX_HEADER:}


# ============================================================== START===
\normalsize





* _ZPRÁVA O VÝSLEDCÍCH A PRŮBĚHU ZAHRANIČNÍ CESTY_
  :PROPERTIES:
  :UNNUMBERED: t
  :END:
#+LATEX: \hskip 1cm

*Pracovník:* XFLD_SIGNATURE
\vskip 4mm

*Vysílající pracoviště:* XFLD_INSTITUTE
\vskip 4mm

*Navštívená země:*
\vskip 4mm

první pobyt - _opakovaný pobyt_
\vskip 4mm

*Délka pobytu:*
\vskip 4mm

*Termín:*
\vskip 4mm

*Druh dohody:*
\vskip 4mm

*Navštívená pracoviště:*
\vskip 4mm

*Účel cesty:*
\vskip 4mm

*Výsledky cesty:*

Cesta


#+LATEX: \vskip 3cm


#+LATEX:\begin{flushright}
#+LATEX:\begin{minipage}{0.45\linewidth}
#+LATEX:\mysignrule{Podpis věd. pracovníka:}
# # DOESNOT WORK!!!...Add SPACE +LATEX:\mysignrule{Datum:}
#+LATEX:\mysignrule{ Datum: XFLD_DATE}
#+LATEX:\end{minipage}
#+LATEX:\end{flushright}

#+LATEX:\begin{flushleft}
\vskip -12mm
#+LATEX:\begin{minipage}{0.45\linewidth}
#+LATEX:\mysignrule{Vedoucí oddělení:}
\vskip 10mm
#+LATEX:\mysignrule{Ředitel ústavu:}
#+LATEX:\end{minipage}
#+LATEX:\end{flushleft}

"""







############







#========================================================================================NOTE NOTICE
noteorg=r"""#+OPTIONS: toc:nil        (no default TOC at all)
#+LATEX_HEADER: \usepackage[margin=1.4in]{geometry}
#    I need czech ---------------
#+LATEX_HEADER: \usepackage[utf8]{luainputenc}
#      THIS was for cs czech
#+LaTeX_HEADER: \linespread{1.0}
#+latex_class_options: [a4paper,12pt]
# ================== no page numbering
#+LATEX_HEADER: \usepackage{nopageno}
#+LATEX_HEADER: \usepackage{fancyhdr}
#+LATEX_HEADER: \usepackage{xcolor}
# =================== no indent filewide https://emacs.stackexchange.com/questions/16889/how-to-control-newline-and-indent-when-export-to-latex-from-org-mode-file
#+LATEX: \setlength\parindent{0pt}

#+LATEX_HEADER: \definecolor{ublue}{RGB}{0,147,221}

#+LATEX_HEADER: \pagestyle{fancy}
#+LATEX_HEADER: \fancyhead{}
#+LATEX_HEADER: \fancyfoot{}
#+LATEX_HEADER: \fancyhead[C]{\footnotesize Sdělení z odd.: XFLD_DEPT, XFLD_INSTT, XFLD_SIGNATURE - XFLD_RECEPIENT}
# # +LATEX_HEADER: \fancyhead[CO,CE]{HiPo - target  beam test}
# # +LATEX_HEADER: \fancyhead[LE,RO]{\slshape \rightmark}
# # +LATEX_HEADER: \fancyhead[LO,RE]{\slshape \leftmark}
# # +LATEX_HEADER: \fancyfoot[RO, LE] {\thepage}
#+LATEX_HEADER: \fancyfoot[R]{\color{ublue}\footnotesize Mobil: XFLD_MOB}
#+LATEX_HEADER: \fancyfoot[L]{\color{ublue}\footnotesize Telefon: XFLD_TEL}
#+LATEX_HEADER: \fancyfoot[C]{\color{ublue}\footnotesize email: XFLD_EMAIL}
# # +LATEX_HEADER: \fancyfoot[RE,RO]{\it --- draft version ---}
# # +LATEX_HEADER: \headrulewidth 0pt
# # +LATEX_HEADER: \footrulewidth 0 pt

#+LATEX_HEADER: \setlength\headheight{26pt}
#+LATEX_HEADER: \lhead{\includegraphics[width=1cm]{logo.png}}



#+LATEX_HEADER:\newcommand{\mysignrule}[1]{%
# +LATEX_HEADER:\vspace{3mm}
# +LATEX_HEADER:\rule{\linewidth}{0.5pt}\newline
#+LATEX_HEADER:#1\par
#+LATEX_HEADER:}


# find . -not -path "./.stversions/*"  -iname "*org" -exec grep -H  "\.j\.:" {} \;


# ============================================================== START===
# \footnotesize Sdělení z odd.: DEP, INS ACAD, City - Name Surnme, Tit.
# +LATEX: \hskip 1cm do odd.: ředitel xxx -
\normalsize

# # ----------- this creates some vskip..... it may have been used for ^{}, but \null is better
\hphantom{}

#+LATEX: \vskip 3mm
# +LATEX: \hrule
#+LATEX: \vskip 3mm

#+LATEX: \hfill
Date: XFLD_DATE

#+LATEX: \hfill
č.j.: 04xxx/XFLD_YEAR2



* To whom it may concern:
  :PROPERTIES:
  :UNNUMBERED: t
  :END:
#+LATEX: \hskip 1cm


Hereby we declare, that


#+begin_center

Centered text

#+end_center

 protection against COVID-19 imposed by the government of the Czech Republic.

#+LATEX: \vskip 3mm

These rules - in the mentioned period of the stay -  required to present a valid negative COVID-19 test at least
once per 7 days.


#+LATEX: \vskip 3cm


#+LATEX:\begin{flushright}
#+LATEX:\begin{minipage}{0.45\linewidth}
#+LATEX:\mysignrule{XFLD_SIGNATURE}
\vskip 2mm
#+LATEX:\mysignrule{XFLD_DEPARTMENT}
#+LATEX:\mysignrule{XFLD_INSTITUTE }
#+LATEX:\mysignrule{XFLD_STREET}
#+LATEX:\mysignrule{XFLD_CITY}
#+LATEX:\mysignrule{XFLD_COUNTRY}
#+LATEX:\end{minipage}
#+LATEX:\end{flushright}


# +LATEX: \vskip 3mm
# +LATEX: \hrule

# +LATEX: \hfill

#   +LATEX: \footnote{Telefon: }


"""


############





#========================================================================================PPT
#========================================================================================PPT
#========================================================================================PPT
pptorg=r"""#+startup: beamer
\beamertemplatenavigationsymbolsempty
\setbeamertemplate{navigation symbols}{}
#+LATEX_CLASS: beamer
#+LATEX_CLASS_OPTIONS: [smaller,a4paper,presentation]

#  ============= aspect  16:9 HERE,  also handout, but problem.
#    +LATEX_CLASS_OPTIONS: [smaller,a4paper,presentation,aspectratio=169]
#                    also handout - it is possible to exclude slides/no overlay though

#+BEAMER_FRAME_LEVEL: 2
#+LATEX_HEADER: \usepackage{times} \usetheme{Madrid} \usepackage{geometry} \usepackage{lipsum} \usepackage{xcolor} \usepackage{soul} \usepackage[utf8]{luainputenc}

# utf8 -  czchar
#
#
#
# =============================== some other definitions ===========
# ---------- BG IMAGE
# \setbeamertemplate{background}
# {
#     \includegraphics[width=\paperwidth,height=\paperheight]{MyBackground.jpg}
# }

# ----------


# =============================== FOOT DEFINITION ====================

#+LATEX_HEADER: \makeatother
#+LATEX_HEADER: \setbeamertemplate{footline}
#+LATEX_HEADER: {
#+LATEX_HEADER:   \leavevmode%
#+LATEX_HEADER:   \hbox{%
#+LATEX_HEADER:   \begin{beamercolorbox}[wd=.2\paperwidth,ht=2.25ex,dp=1ex,center]{author in head/foot}%
#+LATEX_HEADER:     \usebeamerfont{author in head/foot}  XXX XXX
#+LATEX_HEADER:   \end{beamercolorbox}%
#+LATEX_HEADER:   \begin{beamercolorbox}[wd=.7\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%
#+LATEX_HEADER:     \usebeamerfont{title in head/foot}YYY YYY Workshop 20YY\hspace*{3em}
#+LATEX_HEADER:     November 29, 2022\hspace*{0ex}
#+LATEX_HEADER:   \end{beamercolorbox}}%
#+LATEX_HEADER:   \begin{beamercolorbox}[wd=.1\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%
#+LATEX_HEADER:     \usebeamerfont{title in head/foot}\hfill\insertframenumber{} / $\infty$\hspace*{1ex}
#+LATEX_HEADER:   \end{beamercolorbox}}%
#+LATEX_HEADER:   \vskip0pt%
#+LATEX_HEADER: \makeatletter





# =============================== soul <--- highlight with \hl;  xcolors <- faces
#   just gf OR bg: \textcolor{yellow}{ yellow textcolor} \colorbox{black!30}{gray colorbox}

#+LATEX_HEADER: \makeatletter
#+LATEX_HEADER: \let\HL\hl
#+LATEX_HEADER: \renewcommand\hl{%
#+LATEX_HEADER:   \let\set@color\beamerorig@set@color
#+LATEX_HEADER:   \let\reset@color\beamerorig@reset@color
#+LATEX_HEADER:   \HL}
#+LATEX_HEADER: \makeatother



#+STARTUP: showeverything
#+TITLE: Main Title \linebreak \footnotesize subtitle
#+AUTHOR: presenter: \linebreak , NPI CAS, Czech Republic
#+DATE: November 29, 2022
#+OPTIONS: TeX:t LaTeX:t toc:nil skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0
#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)


\newcommand\x{0.5}
\setlength{\unitlength}{12cm}


#+BEAMER_HEADER: \titlegraphic{  \includegraphics[height=1.cm]{logo.png}  }

:PROPERTIES:
  :EXPORT_LaTeX_CLASS: beamer
  :EXPORT_LaTeX_CLASS_OPTIONS: [presentation,smaller]
  :EXPORT_BEAMER_THEME: default
  :EXPORT_FILE_NAME: presentation.pdf
  :END:


# ======================================== colors of the main blocks (but i know all)

#+LATEX: % Change example block colors
#+LATEX: \setbeamercolor{block title example}{fg=white, bg=teal}
#+LATEX: \setbeamercolor{block body example}{ bg=teal!10!white}
#+LATEX: % Change alert block colors
#+LATEX: \setbeamercolor{block title alerted}{fg=white, bg=orange}
#+LATEX: \setbeamercolor{block body alerted}{ bg=orange!25}




# ################################ REAL SLIDES HERE
# ####### REMEMBER - ONLY UNIQUE SECTION NAMES!!!
# #    text:  \small footnotesize scriptsize tiny
# #    :BEAMER_ENV:   exampleblock
# #    :BEAMER_opt:   shrink=10   column  fullframe  alertblock  exampleblock
# #    \only vs. \uncover<1>{} ...  OR LIST #+ATTR_BEAMER: :overlay <+-> and nextline list





* The outline  \null^{nat}Fe
    :PROPERTIES:
#    :BEAMER_opt: shrink=20
    :END:

#  \hphantom^{nat}Fe  # produces an error


Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua.
#+LaTeX: \vskip 0.5cm

#+LaTeX: \setbeamercolor{myblockcolor}{bg=magenta,fg=white}
*** myblockcolor                                           :B_beamercolorbox:
:PROPERTIES:
:BEAMER_env: beamercolorbox
:END:

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua.


*** Example block
    :PROPERTIES:
    :BEAMER_env: exampleblock
    :END:
This is a green block
#  +LaTeX: \vspace{10mm}  # nafukuje box :(


*** myblockcolor                                           :B_beamercolorbox:
:PROPERTIES:
:BEAMER_env: beamercolorbox
:END:

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua.


* Two columns - only common shrink
    :PROPERTIES:
    :BEAMER_opt: shrink=5
    :END:

** Text                                                            :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_col: 0.5
    :END:

#+BEAMER: \only<1> {
#+LATEX: \scriptsize \lipsum[1-1]
#+BEAMER: }

#+BEAMER: \only<2> {
#+LATEX: \tiny \lipsum[1-1]
#+BEAMER: }

** Image
   :PROPERTIES:
   :BEAMER_env: column
   :BEAMER_col: 0.5
   :END:


#+BEAMER: \only<1> {
#+ATTR_LATEX: :options angle=0 :center true :width 0.5\textwidth
[[file:logo.png]]
#+BEAMER: }

#+BEAMER: \only<2> {
#+ATTR_LATEX:  :options angle=0 :center true :width 0.8\textwidth
[[file:logo.png]]
#+BEAMER: }


* Overlay
#+ATTR_BEAMER: :overlay <+->
 - one
 - two



* Uncover
#+BEAMER: \uncover<1-> {
#+ATTR_LATEX: :height 0.15\textheight
 [[file:logo.png]]
#+BEAMER: }

#+BEAMER: \uncover<2-> {
#+ATTR_LATEX: :height 0.15\textheight
 [[file:logo.png]]
#+BEAMER: }

* ASCI ART
    :PROPERTIES:
    :BEAMER_opt: shrink=10
    :END:


** BOX: opt shrink=10 is usefull

m-x artist-mode    - middle mouse for menu
#+BEGIN_EXAMPLE

    +------+                  +--------+  +
    |      |                  |        |  |
    | IC   +------------------+        |  |
    +------+-                 | Storage|  |
             \--          ----+--------+  |
                \--------/                |    +--------+
                |        |                |    |        |
  +--+          |  HPGe  |                |    | offline|
  |..|          +--------+  -----         |    +--------+
  |..|                     (     )        |
  +..+                      -----         |
                                          +
#+END_EXAMPLE




* Backup slide - pdf
:PROPERTIES:
:BEAMER_env: fullframe
:END:
#+ATTR_LATEX: :width 12cm
[[file:backup_slide.pdf]]

"""

# pptorg=r"""#+startup: beamer
# \beamertemplatenavigationsymbolsempty
# \setbeamertemplate{navigation symbols}{}
# #+LATEX_CLASS: beamer
# #+LATEX_CLASS_OPTIONS: [smaller,a4paper,presentation]
# #+BEAMER_FRAME_LEVEL: 2
# #+LATEX_HEADER: \usepackage{times} \usetheme{Madrid} \usepackage{geometry} \usepackage{lipsum}


# # =============================== FOOT DEFINITION ====================

# #+LATEX_HEADER: \makeatother
# #+LATEX_HEADER: \setbeamertemplate{footline}
# #+LATEX_HEADER: {
# #+LATEX_HEADER:   \leavevmode%
# #+LATEX_HEADER:   \hbox{%
# #+LATEX_HEADER:   \begin{beamercolorbox}[wd=.2\paperwidth,ht=2.25ex,dp=1ex,center]{author in head/foot}%
# #+LATEX_HEADER:     \usebeamerfont{author in head/foot}  XXX XXX
# #+LATEX_HEADER:   \end{beamercolorbox}%
# #+LATEX_HEADER:   \begin{beamercolorbox}[wd=.7\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%
# #+LATEX_HEADER:     \usebeamerfont{title in head/foot}YYY YYY Workshop 20YY\hspace*{3em}
# #+LATEX_HEADER:     November 29, 2022\hspace*{0ex}
# #+LATEX_HEADER:   \end{beamercolorbox}}%
# #+LATEX_HEADER:   \begin{beamercolorbox}[wd=.1\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%
# #+LATEX_HEADER:     \usebeamerfont{title in head/foot}\hfill\insertframenumber{} / ZZZ\hspace*{1ex}
# #+LATEX_HEADER:   \end{beamercolorbox}}%
# #+LATEX_HEADER:   \vskip0pt%
# #+LATEX_HEADER: \makeatletter



# #+STARTUP: showeverything
# #+TITLE: Main Title \linebreak \footnotesize subtitle
# #+AUTHOR: presenter: \linebreak , NPI CAS, Czech Republic
# #+DATE: November 29, 2022
# #+OPTIONS: TeX:t LaTeX:t toc:nil skip:nil d:nil todo:t pri:nil tags:not-in-toc
# #+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0
# #+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)

# \newcommand\x{0.5}
# \setlength{\unitlength}{12cm}

# #+BEAMER_HEADER: \titlegraphic{  \includegraphics[height=1.cm]{logo.png}  }

# :PROPERTIES:
#   :EXPORT_LaTeX_CLASS: beamer
#   :EXPORT_LaTeX_CLASS_OPTIONS: [presentation,smaller]
#   :EXPORT_BEAMER_THEME: default
#   :EXPORT_FILE_NAME: presentation.pdf
#   :END:

# # ################################ REAL SLIDES HERE
# # #    text:  \small footnotesize scriptsize tiny
# # #    :BEAMER_ENV:   exampleblock
# # #    :BEAMER_opt:   shrink=10   column  fullframe  alertblock  exampleblock

# * The outline
#     :PROPERTIES:
# #    :BEAMER_opt: shrink=20
#     :END:


# * Two columns with some shrink
# ** Text                                                            :B_column:
#     :PROPERTIES:
#     :BEAMER_env: column
#     :BEAMER_col: 0.5
#     :END:
# \footnotesize\lipsum[1-1]

# ** Image
#    :PROPERTIES:
#    :BEAMER_env: column
#    :BEAMER_col: 0.5
#    :BEAMER_opt: shrink=20
#    :END:
# #+ATTR_LATEX: :width 3cm
# [[file:logo.png]]


# #+BEGIN_EXAMPLE

#     +------+                  +--------+  +
#     |      |                  |        |  |
#     | IC   +------------------+        |  |
#     +------+-                 | Storage|  |
#              \--          ----+--------+  |
#                 \--------/                |    +--------+
#                 |        |                |    |        |
#                 |  HPGe  |                |    | offline|
#                 +--------+                |    +--------+
#                                           |
#                                           |
#                                           +
# #+END_EXAMPLE




# * Backup slide - pdf
# :PROPERTIES:
# :BEAMER_env: fullframe
# :END:
# #+ATTR_LATEX: :width 12cm
# [[file:backup_slide.pdf]]
# """










############




#=====================================================================================BOOK
bookorg=r"""
#+LATEX_CLASS_OPTIONS: [a6paper,10pt,parskip=half, DIV=calc, BCOR=10mm, x11names]
#+LATEX_CLASS: scrbook
#        +LATEX_HEADER: \documentclass[]{scrbook}
#+LATEX_HEADER: \usepackage[utf8]{inputenc}
#+LATEX_HEADER: \usepackage{geometry}
#+LATEX_HEADER: \geometry{ margin=27mm, bottom=10mm, top=10mm}
# -------------------------margin=27mm, bottom=10mm, top=10mm FOR BROTHER
#+LATEX_HEADER: \usepackage[T1]{fontenc}
#+LATEX_HEADER: \usepackage{lmodern, libertine}
#+LATEX_HEADER: \usepackage{amsmath, xcolor, tcolorbox, empheq}
#+LATEX_HEADER: \usepackage[ilines]{scrlayer-scrpage}
#+LATEX_HEADER: \setheadwidth[0pt]{textwithmarginpar}
#+LATEX_HEADER: \usepackage{lipsum}
#+LATEX_HEADER: %\input{slike_1} % TikZ pictures
#+LATEX_HEADER: \clearscrheadfoot
# +LATEX_HEADER: \ihead{\headmark}
# +LATEX_HEADER: \ohead{\pagemark}
#+LATEX_HEADER:
#         +LATEX_HEADER: \setcounter{page}{3}
#+LATEX_HEADER:
#+LATEX_HEADER: \addtokomafont{pagenumber}{\bfseries\Large\color{LightBlue4}}
#+LATEX_HEADER: \addtokomafont{pagehead}{\color{LightBlue4}}
#+LATEX_HEADER:
#+LATEX_HEADER: \renewcommand{\textit}[1]{\textcolor{LightBlue4}{\emph{#1}}}
#+LATEX_HEADER: %\tcbuselibrary{skins,breakable}
#+LATEX_HEADER: \tcbuselibrary{theorems}
#+LATEX_HEADER: \tcbset{colback=blue!60!green!10!white,
#+LATEX_HEADER:   colframe=LightBlue4!50!black, ams nodisplayskip}
#+LATEX_HEADER:

#+OPTIONS: num:nil
#+OPTIONS: toc:nil        (no default TOC at all)
#+OPTIONS: creator:nil
# +OPTIONS: author:nil
# +OPTIONS: date:nil
# +OPTIONS: tex:t
#+OPTIONS: tex:dvipng

#     \chapter{ONE}

* Lorem
\lipsum[1-5]
"""



############



#======================================================================= CODE
#======================================================================= CODE
#======================================================================= CODE
#
# great for running code, tangling(save2file)
#
#======================================================================= CODE
mintedcode=r"""
# ######################## CODE DEFINITIONS ############################
#+OPTIONS: toc:nil        (no default TOC at all)
# # -------------- dont interpret ^_ as in math...
#+OPTIONS: ^:nil
#+OPTIONS: _:nil

# +LATEX_HEADER: \addtolength{\textwidth}{4cm}
# +LATEX_HEADER: \addtolength{\textheight}{3cm}
# +LATEX_HEADER: \addtolength{\hoffset}{-2cm}
# +LATEX_HEADER: \addtolength{\voffset}{-3cm}


# -----------------------------------------  colored SOURCE blocks
# -----------pip install Pygments; mod .emacs
#+LaTeX_HEADER: \usepackage{minted}
#+LaTeX_HEADER: \usepackage{bookmark}
# --------- margins
#+LATEX_HEADER: \makeatletter \@ifpackageloaded{geometry}{\geometry{margin=2cm}}{\usepackage[margin=2cm]{geometry}} \makeatother
#+LaTeX_HEADER: \usemintedstyle{xcode}
#       xcode,monokai, paraiso-dark,, zenburn ... ... NOT shell /cd |
#       fruity .... NOT  + blackbg
#       colorful ... ok
#       vs ... too simple
#       inkpot ... yellowish-brown
#       vim ... pipe too light
#       gruvbox-dark,sas, stata, abap,algol, lovalace, igor, native, rainbow_dash, tango, manni, borland, autumn:), murphy, material, trac    ... italic
#       rrt ... too light green
#       perldoc, pastie, xcode, arduino ... ok (pastie,arduino (gray comments)
#  +LATEX_HEADER: % !TeX TXS-program:compile = txs:///pdflatex/[--shell-escape]


#
# ========================== this is for quote ... easy ========================
#+LaTeX_HEADER: \usepackage{etoolbox}\AtBeginEnvironment{quote}{\itshape\bf}


# =========================== new verbatim environment -------- gray example
#   tricky ... extra package needed... redefinition needed (example => verbatim)
#          ... it breaks "gray!10!white" color definitions......
#+LaTeX_HEADER: \usepackage{verbatim}
#+LaTeX_HEADER: \usepackage{framed,color,verbatim}
#+LaTeX_HEADER: \definecolor{shadecolor}{rgb}{.95, 1., .9}
#+LaTeX_HEADER: \definecolor{codecolor}{rgb}{.95, .95, .99}
#+LATEX_HEADER: \let\oldverbatim=\verbatim
#+LATEX_HEADER: \let\oldendverbatim=\endverbatim
#+LATEX_HEADER: \renewenvironment{verbatim}[1][test]
#+LATEX_HEADER: {
#+LATEX_HEADER:   \snugshade\oldverbatim
#+LATEX_HEADER: }
#+LATEX_HEADER: {
#+LATEX_HEADER:   \oldendverbatim\endsnugshade
#+LATEX_HEADER: }


# -------------- USE THIS FOR every source block ----Colors gray!10!white BROKEN by verbatim
#  +ATTR_LATEX: :options linenos,frame=single,breaklines=true,bgcolor=gray!10!white
# OK:
#  +ATTR_LATEX: :options linenos,frame=single,breaklines=true,bgcolor=shadecolor
#  +ATTR_LATEX: :options linenos,frame=single,breaklines=true,bgcolor=codecolor


#+begin_quote
3> How to do it i python? /this is quoted environment.../
#+end_quote

You can use Playwright with Python as well to automate information
extraction from web pages. Here are the general steps:

#+begin_example
pip install playwright
#+end_example


*SHELL with pipe and cd*

#+ATTR_LATEX: :options numbers=none,frame=single,breaklines=true,bgcolor=shadecolor
#+BEGIN_SRC shell
cd CAENUSBdrvB-1.5.3
make
sudo make install
lsmod | grep CAEN
#+END_SRC

ASC - terminal screen:
#+ATTR_LATEX: :options numbers=none,frame=single,breaklines=true,bgcolor=codecolor
#+BEGIN_SRC asc
009 03:02:48 0.80 MB 0:      [    ] 1: 0.21 [    ]    2.2% /1
009 03:02:49 0.93 MB 0:      [    ] 1: 0.24 [    ]    2.5% /1
009 03:02:50 0.89 MB 0:      [    ] 1: 0.23 [    ]    2.2% /1
#+END_SRC

Example block (no attr)
#+BEGIN_EXAMPLE
Connected to CAEN Digitizer Model DT5780M, recognized as board 0
ROC FPGA Release is 04.08 - Build FA29
AMC FPGA Release is 128.32 - Build F925 (fw)
#+END_EXAMPLE

* New showcase for operations


*OPERATE shortcuts*
 1. ~C-c C-v C-b~ to run all , there is a trick with ~:ver dummy=init_block~ FANTASTIC :red-circle:
 2. ~C-u C-c .~ for input time+date
 3. ~org-babel-tangle~ for output file to disk, executable. Seems does ALL tangle-files


#+ATTR_LATEX: :options frame=single,breaklines=true,bgcolor=codecolor,\scriptsize
#+begin_src python :tangle my_test_code.py :noweb yes :shebang #!/usr/bin/python3
from fire import Fire
def main():
    print("my_test_code ...  created with M-x org-babel-tangle")
    return
if __name__=="__main__":
    Fire(main)
#+end_src

#+NAME: TAB
| a | b |
|---|---|
| 1 | 2 |

#+NAME: init_block
#+ATTR_LATEX: :options frame=single,breaklines=true,bgcolor=codecolor,\scriptsize
#+begin_src python  :results replace output :session test :exports both
INIT='init was run ..  C-c C-v C-b to run all'
#+end_src

#+ATTR_LATEX: :options frame=single,breaklines=true,bgcolor=codecolor,\scriptsize
#+begin_src python :var TAB=TAB :results replace output :session test :exports both :var dummy=init_block
print(TAB )
print(INIT)
#+end_src

"""


############



#=============================================================== REPORT NO BIBLIO
#=============================================================== REPORT NO BIBLIO
#=============================================================== REPORT NO BIBLIO
#=============================================================== REPORT NO BIBLIO
#=============================================================== REPORT NO BIBLIO
report=r"""#+OPTIONS: toc:nil        (no default TOC at all)
#+OPTIONS: creator:nil
#+OPTIONS: author:nil
# +OPTIONS: date:nil
#+OPTIONS: tex:t

# # -------------- dont interpret ^_ as in math...
# # +OPTIONS: ^:nil
# # +OPTIONS: _:nil


#+LATEX_CLASS_OPTIONS: [a4paper,12pt]
#+LATEX_HEADER: \usepackage{parskip}
#+LATEX_HEADER: \usepackage[margin=2cm]{geometry}
#+OPTIONS: tex:dvipng


#+OPTIONS: toc:nil        (no default TOC at all)
# +LATEX_HEADER: \usepackage[margin=1.1in]{geometry}
#  +LaTeX_HEADER: \linespread{1.0}
# +latex_class_options: [a4paper,12pt]
# +LATEX_HEADER: \usepackage{nopageno}
#+OPTIONS: num:nil

#+BIBLIOGRAPHY: biblio
#   unsrt
#+LATEX_HEADER: \bibliographystyle{vancouver}
# use  org-reftex-citation to enter a \cite

# ----- this is for czech when using lualatex in emacs
# -------   but i cannot getit on zen
#+LATEX_HEADER:  \usepackage[utf8]{luainputenc}



* Nice \null^{nat}Mo

/Žluťoučký kůň úpěl ďábelské ódy/


#    +LATEX: \setlength\itemsep{-2.9em}
#+LATEX: \let\tempone\itemize
#+LATEX: \let\temptwo\enditemize
#+LATEX: \renewenvironment{itemize}{\tempone\addtolength{\itemsep}{-0.5\baselineskip}}{\temptwo}

 - The item 1  \cite{avrigeanuLowenergyDeuteroninducedReactions2013}
 - The item 2
 - Item 3

#+ATTR_LATEX: :align r|l|l|l|l
|               | channel | half-live | expected \gamma lines | with intensity |
|---------------+---------+-----------+----------------------+----------------|
| /             | <       | <         | <                    |                |
| \null^{91m}Mo | (d,p2n) | 1.1 m     | 652 keV              |            48% |
| \null^{92}Tc  | (d,2n)  | 4.2 m     | 773, 1509 keV        |           100% |

\vskip 5mm

#+BEGIN_center
#+ATTR_LATEX: :width 0.45\textwidth :center nil
[[file:91mMo.png]]
#+ATTR_LATEX: :width 0.45\textwidth :center nil
[[file:92Tc.png]]
#+END_center

\vskip 5mm

# ** References
\footnotesize
# \small
\bibliography{biblio}

"""


############




#=============================================================== +  REPORT's  BIBLIO
#=============================================================== +  REPORT's  BIBLIO
#=============================================================== +  REPORT's  BIBLIO
report_bib=r"""

@article{avrigeanuLowenergyDeuteroninducedReactions2013,
  title = {Low-energy deuteron-induced reactions on ${}^{93}$Nb},
  author = {Avrigeanu, M. and Avrigeanu, V. and Bém, P. and Fischer, U. and Honusek, M. and Koning, A. J. and Mrázek, J. and Šimečková, E. and Štefánik, M. and Závorka, L.},
  year = {2013},
  month = jul,
  volume = {88},
  pages = {014612},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevC.88.014612},
  abstract = {The activation cross sections of (d,p), (d,2n), (d,2np+nd+t), (d,2nα), and (d,pα) reactions on 93Nb were measured in the energy range from 1 to 20 MeV using the stacked-foil technique. Then, within a simultaneous analysis of elastic scattering and reaction data, the available elastic-scattering data analysis was carried out in order to obtain the optical potential for reaction cross-section calculations. Particular attention was paid to the description of the breakup mechanism and direct reaction stripping and pick-up, followed by pre-equilibrium and compound-nucleus calculations. The measured cross sections as well as all available deuteron activation data of 93Nb were compared with results of local model calculations carried out using the codes fresco and stapre-h and both default and particular predictions of the code talys-1.4 and tendl-2012-evaluated data.},
  file = {/home/ojr/01_Dokumenty/04_ourpublic/ZOTERO/storage/C7J6LQJC/Avrigeanu et al. - 2013 - Low-energy deuteron-induced reactions on $ ^ 93 $.pdf},
  journal = {Phys. Rev. C},
  number = {1}
}

"""


############


#============================================================================ PYTHNON CODE
#============================================================================ PYTHNON CODE
#============================================================================ PYTHNON CODE
#============================================================================ PYTHNON CODE
#============================================================================ PYTHNON CODE
#============================================================================ PYTHNON CODE

def ppt( output = None):
    """
    create presentation
    """
    if output is None:
        print(help_ppt)
    else:
        print("FILE1:  ", output )
        print("FILE2:  ", output[-4:] )
        if output[-4:] != ".org":
            print("FILE NOT ORG...", output )
            output = output+".org"
            print("FILE NOW IS ORG...", output )
        if os.path.exists(output):
            print(f"X... sorry, {output} exists")
        else:
            with open(output,"w") as f:
                f.write(pptorg)


#=================================================================================


def rep( output = None ):
    """
create report with citations
"""
    if output is None:
        print( report )
    else:
        print(help_report)
        if output[-4:] != ".org":
            output = output+".org"
        if os.path.exists(output):
            print(f"X... sorry, {output} exists")
        else:
            with open(output,"w") as f:
                f.write(report)

        output = "biblio.bib"
        if os.path.exists(output):
            print(f"X... sorry, {output} exists, but it may be ok...")
        else:
            with open(output,"w") as f:
                f.write(report_bib)



#=================================================================================


def code( output = None ):
    """
    code printing using minted; see `pygmentize -L styles`
    """
    if output is None:
        print( mintedcode )
    else:
        print(help_code)
        if output[-4:] != ".org":
            output = output+".org"
        if os.path.exists(output):
            print(f"X... sorry, {output} exists")
        else:
            with open(output,"w") as f:
                f.write(mintedcode)


#=================================================================================

def book( output = None):
    """
    Create a book on QL700
    """
    if output is None:
        print( bookorg )
    else:
        print(help_book)
        if output[-4:] != ".org":
            output = output+".org"
        if os.path.exists(output):
            print(f"X... sorry, {output} exists")
        else:
            with open(output,"w") as f:
                f.write(bookorg)

#=================================================================================

def read_note_config():
    global FNAME_CONFIG
    FNAME = os.path.expanduser( FNAME_CONFIG)
    data = {"signame":"Mahatma Gandi",
            "dept":"odd",
            "department":"oddeleni",
            "institute":"INSTITUTE",
            "inst":"INST",
            "street":"street",
            "city":"city",
            "country":"India",
            "recepient":"recepient",
            "tel":"+420 ",
            "mob":"+420 ",
            "email":"a@a",

    }
    if not os.path.exists(FNAME):
        print("X... no ",FNAME," ... creating a new one")
        with open(FNAME, "w+") as f:
            f.write( json.dumps(data, sort_keys=True, indent=4) )
        return data
    else:
        with open(FNAME) as f:
            data = json.load(f)
        return data

#=================================================================================

def note( output = None):
    """
    Create a note with the official header and signature
    """
    global noteorg
    if not os.path.exists("logo.png"):
        print("GIVE ME     logo.png")
        return

    data = read_note_config()

    noteorg = noteorg.replace("XFLD_DATE", dt.datetime.now().strftime("%d.%m.%Y") )
    noteorg = noteorg.replace("XFLD_YEAR2", dt.datetime.now().strftime("%y") )

    noteorg = noteorg.replace("XFLD_SIGNATURE",   data["signame"])
    noteorg = noteorg.replace("XFLD_TEL",         data["tel"])
    noteorg = noteorg.replace("XFLD_MOB",         data["mob"])
    noteorg = noteorg.replace("XFLD_EMAIL",       data["email"])
    noteorg = noteorg.replace("XFLD_DEPT",        data["dept"])
    noteorg = noteorg.replace("XFLD_INSTT",       data["inst"])
    noteorg = noteorg.replace("XFLD_INSTITUTE",   data["institute"])
    noteorg = noteorg.replace("XFLD_STREET",      data["street"])
    noteorg = noteorg.replace("XFLD_CITY",        data["city"])
    noteorg = noteorg.replace("XFLD_COUNTRY",     data["country"])
    noteorg = noteorg.replace("XFLD_RECEPIENT",   data["recepient"])
    noteorg = noteorg.replace("XFLD_DEPARTMENT",  data["department"])

    if output is None:
        print( noteorg )
    else:
        print(help_note)
        if output[-4:] != ".org":
            output = output+".org"
        if os.path.exists(output):
            print(f"X... sorry, {output} exists")
        else:
            with open(output,"w") as f:
                f.write(noteorg)


#=================================================================================

def cest( output = None):
    """
    Create a note with the official header and signature
    """
    global noteorg, cestorg
    data = read_note_config()

    cestorg = cestorg.replace("XFLD_DATE", dt.datetime.now().strftime("%d.%m.%Y") )
    cestorg = cestorg.replace("XFLD_SIGNATURE",   data["signame"])
    cestorg = cestorg.replace("XFLD_INSTITUTE",   data["institute"])


    if output is None:
        print( cestorg )
    else:
        print(help_cest)
        if output[-4:] != ".org":
            output = output+".org"
        if os.path.exists(output):
            print(f"X... sorry, {output} exists")
        else:
            with open(output,"w") as f:
                f.write(cestorg)
            print(f"i... {output} CREATED     ... [ok]")


#=================================================================================

def table( output = None):
    """
    Create one  table/spreadsheet for simple calc -  with help in Notes
    """

    if output is None:
        print( tableorg )
    else:
        print(help_table)
        if output[-4:] != ".org":
            output = output+".org"
        if os.path.exists(output):
            print(f"X... sorry, {output} exists")
        else:
            with open(output,"w") as f:
                f.write(tableorg)
            print("i... {output} CREATED     ... [ok]")

#=================================================================================

def calc( output = None):
    """
    Create a calculation tables - 4 sections: status/spent/demanded and calculated
    """

    if output is None:
        print( calcorg )
    else:
        print(help_calc)
        if output[-4:] != ".org":
            output = output+".org"
        if os.path.exists(output):
            print(f"X... sorry, {output} exists")
        else:
            with open(output,"w") as f:
                f.write(calcorg)
            print("i... {output} CREATED     ... [ok]")


#=================================================================================
# ----------------------------- slides ---------------

def si(image):
    """
    ppt ... return att_latex and  image link
    """
    ti=r"""

#+ATTR_LATEX: :options angle=0 :center true :width 0.7\textwidth
 [[file:image]]

"""
    im2 = image
    if im2.find(".jpg")<0:
        if im2.find(".png")<0:
            im2 = im2+".jpg"
    til = til.replace("image",im2)
    print(til)
    return


#=================================================================================


def sir(image, title="Image on RIGHT"):
    """
    ppt ... return slide with image in the right column...i think
    """
    til=r"""
# #  ________________________ AUTOGENERATED SLIDE ________ BEGIN
* REPLACEFLD_TITLE
    :PROPERTIES:
    :BEAMER_opt: shrink=1
    :END:

** Text                                                            :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_col: 0.5
    :END:

# Comments to  the image____________


*** REPLACEFLD_BLOCK
    :PROPERTIES:
    :BEAMER_env: exampleblock
    :END:

\lipsum[66]

** Image
   :PROPERTIES:
   :BEAMER_env: column
   :BEAMER_col: 0.5
   :END:

#+BEAMER: \only<1> {
#+ATTR_LATEX: :options angle=0 :center true :width 0.9\textwidth
[[file:image]]
#+BEAMER: }
# #  ________________________ AUTOGENERATED SLIDE ________ END

"""
    im2 = image
    if im2.find(".jpg")<0:
        if im2.find(".png")<0:
            im2 = im2+".jpg"
    til = til.replace( "REPLACEFLD_TITLE", title ) # Image left
    til = til.replace( "REPLACEFLD_BLOCK", os.path.splitext(im2)[0] ) # Image left
    til = til.replace("image",im2)

    print(til)
    return


#=================================================================================

def sil(image, title = "Image on LEFT"):
    "ppt ...  IMAGE ON LEFT ... TODO"
    #return sil(image)


#=================================================================================


def main():
    """
i dont know
"""
    print()



#=================================================================================
#=================================================================================
#=================================================================================
#=================================================================================
#=================================================================================
#=================================================================================
#=================================================================================

if __name__=="__main__":

    #print(read_note_config())

    Fire({"ppt":ppt,
          "sir":sir,
          "sil":sil,
          "si":si,
          "book":book,
          "rep":rep,
          "code":code,
          "cestovni":cest,
          "note":note,
          "calc":calc,
          "table":table
    })
