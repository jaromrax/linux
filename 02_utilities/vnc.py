#!/usr/bin/env python3

import subprocess as sp
from fire import Fire
import os
import sys
import stat # chmod
import shlex

geo = "500x768" #

geo = "650x400" # tablet fast
geo = "750x550" # tablet + ntb full

geo = "1100x530" # phone A30
geo = "1280x600" # phone A30 3x2 tails on terminat
geo = "1280x800" # tablet
#geo = "500x240" # phone A30 3x2 tails on terminat

#======================================================
#
#-----------------------------------------------------

def create_necessities( viewonly=False):
    """
    """
    print("i... check .vnc")
    conpath =  os.path.expanduser("~/.vnc")
    if not os.path.exists( conpath ):
        os.makedirs(conpath, exist_ok=True)

    print("i... check vncserver")
    CMD = "which vncserver"
    args = shlex.split(CMD)
    result = sp.run( args, stdout=sp.PIPE, stderr=sp.PIPE)
    if result.returncode == 0:
        print("vncserver exists at:", result.stdout.decode().strip())
    else:
        print("X... sudo -H apt -y install tightvncserver xtightvncviewer")
        sys.exit(1)

    print("i... check .vnc/passwd")
    conpass =  os.path.expanduser("~/.vnc/passwd")
    if not os.path.exists( conpass ):
        print("X... define the password:")
        print("X... for server: vncserver :2 -depth 24 -geometry 500x768")

        CMD = "which vncpasswd"
        args = shlex.split(CMD)
        result = sp.run( args, stdout=sp.PIPE, stderr=sp.PIPE)
        if result.returncode == 0:
            print("vncpasswd  at:", result.stdout.decode().strip())
        else:
            print("X... sudo -H apt -y install  tigervnc-standalone-server  tightvncserver xtightvncviewer")
            sys.exit(1)

        sys.exit(1)
    else:
        print("i... pass is ok")

    if not viewonly: # SERVER
        print("i... check xmonad ")
        CMD = "which xmonad"
        args = shlex.split(CMD)
        result = sp.run( args, stdout=sp.PIPE, stderr=sp.PIPE)
        if result.returncode == 0:
            print("xmonad exists at:", result.stdout.decode().strip())
        else:
            print("X... sudo -H apt -y install xmonad")
            sys.exit(1)

        xstartup=f"""#!/bin/sh
xrdb $HOME/.Xresources
xsetroot -solid grey
export XKL_XMODMAP_DISABLE=1
export GDK_BACKEND=x11
xmonad &
xterm
## /etc/X11/Xsession
"""
        print("i... check .vnc/xstartup")
        XSTART =  os.path.expanduser("~/.vnc/xstartup")
        if not os.path.exists( XSTART ):
            with open(XSTART,"w") as f:
                f.write(xstartup)

        print("i... check .vnc/xstartup executable")
        if os.path.isfile(XSTART) and os.access(XSTART, os.X_OK):
            print("File is executable.")
        else:
            print(f"X...  {XSTART} is not executable.")
            os.chmod(XSTART, os.stat(XSTART).st_mode | 0o111)
            print(f"i...  {XSTART} should be  executable now.")

#======================================================
#
#-----------------------------------------------------

def killvnc():
    """
    kill vnc - just "vncserver -kill :2"
    """
    create_necessities( viewonly=True)
    CMD = "vncserver -kill :2"
    args = shlex.split(CMD)
    result = sp.run( args, stdout=sp.PIPE, stderr=sp.PIPE)
    if result.returncode == 0:
        print("kill was success:", result.stdout.decode().strip())
    else:
        print("X... server either not exists or not killed anyway")
        #sys.exit(1)
    #sp.check_call(CMD.split())
    #print("i... kill was called")
    listvnc()


#======================================================
#
#-----------------------------------------------------

def startvnc( nprg = "" , video = False, portrait = False):
    """
    start vnc with a program. If no argument => no change to '~/.vnc/xstartup'
    """
    print("""
* To use tablet as a monitor over usb cable:
 - # android/version/build/7x tap
 - # new menu appears
 - apt install adb android-tools-adb android-tools-fastboot
 - adb start-server
 - #plug  the cable while looking watch
 - adb devices
 - # allow all things on android
 - adb reverse tcp:5902 tcp:5902
 - adb kill-server
#----------------------------with the hdmi deadplug
# set right monitor to 1400x900;
 x11vnc -clip xinerama1 -cursorpos -cursor X -repeat -multiptr
#  repeat and multiptr are important
""")
    global geo
    create_necessities()

    XSTART = os.path.expanduser("~/.vnc/xstartup")
    print("i... geometry:", geo)
#    print("i... password: ~/.vnc/passwd ?")
    #dbus-launch startplasma-x11 &

    print("i... RUNNING A COMMAND: /{nprg}/")
    if nprg != "":
        xstartup=f"""#!/bin/sh
xrdb $HOME/.Xresources
#xsetroot -solid grey
export XKL_XMODMAP_DISABLE=1
export GDK_BACKEND=x11
xmonad &
{nprg}
## /etc/X11/Xsession
"""
        print(xstartup)
        with open(os.path.expanduser(XSTART),"w") as f:
            f.write(xstartup)

        #sys.exit(0)
    st = os.stat(XSTART)
    print(XSTART)
    os.chmod(XSTART, st.st_mode | stat.S_IEXEC)
    if video:
        geo = geo.split("x")
        geo = [ str(int(int(x)/2)) for x in geo ]
        geo = "x".join(geo)
    if portrait:
        geo = geo.split("x")
        geo = geo[1]+"x"+geo[0]
    CMD = "nohup vncserver :2 -depth 24 -geometry "+geo
    print(CMD)
    print(CMD)
    process = sp.Popen(CMD.split(),start_new_session=True  )
    #sp.check_call(CMD.split())
    print("D.... server probably running....")
    listvnc()

#======================================================
#
#-----------------------------------------------------

def  viewvnc():
    """
    connect vnc "vncviewer :2"
    'vncviewer :2 -passwd ~/.vnc/passwd ' will be the way
    """
    print("view")
    create_necessities(viewonly=True)
    CMD = f"xtightvncviewer -passwd {os.path.expanduser('~/.vnc/passwd')}  :2"
    print(CMD)
    # CMD= "xtigervncviewer -SecurityTypes VncAuth -passwd /home/ojr/.vnc/passwd :0"
    args = shlex.split(CMD)
    print(args)
    result = sp.run( args, stdout=sp.PIPE, stderr=sp.PIPE)
    if result.returncode == 0:
        print("viewer was success:", result.stdout.decode().strip())
    else:
        print("X... viewver  problem   ")
    #CMD = "vncviewer :2"
    #sp.check_call(CMD.split())



#======================================================
#
#-----------------------------------------------------

def  listvnc():
    """
    list vnc "ps -ef"
    """
    CMD = "ps -ef"
    res = sp.check_output(CMD.split()).decode("utf8")
    res = [x for x in res.split("\n") if x.find("vnc")>0]
    res = [x for x in res if x.find("xstartup")<0]
    res = [x for x in res if x.find("vnc.py")<0]
    res = [ "\t".join(x.split()[6:]) for x in res]
    print("\n".join(res))


#======================================================
#
#-----------------------------------------------------

def adb():
    """
connect with adb: usb cable and HDMI-plug
apt install adb
apt install x11vnc
    """
    print("i... adb connection")
    res = sp.check_output( "adb devices".split() ).decode("utf8")
    print(res)
    print("i... it should be connected and allowed on tablet")
    print("i... creating reverse tunnel on port 5900")
    res = sp.check_output( "adb reverse tcp:5900 tcp:5900".split() ).decode("utf8")
    print(res)
    #res = sp.check_output( "adb devices".split() ).decode("utf8")
    CMD = "x11vnc -clip xinerama0 -cursorpos -cursor X -repeat -multiptr"
    process = sp.Popen(CMD.split(),start_new_session=True  )




#======================================================
#
#-----------------------------------------------------

if __name__=="__main__":
    Fire( {"s":startvnc,
           "k":killvnc,
           "v":viewvnc,
           "l":listvnc,
           "a":adb
           }
    )


 # 9887  2022-05-02 10:17  adb tcpip 5555
 # 9888  2022-05-02 10:18  adb devices
 # 9889  2022-05-02 10:22  adb -h
 # 9890  2022-05-02 10:22  adb --help
 # 9891  2022-05-02 10:23  watch adb devices
 # 9892  2022-05-02 10:23  kiillall zoom
 # 9893  2022-05-02 10:30  adb reverse tcp:5902 tcp:5902
 # 9894  2022-05-02 10:32  history| grep adb
