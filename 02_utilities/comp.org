* Decompression times, RPI IMAGE 32GB


#+NAME: mytable
#+PLOT: title:"Decompression"  ind: 1  dep: (2) type:1d with:histograms
| .zst  |  10 | sec. | Pi3Bplus_20210129_093624.img.10.tar.zst |
| .zst  |  13 | sec. | Pi3Bplus_20210129_093624.img.1.tar.zst  |
| .zst  |  13 | sec. | Pi3Bplus_20210129_093624.img.11.tar.zst |
| .zst  |  13 | sec. | Pi3Bplus_20210129_093624.img.12.tar.zst |
| .zst  |  13 | sec. | Pi3Bplus_20210129_093624.img.13.tar.zst |
| .zst  |  13 | sec. | Pi3Bplus_20210129_093624.img.16.tar.zst |
| .zst  |  13 | sec. | Pi3Bplus_20210129_093624.img.2.tar.zst  |
| .zst  |  13 | sec. | Pi3Bplus_20210129_093624.img.3.tar.zst  |
| .zst  |  13 | sec. | Pi3Bplus_20210129_093624.img.7.tar.zst  |
| .zst  |  13 | sec. | Pi3Bplus_20210129_093624.img.8.tar.zst  |
| .zst  |  13 | sec. | Pi3Bplus_20210129_093624.img.9.tar.zst  |
| .zst  |  14 | sec. | Pi3Bplus_20210129_093624.img.14.tar.zst |
| .zst  |  14 | sec. | Pi3Bplus_20210129_093624.img.15.tar.zst |
| .zst  |  14 | sec. | Pi3Bplus_20210129_093624.img.17.tar.zst |
| .zst  |  14 | sec. | Pi3Bplus_20210129_093624.img.4.tar.zst  |
| .zst  |  14 | sec. | Pi3Bplus_20210129_093624.img.5.tar.zst  |
| .zst  |  14 | sec. | Pi3Bplus_20210129_093624.img.6.tar.zst  |
| .zst  |  15 | sec. | Pi3Bplus_20210129_093624.img.18.tar.zst |
| .zst  |  16 | sec. | Pi3Bplus_20210129_093624.img.19.tar.zst |
| .lbz2 |  35 | sec. | Pi3Bplus_20210129_093624.img.1.tar.lbz2 |
| .lbz2 |  36 | sec. | Pi3Bplus_20210129_093624.img.2.tar.lbz2 |
| .lbz2 |  38 | sec. | Pi3Bplus_20210129_093624.img.3.tar.lbz2 |
| .lbz2 |  42 | sec. | Pi3Bplus_20210129_093624.img.4.tar.lbz2 |
| .lbz2 |  45 | sec. | Pi3Bplus_20210129_093624.img.5.tar.lbz2 |
| .lbz2 |  49 | sec. | Pi3Bplus_20210129_093624.img.6.tar.lbz2 |
| .lbz2 |  53 | sec. | Pi3Bplus_20210129_093624.img.7.tar.lbz2 |
| .lbz2 |  56 | sec. | Pi3Bplus_20210129_093624.img.8.tar.lbz2 |
| .lbz2 |  57 | sec. | Pi3Bplus_20210129_093624.img.9.tar.lbz2 |
| .gz   |  88 | sec. | Pi3Bplus_20210129_093624.img.3.tar.gz   |
| .gz   |  89 | sec. | Pi3Bplus_20210129_093624.img.2.tar.gz   |
| .gz   |  90 | sec. | Pi3Bplus_20210129_093624.img.1.tar.gz   |
| .gz   |  96 | sec. | Pi3Bplus_20210129_093624.img.6.tar.gz   |
| .gz   |  96 | sec. | Pi3Bplus_20210129_093624.img.8.tar.gz   |
| .gz   |  97 | sec. | Pi3Bplus_20210129_093624.img.5.tar.gz   |
| .gz   |  97 | sec. | Pi3Bplus_20210129_093624.img.7.tar.gz   |
| .gz   |  97 | sec. | Pi3Bplus_20210129_093624.img.9.tar.gz   |
| .gz   |  98 | sec. | Pi3Bplus_20210129_093624.img.4.tar.gz   |
| .7z   | 104 | sec. | Pi3Bplus_20210129_093624.img.9.tar.7z   |
| .7z   | 105 | sec. | Pi3Bplus_20210129_093624.img.7.tar.7z   |
| .7z   | 105 | sec. | Pi3Bplus_20210129_093624.img.8.tar.7z   |
| .7z   | 106 | sec. | Pi3Bplus_20210129_093624.img.6.tar.7z   |
| .7z   | 108 | sec. | Pi3Bplus_20210129_093624.img.5.tar.7z   |
| .7z   | 113 | sec. | Pi3Bplus_20210129_093624.img.4.tar.7z   |
| .7z   | 115 | sec. | Pi3Bplus_20210129_093624.img.3.tar.7z   |
| .7z   | 120 | sec. | Pi3Bplus_20210129_093624.img.2.tar.7z   |
| .xz   | 123 | sec. | Pi3Bplus_20210129_093624.img.9.tar.xz   |
| .xz   | 125 | sec. | Pi3Bplus_20210129_093624.img.8.tar.xz   |
| .7z   | 126 | sec. | Pi3Bplus_20210129_093624.img.1.tar.7z   |
| .xz   | 127 | sec. | Pi3Bplus_20210129_093624.img.7.tar.xz   |
| .xz   | 129 | sec. | Pi3Bplus_20210129_093624.img.5.tar.xz   |
| .xz   | 129 | sec. | Pi3Bplus_20210129_093624.img.6.tar.xz   |
| .xz   | 132 | sec. | Pi3Bplus_20210129_093624.img.4.tar.xz   |
| .xz   | 133 | sec. | Pi3Bplus_20210129_093624.img.3.tar.xz   |
| .xz   | 135 | sec. | Pi3Bplus_20210129_093624.img.2.tar.xz   |
| .xz   | 138 | sec. | Pi3Bplus_20210129_093624.img.1.tar.xz   |

#+BEGIN_SRC python :results file :exports results :var data=mytable
import matplotlib.pyplot as plt

'''If you have formatting lines on your table
(http://orgmode.org/manual/Column-groups.html) you need to remove them
"by hand" with a line like:
data = data[2:]
'''

'''Turn the table data into x and y data'''
x = [a[0] for a in data]
y1 = [ a[1] for a in data]
a, = plt.plot(x, y1, '.-',label="decompression time")

plt.xlabel("time [s]")
plt.ylabel("archive type")
plt.legend(handles=[a],loc="upper left")
plt.grid(True)
filename = "comp_decomptime_image2.png"
plt.savefig(filename)
return(filename)
#+END_SRC

#+RESULTS:
[[file:comp_decomptime_image2.png]]
