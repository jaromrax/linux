#!/usr/bin/env python3
#
"""
09_analysis   directory soft links
"""

# make directories and links by project
# last word after _ is the project name
import os

from pathlib import Path
from glob import glob
from collections import OrderedDict
import re

import sys
import time
import shutil

#-------------- I used the data from here ------------------------
# import pandas as pd
# df = pd.read_csv('https://raw.githubusercontent.com/andrejewski/periodic-table/master/data.csv')
# # print( list(df) )
# elements = list(df[' symbol'])
# elements = [ i.strip().lower() for i in elements]
# print(elements)

elements = ['h', 'he', 'li', 'be', 'b', 'c', 'n', 'o', 'f', 'ne', 'na', 'mg', 'al', 'si', 'p', 's', 'cl', 'ar', 'k', 'ca', 'sc', 'ti', 'v', 'cr', 'mn', 'fe', 'co', 'ni', 'cu', 'zn', 'ga', 'ge', 'as', 'se', 'br', 'kr', 'rb', 'sr', 'y', 'zr', 'nb', 'mo', 'tc', 'ru', 'rh', 'pd', 'ag', 'cd', 'in', 'sn', 'sb', 'te', 'i', 'xe', 'cs', 'ba', 'la', 'ce', 'pr', 'nd', 'pm', 'sm', 'eu', 'gd', 'tb', 'dy', 'ho', 'er', 'tm', 'yb', 'lu', 'hf', 'ta', 'w', 're', 'os', 'ir', 'pt', 'au', 'hg', 'tl', 'pb', 'bi', 'po', 'at', 'rn', 'fr', 'ra', 'ac', 'th', 'pa', 'u', 'np', 'pu', 'am', 'cm', 'bk', 'cf', 'es', 'fm', 'md', 'no', 'lr', 'rf', 'db', 'sg', 'bh', 'hs', 'mt', 'ds', 'rg', 'cn', 'nh', 'fl', 'mc', 'lv', 'ts', 'og']


DIR = os.getcwd()

if DIR == os.path.expanduser("~/09_DATA_ANALYSIS"):
    DIR_BYPROJECT = DIR + "/" + "by_project"
    DIR_BYISOTOPE = DIR + "/" + "by_isotope"
elif DIR == os.path.expanduser("~/02_GIT/GITLAB/linux/03_applications"):
    DIR_BYPROJECT = DIR + "/" + "by_tag"
    DIR_BYISOTOPE = "" #DIR + "/" + "by_"  #DIR + "/" + "by_isotope"
else:
    print("X... unknown current DIR to me.")
    sys.exit(1)


def extract_tag(name):
    res = name.split("_")[-1].lower()
    return res

def extract_isotope(name):
    res = name.split("_")
    #print("Extr:",name, res)
    isolist = []
    for i in res:
        di0 = ""
        le0 = ""
        #print("\n",i,":", end = " ")
        if (i[0].isdigit()) and not(i[-1].isdigit()):
            di = re.findall(r'^\d+', i)
            if len(di)>1: return # only one number allowed
            if len(di[0])>3:
                continue
            le = re.findall(r'\D+$', i)
            di0=di[0]
            le0=le[0].lower()
            if len(le0)>2:
                continue
            #print(di0+le0 )
        elif (i[-1].isdigit()) and not(i[0].isdigit()):
            di = re.findall(r'\d+$', i)
            if len(di)>1: return # only one number allowed
            if len(di[0])>3:
                continue
            le = re.findall(r'^\D+', i)
            di0=di[0]
            le0=le[0].lower()
            if len(le0)>2:
                continue
            #print(di0+le0)
        elif  not(i[-1].isdigit()) and not(i[0].isdigit()) and (len(i)<3):
            le = re.findall(r'^\D+$', i)
            le0=le[0].lower()
            #print(le0, name)
        else:
            continue
        # --- made it to here: check the correct le0
        if not le0 in elements:
            continue
        Z = "Z{:03d}".format(elements.index(le0)+1)
        isolist.append( Z+"_"+le0+di0 )
        #print(di)

    return isolist


################################################
################################################
#                                              #
################################################
################################################

if __name__=="__main__":

    # just extra security against some dumb error

    if (DIR == DIR_BYPROJECT) or (DIR==DIR_BYISOTOPE):
        print("!!! PANIC IN PROGRAM CODE !!!!")
        sys.exit(1)
    if (len(DIR)<7):
        print("!!! PANIC IN PROGRAM DIR NAME !!!!")
        sys.exit(1)
    if (len(DIR_BYPROJECT)<7+6):
        print("!!! PANIC IN PROGRAM 'PROJECT' DIR NAME !!!!")
        sys.exit(1)
    if (len(DIR_BYISOTOPE)<7+6):
        print("!!! PANIC IN PROGRAM 'ISOTOPE' DIR NAME !!!!")
        print("... but ok... I can go ...")
        time.sleep(2)
    # now the removeal of directories starts !!!!!!!!!!!!!!!!!

    if (os.path.exists(DIR_BYPROJECT)) or (os.path.exists(DIR_BYISOTOPE)):
        if  (os.path.exists(DIR_BYPROJECT)):
            print("X... remove the {} first !".format(DIR_BYPROJECT) )
        if (os.path.exists(DIR_BYISOTOPE)):
            print("X... remove the {} first !".format(DIR_BYISOTOPE) )
        # sys.exit()
        for i in range(5,-1,-1):
            print("{}s - DELETING {} and {} !!!".format(i, DIR_BYPROJECT, DIR_BYISOTOPE) )
            time.sleep(1)
        print("ALL ... ", end="")
        if  (os.path.exists(DIR_BYPROJECT)):
            shutil.rmtree(DIR_BYPROJECT)
            print("... by_project ...", end="")
        if (os.path.exists(DIR_BYISOTOPE)):
            shutil.rmtree(DIR_BYISOTOPE)
            print("... by_isotope ", end="" )
        print("IS DELETED NOW")

    print("i... creating", DIR_BYPROJECT)
    if (len(DIR_BYPROJECT)>7+6):
        Path(DIR_BYPROJECT).mkdir(exist_ok=False)
    print("i... creating", DIR_BYISOTOPE)
    if (len(DIR_BYISOTOPE)>7+6):
        Path(DIR_BYISOTOPE).mkdir(exist_ok=False) # False - crash is exists


    # ANALYZE THE DIRECTORY ----------------------

    all_sub_dir_paths = glob(str(DIR) + '/*/') # returns list of sub directory paths
    dirs = [Path(sub_dir).name for sub_dir in all_sub_dir_paths]
    dirs = sorted(dirs)

    # print(dirs)
    #----------- i will only use dirs starting with 20***
    dirs = [ i for i in dirs if i.find("20")==0]

    dirlist = []
    for i in dirs:
        newdir = extract_tag(i)
        dirlist.append(newdir)
        #print(newdir)

    ulist = sorted(list(OrderedDict.fromkeys(dirlist)))
    #print(dirs) # all internal folders
    print(ulist) # TAGS
    #-----------------------here I have the final set of allowed directories ------


    #
    #----------------- create new symlinks in by_project
    for i in ulist:
        Path(DIR_BYPROJECT+"/"+i).mkdir(exist_ok=True)
    print("by_projects")
    for i in dirs:
        src = DIR + "/" +i
        dst =  DIR_BYPROJECT +"/" + extract_tag(i)+"/"+i
        # print(src, dst)
        if not os.path.islink(dst):
            os.symlink(src,dst)
            print(".", end="")
        else:
            print("x", end="")


    #---- if teh ISOTOPE DIR IS REASONABLE......
    if (len(DIR_BYISOTOPE)>=7+6):
        #----------------- create new symlinks in by_isotope
        print("\nby isotopes")
        for i in dirs:
            # print(i,":")
            r = extract_isotope(i)
            if len(r)!=0:
                #print(">",r)
                for j in r:
                    Path(DIR_BYISOTOPE+"/"+j).mkdir(exist_ok=True)
                    src = DIR + "/" +i
                    dst = DIR_BYISOTOPE +"/" + j +"/"+i
                    if not os.path.islink(dst):
                        os.symlink(src,dst)
                        #print(dst)
                        print(".", end="")
                    else:
                        print("x", end="")
