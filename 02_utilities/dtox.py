#!/usr/bin/env python3

from fire import Fire
from glob import glob
import os
import subprocess as sp
from console import fg
import sys
from unidecode import unidecode

def run_uconv( inp):
    # res = uconv -x 'Any-Latin;Latin-ASCII'
    ###result = sp.run(['uconv', '-x', 'Any-Latin;Latin-ASCII', inp], capture_output=True, text=True)
    #outputs.append(result.stdout)
    #run(['uconv', '-x', 'Any-Latin;Latin-ASCII', file])
    result = sp.run(['uconv', '-x', 'Any-Latin;Latin-ASCII'], input=inp, capture_output=True, text=True)
    #print(result)
    return result.stdout

def badchars( inp ):
    chktext=inp
    chktext=chktext.replace("?","")
    chktext=chktext.replace("!","")
    #chktext=chktext.replace(".","")
    chktext=chktext.replace(",","")
    chktext=chktext.replace("   ","")
    chktext=chktext.replace("  ","")
    chktext=chktext.replace(" ","_")
    #chktext=chktext.replace("/","_")
    chktext=chktext.replace('"',"_")
    chktext=chktext.replace("'","") # Cest

    #if inp.find("KarelK") >= 0: print("  >", chktext)

    badchar=" :<>`~!@#$%&*+=–!-,–… ?--«»()‘…{}[]'"  # canbe /_

    for i in range(len(badchar)):
        chktext=chktext.replace( badchar[i] ,"_")
        #if inp.find("KarelK") >= 0: print("  >", chktext)

    chktext=chktext.replace("____","_")
    chktext=chktext.replace("___","_")
    chktext=chktext.replace("__","_")
    #if inp.find("KarelK") >= 0: print("  >", chktext)

    return chktext


def increment_string(s):
    s = list(s)
    i = len(s) - 1
    while i >= 0:
        if s[i] == 'z':
            s[i] = 'a'
            i -= 1
        else:
            s[i] = chr(ord(s[i]) + 1)
            break
    return ''.join(s)


def create_unique_filename(filepath, newf):
    dir_name, file_name = os.path.split(filepath)
    base, ext = os.path.splitext(file_name)
    suffix = 'aaa'

    if not ext:
        new_filepath = os.path.join(dir_name, f"{base}_{suffix}")
    else:
        new_filepath = os.path.join(dir_name, f"{base}_{suffix}{ext}")

    print( "    dedup:", new_filepath)
    while new_filepath  in newf:#os.path.exists(new_filepath):
        print( "    !!!  ", new_filepath)
        #suffix += 1#= chr(ord(suffix) + 1)
        #for _ in range(5):  # Adjust the range as needed
        suffix = increment_string(suffix)
        if not ext:
            new_filepath = os.path.join(dir_name, f"{base}_{suffix}")
        else:
            new_filepath = os.path.join(dir_name, f"{base}_{suffix}{ext}")

    return new_filepath

# def create_unique_filename(filepath):
#     dir_name, file_name = os.path.split(filepath)
#     base, ext = os.path.splitext(file_name)
#     suffix = 'a'

#     new_filepath = os.path.join(dir_name, f"{base}_{suffix}{ext}")
#     while os.path.exists(new_filepath):
#         suffix = chr(ord(suffix) + 1)
#         new_filepath = os.path.join(dir_name, f"{base}_{suffix}{ext}")

#     return new_filepath

def isduplicate( newf ):
    duplicates = [item for item in set(newf) if newf.count(item) > 1]
    if len(duplicates) > 0:
        return True
    else:
        return False

def deduplicate( newf ):
    if isduplicate(newf):
        newf[-1] = create_unique_filename(newf[-1], newf)



# Function to replace all matching parts of VATA in filename with "_"
def replace_vata(filename ):
    """
    jedna blbost
    """
    VATA = "Kukaj to - Raj online filmov a serialov"
    for i in range(len(VATA), 6, -1):
        if VATA[:i] in filename:
            filename = filename.replace(VATA[:i], "_")
    return filename


def rename_wise(old, new, dry=True):
    # Remember the current directory
    current_dir = os.getcwd()
    # Extract the directory and file names
    old_dir = os.path.dirname(old)
    old_file = os.path.basename(old)
    new_file = os.path.basename(new)
    # Change to the directory
    if old_dir is not None and old_dir != "":
        os.chdir(old_dir)
    # Rename the file
    print( f"{fg.cyan}{os.getcwd()}{fg.default} : {old_file} --->>>  {new_file} ", end="")
    if not dry:
        if os.path.exists(old_file):
            try:
                os.rename(old_file, new_file)
                print( fg.green, " -- OK!*: ", fg.default)
            except:
                print(fg.red, " XXX ", fg.default)
        else:
            print(fg.red, " no file ", fg.default)
        #print( f"X... FAILED TO RENAME {old_file}  ***   {new_file} ")
        # Change back to the original directory
    else:
        print()
    os.chdir(current_dir)


def main( really=False,*args):
    unchanged=0
    if args:
        print(f"X... I need no arguments BUT -r  for  --really: ")
        sys.exit(0)
        raise ValueError(f"X... I need no arguments : {args}")
    currf = glob("**/*", recursive=True)
    currf.sort(key=lambda x: x.count(os.sep), reverse=True)
    newf = []
    for i in currf:
        apc = i
        apc = replace_vata(apc) # jedna blbost
        apc = run_uconv(apc)  # uconv - Latin
        apc=unidecode(apc)
        apc = badchars(apc) #
        newf.append( apc  )
        deduplicate(newf)
        if i != newf[-1]:
            #print(f"{i:80s} *** {newf[-1]} ")
            rename_wise(i, newf[-1], dry=not really)
        else:
            unchanged+=1
    print("i... unchanged == ",unchanged)
    #duplicates = [item for item in set(newf) if newf.count(item) > 1]

if __name__ == "__main__":
    Fire(main)
