#!/usr/bin/env python3

import os
import hashlib
import math
import time
import sys
from fire import Fire

PASSLOCATION = "/home/HIBP/pwned-passwords-sha1-ordered-by-hash-v8.txt"
mypasswords = [
    "1234",
    "12345",
    "123456",
    "password",
    "michel",
    "obama",
    "andrejbabis",
    "babis1",
    "babis2",
    "Babis1",
    "Babis2-ok",
    "57andrejbabis-ok"
]

FOUND={}


pwned_passwords_files = [
        PASSLOCATION
        ]
#    "pwned-passwords-update-1.txt",
#    "pwned-passwords-update-2.txt",
#    "pwned-passwords-1.0.txt"
#]

def getFileSize(filename):
    return os.path.getsize(filename)

def getLineLength(filename):
    with open(filename, "r") as file:
        line = file.readline()
        #line = file.readline()
        return file.tell()
    # 123456 has  37 359 195 hits

def jump_line(file, n):
    line = file.readline()


def get_line(file, lenhash , passw = "", hash1 = ""):
    #print()
    line = file.readline().strip()
    if len(line)==0:
        file.seek( file.tell()-46)
        line = file.readline().strip()
        #print("!",line,len(line))

    if len(line)==0:
        file.seek( file.tell()-26)
        line = file.readline().strip()
        #print("!!",line, len(line), passw,hash1 )

    if len(line)<10:
        file.seek( file.tell()-26)
        line = file.readline().strip()
        #print("!!!",line, len(line), passw,hash1 )

    if len(line)==0:
        return "ggggggggggggggg1"

    #print(f"  #1    {line:>40s} ... len=={len(line)})", lenhash, passw, hash1)

    if ":" in line and line.find(":")>1:
        pq = file.tell()
        a = line.strip().split(":")[0] # appended str
        b = line.strip().split(":")[1] # appended str
        al = len(a)
        bl = len(b)

        pqn = pq - (lenhash+bl+3)
        #pq = pq - (lenhash -a )
        #print( f"  {al:02d}~ {a:>40s}   {pq}  {pqn} ")
        #print(":")
        file.seek(pqn)
    # elif len(line)<10: # :37 356 195 for pass 123456
    #     print("S", line)
    line = file.readline().strip()
    #print("  #2 ",line, len(line))
    return line




def cmp_line( mhash, line ):
    a = line.strip().split(":")[0]
    if mhash == a: ret =0
    if mhash < a: ret =-1
    if mhash > a: ret =1
    return ret

def searchForPass(password):
    global FOUND
    FOUND[password]=None
    #print(FOUND)

    #print()
    pass_hash = hashlib.sha1(password.encode("utf8")).hexdigest().upper()
    maxlinelen = len(pass_hash)+10
    print(f".. {password:16}  {pass_hash}")
    ##print("Searching for SHA1 hash: %s" % pass_hash)

    for password_file in pwned_passwords_files:
        #print("Searching in: %s" % password_file)

        filesize = getFileSize(password_file)
        line_length = getLineLength(password_file)
        line_length = maxlinelen
        line_number = filesize / line_length
        line_number = 847223402
        ##print("File size is: %s bytes, line length is %s bytes, line number is %s" % (filesize, line_length, line_number))
        pos_from = 0
        pos_to = line_number

        #print(f"@@@@  {pass_hash} {password}")
        divi = 0
        with open(password_file, "r") as file:
            while True:
                current_pos = (pos_to + pos_from) / 2
                current_pos = math.floor(current_pos)
                divi+=1
                file.seek(current_pos * line_length)
                line = get_line(file, len(pass_hash), password, pass_hash)
                #print(line)
                pwned_hash = line.strip().split(":")[0]
                #print(f"Go to {pwned_hash} line {current_pos} /  {divi}")

                if pwned_hash == pass_hash:
                    #print(f"@@@@  {pwned_hash} {password}")
                    #print(f"@@@@  {pass_hash}  {line}")
                    FOUND[password] = line
                    return

                if abs(pos_to - pos_from) < 1:
                    break

                if pwned_hash < pass_hash:
                    pos_from = current_pos
                    if abs(pos_to - pos_from) <= 1:
                        pos_from = pos_to
                else:
                    pos_to = current_pos
                    if abs(pos_to - pos_from) <= 1:
                        pos_to = pos_from
                        print(f":      {pass_hash}          {password}")


            line = get_line(file, len(pass_hash), password, pass_hash)
            while  cmp_line(pass_hash, line) == -1:
                file.seek( file.tell()-1*len(pass_hash)-15)
                line = get_line(file, len(pass_hash))
                #print("  ^^ ")
            if  cmp_line(pass_hash, line) !=0:

                if cmp_line(pass_hash, line) != 1:
                    print(pass_hash)
                    print(line)
                    print("CODING PROBLEM=====================")
                    sys.exit(1)
                while cmp_line(pass_hash, line) == 1:
                    line = get_line(file, len(pass_hash) , password, pass_hash)
                    #print(";     ",line)
                    if cmp_line(pass_hash, line)==0:
                        FOUND[password] = line
                    file.seek( file.tell() +1)
            else:
               FOUND[password] = line


def searchone(passw):
    searchForPass(passw)
    print("__________________________________Result:")
    if len(FOUND)>0:
        print(FOUND[passw])
    else:
        print("NOT FOUND")


def printout():
    """
    print it
    """
    print()
    print("_"*58)
    for i in FOUND.keys():
        print(f">> {i:16}  {FOUND[i]}")



def do_file( mypasswords , save = False):
    """
    check file - line by line - SAVE TO OUT.TXT!!
    """
    with  open("out.txt","w") as fw:
        ip = 0
        for pwd in mypasswords:
            print(f"\n{pwd:18s}   ========================================")
            searchForPass(pwd)
            if save:
                fw.write(f"{pwd:17s} {FOUND[pwd]}"+"\n")
            #time.sleep(0.21)
            ip+=1
            #if ip>1000: break
    printout()


def parse_org( fname):
    with open(fname) as f:
        al=f.readlines()
    al = [ x.strip()  for x in al ]
    al = [ x.split("=")[1] for x in al if len(x)>0]
    #print(al)
    do_file( al , False )

def parse_txt( fname):
    with open(fname) as f:
        al=f.readlines()
    al = [ x.strip()  for x in al if len(x)>0]
    al = [ x.strip()  for x in al if len(x)>0]
    # al = [ x.split("=")[1] for x in al ]
    #print(al)
    do_file( al, False )


if __name__=="__main__":
    # searchForPass("friendofemily")
    Fire( {"txt":parse_txt,
           "org":parse_org,
           "one":searchone})
