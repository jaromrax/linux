#!/usr/bin/python3
import os
import subprocess
import argparse
import re
####################################
#
#   i create terminat.zen etc.
#   ctrl-alt-f  shortcut to operate
#
#
####################################
#  to add a layout:
#   start terminator
#     create layoout, go to preferences/layouts ADD and save it
#      open  .config/terminator/config
#        copy that part:  put X0:Y0 and DX,DY
#        and CMD1;/bin/bash ...... CMDlast;/bin/bash
#        the LayoutXX  must start with [[special]]...


HELP="""terminator
 PROBLEM WITH SLEEP 1:   account-daemon on high!!!!

  -l 21    # horizontal split
  -l 12    # vertical split
  -l 31
  -l 321   # 3 windows 2 on left, one on right
  -l 22
  -l 32
  -l 42
./terminat.py -l 321 -g 1700x1000+1+1
 ./terminat.py -l 32 -f -g 1700x1000+1+1 -c "iftop -i wlan0","iftop -i eth1","date;sleep 1","date +%H:%M:%S| toilet ; sleep 1"

"""

parser=argparse.ArgumentParser(description="",usage=HELP,
 formatter_class=argparse.RawTextHelpFormatter)

parser.add_argument('-f','--fullscreen',  action='store_true')
#parser.add_argument('-h','--help',  action='store_true')
#parser.add_argument('--feature', dest='feature', )
parser.add_argument('-l','--layout',  default="None" , help='')
parser.add_argument('-c','--command',  default="bash" , help='')
parser.add_argument('-g','--geometry',  default="800x600+2+20" , help='')
args=parser.parse_args()

#if args.help:
#    print()
#    quit()

CONFIG=os.path.expanduser("~/.config/terminator")
if not os.path.exists( CONFIG ):
    os.makedirs(CONFIG)
CONFIG=CONFIG+"/config"
print("i... ", CONFIG)


###########  this somehow must be there else no command
Layout00="""
  [[default]]
    [[[child0]]]
      fullscreen = False
      last_active_term = 80d9e811-7995-4c4d-a64c-857bfc909231
      last_active_window = True
      maximised = False
      order = 0
      parent = ""
      position = 0:0
      size = 640,400
      title = I_AM_CHILD0DEFAULT
      type = Window
    [[[terminal1]]]
      order = 0
      parent = child0
      profile = default
      type = Terminal
      uuid = 80d9e811-7995-4c4d-a64c-857bfc909231
"""

########################################

Layout11="""
  [[special]]
    [[[child0]]]
      fullscreen = False
      last_active_term = 80d9e811-7995-4c4d-a64c-857bfc909231
      last_active_window = True
      maximised = False
      order = 0
      parent = ""
      position = X0:Y0
      size = DX,DY
      title = I_AM_CHILD0
      type = Window
    [[[terminal1]]]
      command = CMD1;/bin/bash
      directory = ~/
      order = 0
      parent = child0
      profile = colorprof
      type = Terminal
      uuid = 80d9e811-7995-4c4d-a64c-857bfc909231
"""


Layout21="""
  [[special]]
    [[[child0]]]
      fullscreen = False
      last_active_term = bedd8394-67ac-496c-8ea8-d482cc6789ec
      last_active_window = True
      maximised = False
      order = 0
      parent = ""
      position = X0:Y0
      size = DX,DY
      title = I_AM_CHILD0
      type = Window
    [[[child1]]]
      order = 0
      parent = child0
      ratio = 0.3
      type = HPaned
    [[[terminal2]]]
      command = CMD1;/bin/bash
      directory = ~/
      order = 0
      parent = child1
      profile = colorprof
      type = Terminal
      uuid = bedd8394-67ac-496c-8ea8-d482cc6789ec
    [[[terminal3]]]
      command = CMD2;/bin/bash
      directory = ~/
      order = 1
      parent = child1
      profile = colorprof
      type = Terminal
      uuid = bde6933f-63a7-4d49-85e1-a4f24bc54547
      """

Layout12="""
  [[special]]
    [[[child0]]]
      fullscreen = False
      last_active_term = 36943b13-e43e-4f7c-8ad3-3f37626041c0
      last_active_window = True
      maximised = False
      order = 0
      parent = ""
      position = X0:Y0
      size = DX,DY
      title = I_AM_CHILD0
      type = Window
    [[[child1]]]
      order = 0
      parent = child0
      position = 11
      ratio = 0.11
      type = VPaned
    [[[terminal2]]]
      command = CMD1;/bin/bash
      directory = ~/
      order = 1
      parent = child1
      profile = colorprof
      type = Terminal
      uuid = 36943b13-e43e-4f7c-8ad3-3f37626041c0
    [[[terminal3]]]
      command = CMD2;/bin/bash
      directory = ~/
      order = 0
      parent = child1
      profile = colorprof
      type = Terminal
      uuid = 5dda9093-2575-4bf4-ac49-1a0c38eb1eb6
"""

# 13
# 23
Layout321="""
  [[special]]
    [[[child0]]]
      fullscreen = False
      last_active_term = d5e190bd-b97a-4056-a6ce-292e3c8497e7
      last_active_window = True
      maximised = False
      order = 0
      parent = ""
      position = X0:Y0
      size = DX,DY
      title = I_AM_CHILD0
      type = Window
    [[[child1]]]
      order = 0
      parent = child0
      position = 730
      ratio = 0.5
      type = HPaned
    [[[child2]]]
      order = 0
      parent = child1
      position = 441
      ratio = 0.5
      type = VPaned
    [[[terminal3]]]
      command = CMD1;/bin/bash
      directory = ~/
      order = 0
      parent = child2
      profile = colorprof
      type = Terminal
      uuid = feee8232-1b45-42c1-9235-6ff3586a9df2
    [[[terminal4]]]
      command = CMD2;/bin/bash
      directory = ~/
      order = 1
      parent = child2
      profile = colorprof
      type = Terminal
      uuid = eb32c7df-282a-4222-86e1-1c6da786d1fc
    [[[terminal5]]]
      command = CMD3;/bin/bash
      directory = ~/
      order = 1
      parent = child1
      profile = colorprof
      type = Terminal
      uuid = d5e190bd-b97a-4056-a6ce-292e3c8497e7
      """

Layout22="""
  [[special]]
    [[[child0]]]
      fullscreen = False
      last_active_term = ad101f59-2081-428b-a567-42641ac648eb
      last_active_window = True
      maximised = False
      order = 0
      parent = ""
      position = X0:Y0
      size = DX,DY
      title = I_AM_CHILD0
      type = Window
    [[[child1]]]
      order = 0
      parent = child0
      position = 441
      ratio = 0.5
      type = VPaned
    [[[child2]]]
      order = 0
      parent = child1
      position = 730
      ratio = 0.5
      type = HPaned
    [[[child5]]]
      order = 1
      parent = child1
      position = 730
      ratio = 0.5
      type = HPaned
    [[[terminal3]]]
      command = CMD1;/bin/bash
      directory = ~/
      order = 0
      parent = child2
      profile = colorprof
      type = Terminal
      uuid = ad101f59-2081-428b-a567-42641ac648eb
    [[[terminal4]]]
      command = CMD2;/bin/bash
      directory = ~/
      order = 1
      parent = child2
      profile = colorprof
      type = Terminal
      uuid = 0a23a969-6deb-494b-bb68-13172f85cf17
    [[[terminal6]]]
      command = CMD3;/bin/bash
      directory = ~/
      order = 0
      parent = child5
      profile = colorprof
      type = Terminal
      uuid = 366b7598-aee0-416d-bc8a-6a3ddb7b5bd3
    [[[terminal7]]]
      command = CMD4;/bin/bash
      directory = ~/
      order = 1
      parent = child5
      profile = colorprof
      type = Terminal
      uuid = 736f79c8-4565-4346-a2f6-7676adc3c86e
      """

### three in a row
Layout31="""
  [[special]]
    [[[child0]]]
      fullscreen = False
      last_active_term = 4fe42348-ec23-4c0f-9f9d-a34a77c7c927
      last_active_window = True
      maximised = False
      order = 0
      parent = ""
      position = X0:Y0
      size = DX,DY
      title = I_AM_CHILD0
      type = Window
    [[[child1]]]
      order = 0
      parent = child0
      position = 548
      ratio = 0.324117647059
      type = HPaned
    [[[child3]]]
      order = 1
      parent = child1
      position = 581
      ratio = 0.509598603839
      type = HPaned
    [[[terminal2]]]
      command = CMD1;/bin/bash
      directory = ~/
      order = 0
      parent = child1
      profile = colorprof
      type = Terminal
      uuid = 4fe42348-ec23-4c0f-9f9d-a34a77c7c927
    [[[terminal4]]]
      command = CMD2;/bin/bash
      directory = ~/
      order = 0
      parent = child3
      profile = colorprof
      type = Terminal
      uuid = 7a875db0-7185-431c-84f2-8d7223b0ffa8
    [[[terminal5]]]
      command = CMD3;/bin/bash
      directory = ~/
      order = 1
      parent = child3
      profile = colorprof
      type = Terminal
      uuid = 52bae30d-30dd-464c-8d3e-c2d57fa70b73
      """





### Thre in one column
Layout13="""
 [[special]]
    [[[child0]]]
      fullscreen = False
      last_active_term = 80d9e811-7995-4c4d-a64c-857bfc909231
      last_active_window = True
      maximised = False
      order = 0
      parent = ""
      position = X0:Y0
      size = DX,DY
      title = I_AM_CHILD0DEFAULT
      type = Window
    [[[child1]]]
      order = 0
      parent = child0
      position = 427
      ratio = 0.309798270893
      type = VPaned
    [[[child3]]]
      order = 1
      parent = child1
      position = 461
      ratio = 0.485863874346
      type = VPaned
    [[[terminal2]]]
        command = CMD1;/bin/bash
    order = 0
      parent = child1
      profile = colorprof
      type = Terminal
      uuid = 80d9e811-7995-4c4d-a64c-857bfc909231
    [[[terminal4]]]
      command = CMD2;/bin/bash
       profile = colorprof
    order = 0
      parent = child3
      type = Terminal
      uuid = f05092dc-baa2-4e87-9450-e410b3533411
    [[[terminal5]]]
        command = CMD3;/bin/bash
    order = 1
      parent = child3
      profile = colorprof
      type = Terminal
      uuid = 47725717-e6fd-458c-9ad8-1e65799d4710
"""


Layout32="""
  [[special]]
    [[[child0]]]
      fullscreen = False
      last_active_term = 4fe42348-ec23-4c0f-9f9d-a34a77c7c927
      last_active_window = True
      maximised = False
      order = 0
      parent = ""
      position = X0:Y0
      size = DX,DY
      title = I_AM_CHILD0
      type = Window
    [[[child1]]]
      order = 0
      parent = child0
      position = 602
      ratio = 0.323702514714
      type = HPaned
    [[[child2]]]
      order = 0
      parent = child1
      position = 497
      ratio = 0.5
      type = VPaned
    [[[child5]]]
      order = 1
      parent = child1
      position = 638
      ratio = 0.508326724822
      type = HPaned
    [[[child6]]]
      order = 0
      parent = child5
      position = 497
      ratio = 0.5
      type = VPaned
    [[[child9]]]
      order = 1
      parent = child5
      position = 497
      ratio = 0.5
      type = VPaned
    [[[terminal10]]]
      command = CMD1;/bin/bash
      directory = ~/
      order = 0
      parent = child9
      profile = colorprof
      type = Terminal
      uuid = 52bae30d-30dd-464c-8d3e-c2d57fa70b73
    [[[terminal11]]]
      command = CMD2;/bin/bash
      directory = ~/
      order = 1
      parent = child9
      profile = colorprof
      type = Terminal
      uuid = cc0f64c1-902c-4c3d-86e7-6b2342af1b3d
    [[[terminal3]]]
      command = CMD3;/bin/bash
      directory = ~/
      order = 0
      parent = child2
      profile = colorprof
      type = Terminal
      uuid = 4fe42348-ec23-4c0f-9f9d-a34a77c7c927
    [[[terminal4]]]
      command = CMD4;/bin/bash
      directory = ~/
      order = 1
      parent = child2
      profile = colorprof
      type = Terminal
      uuid = 16076945-8c59-498f-ad3f-bb8ba4d9fbc2
    [[[terminal7]]]
      command = CMD5;/bin/bash
      directory = ~/
      order = 0
      parent = child6
      profile = colorprof
      type = Terminal
      uuid = 7a875db0-7185-431c-84f2-8d7223b0ffa8
    [[[terminal8]]]
      command = CMD6;/bin/bash
      directory = ~/
      order = 1
      parent = child6
      profile = colorprof
      type = Terminal
      uuid = feb8447a-df24-4317-98ff-7efda3d270e2
      """



################## this is 2x3rows

Layout23="""
  [[special]]
    [[[child0]]]
      fullscreen = False
      last_active_term = f04fb36b-50d7-42b8-9a56-222520ccac04
      last_active_window = True
      maximised = False
      order = 0
      parent = ""
      position = X0:Y0
      size = DX,DY
      title = I_AM_CHILD0DEFAULT
      type = Window
    [[[child1]]]
      order = 0
      parent = child0
      position = 108
      ratio = 0.273417721519
      type = VPaned
    [[[child2]]]
      order = 0
      parent = child1
      position = 318
      ratio = 0.500787401575
      type = HPaned
    [[[child5]]]
      order = 1
      parent = child1
      position = 143
      ratio = 0.501754385965
      type = VPaned
    [[[child6]]]
      order = 0
      parent = child5
      position = 318
      ratio = 0.500787401575
      type = HPaned
    [[[child9]]]
      order = 1
      parent = child5
      position = 318
      ratio = 0.500787401575
      type = HPaned
    [[[terminal10]]]
      command = CMD5;/bin/bash
      directory = ~/
      order = 0
      parent = child9
      profile = colorprof
      type = Terminal
      uuid = df84dd56-c591-4ade-861d-7114443ceed9
    [[[terminal11]]]
      command = CMD6;/bin/bash
      directory = ~/
      order = 1
      parent = child9
      profile = colorprof
      type = Terminal
      uuid = 594325c1-50c5-4edc-a4c8-0e4d33f8bb9a
    [[[terminal3]]]
      command = CMD1;/bin/bash
      directory = ~/
      order = 0
      parent = child2
      profile = colorprof
      type = Terminal
      uuid = 80d9e811-7995-4c4d-a64c-857bfc909231
    [[[terminal4]]]
      command = CMD2;/bin/bash
      directory = ~/
      order = 1
      parent = child2
      profile = colorprof
      type = Terminal
      uuid = f04fb36b-50d7-42b8-9a56-222520ccac04
    [[[terminal7]]]
      command = CMD3;/bin/bash
      directory = ~/
      order = 0
      parent = child6
      profile = colorprof
      type = Terminal
      uuid = e86f0b20-e9d5-48d4-b778-cd47a9ea3f65
    [[[terminal8]]]
      command = CMD4;/bin/bash
      directory = ~/
      order = 1
      parent = child6
      profile =colorprof
      type = Terminal
      uuid = 0685b5f8-ed5a-4bd3-89eb-59e67a793231
      """


#=============== it was  2x3rows



Layout42="""
[[special]]
 [[[child0]]]
      fullscreen = False
      last_active_term = 80d9e811-7995-4c4d-a64c-857bfc909231
      last_active_window = True
      maximised = False
      order = 0
      parent = ""
      position = X0:Y0
      size = DX,DY
      title = I_AM_CHILD0
      type = Window
    [[[child1]]]
      order = 0
      parent = child0
      position = 798
      ratio = 0.5
      type = HPaned
    [[[child10]]]
      order = 0
      parent = child9
      position = 385
      ratio = 0.505208333333
      type = VPaned
    [[[child13]]]
      order = 1
      parent = child9
      position = 385
      ratio = 0.505208333333
      type = VPaned
    [[[child2]]]
      order = 0
      parent = child1
      position = 396
      ratio = 0.5
      type = HPaned
    [[[child3]]]
      order = 0
      parent = child2
      position = 385
      ratio = 0.505208333333
      type = VPaned
    [[[child6]]]
      order = 1
      parent = child2
      position = 385
      ratio = 0.505208333333
      type = VPaned
    [[[child9]]]
      order = 1
      parent = child1
      position = 396
      ratio = 0.5
      type = HPaned
   [[[terminal11]]]
      command = CMD1;/bin/bash
      directory = ~/
      order = 0
      parent = child10
      profile = colorprof
      type = Terminal
      uuid = 07e9d796-d0ff-4658-8977-10081e0c92f1
    [[[terminal12]]]
      command = CMD2;/bin/bash
      directory = ~/
      order = 1
      parent = child10
      profile = colorprof
      type = Terminal
      uuid = 4f948f0e-8bbb-4b88-a8d2-0905b501001f
    [[[terminal14]]]
      command = CMD3;/bin/bash
      directory = ~/
      order = 0
      parent = child13
      profile = colorprof
      type = Terminal
      uuid = 1ef943af-ccb3-4b86-aee1-fbf60eb4992f
    [[[terminal15]]]
      command = CMD4;/bin/bash
      directory = ~/
      order = 1
      parent = child13
      profile = colorprof
      type = Terminal
      uuid = 337770f8-7136-4835-9832-3bf5a9f00b35
    [[[terminal4]]]
      command = CMD5;/bin/bash
      directory = ~/
      order = 0
      parent = child3
      profile = colorprof
      type = Terminal
      uuid = 80d9e811-7995-4c4d-a64c-857bfc909231
    [[[terminal5]]]
      command = CMD6;/bin/bash
      directory = ~/
      order = 1
      parent = child3
      profile = colorprof
      type = Terminal
      uuid = 8a2c1e23-b7b5-4e4a-9e55-a6167f696aba
    [[[terminal7]]]
      command = CMD7;/bin/bash
      directory = ~/
      order = 0
      parent = child6
      profile = colorprof
      type = Terminal
      uuid = 9f248ea0-6c1b-4285-9bad-b18f4c44a630
    [[[terminal8]]]
      command = CMD8;/bin/bash
      directory = ~/
      order = 1
      parent = child6
      profile = colorprof
      type = Terminal
      uuid = 917884da-76ad-4263-b77f-0c9a849d6d1c
      """


############################################# SUBS
profiles="""
[plugins]
[profiles]
  [[default]]
    background_image = None
    background_color = "#000000"
    foreground_color = "#ffffff"
    font = Mono 11

  [[colorprof]]
    background_image = None
    background_color = "#300a24"
    exit_action = restart
    foreground_color = "#ffffff"
    font = Mono 11

"""
#    use_custom_command = True



# def create_one_profile(name):
#     txt="""[[NAME]]
#     background_image = None
#     custom_command = COMMAND
#     exit_action = restart
#     use_custom_command = True
#     """
#     txt.replace("NAME",name)
#     txt.replace("COMMAND","iftop -i eth1")
#     return txt
# def create_profiles():
#     txt="""
# [plugins]
# [profiles]
#   [[default]]
#     background_image = None
# """
#     txt=txt+create_one_profile("ahoj")
#     return txt




#############################################################

def monitor_size():
    CMD="xrandr  | grep \* | cut -d' ' -f4"
    p=subprocess.check_output(CMD , shell=True)
    print("==========", p)
    wihe=p.decode('utf8')
    if wihe=="":
        print("X.... xrandr problem, returning std")
        return "1400x1000"
        return "640x480"
    wihe=wihe.split()[0].rstrip().split('x')
    wihe=list(map(int,wihe))
    print("i... monitor size ",wihe)
    return wihe


#############################################################
MS=monitor_size()
print(MS)
if args.fullscreen:
    dx,dy=[ str(x) for x in MS]
    x0,y0="0","0"
else:
    qsize,x0,y0=args.geometry.split("+")
    dx,dy=qsize.split("x")
print("i... geometry decompoosed:",x0,y0,dx,dy)



global_conf="""[global_config]
  suppress_multiple_term_dialog = True
[keybindings]
[layouts]
    """
conf=global_conf

# HERE IS LAYOUT #########
if args.layout=="21":  # BUG
    Layout=Layout21
elif args.layout=="12":
    Layout=Layout12
elif args.layout=="321":
    Layout=Layout321
elif args.layout=="22":
    Layout=Layout22
elif args.layout=="31":  # one row
    Layout=Layout31
elif args.layout=="13":  # one column
    Layout=Layout13
elif args.layout=="32": # 2 rows
    Layout=Layout32
elif args.layout=="23": # 3 rows
    Layout=Layout23
elif args.layout=="42":
    Layout=Layout42
else:
    Layout=Layout11
    print("X... NO SUCH PREDEFINED LAYOUT;  Layouts available:")
    print("One row:     21 31")
    print("Two rows:    12 22 32 42")
    print("Three rows:  13 23")
    print("Asymetric :  321 ")
    print("==============================")
    print("All:  21 12 321 22 31 13 32 42")
    print("terminat.py -l 321 -g 1700x1000+1+1")
    print('terminat.py -l 21 -g 1700x1000+1+1 -c "top","htop"')
    print('terminat.py -l 13 -g 780x1000+1+1 -c "myservice2 infinite","source ~/.zshrc &&wdfc","htop"')
    quit()


Layout=Layout.replace("X0:Y0",x0+":"+y0) # geometry
Layout=Layout.replace("DX,DY",dx+","+dy) # geometry


############################
# commands
if not args.command is None:
    all=args.command.split(",")
    print("i... ",len(all),"COMMANDS")
    i=1
    for co in all:
        #co=co.strip('"')
        print("i... COMMAND", co)
        Layout=Layout.replace("CMD"+str(i)+";/bin/bash", co )
        #Layout=re.sub("CMD1.+\n", co , Layout)
        i=i+1
# Layout=Layout.replace("CMD2","iftop -i eth1")
# Layout=Layout.replace("CMD3","htop")
# Layout=Layout.replace("CMD4","iotop")
# Layout=Layout.replace("CMD5","iftop")
# Layout=Layout.replace("CMD6","iftop")
else:
    print("i...   no COMMANDS - cannot happen, there is always \"\" ")
#quit()

conf=conf+Layout00 + Layout
#conf=conf+create_profiles()
conf=conf+profiles
print(conf)

with open( CONFIG,"w" ) as f:
    f.write( conf )

# no DBUS
CMD="/usr/bin/terminator -u -g "+CONFIG+" -l special"
print(CMD)
subprocess.Popen( CMD.split() )
