#!/usr/bin/env python3
#
#  this is the basic transfer of txt elog files to ORG file
#  v1: just text fields DONE
#  v2: add jpgs
#  v3 add colors  darkgreen ping blue  gray
#         measurement irradiation configuration targets
#
import os
import glob
import fire


header = r"""#+OPTIONS: toc:nil
#+LATEX_HEADER: \usepackage{xcolor}
#+LATEX_HEADER: \usepackage{color}
#+LATEX_HEADER: \definecolor{darkgreen}{rgb}{0.0,0.3,0.0}
#  +LATEX_HEADER: \definecolor{pink}{rgb}{1.0,0.5,0.5}
"""

attachments = []

def convert_elog_to_org(directory='.'):
    global attachments
    log_files = sorted(glob.glob(os.path.join(directory, '*.log')))
    org_content = []

    for log_file in log_files:
        with open(log_file, 'r') as file:
            lines = file.readlines()
            date = None
            record = []
            subject = None
            record_info = []
            for line in lines:
                if line.startswith('$@MID@$:'):
                    if record:
                        # finish previous record
                        record.append('#+end_example')
                        org_content.append('\n'.join(record))
                        if attachments != []:
                            for i in attachments:
                                org_content.append( f"\n[[file:{i}.png]]\n")
                        record = []
                    # start new record
                    record_info = [line.strip()]
                    subject = None
                    attachments = []
                elif line.startswith('Date:'):
                    if not date:
                        date = line.split('Date: ')[1].strip()
                        org_content.append(f'* {date}')
                elif line.startswith('Subject:'):
                    subject = line.split('Subject: ')[1].strip()
                    record.append(f'** {subject}')
                    record.append('#+begin_example')
                    record.extend(record_info)
                elif line.startswith('Author:') or line.startswith('Category:'):
                    record_info.append(line.strip())
                # --------------- colors
                elif line.startswith('Type:'):
                    if line.find("Measurement") > 0:
                        record_info.append("#+end_example\n")
                        record_info.append("\colorbox{green}{" +  line.strip() + "}")
                        record_info.append("#+begin_example\n")
                    elif line.find("Irradiation") > 0:
                        record_info.append("#+end_example\n")
                        record_info.append("\colorbox{pink}{" + line.strip() + "}")
                        record_info.append("#+begin_example\n")
                    elif line.find("Problem") > 0:
                        record_info.append("#+end_example\n")
                        record_info.append("\colorbox{red}{" + line.strip() + "}")
                        record_info.append("#+begin_example\n")
                    elif line.find("Configuration") > 0:
                        record_info.append("#+end_example\n")
                        record_info.append("\colorbox{cyan}{" + line.strip() + "}")
                        record_info.append("#+begin_example\n")
                    elif line.find("Targets") > 0:
                        record_info.append("#+end_example\n")
                        record_info.append("\colorbox{lightgray}{" + line.strip() + "}")
                        record_info.append("#+begin_example\n")
                    elif line.find("Other") > 0:
                        record_info.append("#+end_example\n")
                        record_info.append("\colorbox{yellow}{" + line.strip() + "}")
                        record_info.append("#+begin_example\n")
                    elif line.find("Information") > 0:
                        record_info.append("#+end_example\n")
                        record_info.append("\colorbox{lime}{" + line.strip() + "}")
                        record_info.append("#+begin_example\n")
                    else:
                        record_info.append(line.strip())

                elif line.startswith('Attachment:'):
                    ata = line.strip().split('Attachment:')[-1].strip(" ")
                    if len(ata) > 1:
                        ata = ata.split(",")
                        attachments = [ i for i in ata if (i.find("jpg")==len(i) - 3)or(i.find("png") == len(i) - 3) ]
                        print(f"i... A: {attachments}")
                else:
                    if line.strip():
                        record.append(line.strip())
                    else:
                        record.append('')

            if record:
                record.append('#+end_example')
                org_content.append('\n'.join(record))

    with open('elogbooknfs.org', 'w') as org_file:
        org_file.write(header)
        org_file.write('\n'.join(org_content))

if __name__ == '__main__':
    fire.Fire(convert_elog_to_org)
