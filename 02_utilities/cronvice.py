#!/usr/bin/env python3
# Libraries first
# This part defines libraries for operating with screen

# #+NAME: libs

# [[file:~/02_GIT/GITLAB/linux/02_utilities/cronvice.org::libs][libs]]
import subprocess as sp
import os
import shutil
import datetime as dt
import shlex
import logging
from fire import Fire
from crontab import CronTab  # install python-crontab
from console import fg, bg
from collections import Counter

logger = logging.getLogger(__name__)
LOGNAME = '/tmp/cronvice.log'
logging.basicConfig(filename=LOGNAME, level=logging.INFO,
    format='%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S',)

# Global values and constants ***********************
DEBUG=False
DEBUG = True
GLOBAL_ENV = "DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus"
PYTHON3 = "/usr/bin/python3"
CRON = None # main object

CLOUD_PATH = "~/02_GIT/services/cloud_myservice"
CLOUD_PATH= os.path.expanduser(CLOUD_PATH)
# ========================================================
#
#---------------------------------------------------------

def is_in_cron(cron, tag):
    """
    Not usefull with  cronvice in  crontab
    """
    file_path = os.path.abspath(__file__)
    RMTG = f"{file_path}"
    for job in cron:
        if DEBUG:
            print(f"D...    isincron {job}")
        if job.command.find(RMTG) > 0 and job.command.endswith(f" r {tag}") > 0: # no SPACE
            return True
    return False

# ========================================================
#
#---------------------------------------------------------

def is_in_screen(TAG, sessions):
    """
    if tag in screen list => True
    """
    global logger
    #print("D... @is in screen")
    if sessions is None:
        #print("D... testing screen - sessions NONE")
        return False
    for i in sessions:
        if i.find("Sockets in /run")>=0: continue
        dec = i.strip().strip("\t").split("\t")[0]
        dec2 = dec.split(".")[-1]
        #print(f"D... testing sessions: {i} * {dec} * {dec2}")
        #if DEBUG: print(f"D... dec={dec} dec2={dec2} TAG={TAG}   " )
        if dec2 == TAG:
            #print(f"D... {TAG} already in screen")
            logger.info(f"{TAG} is already in screen.... NO RUN")
            return True
    return False

# ========================================================
#
#---------------------------------------------------------
def get_now():
    res = dt.datetime.now().strftime("%Y%m%d_%H%M%S")
    return res

# ========================================================
#
#---------------------------------------------------------
def strip_path(file_path, base_folder):
    relative_path = os.path.relpath(file_path, base_folder)
    return os.path.join(*relative_path.split(os.sep)[-2:])

def check_unique_executables(folder='.'):
    executables = find_executables(folder)
    stripped_paths = [strip_path(exe, folder) for exe in executables]
    return len(stripped_paths) == len(set(stripped_paths))


# ========================================================
#
#---------------------------------------------------------
def find_executables(folder='.'):
    """
    get all exe in the folder (they will become unique tags)
    """
    executables = []
    for root, _, files in os.walk(folder):
        for file in files:
            file_path = os.path.join(root, file)
            if os.path.isfile(file_path) and os.access(file_path, os.X_OK):
                executables.append(file_path)
    # run the check
    stripped_paths = [strip_path(exe, folder) for exe in executables]
    path_counts = Counter(stripped_paths)
    non_unique = [path for path, count in path_counts.items() if count > 1]
    if len(stripped_paths) == len(set(stripped_paths)):
        #print("i... tags unique")
        pass
    else:
        print("X... TAGS NOT UNIQUE")
        print("X...", non_unique)
    return executables


# ========================================================
#
#---------------------------------------------------------

def is_in_allowed(tag):
    """
    is the tag in allowed?
    """
    exes=find_executables(CLOUD_PATH)
    for onexe in exes:
        if onexe.split("/")[-1] == tag:
            return True
    return False
    #if tag in ["aaa", "asa", "asd", "asad"]:
    #    return True
    #else:
    #    return False
# ========================================================
#
#---------------------------------------------------------

def extract_exe_from_tag(tag):
    """
    from known /tag/ - get exe path.
    """
    exes = find_executables(CLOUD_PATH)
    for onexe in exes:
        if onexe.split("/")[-1] == tag:
            #EXE = f"{CLOUD_PATH}/{onexe}"
            EXE = f"{onexe}"
            #print(f"i... found {EXE}")
            return os.path.expanduser(EXE)



# ========================================================
#
#---------------------------------------------------------

def allocate( exe):
    """
    use which , find location of exe, return complete path
    """
    global logger
    if os.path.exists( exe): # This is total path
        return exe

    # try to find it ith which
    program_path = shutil.which( exe )

    # try some other locations
    if program_path is None:
        #logger.info(f"i... ppath was None  ")
        home_dir = os.environ.get('HOME')
        file_path = os.path.join(home_dir, '.local', 'bin', exe)
        if os.path.exists(file_path):
            logger.info(f"i... return  ... 1 {file_path} ")
            return file_path

    # try some other locations
    if program_path is None:
        home_dir = os.environ.get('HOME')
        file_path = os.path.join(home_dir, 'bin', exe)
        if os.path.exists(file_path):
            logger.info(f"i... return  ... 2 {file_path} \n")
            return file_path

    return exe#program_path # wil do None eventually

# ========================================================
#
#---------------------------------------------------------

def activate(tag): #REAL
    """
    starts SCREEN with environment......  sloopy yet and sleep 30
    """
    logger.info(f"i...  tag is {tag} ")
    # ENV = ""
    # Set environment variables --- same as GLOBAL_ENV
    env = os.environ.copy()
    env["DISPLAY"] = ":0"
    env["DBUS_SESSION_BUS_ADDRESS"] = "unix:path=/run/user/1000/bus"

    SCR = f"/usr/bin/screen -dmS {tag} " #SPACE IS IMPORANT
    #make code sequence with &&
    #CODE = f"echo i...running {tag} && /home/ojr/.local/bin/notifator b hello_{tag} && echo i...running {tag} && sleep 30"
    #MAINCODE = 'notifator'
    MAINCODE = extract_exe_from_tag(tag) # get executable path
    #print(MAINCODE)
    #MAINCODE = allocate(MAINCODE) # probably useless when PATH exists...
    #print(MAINCODE)

    #REALCODE = allocate(realcode)
    if DEBUG:
        print("D... creating CODE")
    #CODE = f"{MAINCODE} b {tag}_{get_now()} && {REALCODE} && sleep 5"
    CODE = f"{MAINCODE} && sleep 5"
    # encapsulate to ''
    CODE = f"bash -c '{CODE}'"
    if DEBUG:
        print(f"D... CODE {CODE}")
    CMD = f"{GLOBAL_ENV} {SCR} {CODE}"
    CMD = f"{SCR} {CODE}"
    if DEBUG:
        print(f"D... CMD {CMD}")
    args = shlex.split(CMD)
    if DEBUG:
        print(f"D... args {args}")
    # print(args)
    logger.info(f"i... ... the args is {args} \n")
    if DEBUG:
        print("D... spPopen")
    process = sp.Popen(args, env=env, stdout=sp.DEVNULL, stderr=sp.DEVNULL)
    process.poll()

# ========================================================
#
#---------------------------------------------------------

def list_screen_sessions():
    try:
        result = sp.run(['screen', '-ls'], capture_output=True, text=True, check=True)
        #print(result.stdout)
        return result.stdout.split('\n')[1:-1]
    except sp.CalledProcessError as e:
        #print(f"x... screen ls - error occurred: {e}")
        return None
# libs ends here

# Basic commands



# [[file:~/02_GIT/GITLAB/linux/02_utilities/cronvice.org::*Basic commands][Basic commands:1]]
# ========================================================
#
#---------------------------------------------------------
def add_job(cron, tag, comment = ""):
    """
    Adds - to CRON - the call to 'cronvice r tag'
    cron:  * * * * * display... path/to/cronvice
    """
    if is_in_cron(cron, tag):
        print(f"X... TAG already present in cron /{tag}/")
        return
    if not is_in_allowed(tag):
        print(f"X... this job-TAG is not allowed /{tag}/")
        return

    file_path = os.path.abspath(__file__)
    MAINCODE = file_path
    CODE = f"{PYTHON3} {MAINCODE} r {tag}"
    #### encapsulate to ''
    #CODE = f"bash -c '{CODE}'"
    #CMD = f"{ENV} {SCR} {CODE}"
    CMD = f"{GLOBAL_ENV} {CODE}"

    print(f"i... adding job TAG /{tag}/ ")

    job = cron.new(command=CMD)
    job.minute.every(1)
    #job.set_command("new_script.sh")
    job.set_comment(f"{comment}")
    cron.write()

    return

# ========================================================
#
#---------------------------------------------------------
def ls_job(cron):
    """
    lists all cron items and checks for a valid tag, returns all tags
    """
    #SCRDMS = f"/usr/bin/screen -dmS " #SPACE IS IMPORANT
    #sessions = list_screen_sessions() # PREPARE
    #if DEBUG:
    #    print("D... screen sessions:",sessions)
    tags_found=[]
    for job in cron:
        if DEBUG:
            print(f"D... job in cron: {job}")
        txt = job.command
        if txt[0] == "#": continue
        cmdex = txt.split(GLOBAL_ENV)[-1].strip()
        cmdex = cmdex.split(PYTHON3)[-1].strip()
        tag = cmdex.split()[-1]
        tags_found.append(tag)
        print(f">>>  TAG OBTAINED in CRON: {tag} ")
    return tags_found

# ========================================================
#
#---------------------------------------------------------
def run_job(cron, tag):
    """
    ON 'r' CALL: if it is in cron and screen not running......
    """
    logger.info("run_job")
    #if is_in_cron(cron, tag): # not for screen-cronvice
    if is_in_allowed(tag):
        #print("D...  in allowed....")
        sessions = list_screen_sessions() # PREPARE
        if is_in_screen(tag, sessions):
            #print("D... it is in screen already....")
            print(f"X... {fg.orange}already running: /{tag}/ {fg.default} ")
        else:
            logger.info("RUN/Start NOW")
            print(f"i... {fg.green} Starting .... /{tag}/ .... {fg.default}" )
            activate(tag)
    return

# ========================================================
#
#---------------------------------------------------------
def del_job(cron, tag):
    """
    delete job FROM CRON
    """
    ACT = False
    file_path = os.path.abspath(__file__)
    # RMTG = f"screen -dmS {tag} " #SPACE IMPORTANT
    RMTG = f"{file_path}"
    for job in cron:
        #print(f"/{job.command}/..")
        if job.command.find(RMTG) > 0 and job.command.endswith(f" r {tag}") > 0: # no space
            print(f"i... removing TAG /{tag}/ ")#... {job}")
            cron.remove(job)
            ACT = True
    if ACT:
        cron.write()
    else:
        print("X... nothing deleted")

    return

# ========================================================
#
#---------------------------------------------------------
def watch_jobs(cron):
    """
    watch - go through CRON tags and show if it is running in screen
    """
    file_path = os.path.abspath(__file__)
    # RMTG = f"screen -dmS {tag} " #SPACE IMPORTANT
    RMTG = f"{file_path}"
    tags_in_cron=ls_job(cron)
    if len(tags_in_cron)==0:
        print("X... no /tags/ in CRON")
    sessions = list_screen_sessions() # PREPARE
    if sessions is None or len(sessions)==0:
        print("X... no /tags/ in SCREEN")
        if len(tags_in_cron)==0:
            for i in tags_in_cron:
                print(f"SCREEN ONLY: {i}")

    for tag in tags_in_cron:
        if is_in_screen(tag, sessions):
            print(f"{fg.green}/{tag}/{fg.default}")
        else:
            print(f"{fg.red}/{tag}/{fg.default}")
    return
# Basic commands:1 ends here

# visualization


# #+NAME: visualization

# [[file:~/02_GIT/GITLAB/linux/02_utilities/cronvice.org::visualization][visualization]]
def monitor_jobs(cron):
    return
def enter_jobs(cron):
    return
# visualization ends here

# MAIN with all the functions

# #+NAME: main
# #+HEADER: :var a=libs

# [[file:~/02_GIT/GITLAB/linux/02_utilities/cronvice.org::main][main]]
a=""
# This runs command(path in myservice) with tag
# ========================================================
#
#---------------------------------------------------------
def main(command, tag=None, debug = False):
    """
    HUB for commands.
    a - add to crontab if not there
    l - list crontab
    r - RUN * MAIN CALL from cron
    d - del from crontab
    w - watch jobs in screen - running
    m - monitor ??? i dont know
    """
    global DEBUG, cron
    DEBUG = debug
    cron = CronTab(user=True)
    if command == "a" and tag is not None:
        add_job(cron, tag)
    elif command == "l" :
        ls_job(cron)
    elif command == "w":
        watch_jobs(cron)
    elif command == "e":
        enter_jobs(cron)
    elif command == "m":
        monitor_jobs(cron)
    elif command == "r" and tag is not None:
        # MAIN WAY TO OPERATE FROM CRON *********** r *****
        run_job(cron, tag)
    elif command == "d" and tag is not None:
        del_job(cron, tag)
    elif tag is None:
        print("X... give me screen tag")
        return
    else:
        print("X... unknown command")
        return

    # TAG="abc"
    # CLEAR=False
    # res=list_screen_sessions()
    # if res is None:
    #     CLEAR=True
    # else:
    #     CLEAR=is_in_screen(TAG, res)
    # print("i...",CLEAR,res)
    # print("---")
    # if CLEAR:
    #     res=start("abc","echo a")
# main ends here

# Fire block

# #+NAME: Fire
# #+HEADER: :var a=libs

# [[file:~/02_GIT/GITLAB/linux/02_utilities/cronvice.org::Fire][Fire]]
a=""
# This runs command(path in myservice) with tag
if __name__ == "__main__":
    Fire(main)
# Fire ends here
