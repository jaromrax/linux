#!/usr/bin/env python3

from fire import Fire
import pandas as pd
import datetime as dt
import re
import subprocess as sp
import os
import glob

# apt install inotify
# while inotifywait -e close_write myfile.py; do ./myfile.py; done
# A="./vlog.py" ; while inotifywait -e close_write $A; do $A; done
#

INPUT = "/var/log/ufw.log"
INPUT1 = "/var/log/ufw.log.1"
SIDLOG = "SSID.log"
DIR = "~/.vlog" # DIRECTORY
MACDB = "db_"


# --------------------------------------------
# prepare dirs and files
DIR = os.path.expanduser(DIR)
if DIR[-1] !="/": DIR=DIR+"/"
try:
    os.mkdir(DIR)
except:
    print()
SIDLOG=DIR+SIDLOG

now = dt.datetime.now()
Y=now.strftime("%Y")

#----------------------------------------------




def get_nearest(data, timestamp):
    data['d'] = data[0].sub(timestamp).abs() # abs works for deltat
    mindi = data['d'].idxmin()
    #print("index for min d=",mindi)  # finally not 0
    return data[0].iloc[mindi],data[1].iloc[mindi]



def get_sid_now():
    ok = False
    try:
        sid = str(sp.check_output(["/sbin/iwgetid -r"], shell = True).decode("utf8").strip())
        ok = True
    except:
        ok = False
    if not ok: return "eth"
    with open( SIDLOG,"a+") as f:
        f.write(f"{now.isoformat()} {sid}"+"\n")
    return sid

def get_sid(tag):
    ok = False
    try:
        with open( SIDLOG ) as f:
            l = f.readlines()
        ok = True
    except:
        # print(" ... no SIDLOG")
        ok = False
    if not ok:
        return "None"
    l = [x.strip() for x in l]
    #l = [ [ x[:26],x[27:] ] for x in l]
    l = [ [dt.datetime.fromisoformat(x[:26]),x[27:]] for x in l ]
    df = pd.DataFrame(l)
    #df = df.sort_values(0, ascending=True)
    #print(df.loc[0][0], df.loc[len(df)-1][0])
    #print(tag, type(tag) )
    rest,resn = get_nearest(df,tag)
    ##print(f"i... min={df.loc[0][0]}; best={rest}  fortag={tag}")

    #print(f"i...  best={rest}  fortag={tag}")
    return resn

def form(ip):
    a = ip.split(".")
    b = ""
    for i in a:
        i = int(i)
        b=b+f"{i:03d}."
    return b[:-1]

def get_time(li):
    if len(li)!=3:
        print("X... error ",li)
        return None
    else:
        #print(Y)
        #print(li)
        tag = f"{Y}-{li[0]}-{li[1]}-{li[2]}"
        #print(tag)
        #print(tag)
        tag2 = dt.datetime.strptime(tag,"%Y-%b-%d-%H:%M:%S")
        #print("     ",tag,tag2)
        return tag2


def get_unique_wifi(df):
    return df['ssid'].unique()




#--------------------------------------------------------------------

def file2mem():
    with open(INPUT) as f:
        l = f.readlines()
    if len(l)<3:
        if os.path.exists(INPUT1):
            with open(INPUT1) as f:
                l = f.readlines()

    l = [x.strip() for x in l]
    l = [x.replace("   "," ") for x in l]
    l = [x.replace("  "," ") for x in l]
    l = [x.split(" ") for x in l]
    print("i... total lines in ufw.log=", len(l) )

    di = {}
    sid = get_sid_now() # write SSID everytime

    ni = 0
    ipv6 = 0
    for i in l:
        ni+=1
        #print(i)
        tag = get_time(i[:3])
        di[ni] = [tag]
        di[ni].append(  i[3] ) # zen
        line = " ".join(i[8:])

        #print(line)

        di[ni].append( re.search(r"IN=(\w+)", line).group(1) )

        di[ni].append( get_sid(tag) ) # ONLY IF SID IS NOW...

        mac2 = re.search(r"MAC=([:\w]+)", line)
        if mac2 is not None:
            #print(mac2)
            mac2 = mac2.group(1)
            #di[tag].append( mac2[:17] ) # local
            di[ni].append( mac2[18:35] ) # remote

        # SRC can be IPv6 ....
        ok = False
        try:
            ip = re.search(r"SRC=([\.\d]+)", line).group(1)
            ok = True
        except:
            ipv6+=1
        if not ok:
            del di[ni]
            continue
        ip = form(ip)
        di[ni].append( ip )

        di[ni].append( re.search(r"PROTO=(\w+)", line).group(1) )

        if line.find("SPT")>0:
            di[ni].append( int(re.search(r"SPT=(\w+)", line).group(1) ) )

            di[ni].append( int(re.search(r"DPT=(\w+)", line).group(1)) )
        #else:
        #    print(line) # proto=2  239.255.255.250


        #print(di)
    df = pd.DataFrame.from_dict( di , orient="index", columns=["t","pc","if","ssid","mac","ip","pro","from","to"])
    if ipv6>0: print(f"\nX... ipv6 records = {ipv6}\n")
    return df

# --------------------------------------------------------------------------------
def load_mac_nick_dict():
    """
    databases with nicks(handworked) will be used to replace MAC
    """
    retd = {}
    lid = glob.glob(DIR+MACDB+'*.csv')
    df = None
    for FNAME in lid:
        print(FNAME)
        df1 = pd.read_csv( FNAME , index_col=False)
        df1 = df1[ df1["nick"].notnull() ]
        if df is None:
            df =df1
        else:
            df = pd.concat( [df,df1] )
    #print("___ reduced nick:____")
    #print(df)
    if df is None or len(df)==0: return {}
    df = df.drop(['ip'], axis=1)
    df.set_index('mac',inplace=True) # make it index to expport
    dd = df.to_dict( )['nick']  # index -> nick pairs
    #print(dd)
    return dd



def mk_db(df,wifi):
    """
    take df, extract MACs for a given wifi(if more in ufw.log)
    sort, add nick field and return
    MACS can be different at hoe and work
    """
    ali = df[ df["ssid"]==wifi]['mac'].unique() # list
    # list of MACS # print(ali)
    print("========== for wifi =========",wifi)
    adf = pd.DataFrame( sorted(ali) , columns=["mac"])
    adf['ip'] = None
    adf['nick'] = None

    for i in adf["mac"]:
        a = df[ df['mac']==i ]
        mfv = a["ip"].value_counts().idxmax() # most frequent value
        #print(i, mfv ) # this works...
        #adf[ adf["mac"]==i]["ip"] = mfv # NOT LIKE THIS BUT
        adf.loc[ adf["mac"]==i, "ip"] = mfv
        #print(adf) # this works too
        #print("--- mac--",i )
        #print()
    #adf['ip']
    #print(adf)
    print("___________")
    return adf

def merge_csv(wifi, cdf):
    """
    open earlier csv file for wifi with MACs...
    MACS can be different at hoe and work
    """
    FNAME = DIR+MACDB+wifi+".csv"
    if not os.path.exists(FNAME): return cdf
    df = pd.read_csv( FNAME , index_col=False)
    df = df.set_index('mac')
    #df = pd.concat( [df, cdf] , on= )
    #df = df.merge(  cdf , how="outer",on="mac", left_index= False,right_index=False  )
    #df = df.merge(  cdf , how="outer",on="mac", suffixes=(None,"_")  )
    #
    #  problem with others where they are None, they stick
    #
    #print("_"*50)
    #print(df) #contains NICK
    #print(cdf) # contains IP
    #print("_"*50)
    df = pd.merge(  df, cdf , how="outer",on="mac", suffixes=(None,"_delme")  )
    df = df[[c for c in df.columns if not c.endswith('_delme')]]

    # now put IP if missing
    for i,row in df.iterrows():
        if pd.isna(df.loc[i,'ip']):
            #print("NONE",i,df.loc[i, "ip"])
            if i in cdf.index:
                #print( cdf.loc[i, "ip"] )
                df.loc[i, "ip"] = cdf.loc[i, "ip"]
    return df


# ********************************************************************** MAIN ***
def main():
    """
    analyse ufw.log file (last day file)
    """

    dmn = load_mac_nick_dict() # dict mac nick
    print()
    df = file2mem() # complete ufw.log .... to df

    dfni=df.replace({"mac": dmn})

    print(dfni )
    print("i... total standard items in df: ",len(df))
    print("############################## SUSPECTS <10000 #############")
    print(dfni[  df['to']<10000] )
    print("##############################___example___ALLDISPL#########")
    with pd.option_context('display.max_rows', None):
        #print(dfni[  dfni['mac']=="00:21:91:ea:d8:5b"] )
        print(dfni[  dfni['to']<1024] )


    print("######################## updating name databases ##########")
    wifis = get_unique_wifi(df) # from ufw.log file
    #print( wifis)
    #print("                       =================== save to csv")
    for i in wifis:
        adf = mk_db( df, i ) #  returns - mac ip nick - IP is there
        #print(adf)
        adf = adf.set_index('mac')
        #print("-------------- i have adf... now merge")
        # merge is not too good - None persists sometimes...
        adf = merge_csv(i, adf )
        adf = adf.sort_values('ip')
        print(adf)
        # adf.to_csv(DIR+MACDB+i+".csv" , index=False) when mac not index...
        adf.to_csv(DIR+MACDB+i+".csv" , index=True)

    # print(adf.index)# it is a list
if __name__=="__main__":
    Fire(main)
    print("....  edit names in ~/.vlog")
