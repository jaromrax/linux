#!/usr/bin/env python3
"""

NOT LAST VERSION see .org

https://github.com/whdev1/pycron
   A simple Python library providing cron functionality via the use of a single decorator.

https://gitlab.com/doctormo/python-crontab
    for reading and writing crontab files and accessing the system cron automatically and simply using a direct API.
   THIS SEEMS OLD AND NEW

https://github.com/kiorky/croniter
    croniter provides iteration for the datetime object with a cron like format.
    - just iterations ----
https://github.com/dbader/schedule
    A simple to use API for scheduling jobs, made for humans.
    - NOT CRON
"""
from fire import Fire
from crontab import CronTab  # install python-crontab
from console import fg, bg
import shutil
import os
import datetime as dt
import shlex
import subprocess as sp




def list_screen_sessions():
    """
    return the existing screen sessions
    """
    try:
        result = sp.run(['screen', '-ls'], capture_output=True, text=True, check=True)
        #print(result.stdout)
        return result.stdout.split('\n')[1:-1]
    except sp.CalledProcessError as e:
        #print(f"x... screen ls - error occurred: {e}")
        return None


def allocate( exe):
    """
    use which , find location of exe, return complete path
    """
    with open("/tmp/cronvice.log", "a") as f:
        f.write(f"i... in ALLOCATE ... the exe is {exe} \n")

    if os.path.exists( exe): # This is total path
        with open("/tmp/cronvice.log", "a") as f:
            f.write(f"i... ret  ... the exe is {exe} \n")
        return exe

    # try to find it ith which
    program_path = shutil.which( exe )
    with open("/tmp/cronvice.log", "a") as f:
        f.write(f"i... ret  ... the shutil result is {program_path} \n")

    # try some other locations
    if program_path is None:
        with open("/tmp/cronvice.log", "a") as f:
            f.write(f"i... ppath was None  \n")
        home_dir = os.environ.get('HOME')
        with open("/tmp/cronvice.log", "a") as f:
            f.write(f"i... return  ... 1a homedir: {home_dir} \n")
        file_path = os.path.join(home_dir, '.local', 'bin', exe)
        with open("/tmp/cronvice.log", "a") as f:
            f.write(f"i... return  ... 1a  joined {home_dir} - {file_path} \n")
        if os.path.exists(file_path):
            with open("/tmp/cronvice.log", "a") as f:
                f.write(f"i... return  ... 1 {file_path} \n")
            return file_path

    # try some other locations
    if program_path is None:
        home_dir = os.environ.get('HOME')
        file_path = os.path.join(home_dir, 'bin', exe)
        if os.path.exists(file_path):
            with open("/tmp/cronvice.log", "a") as f:
                f.write(f"i... return  ... 2 {file_path} \n")
            return file_path

    return program_path # wil do None eventually


def is_in_screen(TAG, sessions):
    """
    if tag in screen list => True
    """
    if sessions is None:return False
    for i in sessions:
        #print(i, TAG, i.find(TAG) )
        if i.find(TAG) > 0:
            return True
    return False

# -----------------------------------------------------------------------
# -----------------------------------------------------------------------
# -----------------------------------------------------------------------
# -----------------------------------------------------------------------
def view_cron(cron, short=False):
    SCRDMS = f"/usr/bin/screen -dmS " #SPACE IS IMPORANT
    sessions = list_screen_sessions() # PREPARE
    #print(sessions)
    for job in cron:
        txt = job.command
        if txt[0] == "#": continue
        if short:
            if txt.find(SCRDMS) > 0:
                ATAG = txt.split(SCRDMS )[1].split()[0]
                BCMD = " ".join(txt.split(SCRDMS )[1].split()[1:])

                IIS = is_in_screen(ATAG, sessions)
                if IIS:
                    col = f"{fg.green}* "
                else:
                    col = f"{fg.red}x "
                print( f" {col}{ATAG:12s}{fg.default} - {BCMD}" )
            #else:
            #    print(" - ")
        else:
            print(txt)

def is_in_cron(cron, tag):
    """
    Not usefull with  cronvice in  crontab
    """

    file_path = os.path.abspath(__file__)
    RMTG = f"{file_path}"
    for job in cron:
        if job.command.find(RMTG) > 0 and job.command.endswith(f" r {tag}") > 0: # no SPACE
            return True
    return False


def is_in_allowed(tag):
    """
    is the tag in allowed-list ?
    """
    if tag in ["aaa", "asa", "asd", "asad"]:
        return True
    else:
        return False


def del_job_anycommand( cron, tag):
    """
    older
    """
    ACT = False
    RMTG = f"screen -dmS {tag} " #SPACE IMPORTANT
    for job in cron:
        if job.command.find(RMTG) > 0:
            print(f"i... removing /{RMTG}/ ")#... {job}")
            cron.remove(job)
            ACT = True
    if ACT:
        cron.write()


def del_job( cron, tag): #REAL
    """
    delete with the screen
    """
    ACT = False
    file_path = os.path.abspath(__file__)
    # RMTG = f"screen -dmS {tag} " #SPACE IMPORTANT
    RMTG = f"{file_path}"
    for job in cron:
        #print(f"/{job.command}/..")
        if job.command.find(RMTG) > 0 and job.command.endswith(f" r {tag}") > 0: # no space
            print(f"i... removing /{RMTG}/{tag}/ ")#... {job}")
            cron.remove(job)
            ACT = True
    if ACT:
        cron.write()
    else:
        print("X... nothing deleted")


def get_now():
    res = dt.datetime.now().strftime("%Y%m%d_%H%M%S")
    return res


def add_job_anycommand( cron, tag):
    """
    first attempt to add any command, basically
    """
    if is_in_cron(cron, tag):
        print(f"X... already present {tag}")
        return
    ENV = "DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus"
    SCR = f"/usr/bin/screen -dmS {tag} " #SPACE IS IMPORANT
    #make code sequence with &&
    #CODE = f"echo i...running {tag} && /home/ojr/.local/bin/notifator b hello_{tag} && echo i...running {tag} && sleep 30"
    MAINCODE = 'notifator'
    MAINCODE = allocate(MAINCODE)
    CODE = f"{MAINCODE} b hello_{tag}_{get_now()} && sleep 30"
    # encapsulate to ''
    CODE = f"bash -c '{CODE}'"
    CMD = f"{ENV} {SCR} {CODE}"


    print("i... adding job", tag)

    job = cron.new(command=CMD)
    job.minute.every(1)
    #job.set_command("new_script.sh")
    job.set_comment("test of notifator")
    cron.write()


def add_job( cron, tag): #REAL
    """
    Operates just on SCREEN
    cron:  * * * * * display... path/to/cronvice
    """
    if is_in_cron(cron, tag):
        print(f"X... already present {tag}")
        return
    if not is_in_allowed(tag):
        print(f"X... this job is not allowed {tag}")
        return
    ENV = "DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus"

    file_path = os.path.abspath(__file__)

    #return

    #SCR = f"/usr/bin/screen -dmS {tag} " #SPACE IS IMPORANT
    ###make code sequence with &&
    ###CODE = f"echo i...running {tag} && /home/ojr/.local/bin/notifator b hello_{tag} && echo i...running {tag} && sleep 30"
    #MAINCODE = 'notifator'
    #MAINCODE = allocate(MAINCODE)
    MAINCODE = file_path
    CODE = f"/usr/bin/python3 {MAINCODE} r {tag}"
    #### encapsulate to ''
    #CODE = f"bash -c '{CODE}'"
    #CMD = f"{ENV} {SCR} {CODE}"
    CMD = f"{ENV} {CODE}"

    print("i... adding job", tag)

    job = cron.new(command=CMD)
    job.minute.every(1)
    #job.set_command("new_script.sh")
    job.set_comment("test of notifator")
    cron.write()



def mod_job(cron, tag):
    del_job(cron, tag)
    add_job(cron, tag)



########################################################################

def start(tag): #REAL
    """
    starts screen in BG with environment......  sloopy yet and sleep 30
    """
    with open("/tmp/cronvice.log", "a") as f:
        f.write(f"i... in START ... the tag is {tag} \n")
    #ENV = "DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus"
    ENV = ""
    # Set environment variables
    env = os.environ.copy()
    env["DISPLAY"] = ":0"
    env["DBUS_SESSION_BUS_ADDRESS"] = "unix:path=/run/user/1000/bus"


    SCR = f"/usr/bin/screen -dmS {tag} " #SPACE IS IMPORANT
    #make code sequence with &&
    #CODE = f"echo i...running {tag} && /home/ojr/.local/bin/notifator b hello_{tag} && echo i...running {tag} && sleep 30"
    MAINCODE = 'notifator'
    with open("/tmp/cronvice.log", "a") as f:
        f.write(f"i... in START ... the main is {MAINCODE} \n")
    MAINCODE = allocate(MAINCODE)
    with open("/tmp/cronvice.log", "a") as f:
        f.write(f"i... in START ... the main is {MAINCODE} \n")
    CODE = f"{MAINCODE} b hello_{tag}_{get_now()} && sleep 30"
    # encapsulate to ''
    CODE = f"bash -c '{CODE}'"
    CMD = f"{ENV} {SCR} {CODE}"
    args = shlex.split(CMD)
    print(args)
    with open("/tmp/cronvice.log", "a") as f:
        f.write(f"i... in START ...and... the args is {args} \n")
    process = sp.Popen(args, env=env, stdout=sp.DEVNULL, stderr=sp.DEVNULL)
    process.poll()
    with open("/tmp/cronvice.log", "a") as f:
        f.write(f"i... in START ...finish \n")


######################################################################

def run_job(cron, tag): # REAL
    """
    if it is in cron and screen not running......
    """
    with open("/tmp/cronvice.log", "a") as f:
        f.write(f"i... in run_job ...and... the tag is {tag} \n")
    #if is_in_cron(cron, tag): # not for screen-cronvice
    if is_in_allowed(tag):
        sessions = list_screen_sessions() # PREPARE
        with open("/tmp/cronvice.log", "a") as f:
            f.write(f"i... in run_job ...and... sessions is {sessions} \n")
        if is_in_screen(tag, sessions):
            with open("/tmp/cronvice.log", "a") as f:
                f.write(f"x... already running \n")
            print(f"X...  {fg.green} already running {tag} {fg.default} ")
        else:
            with open("/tmp/cronvice.log", "a") as f:
                f.write(f"i... GO running \n")
            print(f"i... {fg.orange} Starting {tag} .... {fg.default}" )
            start(tag)


#########################################################
#
# MAIN
#
#########################################################

def main( cmd="", tag=None):
    with open("/tmp/cronvice.log", "a") as f:
        f.write(f"i... so it started {dt.datetime.now()} \n")

    #print(f"...")
    cron = CronTab(user=True) #CronTab(user='root')

    #for job in cron: print(job)

    with open("/tmp/cronvice.log", "a") as f:
        f.write(f"i... the command is {cmd} ...and... the tag is {tag} \n")
    if cmd != "":
        if cmd == "a" and tag is not None:
            add_job(cron, tag )
            return
        if cmd == "m" and tag is not None:
            mod_job(cron, tag )
            return
        if cmd == "l" :
            view_cron(cron, short=True)
            return
        if cmd == "r" :
            run_job(cron, tag)
            return
        elif cmd == "d" and tag is not None:
            del_job(cron, tag)
            return
        elif  tag is None:
            print("X... give me a screen tag to delete")
            return
        else:
            print("X... unknown command")
            return

    print("______________________________________________-actual cron content_______")
    view_cron(cron)
    print("_________________________________________________________________________")
    print("""
l ... list
a ... add just command
m ... d and a
d ... delete command
r ... run screen job #real
""")
    #
        #cron.remove( job )


if __name__ == "__main__":
    Fire(main)
