#  !/bin/bash


ZSH_THEME_GIT_PROMPT_PREFIX=" %{$fg[green]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY=" %{$fg[red]%}⚡"
ZSH_THEME_GIT_PROMPT_CLEAN=""

#=============================================

function get_deterministic_color_term_u(){
 termcodes=(62 124 5 30 73 60 22 130 167 29)
    hash_value=$(echo -n "$USER" | sha256sum | awk '{print $1}')
    short_hash="${hash_value:0:8}"
    index=$((0x${short_hash} % ${#termcodes[@]}))
    index=$((index + 1)) # Bash arrays by default behave as one-based in some shells
    color="${termcodes[$index]}"
    echo $color
}


function get_deterministic_color_term(){
 termcodes=(62 124 5 30 73 60 22 130 167 29)

#https://www.w3.org/TR/css-color-3/#svg-color
# same results as with topbar python

    # Get hostname
    hostname=$(hostname)

    # Compute SHA256 hash of hostname
    hash_value=$(echo -n "$hostname" | sha256sum | awk '{print $1}')
    short_hash="${hash_value:0:8}"
    #echo $hash_value

    # Compute deterministic index
    index=$((0x${short_hash} % ${#termcodes[@]}))
    # Convert the full hash to a decimal integer and compute the index
    #index=$((0x$hash_value % ${#termcodes[@]})) # too mcuh for bash

    # Adjust Bash array for zero-based indexing
    #echo index number $index
    index=$((index + 1)) # Bash arrays by default behave as one-based in some shells
    #echo index number $index


    # Return the color code
    color="${termcodes[$index]}"
    #color="${termcodes[1]}"
    #echo color number $color
    echo $color
}


# function getn(){
#     #echo i... iam in function getn

#     NAME=$1
#     MAX=$2
#     SILENT=$3
#     stopme=0
#     NAME=$NAME+`echo $NAME | base64`

#    # linewidth=100, no Aradix, decimal
#     stop=`echo -n $NAME | od -w100 -A none -d | sed "s/\s//g" `
#     added=`expr $(echo "$stop" | sed 's/[0-9]/ + &/g' | sed 's/^ +//g')`
#     if [ "$?" != "0" ]; then
# 	echo XXX1... $NAME
# 	stopme=1
#     fi

#     stop=`expr $MAX + $added`
#     if [ "$?" != "0" ]; then
# 	echo XXX2... $NAME
# 	stopme=1
#     fi
#     stop=`echo $stop % $MAX | bc`

#     if [ "$SILENT" = "0" ]; then
#     echo ________
#     echo -n  $NAME | od -A none -chd
#     echo ________
#     echo  NAME= $NAME ... first level /od/ = $stop " "

#     echo second level ... add all digits:
#     echo "$stop" | sed 's/[0-9]/ + &/g' | sed 's/^ +//g'
#     echo result = $added
#     #echo -n "..." modulo  $stop " "
#     echo  "D... modulo="  $stop

#     fi

#     echo $stop
#     if [ "$stopme" = "1" ]; then
# 	exit 1
#     fi

#     }
# #=====================================================


# # =============== all possible combinations===============
# AR=(black blue magenta yellow white green cyan)
# #
# # here - remove combinations
# #
# #orifginal exclude EXCLUDE=(8 9 11 12 14  22 23 24 43 36 42 )
# # exclude 20201012
# EXCLUDE=(8 9 11 12 14  22 23 24 43 36 42 8 13 14 15 21 29 30 33 34 35 36 41 42)
# # this is howto tune wrong combinations: uncomment this
# #EXCLUDE=( )
# con=0
# ARACOLO=()
# for i in $AR[@]; do
#  for j in $AR[@]; do
#   if [ $i != $j ]; then
#       con=$(( $con + 1 ))
#       # print colors
#       # echo -n $con $i $j $fg[$i]$bg[$j]AHOJ$reset_color
#       if    [[ ! " ${EXCLUDE[@]} " =~ " ${con} " ]]; then
#          ARACOLO=($ARACOLO "$fg[$i]$bg[$j]")
#       fi
#   fi
# done
# echo " "
# done


# combin=${#ARACOLO[@]}
# echo GOOD COLOR COMBINATIONS $combin

# num=0
# for i in ${ARACOLO[@]}; do
#  num=`expr $num + 1`
#  # print color combintations 2
#   echo -n  "${i}${num}   "
# done
# echo $reset_color

# # =========================================================

# echo -n "i... color rnd USER:" $reset_color
# getn $USER $combin 1
# stop1=$stop
# echo $ARACOLO[$stop1] USER $reset_color

# echo -n "i... color rnd HOST:"$reset_color
# getn $HOST $combin 1
# stop2=$stop
# echo $ARACOLO[$stop2] HOST $reset_color
# #echo red HOST $reset_color



# p1=$ARACOLO[$stop1]
# p2=$ARACOLO[${stop2}]


# # MY OLD ARACOLO ............
# colo="{$p1%}%n%{$reset_color%}@%{$p2%}%m%{$reset_color%}"


#  202502--------------------------------------------------
#
# ------------------------ GET COLOR CONSISTENT WITH  TOPBAR
#
# ########################################################


export ZSH_PROMPT_COLOR_USER=$(get_deterministic_color_term_u)
export ZSH_PROMPT_COLOR=$(get_deterministic_color_term)
#echo $ZSH_PROMPT_COLOR
#ZSH_PROMPT_COLOR=$(python3 $HOME/02_GIT/GITLAB/cronvice/topbar.py termcolor)
#colo="K{$ZSH_PROMPT_COLOR}%F{15}%n@%m%f%k"
colo="K{$ZSH_PROMPT_COLOR_USER}%F{15}%n%f%K{$ZSH_PROMPT_COLOR}%F{15}@%m%f%k"


# # Update the .screenrc file with the dynamically chosen color
# cat << EOF > $HOME/.screenrc
# bindkey "^[Od" prev  # change window with ctrl-left
# bindkey "^[Oc" next  # change window with ctrl-right
# defscrollback 1024
# hardstatus alwayslastline "%{= C$ZSH_PROMPT_COLOR} %H %{= CK}|%{= Cr} %d %M %{= CK}| %-Lw%{= BW}%50>%n%f* %t%{-}%+Lw%<"
# startup_message off
# EOF


# printf "i... test: $p2 HOST $p1 USER $reset_color\n"
# echo "i... test $p1 USER $p2 HOST $reset_color"


###### this is a preparation for
# tail -f ~/t & script -f ~t | ssh fedo 'cat > /dev/pts/$PTS'
#####

###########################
#  this is probably some trick for telling PTS so that one can redirect to it
###########################
PTS=` ps | grep $$ | awk '{print $2}' |  awk -F "/" '{print $2}'`
# i used ps -q pid  but raspberry crashes
ps  -p $PPID -f | awk '{print $9}' >/dev/null


# DEBUGGING
# if [ "$?" = "0" ]; then
#     #=========== this is for scripting to terminal ======================
#     echo terminator $PTS
#     echo "parent=\`ps -ef | grep pts/$PTS | grep zsh |  awk '{print \$3}'\`"  > $HOME/.scriptterm
#     echo "if [ \"\$parent\" != \"\" ]; then " >> $HOME/.scriptterm
#     echo "cat > /dev/pts/$PTS" >> $HOME/.scriptterm
#     echo "fi" >>$HOME/.scriptterm
# fi


function prompt_char {
	if [ $UID -eq 0 ]; then echo "%{$fg[red]%}#%{$reset_color%}"; else echo $; fi
}


PROMPT='%(?,,%{$fg[red]%}FAIL: $?%{$reset_color%})%$colo: %{$fg_bold[blue]%}%~%{$reset_color%}$(git_prompt_info) %_$(prompt_char) '

# PRoblem with git prompt
#PROMPT='%(?,,%{$fg[red]%}FAIL: $?%{$reset_color%})%$colo: %{$fg_bold[blue]%}%~%{$reset_color%} %_$(prompt_char) '


###)%{$fg[magenta]%}%n%{$reset_color%}@%{$fg[yellow]%}%m%{$reset_color%}: %{$fg_bold[blue]%}%~%{$reset_color%}$(git_prompt_info) %_$(prompt_char) '

RPROMPT='%{$fg[green]%}[%*]%{$reset_color%}'
echo -n  ____ theme end ____
