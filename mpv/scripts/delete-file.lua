local utils = require "mp.utils"

del_list = {}
ren_list = {}

-- ===================================================================

function GetFileName(url)
   print("D... getting filename")
   return url:match("^.+/(.+)$")
end

-- ===================================================================

function GetFileNameNoext(url)
   print("D... getting filename noext", url)
   local e=url:match("^.+(%..+)$")
   print("D... ext=", e)
   -- filename with ext
   local f=url:match("^.+/(.+)$")
   if (f==nil) then
      f=url
   end
   -- x
   print("D... 1f/e",f,e)
   local f=f:match("^([%w%p%s]+)%..+$")
   print("D... 2f/e",f,e)
   return f
end

function GetFileExtension(url)
  return url:match("^.+(%..+)$")
end

-- ===================================================================

function contains_item(l, i, msg)
   for k, v in pairs(l) do
      if v == i then
         mp.osd_message("un"..msg.." current file")
         l[k] = nil
         return true
      end
   end
   mp.osd_message(msg.." current file")
   return false
end

function mark_delete()
   mp.osd_message("mark_delete.... file")
   local work_dir = mp.get_property_native("working-directory")
   local file_path = mp.get_property_native("path")
   local s = file_path:find(work_dir, 0, true)
   local final_path
   if s and s == 0 then
      final_path = file_path
   else
      final_path = utils.join_path(work_dir, file_path)
   end
   if not contains_item(del_list, final_path,"DELETING") then
      table.insert(del_list, final_path)
   end
end


function mark_rename()
   mp.osd_message("mark_rename.... file")
   local work_dir = mp.get_property_native("working-directory")
   local file_path = mp.get_property_native("path")
   local s = file_path:find(work_dir, 0, true)
   local final_path
   if s and s == 0 then
      final_path = file_path
   else
      final_path = utils.join_path(work_dir, file_path)
   end
   if not contains_item(ren_list, final_path,"renaming") then

      mp.set_property("pause", "yes")

      --res = os.execute("zenity --entry --entry-text="..final_path)
      local openPop = assert(io.popen("zenity --entry --entry-text="..final_path, 'r'))
      local output = openPop:read('*all')
      openPop:close()
      print("RENAME "..final_path.." TO "..output) -- > Prints the output of the command.

      table.insert(ren_list, "mv "..final_path.."  "..output)

   end
end


function delete()
   for i, v in pairs(del_list) do
      print("deleting: "..v)
      os.remove(v)
   end
-- end

-- function rename()
   for i, v in pairs(ren_list) do
      print("RENAMING: "..v)
      os.execute(v)
      --local fname=GetFileNameNoext(v)
      --local ext=GetFileExtension(v)
      --local outname=fname.."_renamed_"..ext
      --local outname="_renamed_"..fname.."_renamed_"..ext
      --print("look : "..v.." to "..outname)
     -- #os.rename(CurrPath.."OldName.txt", CurrPath.."NewName.txt" )
   end
end

mp.add_key_binding("ctrl+INS", "rename_file", mark_rename)
mp.add_key_binding("ctrl+DEL", "delete_file", mark_delete)
mp.register_event("shutdown", delete)
