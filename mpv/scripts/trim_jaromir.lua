-- taken from general example
require 'mp'
-- taken from encode
local utils = require "mp.utils"
local msg = require "mp.msg"
local options = require "mp.options"

local start_timestamp = nil
local profile_start = ""

-- implementation detail of the osd message
local timer = nil
local timer_duration = 2
-- chmod
-- local fs = require "fs"
-- ===================================================================
function on_pause_change(name, value)
    if value == true then
        mp.set_property("fullscreen", "yes")
    end
end

-- ===================================================================


-- ===================================================================

function GetFileName(url)
   print("D... getting filename")
   return url:match("^.+/(.+)$")
end

-- ===================================================================

function GetFileNameNoext(url)
   print("D... getting filename noext", url)
   local e=url:match("^.+(%..+)$")
   print("D... ext=", e)
   -- filename with ext
   local f=url:match("^.+/(.+)$")
   if (f==nil) then
      f=url
   end
   -- x
   print("D... 1f/e",f,e)
   local f2=f:match("^([%w%p%s]+)%..+$")
   if (f2==nil) then
      f2=f
   end
   print("D... 2f/e",f2,e)
   return f2
end
-- ===================================================================

function GetFileExtension(url)
  return url:match("^.+(%..+)$")
end

-- ===================================================================

function file_exists(name)
    local f = io.open(name, "r")
    if f ~= nil then
        io.close(f)
        return true
    else
        return false
    end
end
-- ===================================================================

function seconds_to_time_string(seconds, full)
    local ret = string.format("%02d:%02d.%03d"
        , math.floor(seconds / 60) % 60
        , math.floor(seconds) % 60
        , seconds * 1000 % 1000
    )
    if full or seconds > 3600 then
        ret = string.format("%d:%s", math.floor(seconds / 3600), ret)
    end
    return ret
end

-- =============================================================
function append_into_file(filename, CMD)
   local shebang="#!/bin/bash\n\n"
   if file_exists(filename) then
      shebang="\n"
   end
   jmfile = io.open( filename , "a" )
   jmfile:write(shebang)
   jmfile:write(  CMD )
   jmfile:write(  "\n\n" )
   io.close(jmfile)
   os.execute("chmod +x "..filename)
end

-- ===================================================================
function start_encoding(from, to)
   print("D... inside encoding")
   print("D... "..from)
   print("D... "..to)
   local path = mp.get_property("path")
   local is_stream = not file_exists(path)
   if is_stream then
      path = mp.get_property("stream-path")
   end

   local track_args = {}
   local start = seconds_to_time_string(from, false)
   local input_index = 0

   local ext=GetFileExtension(path)
   local filename=GetFileNameNoext(path)
   print("D... preparing filename, extension is: "..ext)
   print("D... preparing filename, filename is: "..filename)
   local outname="'./"..filename.."_cut_"..from.."_"..to..ext.."'"
   local outnameF="'./"..filename.."_cutF_"..from.."_"..to..".webm'"
   local outnameB="'./"..filename.."_cutB_"..from.."_"..to..".webm'"
   local outnameW="'./"..filename.."_cutW_"..from.."_"..to..".mp4'"
--   local outnameC="'./"..filename.."_cropC_"..from.."_"..to..".webm'"
   print("i... ===================================simple")
   CMD="nice -19 ffmpeg -hide_banner -ss "..start.." -i '"..path.."' -to "..tostring(to-from)
   CMD=CMD.." -async 1  "..outname
   print(CMD)


   print("i... ===================================whatsapp")
   CMDW="nice -19 ffmpeg -hide_banner -ss "..start.." -i '"..path.."' -to "..tostring(to-from)
   CMDW=CMDW.." -map 0 -sn -c:v libx264 -c:a aac  "..outnameW
   print(CMDW)

   print("i... =================================== full")
   CMDF="nice -19 ffmpeg -hide_banner -ss "..start.." -i '"..path.."' -to "..tostring(to-from)
   CMDF=CMDF.." -map 0 -sn -c:v libvpx -crf 4 -b:v 30000k -vf unsharp -max_muxing_queue_size 1024 "..outnameF
   print(CMDF)
   print("i... =================================== bounce")
   CMDB="nice -19 ffmpeg -hide_banner -ss "..start.." -t "..tostring(to-from).." -i '"..path.."' "
   CMDB=CMDB.." -crf 4 -b:v 30000k -filter_complex \"[0:v]reverse,fifo[r];[0:v][r] concat=n=2:v=1 [v]\" -map \"[v]\" -max_muxing_queue_size 1024 "
   CMDB=CMDB..outnameB
   print(CMDB)

   -- ==================WRITE INTO FILE ============
   local filesim="ffmpegcode_simple.bat"
   local filefull="ffmpegcode_full.bat"
   local filebounce="ffmpegcode_bounce.bat"
   local filewhatsapp="ffmpegcode_whatsapp.bat"
--   local filecrop="ffmpegcode_crop.bat"
   append_into_file( filesim,    CMD )
   append_into_file( filefull,   CMDF )
   append_into_file( filebounce, CMDB )
   append_into_file( filewhatsapp, CMDW )
   -- append_into_file( filecrop,   CMDC )
   -- =======================commented out==========
end

-- ===================================================================

function clear_timestamp()
    timer:kill()
    start_timestamp = nil
    profile_start = ""
    mp.remove_key_binding("encode-ESC")
    mp.remove_key_binding("encode-ENTER")
    mp.osd_message("", 0)
end

-- ===================================================================
function create_cut_script(profile)
    if not mp.get_property("path") then
       mp.osd_message("No file currently playing")
       print("X... No file currently playing")
        return
    end
    if not mp.get_property_bool("seekable") then
        mp.osd_message("Cannot encode non-seekable media")
	print("X... Cannot encode nonseekable media")
        return
    end
    if not start_timestamp or profile ~= profile_start then
       -- ===================================== START
       print("D... Profile   PROFILE_START")
       profile_start = profile
       start_timestamp = mp.get_property_number("time-pos")
       print("D... starting", start_timestamp)
       local msg = function()
	  mp.osd_message(
	     string.format("trim 2 ffmpegcode_*.bat [%s]: waiting END timestamp", profile or "default"),
	     timer_duration
            )
       end
       msg()
       timer = mp.add_periodic_timer(timer_duration, msg)
       mp.add_forced_key_binding("ESC", "encode-ESC", clear_cut_script)
       mp.add_forced_key_binding("ENTER", "encode-ENTER", function() create_cut_script(profile) end)
    else
       -- ===========================================END
       print("D... i think I will encode here")
       local from = start_timestamp
       local to = mp.get_property_number("time-pos")
       print("D... starting from ", from, " ... " ,to)
      if to <= from then
	 print("D... end CANNT be before BEGIN")
	  mp.osd_message("Second timestamp cannot be before the first", timer_duration)
	  timer:kill()
	  timer:resume()
	  return
       end
       clear_timestamp()
       mp.osd_message(string.format("Writing line from %s to %s"
				    , seconds_to_time_string(from, false)
				    , seconds_to_time_string(to, false)
				   ), timer_duration)
       -- include the current frame into the extract
       local fps = mp.get_property_number("container-fps") or 30
       to = to + 1 / fps / 2
       print("i... START_ENCODING function")
       start_encoding(from, to)
    end
end

function remove_ffmpegcode()
   print("D... removing all ffmpegcode_*")
   mp.osd_message("removing all ffmpegcode_*.bat (simple, full, bounce, delogo")

   os.remove("ffmpegcode_bounce.bat")
   os.remove("ffmpegcode_delogo.bat")
   os.remove("ffmpegcode_full.bat")
   os.remove("ffmpegcode_simple.bat")
   -- os.remove("")
end

print("JAROMIR1 LOADED")
-- mp.observe_property("pause", "bool", on_pause_change)
-- HERE I remove the binding - physical key here (if nil => create in input.conf )
mp.add_key_binding("b", "create-cut-script", create_cut_script)
mp.add_key_binding("B", "remove-ffmpegcode", remove_ffmpegcode)
