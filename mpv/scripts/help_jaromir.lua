
local msg = require("mp.msg")
local utils = require("mp.utils") -- utils.to_string()
local assdraw = require('mp.assdraw')


local update_timeout = 2 -- in seconds





-- Class creation function
function class_new(klass)
    -- Simple Object Oriented Class constructor
    local klass = klass or {}
    function klass:new(o)
        local o = o or {}
        setmetatable(o, self)
        self.__index = self
        return o
    end
    return klass
end

-- Show OSD Clock
local OSDHelp = class_new()


function OSDHelp:_show_help()
   print("D... show HELP")
    local osd_w, osd_h, aspect = mp.get_osd_size()
    local scale = 2
    local fontsize = tonumber(mp.get_property("options/osd-font-size")) / scale
    fontsize = math.floor(fontsize)
    -- msg.info(fontsize)

    local ass = assdraw:ass_new()
    ass:new_event()
    ass:pos(10,100)
    ass:an(1)
    ass:append(string.format("{\\fs%d}", fontsize))
    ass:append( "" )
    ass:append( "\n" )
    ass:append(string.format("{\\fs%d}", fontsize))
    ass:append( "c      ... crop, save delogo to ffmepcode.bat\n" )
    ass:append(string.format("{\\fs%d}", fontsize))
    ass:append( "v      ... show clock, battery, filename\n" )
    ass:append(string.format("{\\fs%d}", fontsize))
    ass:append( "b      ... save cut to ffmpegcode's (simple,full,bounce)\n" )
    ass:append(string.format("{\\fs%d}", fontsize))
    ass:append( "n      ... compressor, F1,F2...\n" )
    ass:append(string.format("{\\fs%d}", fontsize))
    ass:append( "alt-f  ... file navigator\n" )
    ass:append(string.format("{\\fs%d}", fontsize))
    ass:append( "ctrl-d ... delete file\n" )
    ass:append(string.format("{\\fs%d}", fontsize))
    ass:append( "B      ... rm all ffmpegcode_*\n" )

    ass:append(string.format("{\\fs%d}", fontsize))
    ass:append( "alt arrow        ... rotate\n" )

    ass:append(string.format("{\\fs%d}", fontsize))
    ass:append( "shift-#   ... select language\n" )
    ass:append(string.format("{\\fs%d}", fontsize))
    ass:append( "v        ... subtit visi\n" )
    ass:append(string.format("{\\fs%d}", fontsize))
    ass:append( "j        ... subtit cycle\n" )
    ass:append(string.format("{\\fs%d}", fontsize))
    ass:append( "z/Z        ... subtit delay\n" )


    ass:append(string.format("{\\fs%d}", fontsize))
    ass:append( "h      ... this help\n" )
    ass:an(0)
    print("D...",osd_w, osd_h, ass.text)
    mp.set_osd_ass(osd_w, osd_h, ass.text)

end

function clear_osd()
    local osd_w, osd_h, aspect = mp.get_osd_size()
    mp.set_osd_ass(osd_w, osd_h, "")
end

function OSDHelp:toggle_show_help(val)
    local trues = {["true"]=true, ["yes"] = true}
    if self.tobj then
        if trues[val] ~= true then
            self.tobj:kill()
            self.tobj = nil
            clear_osd()
        end
    elseif val == nil or trues[val] == true then
        self:_show_help()
        local tobj = mp.add_periodic_timer(update_timeout,
            function() self:_show_help() end)
        self.tobj = tobj
    end
end


-- =======================================================
local osd_help = OSDHelp:new()
function toggle_show_help(v)
   print("D... toggle help")
    osd_help:toggle_show_help(v)
end

-- hard bind
mp.add_key_binding("h", "show-help", toggle_show_help)
--  too weak bind
-- mp.register_script_message("show-help", toggle_show_help)
