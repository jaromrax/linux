-- Mozbugbox's lua utilities for mpv
-- Copyright (c) 2015-2018 mozbugbox@yahoo.com.au
-- Licensed under GPL version 3 or later
-- ************ this shows bar-remain  and remaining time **********
-- ************ this shows bar-remain  and remaining time **********
-- ************ this shows bar-remain  and remaining time **********
-- ************ this shows bar-remain  and remaining time **********
-- ************ this shows bar-remain  and remaining time **********



--[[
Show current time on video
Usage: c script_message show-clock [true|yes]
--]]

local msg = require("mp.msg")
local utils = require("mp.utils") -- utils.to_string()
local assdraw = require('mp.assdraw')

local update_timeout = 1 -- in seconds


-- ==================================================


function file_exists(name)
    local f = io.open(name, "r")
    if f ~= nil then
        io.close(f)
        return true
    else
        return false
    end
end


-- ==================================================


-- Class creation function
function class_new(klass)
    -- Simple Object Oriented Class constructor
    local klass = klass or {}
    function klass:new(o)
        local o = o or {}
        setmetatable(o, self)
        self.__index = self
        return o
    end
    return klass
end

-- print content of a lua table
function print_table(tbl)
    msg.info(utils.to_string(tbl))
end


function disp_time(time)
--   print("AAA"..time)
   local time = math.floor(time)
--   print(time)
  -- local days = floor(time/86400)
  local hours = math.floor(math.fmod(time, 86400)/3600)
  local minutes = math.floor(math.fmod(time,3600)/60)
  local seconds = math.floor(math.fmod(time,60))
  return string.format("%02d:%02d:%02d",hours,minutes,seconds)
end

-- Show OSD Clock
local OSDClock = class_new()
function OSDClock:_show_bar()
    -- Show wall clock on bottom left corner
    local osd_w, osd_h, aspect = mp.get_osd_size()

    local scale = 1.5
    local fontsize = tonumber(mp.get_property("options/osd-font-size")) / scale
        fontsize = math.floor(fontsize)
    -- msg.info(fontsize)

    -- COLLECT INFORMATION --
    local path = mp.get_property("path")

    -- BATTERY
    local batfile="/sys/class/power_supply/BAT0/capacity"
    local block={}
    local index
    local bat=""
    -- if file_exists(batfile) then
    --    local file = io.open(batfile, "r")
    --    io.input(file)
    --    bat=io.read()
    --    io.close(file)
    --    index=math.floor( 30* tonumber(bat)/100)
       block[38]="▕█████".."".."▏"
       block[37]="▕████▉".."".."▏"
       block[36]="▕████▊".."".."▏"
       block[35]="▕████▋".."".."▏"
       block[34]="▕████▌".."".."▏"
       block[33]="▕████▍".."".."▏"
       block[32]="▕████▎".."".."▏"
       block[31]="▕████▏".."".."▏"

       block[30]="▕████ ".." ".."▏"
       block[29]="▕███▉ ".." ".."▏"
       block[28]="▕███▊ ".." ".."▏"
       block[27]="▕███▋ ".." ".."▏"
       block[26]="▕███▌ ".." ".."▏"
       block[25]="▕███▍ ".." ".."▏"
       block[24]="▕███▎ ".." ".."▏"

       block[23]="▕███  ".."  ".."▏"
       block[22]="▕██▉  ".."  ".."▏"
       block[21]="▕██▊  ".."  ".."▏"
       block[20]="▕██▋  ".."  ".."▏"
       block[19]="▕██▌  ".."  ".."▏"
       block[18]="▕██▍  ".."  ".."▏"
       block[17]="▕██▎  ".."  ".."▏"

       block[16]="▕██▏   ".."  ".."▏"

       block[15]="▕█▉   ".."   ".."▏"
       block[14]="▕█▊   ".."   ".."▏"
       block[13]="▕█▋   ".."   ".."▏"
       block[12]="▕█▌   ".."   ".."▏"
       block[11]="▕█▍   ".."   ".."▏"
       block[10]="▕█▎   ".."   ".."▏"
       block[9]= "▕█▏   ".."   ".."▏"

       block[8]= "▕█    ".."    ".."▏"
       block[7]= "▕▉    ".."    ".."▏"
       block[6]= "▕▊    ".."    ".."▏"
       block[5]= "▕▋    ".."    ".."▏"
       block[4]= "▕▌    ".."    ".."▏"
       block[3]= "▕▍    ".."    ".."▏"
       block[2]= "▕▎    ".."    ".."▏"
       block[1]= "▕     ".."    ".."▏"
       --  this is u+2007 figure space" "
       --  this is u+2002 1/2em space" "
       --  this is u+2006 1/6em space" "
       -- ass:append(" 🕵 	🔋 ")
    -- else
    --    index=1
    --    block[1]=" "
    -- end

    local now = os.date("%H:%M:%S")
    local ass = assdraw:ass_new()
    ass:new_event()
    -- ************ POSITION ************
    ass:an(9)
    ass:append(string.format("{\\fs%d}", fontsize))
    -- if #block[index]>1 then
    --    bat=bat.."%"
    -- end

    -- ************ this shows battery and remaining time **********

    local time_pos = mp.get_property_number("time-pos")
    local time_dur = mp.get_property_number("duration")
    local remains = time_dur - time_pos
    local perc = time_pos/time_dur * 100
    index=math.floor( perc/100*38)+1

    ass:append( block[index] )
    ass:append( " " )
    ass:append( disp_time(remains))
    -- ass:append( " " )
    -- ass:append(time_pos)
    -- ass:append( " " )
    -- ass:append(index)
    -- ass:append( " " )
    -- ass:append(path)
    -- ass:append( " " )
    ass:an(0)


    mp.set_osd_ass(osd_w, osd_h, ass.text)
    -- msg.info(ass.text, osd_w, osd_h)
end

function clear_osd()
    local osd_w, osd_h, aspect = mp.get_osd_size()
    mp.set_osd_ass(osd_w, osd_h, "")
end

function OSDClock:toggle_show_bar(val)
    local trues = {["true"]=true, ["yes"] = true}
    if self.tobj then
        if trues[val] ~= true then
            self.tobj:kill()
            self.tobj = nil
            clear_osd()
        end
    elseif val == nil or trues[val] == true then
        self:_show_bar()
        local tobj = mp.add_periodic_timer(update_timeout,
            function() self:_show_bar() end)
        self.tobj = tobj
    end
end

local osd_clock = OSDClock:new()
function toggle_show_bar(v)
    osd_clock:toggle_show_bar(v)
end

mp.register_script_message("show-jaro-bar", toggle_show_bar)
