//=================================================
//
//  this returned json for A0 A1 after 100 cycles.
//  NOW I want to adapt for DHT
// COMPILE THE SAME - only this switch:!!!
//===========================================
int USEDHT=0;
//-------------------------the beggining of the JSON
char measurement[40]="{\"measurement\":\"pt100_2\",";
//char measurement[40]="{\"measurement\":\"phid\",";
//char measurement[30]="\"phid\",";
//char measurement[30]="\"ard04\",";
//============================================
//
//================ WORD INDENT ;=============
#include <SoftwareSerial.h>
SoftwareSerial mySerial(10, 11); // RX, TX
int i=0;
int timeout=200;
char s[150]="mnbvcxz";

//============ analog part:
int sensorPin = A0;    // select the input pin for the potentiometer
int sensorPin1 = A1;    // select the input pin for the potentiometer
int sensorPin2 = A2;    // select PT100
int sensorValue = 0;  // variable to store the value coming from the sensor

//==========================DHT PART===================
// NOT THIS NOW
//https://github.com/RobTillaart/Arduino/blob/master/libraries/DHTlib/examples/dht21_test/dht21_test.ino
// BUT THIS
//https://github.com/adafruit/DHT-sensor-library/blob/master/examples/DHTtester/DHTtester.ino
//
//  INSTALL Adafruit Unified Sensor library (Sketch/IncludeLib/Manage)
//  INSTALL DHT sensor library by adafruit
//    for chineese Nano clone - use OLD bootloader
//
#include <Adafruit_Sensor.h>
#include "DHT.h"
#define DHTPIN 2     // what digital pin we're connected to
#define DHTTYPE DHT21   // DHT 21 (AM2301)
DHT dht(DHTPIN, DHTTYPE);
#define DHT21_PIN 2
float dht_t=0;
float dht_h=0;
/*
#include "DHT.h"
#define DHTPIN 2    // modify to the pin we connected
 
#define DHTTYPE DHT21   // AM2301 
 
DHT dht(DHTPIN, DHTTYPE);
*/

/* 
int firstSensor = 0;    // first analog sensor
float t=0.0;
float h=0.0;
int secondSensor = 0;   // second analog sensor
int thirdSensor = 0;    // digital sensor
int inByte = 0;         // incoming serial byte
*/

void setup()
{
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  // start serial port at 9600 bps and wait for port to open:
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }    

  Serial.println("Goodnight moon! sleep=");
  Serial.println(timeout);
//======================= DHT PART IN SETIP========

 pinMode(2, INPUT);   // digital sensor is on digital pin 2
 establishContact();  // send a byte to establish contact until receiver responds 
 
 dht.begin();   // for old DHT
  //======================= DHT PART IN SETIP========

}


//==========================================================
//==========================================================
//===========================================================
void loop()
{
  //=========indicate the loop======
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
                

//===================average ANALOGS
  float average = 0;
  for (int i=0; i < 100; i++) {
   sensorValue = analogRead(sensorPin);
   average = average + (float)sensorValue;
  }
  average = average/100;
 float T0=average*0.21699-61.111;

//------------------------------------
  float average1 = 0;
  for (int i=0; i < 100; i++) {
   sensorValue = analogRead(sensorPin1);
   average1 = average1 + (float)sensorValue;
  }
  average1 = average1/100;
 float H1=average1*0.1861328-40.2;
 H1=1.1*H1 + 4;

 
//-------------------------------------  
  float average2 = 0;
  for (int i=0; i < 100; i++) {
   sensorValue = analogRead(sensorPin2);
   average2 = average2 + (float)sensorValue;
  }
  average2 = average2/100;
  float R1=10000; //strange, i have 100k
  float logR2,R2,T2;
  float c1=1.009249522e-03,c2=2.378405444e-04,c3=2.019292697e-07;
 float V0=average2;
 R2=R1*(1023.0 / (float)V0 - 1.0);
 logR2=log(R2);
 T2=(1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
 T2=T2-273.15;
 
 // =========================A0  and A1 ========+DONE



    if (USEDHT==1){//=============USEDHT==1
    dht_h = dht.readHumidity();
    dht_t = dht.readTemperature();
    if (isnan(dht_t) || isnan(dht_h)) {
       dht_t=0.;
       dht_h=0.;
    }
    }// USEDHT==1//======================USEDHT==1

  //===================== analog end

  // if we get a valid byte, read analog ins:
  if (Serial.available() > 0) {
    int j=0;
  s[0]=' ';
  while  ( j<30 ){//-------------------30 read
    s[j]=char(Serial.read());
    s[j+1]='\0';
    if ((j>30)||(s[j]==';') ) {
        break;
    }
    if (s[j]!='\xFF' ){ j++;}
  }//read the input sentence------------30 read
  
  i++;
  //=========== EOL means END and read it....===========
  
   //=================== JSON OUT===========
    Serial.print( measurement );
    Serial.print("  \"command\":\"");
    Serial.print(s);
    Serial.print("\",");
    
    Serial.print("  \"loop\":\"");
    Serial.print(i);
    Serial.print("\",");
    
    Serial.print("  \"valuea0\":");
    Serial.print(average);
    Serial.print(",");

    Serial.print("  \"valuea1\":");
    Serial.print(average1);
    Serial.print(",");

    Serial.print("  \"valuea2\":");
    Serial.print(average2);
    Serial.print(",");
 
    Serial.print("  \"T0\":");
    Serial.print( T0 );
    Serial.print(",");

    Serial.print("  \"H1\":");
    Serial.print( H1 );
    Serial.print(",");
 
    Serial.print("  \"pt_2\":");
    Serial.print( T2 );
                       
    if (USEDHT==1){//=============USEDHT==1
    Serial.print(",");
    Serial.print("  \"temp\":");
    Serial.print(dht_t);
      
    Serial.print(",");
    Serial.print("  \"hum\":");
    Serial.print(dht_h);
      
    }//===========================USEDHT==1 END
  
    Serial.println("}" );
    //=================== JSON OUT===========
    //  delay(timeout);
  } //serial available 
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(timeout);
}//loo




void establishContact() {
  while (Serial.available() <= 0) {
    //Serial.println("0,0,0;\n");   // send an initial string
    Serial.println("waiting for connection ... word;\n");   // send an initial string
    delay(1000);
  }
}
